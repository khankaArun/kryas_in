import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:krays_in/map.dart';
import 'package:krays_in/model/lat_lon_pojo.dart';
import 'package:krays_in/services/service.dart';
import 'package:krays_in/util/R.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/preferences_until.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:krays_in/view/home/home_page.dart';
import 'package:uuid/uuid.dart';
import 'package:http/http.dart' as http;

class CurrentLocation extends StatefulWidget {
  const CurrentLocation({Key key}) : super(key: key);

  @override
  State<CurrentLocation> createState() => _CurrentLocationState();
}

class _CurrentLocationState extends State<CurrentLocation> {
  TextEditingController seach_place = new TextEditingController();
  var uuid = Uuid();
  String _sessionToken = '12345';
  List<dynamic> placeList = [];



  @override
  void initState() {
    seach_place.addListener(() {
      onChange();
    });
  }

  void onChange() {
    if (_sessionToken == null) {
      setState(() {
        _sessionToken = uuid.v4();
      });
    }
    getSuggestion(seach_place.text);
  }
 // https://maps.googleapis.com/maps/api/place/textsearch/json?key=AIzaSyD05XV4wUvJ_FBxWPuESenkcJvAMt1DVTU&components=country:IN&query=yaan
  void getSuggestion(String place) async {
    String place_api_key = 'AIzaSyDh8xcT9FrJQrGxUQkt-kwahXYq__fKL2g';
    String base_url =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json';
    String request =
        '$base_url?input=$place&key=$place_api_key&sessiontoken=$_sessionToken&components=country:IN';

    print(request);

    var response = await http.get(Uri.parse(request));
    print(response.body.toString());
    if (response.statusCode == 200) {
      setState(() {
        placeList = jsonDecode(response.body.toString())['predictions'];
      });
    } else {
      throw Exception('Failed to load data');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(

              child: Text(
                "Powered by",
                style: TextStyle(
                    fontSize: 10,
                    color: Colors.black38,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.2),
              )),
          Container(
            margin: EdgeInsets.only(),
            width: 45,
            height: 45,
            child: Image.asset(
              R.images.google_icon,
            ),
          ),

        ],
      ),
      appBar: _buildBar(context),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 15, left: 20),
              ),

              InkWell(
                onTap: () {
                /*  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => MapScreen()));*/
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => MapScreen(false,0.00,0.00,WHERE_IT_COME_FROM.search)));
                },
                child: SizedBox(
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 10),
                        child: Icon(
                          Icons.my_location,
                          color: Colors.blue,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10, left: 20),
                        child: Text(
                          "Current Location",
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 0.2),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {

                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => MapScreen(false,
                          0.00,
                          0.00,
                          WHERE_IT_COME_FROM.search)
                      )
                  );

                },
                child: Container(
                  margin: EdgeInsets.only(left: 55),
                  child: Text(
                    "Using GPS",
                    style: TextStyle(
                        fontSize: 12, color: Colors.blue, letterSpacing: 0.2),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Divider(
                  color: Colors.black12,
                  thickness: 7,
                ),
              ),

              Container(
                margin: EdgeInsets.only(left: 10, top: 20),
                height: MediaQuery.of(context).size.height * 0.7,
                child: ListView.builder(
                    itemCount: placeList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListTile(
                          onTap: () {
                            var result = placeList[index]['description'];
                            var place_id = placeList[index]['place_id'];
                            log("+88888  $result");
                            log("+88888  $place_id");
                            getLatlong(place_id);
                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(builder: (context) => HomePage()),
                            // );
                          },
                          leading: const Icon(Icons.location_on),
                          title: Text(placeList[index]['description'],
                              style:
                                  TextStyle(color: Colors.black, fontSize: 15)),
                          // subtitle: Text(
                          //   "A- 47, A Block, Sector 58, Noida, Uttar Pradesh 201301",
                          //   style: TextStyle(color: Colors.black38),
                          // ),
                        ),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void getLatlong(dynamic placeId) {
//ChIJixlmm5JUw4ARIvu9De-TfSQ&key=AIzaSyD05XV4wUvJ_FBxWPuESenkcJvAMt1DVTU
    try {
      Service()
          .postBodyEmpty(
          'https://maps.googleapis.com/maps/api/place/details/json?&placeid=$placeId&key=AIzaSyDh8xcT9FrJQrGxUQkt-kwahXYq__fKL2g')
          .then((response) async {
        if (response != null) {
          var responseJSON = json.decode(response);
          LatLongPojoItem latLong = LatLongPojoItem.fromJson(responseJSON);

          if (latLong.status == 'OK') {
            // var responseJSON = json.decode(response.body);
            //  LatLongPojoItem latLong = LatLongPojoItem.fromJson(responseJSON);
            print(latLong.result.geometry.location.lat);
            print(latLong.result.geometry.location.lng);


            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) =>
                    MapScreen(true, latLong.result.geometry.location.lat,
                        latLong.result.geometry.location.lng,WHERE_IT_COME_FROM.search)));


            /* List<LatLongPojoItem> list = List<LatLongPojoItem>.from(
                responseJSON.map((i) => LatLongPojoItem.fromJson(i)));
            var lat=list[0].result*/
          } else
          if (latLong.status.toUpperCase().contains("OVER_QUERY_LIMIT")) {
            AppUtil.showAlertDialog(context, latLong.status, "Alert");
          } else {
            AppUtil.showAlertDialog(context, "Unable to find address", "Alert");
          }

          // Navigator.pop(context);
          // AppUtils.showAlertDialog(context, 'Something went wrong');
        }
      }
      );
    } on Exception {
      AppUtil.showAlertDialog(context, "Unable to find address", "Alert");
      // AppUtils.showLog("Exception error");
    }


    Future<bool> checkLogin() async {
      bool isUserLogin =
      await PreferenceUtils.getBool(PreferencesKey.IS_USER_LOGIN, false);

      String userLoginId =
      await PreferenceUtils.getString(PreferencesKey.USER_LOGIN_NAME, "");


      if (isUserLogin != null && isUserLogin) {
        return true;
      } else {
        return false;
      }
    }
  }


  Widget _buildBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(color: Colors.black),
      elevation: 1,
      title: TextField(
        textCapitalization: TextCapitalization.words,
        autofocus: true,
        textInputAction: TextInputAction.search,

        onSubmitted: (value) async {

          if(value.isNotEmpty&& value.toString().length>2) {
          }

        },
        controller: seach_place,

        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: 'Search fir area,street name...',
          suffixIcon: IconButton(
            onPressed: (){

              setState(() {
                seach_place.text = "";

              });

            },
            icon: Icon(Icons.clear),
          ),
        ),
      ),
    );
  }
  }


