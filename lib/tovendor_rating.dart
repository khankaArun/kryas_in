import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/model/order_lis_model.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/provider/order_provider.dart';
import 'package:krays_in/util/R.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/view/market/order_detail_page.dart';
import 'package:provider/provider.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class ToVendorRating extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<ToVendorRating> {
  TextEditingController description=TextEditingController();
  var rating = 3.0;
  Image _image;
  BoxDecoration _boxDecoration;

  String messageeeeeee = "Average";
  double rating_val = 3.0;
  bool comment = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getColor(3.0);
  }

  @override
  Widget build(BuildContext context) {
    return    Scaffold(
      //button code
      // scaffold
        body: Consumer<OrderProvider>(
          builder: (context, provider, child) {
            return Container(
              height: double.infinity,
              decoration: _boxDecoration,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SafeArea(
                      child: Container(
                        margin: const EdgeInsets.only(top: 25, left: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                                child: Icon(
                                  Icons.close,
                                  size: 20,
                                  color: Colors.white,
                                )),
                            Expanded(
                              child: Align(
                                alignment: Alignment.topCenter,
                                child: Text(
                                  "Order complete",
                                  style: TextStyle(
                                      fontSize: 18, color: Colors.white),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 100),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RichText(
                            text: const TextSpan(
                              text: 'How Was',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 28,
                                  fontWeight: FontWeight.bold),
                              /*defining default style is optional */
                              children: <TextSpan>[],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RichText(
                            text: const TextSpan(
                              text: 'Your ',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 28,
                                  fontWeight: FontWeight.bold),
                              /*defining default style is optional */
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'order?',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 28,
                                        color: Colors.black)),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 30),
                      child: Text(
                        messageeeeeee,
                        style: const TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40),
                      height: 100,
                      width: 100,
                      child: _image,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 60),
                      child: Center(
                          child: SmoothStarRating(
                            color: Colors.amber,
                            rating: rating,
                            isReadOnly: false,
                            size: 45,
                            filledIconData: Icons.star,
                            defaultIconData: Icons.star_border_outlined,
                            borderColor: Colors.white,
                            starCount: 5,
                            allowHalfRating: false,
                            spacing: 2.0,
                            onRated: (value) {
                              rating_val = value;
                             if (rating_val == 1.0) {
                                getColor(1.0);
                              } else if (rating_val == 2.0) {
                                getColor(2.0);
                              } else if (rating_val == 3.0) {
                                getColor(3.0);
                              } else if (rating_val == 4.0) {
                                getColor(4.0);
                              } else {
                                getColor(5.0);
                              }
                              print("rating value -> $rating_val");
                              // print("rating value dd -> ${value.truncate()}");
                            },
                          )),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 25),
                      child: Container(
                        margin: const EdgeInsets.only(left: 10),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              comment = true;
                            });
                          },
                          child: Row(
                            children: const [
                              Icon(
                                Icons.message,
                                size: 18,
                                color: Colors.white,
                              ),
                              Expanded(
                                child: Text(
                                  "  Add a comment",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white,letterSpacing: 1.0),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Visibility(
                      visible: comment,
                      child: Container(
                        margin: const EdgeInsets.only(top: 5,left: 25,right: 25),
                        child: Container(
                            margin: const EdgeInsets.only(left: 15, right: 15),
                            child:  TextField(
                              controller: description,
                             textCapitalization: TextCapitalization.sentences,
                              style: TextStyle(color: Colors.white),
                              keyboardType: TextInputType.multiline,
                              textInputAction: TextInputAction.newline,
                              minLines: 1,
                              maxLines: 5,

                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),

                                ),
                                hintText: "Comment",
                                hintStyle: TextStyle(color: Colors.white),
                                //     focusedBorder: OutlineInputBorder(
                                // borderSide: BorderSide(color: Colors.white)
                                //   )
                              ),
                            )),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 25),
                      child: Container(
                        margin: const EdgeInsets.only(left: 10),
                        child: Container(
                          margin: const EdgeInsets.only(
                            left: 10,
                            right: 10,
                            top: 25,
                          ),
                          child: MaterialButton(
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10.0))),
                            elevation: 5.0,
                            minWidth: 400,
                            height: 35,
                            color: Colors.black,
                            child: const Text('Done',
                                style: TextStyle(
                                    fontSize: 16.0, color: Colors.white)),
                            onPressed: () async {
                              if(rating_val==0.0){
                                Fluttertoast.showToast(
                                    msg: 'Kindly select rating',
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.BOTTOM,
                                    backgroundColor: R.color.errorColor,
                                    textColor: Colors.white,
                                    fontSize: 16.0);
                                return;
                              }
                              else if(description.text==''){
                                Fluttertoast.showToast(
                                    msg: 'Kindly add the comment',
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.BOTTOM,
                                    backgroundColor: R.color.errorColor,
                                    textColor: Colors.white,
                                    fontSize: 16.0);
                                return;
                              }
                              AppUtil.showLoadingProgress(context);
                              var res = await provider.VendorRating(
                                  vendorid: provider.orderList.data[0].vendor,
                                  Rate: rating_val,
                                  Desc: description.text,
                                  orderid: provider.orderList.data[0].id);
                              Navigator.of(context).pop();
                              if (res == true) {
                                Navigator.of(context).pop();

                              }

                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ));
  }

  getColor(number) {

    if (number == 1.0) {
      setState(() {
        messageeeeeee = "Hideous";
        _image = Image.asset("assets/images/face_1.png");
        _boxDecoration = const BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(255, 252, 114, 114),
            Color.fromARGB(255, 249, 94, 94)
          ],
        ));
      });
      print(number);
      return;
    }
    if (number == 2.0) {
      setState(() {
        messageeeeeee = "Bed";
        _image = Image.asset("assets/images/face_2.png");

        _boxDecoration = const BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(255, 185, 46, 207),
            Color.fromARGB(255, 185, 46, 207)
          ],
        ));
      });
      print(number);
      return;
    }

    if (number == 3.0) {
      setState(() {
        messageeeeeee = "Average";
        _image = Image.asset("assets/images/face_3.png");
        _boxDecoration = const BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(255, 42, 168, 193),
            Color.fromARGB(255, 46, 174, 198)
          ],
        ));
      });

      print(number);
      return;
    }
    if (number == 4.0) {
      setState(() {
        messageeeeeee = "Good";
        _image = Image.asset("assets/images/face_4.png");
        _boxDecoration = const BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(255, 35, 198, 98),
            Color.fromARGB(255, 33, 211, 108)
          ],
        ));
      });
      print(number);
      return;
    }
    if (number == 5.0) {
      setState(() {
        messageeeeeee = "Awesome";
        _image = Image.asset("assets/images/face_5.png");
        _boxDecoration = const BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(255, 9, 151, 237),
            Color.fromARGB(255, 9, 151, 237)
          ],
        ));
      });
      print(number);
    }
  }
}
