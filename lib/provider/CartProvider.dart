import 'package:flutter/cupertino.dart';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/model/listing_model.dart';
import 'package:krays_in/model/save_cart.dart';
import 'package:krays_in/model/user_cart.dart';
import 'package:krays_in/services/service.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/api_variable.dart';
import 'package:krays_in/util/r.dart';

class CartProvider extends ChangeNotifier {

  SaveCartModelItem _saveCartItemList;
  SaveCartModelItem get saveCartItemList => _saveCartItemList;


  UserCartModel _userCartList ;
  UserCartModel get userCartList => _userCartList;

  int _cartCount=0;
  int  get cartCount   => _cartCount;



  BuildContext buildContext;

  CartProvider(BuildContext context) {
    buildContext = context;
  }


  Future<dynamic> doSaveCart({ BuildContext context,
    dynamic productId, dynamic qty,dynamic price,dynamic unit,dynamic weight,dynamic variantId }) async {
    var res = await Service().post(API.SAVE_CART,
        body: APIVariable().saveCart(productId: productId,qty: qty,
          price: price,unit: unit,
          weight:  weight,
          variantId: variantId
        ));
    if (res != "error") {
      _saveCartItemList = SaveCartModelItem.fromJson(jsonDecode(res));
      getUserCart(context: context);
      notifyListeners();
      return true;
    } else {
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }



  Future<dynamic> getUserCart({ BuildContext context}) async {
    var res = await Service().post(API.GET_USER_CART,
        body: APIVariable().getCart(loginId: 0,));
    if (res != "error") {


      final parsedJson = jsonDecode(res);
      bool isItemAvailable = parsedJson["status"];
      if(isItemAvailable){
        _userCartList = UserCartModel.fromJson(jsonDecode(res));
        _cartCount=_userCartList.data!=null?_userCartList.data.length:0;
      }else{
        _cartCount=0;
        _userCartList=null;
      }




      notifyListeners();
      return true;
    } else {
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }


  Future<dynamic> deleteItemInCart({ BuildContext context,
    dynamic productId,dynamic variantId}) async {
    var res = await Service().post(API.DELETE_CART_ITEM,
        body: APIVariable().getDeleteCartItem(productId: productId,variantId: variantId));
    if (res != "error") {

      final parsedJson = jsonDecode(res);
     bool isItemDelete = parsedJson["status"];
      print(isItemDelete);

      getUserCart(context: context);
     // notifyListeners();
      return isItemDelete;
    } else {
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }


  void updateQty(int qty ,index,dynamic variantId){
    String qty1 = "$qty";
    _userCartList.data[index].quantity=qty1;
    doUpdateCartItemQty(context: buildContext,productId: _userCartList.data[index].id,
        qty: qty,price:_userCartList.data[index].salePrice,unit: _userCartList.data[index].unit ,weight:_userCartList.data[index].weight,
    variantId:     variantId
    );
    notifyListeners();
  }

  Future<dynamic> doUpdateCartItemQty({ BuildContext context,
    dynamic productId, dynamic qty,dynamic price,dynamic unit,dynamic weight,dynamic variantId}) async {
    var res = await Service().post(API.UPDATE_CART_ITEM,
        body: APIVariable().updateItemQtyCart(productId: productId,qty: qty,unit: unit,price: price,weight: weight,
            variantId: variantId));
    if (res != "error") {
      _saveCartItemList = SaveCartModelItem.fromJson(jsonDecode(res));
      getUserCart(context: context);
      notifyListeners();
      return true;
    } else {
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }


  Future<dynamic> deleteCart({ BuildContext context,
    dynamic loginId}) async {
    var res = await Service().post(API.DELETE_CART,
        body: APIVariable().deleteCart(loginId: loginId));
    if (res != "error") {

      final parsedJson = jsonDecode(res);
      bool isItemDelete = parsedJson["status"];
      print(isItemDelete);

      getUserCart(context: context);
      notifyListeners();
      return true;
    } else {
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

}
