import 'dart:convert';
import 'dart:developer';


import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/model/banner_model.dart';
import 'package:krays_in/model/brand_model.dart';
import 'package:krays_in/model/category_bag_model.dart';
import 'package:krays_in/model/exclusive_model.dart';
import 'package:krays_in/model/menu_model.dart';
import 'package:krays_in/model/product_groups_model.dart';
import 'package:krays_in/model/product_list_model.dart';
import 'package:krays_in/provider/two_thumBanner.dart';
import 'package:krays_in/services/service.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/api_variable.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/view/home/banner.dart';
import 'package:krays_in/view/home/category_bag.dart';
import 'package:krays_in/view/home/exclusive_product.dart';
import 'package:krays_in/view/home/home_brand_list.dart';
import 'package:krays_in/view/home/product_groups.dart';
import 'package:krays_in/view/home/slider_banner.dart';
import 'package:krays_in/view/home/top_slider_category.dart';


class HomeProvider extends ChangeNotifier{

  BuildContext _buildContext;
  List<BannerModel> _bannerList = [];
  List<BannerModel> get bannerList => _bannerList;
  List<BannerModel> _thumBannerList = [];
  List<BannerModel> get thumBannerList => _thumBannerList;
  List<MenuModel> _menuList = [];
  List<MenuModel> get menuList => _menuList;
  List<BannerModel> _twoThumBannerList = [];
  List<BannerModel> get twoThumBannerList => _twoThumBannerList;
  List<BrandModel> _brandList = [];
  List<BrandModel>  get brandList => _brandList;
  List<CategoryBagModel> _categoryBagList = [];
  List<CategoryBagModel> get categoryBagList => _categoryBagList;
  List<ExclusiveModel> _exclusiveProduct = [];
  List<ExclusiveModel> get exclusiveProduct => _exclusiveProduct;


  List<Widget> _pageList = [TopSliderCategory(), SliderBanner() /*TwoThumBanner(),
  ExclusiveProduct(title:"Exclusive"), HomeBrandList()*/];

  List<Widget> get pageList => _pageList;

  HomeProvider(BuildContext context){
     _buildContext =  context;
    // getHomeBanner();
    // getThumBanner();
    // getExclusiveProduct("0");
   // getProductGroup();
    // getGroup("0", "");
   //  getMenuList();
   //  getBrandList("10");
   //  getCategoryBag();
  }

  Future <dynamic> getHomeBanner() async {

    var res =  await Service().post( API.HomeBanner,
        body: APIVariable().marketHome());
    if(res != "error"){
      _bannerList = fromJsonToBanner(res);
      notifyListeners();
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  Future <dynamic> getThumBanner() async {

    var res =  await Service().post( API.ThumbBanner,
        body: APIVariable().marketHome());
    print(res);
    if(res != "error"){
      _thumBannerList = fromJsonToBanner(res);
      print(_thumBannerList.length.toString());
      notifyListeners();
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  Future <dynamic> getTwoThumBanner() async {

    var res =  await Service().post( API.ThumbBanner,
        body: APIVariable().marketHome());
    print(res);
    if(res != "error"){
      _twoThumBannerList = fromJsonToBanner(res);
      print(_twoThumBannerList.length.toString());
      notifyListeners();
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  Future<dynamic> getProductGroup() async {

    var res =  await Service().post( API.ProductGroups,
        body: "");
    if(res != "error"){
      print(res);
      List<ProductGroupsModel> _productGroups = fromJsonToProductGroups(res);

      for(int i = 0; i < _productGroups.length; i++){
        await getGroup(_productGroups[i].dashboardID, _productGroups[i].title);
      }

      _productGroups.forEach((produtGroups) async {

      });
    } else{
      Fluttertoast.showToast(
          msg: R.string.internal_server_error,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  Future <dynamic> getGroup(dynamic DashBoardId, String title) async {

    var res =  await Service().post( API.ProductGroupWise,
        body: APIVariable().GetProductGroupsList(DashBoardId.toString(),"Home Page"));
    if(res != "error"){
      if(res.length != 0){
        print(res);
        var result = jsonDecode(res);
        for(int i = 0; i < result.length; i++){
          List<ProductListModel> _produtGrouplist  = fromJsonToProductList(jsonEncode(result[i]['ProductList']));
          if(_produtGrouplist.length != 0){
            _pageList.add(ProductGroups(productListModel: _produtGrouplist,
              title: result[i]['DashboardTitle'],
              dashBoardId: result[i]['DashboardTitleID'],));
          }
        }
      }
      notifyListeners();
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  Future<dynamic> getMenuList () async {

    var res =  await Service().post( API.MENU,
        body: "");
    if(res != "error"){
      if(res.length != 0){
        _menuList = fromJsonToMenu(res);
        print(_menuList.length.toString());
        notifyListeners();
      }
      notifyListeners();
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  Future<dynamic> getBrandList (topItemSelection) async {

    var res =  await Service().post( API.BrandList,
        body: APIVariable().GetBrandList(topItemSelection));
    if(res != "error"){
      if(res.length != 0){
        print(res);
        _brandList = fromJsonToBrand(res);
        print(_brandList.length.toString());
        notifyListeners();
      }
      notifyListeners();
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  Future<dynamic> getCategoryBag () async {

    var res =  await Service().post( API.Category_Bag, body: "");
    if(res != "error"){
      if(res.length != 0){
        var result =  jsonDecode(res);
        _categoryBagList = fromJsonCategoryToBag(jsonEncode(result['data']));
        _pageList.add(CategoryBag(isHomePage: true,categoryBagList: _categoryBagList));
        _pageList.add(SingleBanner(bannerUrl: API.IMAGE_BASE_URL+"ItemImages/Banner/Large/Mobile/best-summer-collection-banner.jpg"));
        notifyListeners();
      }
      notifyListeners();
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  Future <dynamic> getExclusiveProduct(dynamic DashBoardId) async {

    var res =  await Service().post( API.ProductGroupWise,
        body: APIVariable().GetProductGroupsList(DashBoardId.toString(), "Exclusive"));
    if(res != "error"){
      if(res.length != 0){
        _exclusiveProduct = fromExclusiveProduct(res);
      }
      notifyListeners();
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }


}