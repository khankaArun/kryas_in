
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/model/market_category_item.dart';
import 'package:krays_in/model/product_list_model.dart';
import 'package:krays_in/provider/MarketProvider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/images.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/view/market/market_listing_page.dart';
import 'package:provider/provider.dart';

import 'HomeProvider.dart';
import 'ProductListingProvider.dart';

class TwoThumBanner extends StatefulWidget {

  @override
  _TwoThumBannerState createState() => _TwoThumBannerState();
}

class _TwoThumBannerState extends State<TwoThumBanner> {

  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    return Consumer<MarketProvider>(
        builder: (context, provider, child) {
         return Column(
           children: [
             Container(
               height: SizeConfig.verticalBloc * 20,
               child: provider.homeDataModel.data.banners.length != 0 ? ListView.builder(
                   itemCount:provider.homeDataModel.data.banners.length,
                   scrollDirection: Axis.horizontal,
                   itemBuilder: (context, index) {
                     return _myBanners(provider,index);
                   }): SizedBox.shrink(),
             ),

             Container(
               height: SizeConfig.verticalBloc * 20,
               child: provider.marketCategoryModel!=null? ListView.builder(
                   itemCount:provider.marketCategoryModel.data.length,
                   scrollDirection: Axis.horizontal,
                   itemBuilder: (context, index) {
                     return _relatedCategory(provider.marketCategoryModel.data,index);
                   }): SizedBox.shrink(),
             ),
           ],
         );
        });
  }

  Widget _relatedCategory(List<Data> data,int index){
    return InkWell(
      onTap: () async{

        AppUtil.showLoadingProgress(context);
        var myProductList =await  Provider.of<ProductListingProvider>(context, listen: false).getProductList(context: context,
            categoryId: 2,
            subCategoryId:data[index].id,
            count: 100,
            page: 1,
            search: ""/*data[index].name*/

        );
        Navigator.of(context).pop();

        if(myProductList){
          Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage(myTitle: data[index].name,
            isFruits: false,
            isVegetables: false,
            isOthers: true,
            id: 2,
            subCatId: data[index].id,)
          ))
          ;
        }



      },
      child: Container(
        margin: EdgeInsets.only(right: 1, top: 5),
       // padding: EdgeInsets.only(left: 10),
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.verticalBloc * 15,
              width: SizeConfig.horizontalBloc * 30,
              child: Container(
                decoration: BoxDecoration(
                  color: R.color.white_color,
                  /*border: Border.all(
                    color: Colors.grey,
                    width: 3,
                  ),*/
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Images.buildImage(data[index].image),
                ),

              ),
            ),
            SizedBox(
              height: SizeConfig.horizontalBloc * 2,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width/2.9,
              child: Text(""+data[index].name,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,  textAlign: TextAlign.center, style: TextStyle(
                    fontSize: SizeConfig.smallFontSize,
                    fontWeight: FontWeight.bold,

                    color: R.color.black_color),
              ),
            ),

          ],
        ),
      ),
    );
  }

  Widget _myBanners(MarketProvider provider,int index){
    return  Container(
     // margin: EdgeInsets.only( bottom: SizeConfig.verticalBloc * 0),
      color: R.color.grey_background,
      child: Container(
        color: R.color.grey_background,
        margin: EdgeInsets.only(top: SizeConfig.verticalBloc * 1, left: 2,right: 2,bottom: SizeConfig.verticalBloc * 1),
        child: Row(
          children: [

            InkWell(
              onTap: () async {
                AppUtil.showLoadingProgress(context);
                var myProductList =await  Provider.of<ProductListingProvider>(context, listen: false).getProductList(context: context,
                    categoryId: provider.homeDataModel.data.banners[index].category,
                    subCategoryId: provider.homeDataModel.data.banners[index].subcategory,
                    count: 100,
                    page: 1,
                    search: ""

                );
                Navigator.of(context).pop();

                if(myProductList){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage(myTitle: provider.homeDataModel.data.banners[index].bannerName,
                    isFruits: false,
                    isVegetables: false,
                    isOthers: true,
                    id:  provider.homeDataModel.data.banners[index].category,
                    subCatId:provider.homeDataModel.data.banners[index].subcategory,)));
                }

              },

              child:   Container(
                height: SizeConfig.verticalBloc * 18,
                width: SizeConfig.horizontalBloc * 66,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(provider.homeDataModel.data.banners[index].image),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
              ),
            ),

          /*  InkWell(

              onTap: () async {
                AppUtil.showLoadingProgress(context);
                var myProductList =await  Provider.of<ProductListingProvider>(context, listen: false).getProductList(context: context,
                    categoryId: provider.homeDataModel.data.banners[1].category,
                    subCategoryId: provider.homeDataModel.data.banners[1].subcategory,
                    count: 20,
                    page: 1,
                    search: ""

                );
                Navigator.of(context).pop();

                if(myProductList){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage(myTitle: "Banners",
                    isFruits: false,
                    isVegetables: false,
                    isOthers: true,)));
                }

              },

              child:  Container(
                height: SizeConfig.verticalBloc * 13,
                width: SizeConfig.horizontalBloc * 45,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(provider.homeDataModel.data.banners[1].image),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
              ),
            )*/
          ],
        ),
      ),
    );
  }
}
