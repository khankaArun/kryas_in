

import 'dart:convert';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/model/listing_model.dart';
import 'package:krays_in/model/product_detail_model.dart';
import 'package:krays_in/services/service.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/api_variable.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/r.dart';
import 'package:provider/provider.dart';

import 'CartProvider.dart';


class ProductListingProvider extends ChangeNotifier {

  ListingModelItem _productListing;
  ListingModelItem get productListing => _productListing;


  BuildContext buildContext;
  ProductListingProvider(BuildContext context){
    buildContext =  context;
  }



  Future<dynamic> getProductList({ BuildContext context,
  dynamic categoryId, dynamic subCategoryId, dynamic search, dynamic page, dynamic count}) async {

    var res =  await Service().post( API.PRODUCT_LISTING,
        body: APIVariable().ProductList(categoryId :categoryId,subCategoryId: subCategoryId,
            search: search,page: page,count: count));
    if(res != "error"){

      _productListing =  ListingModelItem.fromJson(jsonDecode(res));

      if(! _productListing.status){

        Fluttertoast.showToast(
            msg: _productListing.message,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: R.color.errorColor,
            textColor: Colors.white,
            fontSize: 16.0);

        notifyListeners();
        return false;
      }
      notifyListeners();
      return true;
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }

  }

  setQuantity(BuildContext context,int index ,int count) async {

    _productListing.data[index].quantity=count;


    var isItemSaved =await  Provider.of<CartProvider>(context, listen: false).doSaveCart(context: context,
      productId:productListing.data[index].id,
      qty:    productListing.data[index].quantity,
        unit: productListing.data[index].unit,
        price: productListing.data[index].salePrice,
        weight: productListing.data[index].weight,
        variantId: 0,
    );
    if(isItemSaved){
      setIsItemAddedInCart(index, "1");
      AppUtil.showToast("Item has been moved on your cart");
    }
    notifyListeners();

  }

  setIsItemAddedInCart(int index ,String added){

    _productListing.data[index].isCart=added;
    notifyListeners();

  }

}