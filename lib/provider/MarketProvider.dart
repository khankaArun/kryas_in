import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/model/banner_model.dart';
import 'package:krays_in/model/brand_model.dart';
import 'package:krays_in/model/category_bag_model.dart';
import 'package:krays_in/model/exclusive_model.dart';
import 'package:krays_in/model/market_category_item.dart';
import 'package:krays_in/model/market_model.dart';
import 'package:krays_in/model/menu_model.dart';
import 'package:krays_in/model/product_groups_model.dart';
import 'package:krays_in/model/product_list_model.dart';
import 'package:krays_in/provider/two_thumBanner.dart';
import 'package:krays_in/services/service.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/api_variable.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/view/home/banner.dart';
import 'package:krays_in/view/home/category_bag.dart';
import 'package:krays_in/view/home/exclusive_product.dart';
import 'package:krays_in/view/home/home_brand_list.dart';
import 'package:krays_in/view/home/product_groups.dart';
import 'package:krays_in/view/home/slider_banner.dart';
import 'package:krays_in/view/home/top_slider_category.dart';
import 'package:krays_in/view/market/market_search.dart';


class MarketProvider extends ChangeNotifier{

  BuildContext _buildContext;
  MarketHomeItemModel _homeDataModel ;
  MarketHomeItemModel get homeDataModel => _homeDataModel;

  MarketCategoryModel _marketCategoryModel ;
  MarketCategoryModel get marketCategoryModel => _marketCategoryModel;



  List<Widget> _pageList = [MarketSearch(), TwoThumBanner() , MarketHomePageList()
    /*TwoThumBanner(), ExclusiveProduct(title:"Exclusive"), HomeBrandList()*/];

  List<Widget> get pageList => _pageList;

  MarketProvider(BuildContext context){
    _buildContext =  context;

  }

  Future <dynamic> getHomeData() async {

    var res =  await Service().post( API.MARKET_HOME,
        body: APIVariable().marketHome());
    if(res != "error"){
      //homeDataModel = fromJsonToBanner(res);

      try{
        _homeDataModel = MarketHomeItemModel.fromJson(jsonDecode(res));
        getTopCategory();
      }catch(error){
        print(error.toString());
      }

      notifyListeners();
      return true;
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);

      return false;
    }
  }

  Future <dynamic> getTopCategory() async {

    var res =  await Service().post( API.MARKET_CATEGORY,
        body: APIVariable().getCategory());
    print(res);
    if(res != "error"){
      _marketCategoryModel = MarketCategoryModel.fromJson(jsonDecode(res));;
      print(_marketCategoryModel.data.length);
      notifyListeners();
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }



}