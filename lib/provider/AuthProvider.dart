import 'dart:convert';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/model/user_address.dart';
import 'package:krays_in/model/user_model_item.dart';
import 'package:krays_in/services/service.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/api_variable.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/preferences_until.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/string_confi.dart';

class AuthProvider extends ChangeNotifier {

  BuildContext buildContext;


  UserModelItem _userModel;
  UserModelItem get userModel => _userModel;

  OtpAuthItem _otpAuthItem;
  OtpAuthItem get otpAuthItem => _otpAuthItem;

  UserAddressListModelItem _userAddressListModelItem;
  UserAddressListModelItem get userAddressListModelItem => _userAddressListModelItem;

  AuthProvider(BuildContext context) {
    buildContext = context;
  }


  Future <dynamic> login(dynamic userName, dynamic password,
      dynamic latitude,dynamic longitude,dynamic address,dynamic pincode) async {
    var res = await Service().post(API.Login,
        body: APIVariable().GetLogin(userName.toString(),
          password.toString(),latitude,longitude,address,pincode));
    print("URL" + API.Login);



    if (res != "error") {
      if (res.length != 0) {
        _userModel = UserModelItem.fromJson(jsonDecode(res));

        if (_userModel.error == 0) {
          try{

            PreferenceUtils.setBool(PreferencesKey.IS_USER_LOGIN, true);
            PreferenceUtils.setString(PreferencesKey.USER_LOGIN_NAME, userName);
            PreferenceUtils.setString(PreferencesKey.PASSWORD, password);
            PreferenceUtils.setString(
                PreferencesKey.ACESS_TOKEN, _userModel.accessToken);
            PreferenceUtils.setString(
                PreferencesKey.APP_TYPE, _userModel.appType);

            PreferenceUtils.setString(
                PreferencesKey.REFFER_CODE, _userModel.user.refer_code);



          }catch(error){
            print(error.toString()) ;
          }




          userAddressList(loginId: _userModel.user.id);

     /*     Fluttertoast.showToast(
              msg: R.string.successfully_login,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.green_color,
              textColor: Colors.white,
              fontSize: 16.0);*/
          return true;
        } else if (_userModel.error == 1) {

          PreferenceUtils.setBool(PreferencesKey.IS_USER_LOGIN, false);
          Fluttertoast.showToast(
              msg: "Invalid credential",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);
          return false;
        } else {
          Fluttertoast.showToast(
              msg: R.string.internal_server_error,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);
          PreferenceUtils.setBool(PreferencesKey.IS_USER_LOGIN, false);
          return false;
        }
      } else {
        return false;
      }
    } else {
      Fluttertoast.showToast(
          msg: R.string.internal_server_error,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

  Future <dynamic> otpValidator(dynamic recipientMobileEmail) async {


    var res = await Service().post(API.OTP,
        body: APIVariable().otpRequest(recipientMobileEmail));
    print("URL:" + API.OTP);



    print("+++++++++++++" +res);
    if (res != "error") {
      if (res.length != 0) {

        if (jsonDecode(res)['message'].toString().toLowerCase() == "invalid numbers") {

          Fluttertoast.showToast(
              msg: jsonDecode(res)['message'].toString(),
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);
          return false;
        }


        _otpAuthItem = OtpAuthItem.fromJson(jsonDecode(res));


        if (_otpAuthItem.error.toString() == "0") {
          return true;
        } else if (_otpAuthItem.error.toString() == "1") {
          Fluttertoast.showToast(
              msg: _otpAuthItem.message.toString(),
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);
          return false;
        } else {
          Fluttertoast.showToast(
              msg: R.string.internal_server_error,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);
          return false;
        }
      } else {
        return false;
      }
    } else {
      Fluttertoast.showToast(
          msg: R.string.internal_server_error,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

  Future <dynamic> userRegistration({dynamic name,dynamic email,
    dynamic mobile, dynamic password, dynamic referCode}) async {


    dynamic getLat= await PreferenceUtils.getDouble(
        PreferencesKey.userLat);

    dynamic getLong= await PreferenceUtils.getDouble(
        PreferencesKey.userLong);

    dynamic getPin= await PreferenceUtils.getString(
        PreferencesKey.userLocationPincode);

    dynamic getAddress= await PreferenceUtils.getString(
        PreferencesKey.ADDFULL);



    var res = await Service().post(API.USER_REGISTRATION,
        body: APIVariable().userRegistration(
            name: name,
            email: email,
            mobile: mobile,
            password: password,
            referCode: referCode,
            latitude:  getLat,
          longitude: getLong,
          address: getAddress,
          pincode: getPin
        ));

    print(res);

    if (res != "error") {
      if (res.length != 0) {
        SignupItem signupItem = SignupItem.fromJson(jsonDecode(res));

        if (signupItem.error.toString() == "0") {

          Fluttertoast.showToast(
              msg: "User successfully registered ",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.green_color,
              textColor: Colors.white,
              fontSize: 16.0);
          return true;
        } else if(signupItem.error.toString() == "4"){

          Fluttertoast.showToast(
              msg: "Already registered, error code 4",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);
          return false;
        }

        else if (signupItem.error.toString() == "1") {

          Fluttertoast.showToast(
              msg: "Already registered error code 1 ",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);
          return false;
        } else if (signupItem.error.toString() == "3"){

          Fluttertoast.showToast(
              msg: "Already registered, error code 3 ",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);
          return false;
        } else {
          Fluttertoast.showToast(
              msg: R.string.internal_server_error,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);
          return false;
        }
      } else {
        return false;
      }
    } else {
      Fluttertoast.showToast(
          msg: R.string.internal_server_error,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

  Future <dynamic> forgotPassword({dynamic email}) async {

    var res = await Service().post(API.FORGOT_PASSWORD,
        body: APIVariable().forgotPassword(
            email: email));


    print("+++" + API.FORGOT_PASSWORD);

    print(res);

    if (res != "error") {
      if (res.length != 0) {

        var json=jsonDecode(res);

        if (json['error'].toString() == "0") {

          Fluttertoast.showToast(
              msg: json['message'].toString(),
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.green_color,
              textColor: Colors.white,
              fontSize: 16.0);
          return true;
        }  else {
          Fluttertoast.showToast(
              msg: json['message'].toString(),
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);
          return false;
        }
      } else {
        return false;
      }
    } else {
      Fluttertoast.showToast(
          msg: R.string.internal_server_error,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }


  Future <dynamic> userAddressList({dynamic loginId}) async {
    String   token=await "Bearer "+PreferenceUtils.getString(PreferencesKey.ACESS_TOKEN);
    bool   myBool=await PreferenceUtils.getBool(PreferencesKey.IS_USER_LOGIN,false);

    var res = await Service().post(API.GET_USER_ADDRESS,
        body: APIVariable().addressList(
            loginId:  loginId)
    );

    try{

      if (res != "error") {

        final parsedJson = jsonDecode(res);
        bool isAddressAvailable = parsedJson["address"].isEmpty;
        if(!isAddressAvailable){
          _userAddressListModelItem = UserAddressListModelItem.fromJson(jsonDecode(res));
          print("user has address");

        }else{
          print("user don't have address");
          _userAddressListModelItem=null;
        }


        notifyListeners();
        return true;

      } else {

        return false;
      }

    }catch(error){

    }


  }




  Future<dynamic> saveShippingAddress( {dynamic addressId, dynamic fName,
    dynamic lName, dynamic contactNo, dynamic email,
    dynamic address, dynamic addressLine2, dynamic lat, dynamic lng,
    dynamic pincode, dynamic isDefaultAddress,
    dynamic type,dynamic latitude,dynamic longitude}) async {


    var res =  await Service().post( API.SAVE_USER_ADDRESS,
        body: APIVariable().getShippingAddress(
          addressId: addressId,
          fName: fName,
          lName: lName,
          contactNo: contactNo,
          email: email,
          address:  address,
          addressLine2: addressLine2,
          pincode: pincode,
          isDefaultAddress: isDefaultAddress,
          type: type,
            latitude: latitude,
            longitude: longitude

        )

    );
    print(res);
    if(res != "error"){
      if(res.length != 0){
        var result =  jsonDecode(res);
        if(result['error'].toString().contains("0")){
          Fluttertoast.showToast(
              msg: "Address has been saved",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              backgroundColor: R.color.greenColor,
              textColor: Colors.white,
              fontSize: 16.0);

          userAddressList(loginId: 0);
          return true;
        } else{

          Fluttertoast.showToast(
              msg: R.string.internal_server_error,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);

          return false;
        }
      }
    } else{
      Fluttertoast.showToast(
          msg: R.string.internal_server_error,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }

  }






  Future<dynamic> saveSlot( {dynamic address, dynamic phone,
    dynamic lat, dynamic lng, dynamic frSlotTm,
    dynamic toSlotTm,dynamic paymentMode,dynamic payableAmt,dynamic transactionId}) async {


    var res =  await Service().post( API.SAVE_ORDER_SLOT,
        body: APIVariable().saveTimeSlot(
            address: address,
            lat: lat,
            phone: phone,
            lng : lng,
            frSlotTm: frSlotTm,
            toSlotTm: toSlotTm,

          paymentMode : paymentMode,
          payableAmt: payableAmt,
          transactionId: transactionId,



        )

    );
    print(res);
    if(res != "error"){
      if(res.length != 0){
        var result =  jsonDecode(res);
        if(result['status']){
          Fluttertoast.showToast(
              msg: result['message'],
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              backgroundColor: R.color.greenColor,
              textColor: Colors.white,
              fontSize: 16.0);

          return true;
        } else{

          Fluttertoast.showToast(
              msg: R.string.internal_server_error,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);

          return false;
        }
      }
    } else{
      Fluttertoast.showToast(
          msg: R.string.internal_server_error,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }

  }


  Future <dynamic> resetPassword({dynamic oldPassword,dynamic newPassword }) async {

    var res = await Service().post(API.changePassword,
        body: APIVariable().resetPassword(
            oldPassword: oldPassword,
            newPassword: newPassword
        )
    );


    print(res);

    if (res != "error") {
      if (res.length != 0) {

        var json=jsonDecode(res);

        if (json['error'].toString() == "0") {

          Fluttertoast.showToast(
              msg: json['message'].toString(),
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.green_color,
              textColor: Colors.white,
              fontSize: 16.0);
          return true;
        }  else {
          Fluttertoast.showToast(
              msg: json['message'].toString(),
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);
          return false;
        }
      } else {
        return false;
      }
    } else {
      Fluttertoast.showToast(
          msg: R.string.internal_server_error,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

  Future <dynamic> getStateCity({dynamic pincode}) async {


    var res = await Service().post(API.pincode,
        body: APIVariable().pincodeAvailable(
            pincode:    pincode)
    );

    try{

      if (res != "error") {

        final parsedJson = jsonDecode(res);
        bool isAddressAvailable = parsedJson["status"];
       /* if(!isAddressAvailable){
          _userAddressListModelItem = UserAddressListModelItem.fromJson(jsonDecode(res));
          print("user has address");

        }else{
          print("user don't have address");
          _userAddressListModelItem=null;
        }*/


       // notifyListeners();
        return isAddressAvailable;

      } else {

        return false;
      }

    }catch(error){
      return false;
    }


  }



  Future <dynamic> userFcmToken({dynamic fcmToken,dynamic brand,dynamic model}) async {

    var res = await Service().post(API.SAVE_FCM_TOKEN,
        body: APIVariable().saveFcmToken(
            fcmToken:  fcmToken,
          brand: brand,
          model: model
        )
    );

    try{

      if (res != "error") {

        final parsedJson = jsonDecode(res);
        bool isAddressAvailable = parsedJson["address"].isEmpty;
        if(!isAddressAvailable){
          _userAddressListModelItem = UserAddressListModelItem.fromJson(jsonDecode(res));
          print("user has address");

        }else{
          print("user don't have address");
          _userAddressListModelItem=null;
        }


        notifyListeners();
        return true;

      } else {

        return false;
      }

    }catch(error){

    }


  }



}