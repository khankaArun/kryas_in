import 'dart:convert';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/model/order_lis_model.dart';
import 'package:krays_in/model/orderd_details.dart';
import 'package:krays_in/services/service.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/api_variable.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/view/market/cart_page.dart';
import 'package:provider/provider.dart';

import '../util/app_util.dart';
import 'AuthProvider.dart';
import 'CartProvider.dart';

class OrderProvider extends ChangeNotifier{

  BuildContext _buildContext;
  OrderListItem _orderList;
  OrderListItem get orderList => _orderList;

  OrderCompleteDetailsItem _orderDetailList;
  OrderCompleteDetailsItem get orderDetailList => _orderDetailList;

  dynamic _selectedReason;
  dynamic get selectedReason => _selectedReason;


  OrderProvider(BuildContext buildContext){
    _buildContext =  buildContext;
  }


  Future<dynamic> getOrderList(BuildContext buildContext) async {

    _orderList = null;
    var  provider =  await Provider.of<AuthProvider>(buildContext, listen: false);
    var res =  await Service().post( API.GetOrderList,
        body: APIVariable().GetOrderList(provider.userModel.user.id.toString()));


    if(res != "error"){
       var result =  jsonDecode(res);

       final parsedJson = jsonDecode(res);
       bool isItemAvailable = parsedJson["status"];
       if(isItemAvailable){
         _orderList =  OrderListItem.fromJson(jsonDecode(res));
       }else{
         _orderList=null;
       }


       notifyListeners();
      if(_orderList==null){
        return false;
      }else{
        return true;
      }

    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);

      return false;
    }
  }


  Future<dynamic> getDetails(BuildContext buildContext,dynamic orderId) async {

    _orderDetailList = null;
    var res =  await Service().post( API.GetDetails,
        body: APIVariable().OrderId(orderId));


    if(res != "error"){


      final parsedJson = jsonDecode(res);
      bool isItemAvailable = parsedJson["status"];
      if(isItemAvailable){
        _orderDetailList =  OrderCompleteDetailsItem.fromJson(jsonDecode(res));
      }else{
        _orderDetailList=null;
      }


      notifyListeners();
      if(_orderDetailList==null){
        return false;
      }else{
        return true;
      }

    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);

      return false;
    }
  }


  Future<dynamic> getCancelOrder(BuildContext buildContext, dynamic orderId, dynamic status, dynamic productIds, dynamic remark) async {

    var  provider =  await Provider.of<AuthProvider>(buildContext, listen: false);
    var res =  await Service().post( API.OrderCancel,
        body: APIVariable().GetOrderCancel(
            orderId,status,productIds,remark
        ));

    print(res);
    if(res != "error"){
      var result =  jsonDecode(res);
      if(result['status']){
        Fluttertoast.showToast(
            msg: result['message'].toString(),
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: R.color.errorColor,
            textColor: Colors.white,
            fontSize: 16.0);
        var res =  await getOrderList(buildContext);
        return res;
      }
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

  checkSelectedRadio(dynamic value){

    _selectedReason =  value;
    notifyListeners();
  }

  Future<dynamic> VendorRating( {dynamic vendorid, dynamic Rate,
    dynamic Desc, dynamic orderid}) async {


    var res =  await Service().post( API.VENDOR_RATING,
        body: APIVariable().ratingvendorPara(
            Vendorid: vendorid,
            ratE: Rate,
            desC: Desc,
            orderid: orderid
        )

    );
    print(res);
    if(res != "error"){
      if(res.length != 0){
        var result =  jsonDecode(res);
        if(result['error'].toString().contains("0")){

          Fluttertoast.showToast(
              msg: "Vendor rating has been saved successfully",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.greenColor,
              textColor: Colors.white,
              fontSize: 16.0);

          // userAddressList(loginId: 0);
          return true;
        } else{

          Fluttertoast.showToast(
              msg: R.string.internal_server_error,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: R.color.errorColor,
              textColor: Colors.white,
              fontSize: 16.0);

          return false;
        }
      }
    } else{
      Fluttertoast.showToast(
          msg: R.string.internal_server_error,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }

  }

  Future<dynamic> reOrder(BuildContext buildContext, dynamic orderId) async {

    var  provider =  await Provider.of<AuthProvider>(buildContext, listen: false);
    var res =  await Service().post( API.RE_ORDER,
        body: APIVariable().reOrder(orderId
        ));

    if(res != "error"){
      var result =  jsonDecode(res);
      if(result['status']){
      /*  Fluttertoast.showToast(
            msg: result['message'].toString(),
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: R.color.errorColor,
            textColor: Colors.white,
            fontSize: 16.0);*/
        var cartProvider =    await  Provider.of<CartProvider>(buildContext,listen: false).getUserCart();

        return true;
      }else{

        Fluttertoast.showToast(
            msg: result['message'].toString(),
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: R.color.black_color,
            textColor: Colors.white,
            fontSize: 16.0);
        return false;
      }
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }


  Future<dynamic> ratingVendor(BuildContext buildContext, dynamic orderId) async {

    var  provider =  await Provider.of<AuthProvider>(buildContext, listen: false);
    var res =  await Service().post( API.RE_ORDER,
        body: APIVariable().reOrder(orderId
        ));

    if(res != "error"){
      var result =  jsonDecode(res);
      if(result['status']){
        /*  Fluttertoast.showToast(
            msg: result['message'].toString(),
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: R.color.errorColor,
            textColor: Colors.white,
            fontSize: 16.0);*/
        var cartProvider =    await  Provider.of<CartProvider>(buildContext,listen: false).getUserCart();

        return true;
      }else{

        Fluttertoast.showToast(
            msg: result['message'].toString(),
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: R.color.black_color,
            textColor: Colors.white,
            fontSize: 16.0);
        return false;
      }
    } else{
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

}