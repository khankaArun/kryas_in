

import 'dart:convert';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/model/product_detail_model.dart';
import 'package:krays_in/model/related_model_item.dart';
import 'package:krays_in/services/service.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/api_variable.dart';
import 'package:krays_in/util/r.dart';
import 'package:provider/provider.dart';

import 'CartProvider.dart';


class ProductDetailProvider extends ChangeNotifier {

  ProductDetailModel _productDetail;

  ProductDetailModel get productDetail => _productDetail;

  RelatedModelItem _relatedProduct;

  RelatedModelItem get relatedProduct => _relatedProduct;

  int _qty = 1;
  int get qty => _qty;

  String _id;
  String get id => _id;

  String _subId;
  String get subId => _subId;

  String _productName;
  String get productName => _productName;


  dynamic _weight ;
  get weight => _weight;

  dynamic _unit ;
  get unit => _unit;

  dynamic _salePrice ;
  get salePrice => _salePrice;

  dynamic _varientId ;
  get varientId => _varientId;



  BuildContext buildContext;

  ProductDetailProvider(BuildContext context) {
    buildContext = context;
  }


  Future<dynamic> getProductDetail(dynamic ProductID,
      BuildContext context) async {
    _qty=1;
    var res = await Service().post(API.PRODUCT_DETAILS,
        body: APIVariable().ProductDetail(ProductID.toString()));
    if (res != "error") {
      _productDetail = ProductDetailModel.fromJson(jsonDecode(res));
      _id = _productDetail.data.id;
      _subId = _productDetail.data.subcategory;
      _productName = _productDetail.data.name;

      _weight=_productDetail.data.weight;
      productDetail.data.cartQty=productDetail.data.cartQty==0?_weight:productDetail.data.cartQty;

      _unit= _productDetail.data.unit;
      _salePrice= _productDetail.data.salePrice;
      _varientId= 0;

      notifyListeners();

      getRelatedProduct(context: context,
        categoryId: _productDetail.data.id,
        subCategoryId: _productDetail.data.subcategory,
        productName: _productDetail.data.name,);

      notifyListeners();
      return true;
    } else {
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }



  Future<dynamic> getRelatedProduct({BuildContext context,
    dynamic categoryId,
    dynamic subCategoryId, dynamic productName}) async {
    var res = await Service().post(API.GET_RELATED_PRODUCT,
        body: APIVariable().RelatedProduct(categoryId: categoryId,
            subCategoryId: subCategoryId,
            productName: productName));
    if (res != "error") {
      final parsedJson = jsonDecode(res);
      bool isItemAvailable = parsedJson["status"];
      if (isItemAvailable) {
        _relatedProduct = RelatedModelItem.fromJson(jsonDecode(res));
      } else {
        _relatedProduct = null;
      }

      notifyListeners();
      return true;
    } else {
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }



  Future<void> changeUnit(BuildContext context ,int index) async {

    for (int i = 0; i < productDetail.data.unitVarients.length; i++) {
      productDetail.data.unitVarients[i].isUnitSelected=false;

    }


    if( productDetail.data.unitVarients[index].isUnitSelected){
      productDetail.data.unitVarients[index].isUnitSelected=false;
    }else{
      productDetail.data.unitVarients[index].isUnitSelected=true;

      _weight = productDetail.data.unitVarients[index].weight;
      _unit = productDetail.data.unitVarients[index].unit;
      _salePrice = productDetail.data.unitVarients[index].salePrice;
      _varientId= productDetail.data.unitVarients[index].variantId;
      notifyListeners();

      if(productDetail.data.isCart!="0"){

        Provider.of<CartProvider>(context,listen: false).doUpdateCartItemQty(
            context: context,
            unit: _unit,
            price: _salePrice,
            qty: _qty,
            productId: _id,
            weight: _weight,
            variantId: _varientId,
        );
      }


    }


  }


  void updateQty(BuildContext buildContext,int qty,dynamic _weight,dynamic varientId) {
    _qty = qty;
   // productDetail.data.cartQty=qty;


    notifyListeners();

    if(productDetail.data.isCart!="0"){

      Provider.of<CartProvider>(buildContext,listen: false).doUpdateCartItemQty(
          context: buildContext,
          unit: _unit,
          price: _salePrice,
          qty:  _qty,
          productId: _id,
          weight: _weight,
        variantId: varientId,
      );
    }
  }
}