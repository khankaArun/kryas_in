import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geocoder/geocoder.dart';

import 'package:geolocator/geolocator.dart' as g;
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/provider/CartProvider.dart';
import 'package:krays_in/provider/MarketProvider.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:krays_in/view/home/home_page.dart';
import 'package:krays_in/view/loginScreens/login_page.dart';
import 'package:krays_in/view/market/shipping_address.dart';
import 'package:location/location.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:uuid/uuid.dart';

import 'util/preferences_until.dart';

class MapScreen extends StatefulWidget {

  bool isComeFromSearch=false;

  double latitude, longitude;

  dynamic whereItComeFrom;

  MapScreen(this.isComeFromSearch,this.latitude,this.longitude,this.whereItComeFrom);

  @override
  _MapScreenState createState() => _MapScreenState(isComeFromSearch,latitude,longitude,whereItComeFrom);


}

class _MapScreenState extends State<MapScreen> {

  bool isComeFromSearch=false;
  double latitude, longitude;
  dynamic whereItComeFrom;

  _MapScreenState(this.isComeFromSearch,this.latitude,this.longitude,this.whereItComeFrom);

  static LatLng _initialPosition;
  Completer<GoogleMapController> controller1;
  GoogleMap map;
  Location currentLocation = Location();
  Set<Marker> _markers = {};
  bool getLocationLoading;
  bool visible = false;
  bool bottomList = false;

  var Address = "";
  var subLocality = "";
  static LatLng _lastMapPosition = _initialPosition;


  void _getUserLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: g.LocationAccuracy.high);

     dynamic coordinates ;
    if(isComeFromSearch){

       coordinates = new Coordinates(latitude, longitude);

       PreferenceUtils.setDouble(PreferencesKey.userLat, latitude);
       PreferenceUtils.setDouble(PreferencesKey.userLong, longitude);



    }else{
       coordinates = new Coordinates(position.latitude, position.longitude);

       PreferenceUtils.setDouble(PreferencesKey.userLat, position.latitude);
       PreferenceUtils.setDouble(PreferencesKey.userLong, position.longitude);


    }

    var address = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    print(isComeFromSearch);

    setState(() {

      if(isComeFromSearch){

        _initialPosition = LatLng(latitude, longitude);



      }else{
        _initialPosition = LatLng(position.latitude, position.longitude);

      }



      Address = address.first.addressLine.toString();
      subLocality = address.first.featureName.toString();
      PreferenceUtils.setString(PreferencesKey.ADDTITLE, subLocality);
      PreferenceUtils.setString(PreferencesKey.userLocationPincode, address.first.postalCode);

      print("my pincode"+address.first.postalCode);
      PreferenceUtils.setString(PreferencesKey.ADDFULL, Address);
      getLocationLoading = true;
      visible = true;
      bottomList = true;
    });
  }

  navigationPage() async {
    bool isUserLogin =
        await PreferenceUtils.getBool(PreferencesKey.IS_USER_LOGIN, false);



    if (isUserLogin != null && isUserLogin) {

      if(whereItComeFrom==WHERE_IT_COME_FROM.userUpdateAddress.toString()){

        Navigator.of(context).pop();
        Navigator.pushReplacement(context, PageTransition(
            type: PageTransitionType.rightToLeft,
            child: ShippingAddressPage(
                true, "User Profile")));

      }else{

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => HomePage()),
                (Route<dynamic> route) => false);
      }



    } else {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => LoginScreen()),
          (Route<dynamic> route) => false);

      /* Navigator.pushReplacement(
    context, MaterialPageRoute(builder: (context) => OnBoardong()));*/
    }

    // if(isviewed != 0){
    // }else if(email != null){
    //   Navigator.pushNamed(context, StringConfig.HOME_PAGE);
    // }
    // else{
    //   Navigator.pushNamed(context, StringConfig.LOGIN_PAGE);
    //
    // }
  }

  _onMapCreated(GoogleMapController controller) {
    setState(() {
      controller1.complete(controller);
    });
  }

  MapType _currentMapType = MapType.normal;
  void _onCameraMove(CameraPosition position) async {
    var first;
    final coordinates =
        new Coordinates(_lastMapPosition.latitude, _lastMapPosition.longitude);
    var address =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    setState(() {
      _lastMapPosition = position.target;
      first = address.first;
      Address = first.addressLine.toString();
      subLocality = address.first.featureName.toString();

      PreferenceUtils.setString(PreferencesKey.userLocationPincode, address.first.postalCode);

      PreferenceUtils.setDouble(PreferencesKey.userLat, _lastMapPosition.latitude);
      PreferenceUtils.setDouble(PreferencesKey.userLong, _lastMapPosition.longitude);

      PreferenceUtils.setString(PreferencesKey.userLocationPincode, address.first.postalCode);



      print("my pincode"+address.first.postalCode);
      PreferenceUtils.setString(PreferencesKey.ADDTITLE, subLocality);
      PreferenceUtils.setString(PreferencesKey.ADDFULL, Address);
    });

    print(address.first.subLocality.toString());
    print(address.first.addressLine.toString());

    print(address.first.featureName.toString());

    print('_centre:$_lastMapPosition');
  }

  @override
  void initState() {
    super.initState();
    //_onCameraMove;
    _getUserLocation();
    getLocationLoading = false;

    print(whereItComeFrom);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
       // mainAxisAlignment: MainAxisAlignment.center,
      //  crossAxisAlignment: CrossAxisAlignment.center,
        //crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [


          getLocationLoading?
          Stack(alignment: Alignment.center, children: [

            Center(
              child: Container(
                // margin: EdgeInsets.only(top: 25),
                height: MediaQuery.of(context).size.height * 0.75,
                width: MediaQuery.of(context).size.width,
                child: GoogleMap(
                  mapType: _currentMapType,
                  zoomControlsEnabled: false,
                  initialCameraPosition: CameraPosition(
                    target: _initialPosition,
                    zoom: 17.5,
                  ),
                  onCameraMove: _onCameraMove,
                  onMapCreated: _onMapCreated,
                  markers: _markers,
                ),
              ),
            ),

            Visibility(
              visible: visible,
              child: Center(
                child: Container(
                  height: 55,
                  width: 55,
                  child: new Image(
                      image: new AssetImage('assets/images/pinmarker.png')),
                ),
              ),
            ),
          ]):
          SpinKitRipple(color: Colors.blue),


          Visibility(
            visible: bottomList,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.25,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: Colors.white54,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20.0),
                      topLeft: Radius.circular(20.0))),
              child: SingleChildScrollView(
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 16, left: 15),
                      child: Text(
                        "SELECT DELIVERY LOCATION",
                        style: TextStyle(
                            fontSize: 13,
                            color: Colors.black38,
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 8,
                          child: Container(
                            margin: EdgeInsets.only(top: 10, left: 15),
                            child: Text(
                              subLocality.toUpperCase(),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),

                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 2, left: 15,right: 10),
                      child: Text(Address,
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                            letterSpacing: 1.0
                          ),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      margin: EdgeInsets.only(left:30,right: 30,top: 12),
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: TextButton(
                        child: Text(
                          'CONFIRM LOCATION',
                          style: TextStyle(fontSize: 13.0, color: Colors.white,letterSpacing: 2.0),
                        ),
                        onPressed: () {
                          navigationPage();
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
