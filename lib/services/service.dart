import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:krays_in/util/preferences_until.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/string_confi.dart';

class Service {

  Future<dynamic> post(String url, {var body}) async {

    print("url: "+ url  );
    print(jsonEncode(body));
    // bool isUserLogin = PreferenceUtils.getBool(
    //         PreferencesKey.IS_USER_LOGIN, false);
   // PreferencesKey.ACESS_TOKEN,

    var token= "";
    try{

      token="Bearer "+PreferenceUtils.getString(PreferencesKey.ACESS_TOKEN);
      print("token :"+token);
    }catch(error){
      token='Basic WWFhbnRyYUFkbWluOkFkbWluQDIwMjE=';
    }

    try {
      var response = await http.post(
          Uri.parse(url),
          body: json.encode(body),
          headers: {
            "Authorization" : token,
           // "DeviceRequest" : "Mobile",
            "Content-Type" : "application/json"});
      if(response.statusCode == 200){
        print("response :"+response.body.toString());
        return response.body;
      } else{
        print("error response "+response.body.toString());
        return "error";
      }
    }catch(e){
      print("Message try Catch : "+e.toString());
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return "error";
    }

  }
  Future<dynamic> postBodyEmpty(String url) async {

    print(url);
    var token= "";
    try{

      token="Bearer "+PreferenceUtils.getString(PreferencesKey.ACESS_TOKEN);
      print("token :"+token);
    }catch(error){
      token='Basic WWFhbnRyYUFkbWluOkFkbWluQDIwMjE=';
    }
    try {
      var response = await http.post(
          Uri.parse(url),
          headers: {
            "Authorization" : token,
           // "DeviceRequest" : "Mobile",
            "Content-Type" : "application/json"});
      if(response.statusCode == 200){
        return response.body;
      } else{
        print(response.body);
        return "error";
      }
    }catch(e){
      print("Message try Catch : "+e.toString());
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: R.color.errorColor,
          textColor: Colors.white,
          fontSize: 16.0);
      return "error";
    }

  }
}