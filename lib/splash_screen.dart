
import 'dart:async';
import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/provider/CartProvider.dart';
import 'package:krays_in/provider/MarketProvider.dart';
import 'package:krays_in/splash_slider/onBoradSlider.dart';
import 'package:krays_in/util/R.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/preferences_until.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:krays_in/view/home/home_page.dart';
import 'package:provider/provider.dart';

import 'notification/local_notification_service.dart';



class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

 /* @override
  void initState() {
    // TODO: implement initState
    super.initState();


    startTime();
  }*/

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    FirebaseMessaging.instance.getToken().then((value){

      print("FCM Token " +value);
      PreferenceUtils.setString(PreferencesKey.FCM_TOKEN, value);
    });

    FirebaseMessaging.instance.getInitialMessage().then(
          (message) {
        print("FirebaseMessaging.instance.getInitialMessage");
        if (message != null) {
          print("New Notification");
          if (message.data['_id'] != null) {


          }
        }
      },
    );



    // 2. This method only call when App in forground it mean app must be opened
    FirebaseMessaging.onMessage.listen(
          (message) {
        print("FirebaseMessaging.onMessage.listen");
        if (message.notification != null) {
          PreferenceUtils.setString(PreferencesKey.TITLE_NOTIFICATION, message.notification.title);
          PreferenceUtils.setString(PreferencesKey.BODY_NOTIFICATION, message.notification.title);
          print(message.notification.title);
          print(message.notification.body);
          print("message.data11 ${message.data}");
          LocalNotificationService.createanddisplaynotification(message);

        }
      },
    );


    FirebaseMessaging.onMessageOpenedApp.listen(
          (message) {
        print("FirebaseMessaging.onMessageOpenedApp.listen");
        if (message.notification != null) {
          print(message.notification.title);
          print(message.notification.body);
          print("message.data22 ${message.data['_id']}");
        }
      },
    );

    startTime();
  }




  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body:   Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[

          SizedBox(
            width: SizeConfig.horizontalBloc * 80,
            height: SizeConfig.horizontalBloc * 70,
            child: Image.asset(R.images.krays_logo_new_lat),
          ),

          SpinKitRipple(color: Colors.blue),
        ],
      ),/*Container(
          color: R.color.white_color,
          child: Column(
            children: [
              Align(
                alignment: Alignment.center,
                child:  Container(
                  child: Image.asset(R.images.krays_logo_new),
                  // child: SpinKitRipple(color: R.color.app_color),
                ),

              ),

              SizedBox(height: 30.0,),

              SpinKitRipple(color: Colors.blue),
            ],
          ),
      ),*/
    );
  }


  startTime() async {
    await PreferenceUtils.init();
    navigationPage();

   /* var _duration = new Duration(seconds: 5);
    return Timer(_duration, navigationPage);*/
  }

  navigationPage() async {

    bool isUserLogin = await PreferenceUtils.getBool(
        PreferencesKey.IS_USER_LOGIN, false);


    String userLoginId = await PreferenceUtils.getString(
        PreferencesKey.USER_LOGIN_NAME, "");

    String password = await PreferenceUtils.getString(
        PreferencesKey.PASSWORD, "");

    if (isUserLogin != null && isUserLogin) {

      var homeProvider     =await  Provider.of<MarketProvider>(context, listen: false).getHomeData();
      var cartProvider =    await  Provider.of<CartProvider>(context,listen: false).getUserCart();


      dynamic getLat= await PreferenceUtils.getDouble(
          PreferencesKey.userLat);

      dynamic getLong= await PreferenceUtils.getDouble(
          PreferencesKey.userLong);

      dynamic getPin= await PreferenceUtils.getString(
          PreferencesKey.userLocationPincode);

      dynamic getAddress= await PreferenceUtils.getString(
          PreferencesKey.ADDFULL);





      var userAuth     =await  Provider.of<AuthProvider>(context, listen: false).login(userLoginId, password, getLat,getLong,getAddress,getPin);



       if(homeProvider && cartProvider){
         Navigator.pushAndRemoveUntil(
             context,
             MaterialPageRoute(
                 builder: (BuildContext context) =>
                     HomePage()),
                 (Route<dynamic> route) => false);
      }else{
         AppUtil.showToast("Market API, or Cart API not working");
       }




    } else {

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  onBoradSlider()),
              (Route<dynamic> route) => false);

    /* Navigator.pushReplacement(
    context, MaterialPageRoute(builder: (context) => OnBoardong()));*/
    }

    // if(isviewed != 0){
    // }else if(email != null){
    //   Navigator.pushNamed(context, StringConfig.HOME_PAGE);
    // }
    // else{
    //   Navigator.pushNamed(context, StringConfig.LOGIN_PAGE);
    //
    // }

  }

 /* checkLogin() async {
    await PreferenceUtils.init();


    bool isUserLogin = await PreferenceUtils.getBool(
        PreferencesKey.IS_USER_LOGIN, false);

    if (isUserLogin != null && isUserLogin) {
   *//*   Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => StringConfig.LOGIN_PAGE));*//*

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  HomePage()),
              (Route<dynamic> route) => false);

    } else {
    *//*  Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => OnBoardong()));*//*
    }
  }*/
}