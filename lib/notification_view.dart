import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/util/preferences_until.dart';
import 'package:krays_in/util/string_confi.dart';

class NotificatioView extends StatefulWidget {
  const NotificatioView({Key key}) : super(key: key);

  @override
  State<NotificatioView> createState() => _NotificatioViewState();
}

class _NotificatioViewState extends State<NotificatioView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: Text("Notification"),
      ),
      body: Container(

        width: double.infinity,
        height: 150,
        child: Card(

          child: Column(

            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Center(child: Text(PreferenceUtils.getString(PreferencesKey.TITLE_NOTIFICATION),style: TextStyle(fontSize: 19),))),
              Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Center(child: Text(PreferenceUtils.getString(PreferencesKey.BODY_NOTIFICATION),style: TextStyle(fontSize: 19),)))
            ],
          ),
        ),
      ),
    );
  }
}
