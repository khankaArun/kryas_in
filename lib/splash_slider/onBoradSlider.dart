import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:krays_in/current_location.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:krays_in/view/loginScreens/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'OnboardModel.dart';

class onBoradSlider extends StatefulWidget {

  @override
  _onBoradSliderState createState() => _onBoradSliderState();
}

class _onBoradSliderState extends State<onBoradSlider> {

  int currentIndex = 0;
  PageController _pageController;




  List<OnboardModel> screens = <OnboardModel>[
    OnboardModel(
      icon: 'Welcome!',
      img:  'assets/images/buy_at_home.png',
      text:  "Buy at home!",
      desc:"We deliver freshness to your home. Handpick fresh fruits and veggies right at your home doorstep at your scheduled date and time ",
      bg: Colors.white,
      button: Color(0xFF4756DF),
    ),
    /*OnboardModel(
      icon: 'assets/images/logo.png',
      img: 'assets/images/onbor_two.png',
      text:  "Get 'everything' delivered \n 5 to 59 minutes",
      desc: "Buy anything & everything and get it delivered in  5 to 59 minutes at your doorstep.No minimum cart value needed",
      bg:  Colors.white,
      button: Color(0xFF4756DF),
    ),
    OnboardModel(
      icon: 'assets/images/logo.png',
      img: 'assets/images/onbor_three.png',
      text: "Explore the Pvtroom",
      desc: "Long distance relationship or long lost friendship? \n Watch your favourite videos together while being on the run",
      bg:  Colors.white,
      button: Color(0xFF4756DF),
    ),
*/
  ];

  @override
  void initState() {
    _pageController = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {

    double width = MediaQuery.of(context).size.width;
    return Scaffold(

        backgroundColor: Colors.white,

        body:SafeArea(
          child: PageView.builder(
              itemCount: screens.length,
              controller: _pageController,
              // physics: NeverScrollableScrollPhysics(),
              onPageChanged: (int index) {
                setState(() {
                  currentIndex = index;
                });
              },
              itemBuilder: (_, index) {
                return Container(
                  height: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,

                  child: Column(

                    children: [

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,

                        children: <Widget>[
                          Image(
                            image: new AssetImage(R.images.curvesecond),
                            height: 80,
                          ),




                        ],

                      ),

                      Text(screens[index].icon,
                        style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold, letterSpacing: 2,
                            color: R.color.grey
                        ),
                      ),
                      SizedBox(height: 10,),
                      Container(
                        alignment: Alignment.center,
                        child: Image.asset(screens[index].img,scale: 1.5,),
                      ),
                      SizedBox(height: 10,),
                      Text(
                        screens[index].text,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold, letterSpacing: 0.5,
                            // color: index % 2 == 0 ? kblack : kwhite,
                            color: R.color.black_color
                        ),
                      ),
                      SizedBox(height: 20,),


                      Container(
                        margin: EdgeInsets.only(left: 20,right: 20),
                        child:  Text(
                        screens[index].desc,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 14,
                            letterSpacing: 0.5,
                            fontFamily: 'ralewayMedium',
                            color: Colors.grey

                        ),
                      ),),
                      SizedBox(height: 40,),

                      InkWell(
                        onTap: () {

                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      CurrentLocation()),
                                  (Route<dynamic> route) => false);



                        },
                        child: Container(
                          margin: EdgeInsets.only(bottom: 10 ,left: 15,right: 15),
                          width: width - 40.0,
                          height: 43,
                          padding: EdgeInsets.all(5.0),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: R.color.app_color,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Text(
                            'Start Now',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Jost',
                                letterSpacing: 0.7,
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )

                    ],
                  ),
                );
              })
        )
    );
  }
}
