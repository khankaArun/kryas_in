import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/string_confi.dart';

class AnimationsPage extends StatefulWidget {
  const AnimationsPage({Key key}) : super(key: key);

  @override
  _AnimationsPageState createState() => _AnimationsPageState();
}

class _AnimationsPageState extends State<AnimationsPage> with TickerProviderStateMixin {

  Animation<double> animation;
  AnimationController _controller;
  @override
  void initState() {
    super.initState();

    _controller =
    new AnimationController(vsync: this, duration: Duration(seconds: 2))
      ..repeat();
    animation = new CurvedAnimation(parent: _controller, curve: Curves.linear);
    startTime();
  }


  startTime() async {
    var _duration = new Duration(seconds: 4);
    return Timer(_duration, navigationPage);
  }


  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  navigationPage() async {
    Navigator.pushNamed(context, StringConfig.SPLASH_SCREEN);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.white_color,
      body: Center(
        child: ScaleTransition(
          scale: animation,
          child:  Padding(
            padding: EdgeInsets.all(8.0),
            child: Image.asset(R.images.animate_logo),
          ),
        ),
      ),
    );
  }
}
