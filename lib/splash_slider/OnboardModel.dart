
import 'package:flutter/material.dart';

class OnboardModel {
  String  img;
  String  text;
  String desc;
  Color bg;
  String icon;
  Color button;

  OnboardModel({
     this.img,
     this.text,
     this.desc,
     this.bg,
     this.button,
     this.icon
  });
}