
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:krays_in/current_location.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/provider/CartProvider.dart';
import 'package:krays_in/provider/HomeProvider.dart';
import 'package:krays_in/provider/MarketProvider.dart';
import 'package:krays_in/provider/ProductListingProvider.dart';
import 'package:krays_in/provider/order_provider.dart';
import 'package:krays_in/provider/product_detail_provider.dart';
import 'package:krays_in/splash_screen.dart';
import 'package:krays_in/splash_slider/animation_page.dart';
import 'package:krays_in/splash_slider/onBoradSlider.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:krays_in/view/home/home_page.dart';
import 'package:krays_in/view/loginScreens/forgot_password.dart';
import 'package:krays_in/view/loginScreens/login_page.dart';
import 'package:krays_in/view/loginScreens/otp_verification.dart';
import 'package:krays_in/view/loginScreens/reset_password.dart';
import 'package:krays_in/view/loginScreens/sing_up.dart';
import 'package:krays_in/view/user_auth/singup_page.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';

import 'map.dart';
import 'notification/local_notification_service.dart';


Future<void> backgroundHandler(RemoteMessage message) async {
  print(message==null?"":message.data.toString());
  print(message==null?"":message.notification.title);
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}


void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  LocalNotificationService.initialize();
  FirebaseMessaging.onBackgroundMessage(backgroundHandler);
  runApp(MyApp());
}

ThemeData _themeData =  ThemeData(
  primarySwatch: Colors.blue,
  primaryColor: R.color.themeColor,
  backgroundColor: R.color.black_color,
  scaffoldBackgroundColor: R.color.black_color,
  cursorColor: R.color.themeColor,
);

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {



    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.dark
    ));

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MultiProvider(providers: [


      ChangeNotifierProvider<AuthProvider>(create: (_) => AuthProvider(context)),
      ChangeNotifierProvider<HomeProvider>(create: (_) => HomeProvider(context)),
      ChangeNotifierProvider<MarketProvider>(create: (_) => MarketProvider(context)),
      ChangeNotifierProvider<ProductDetailProvider>(create: (_) => ProductDetailProvider(context)),
      ChangeNotifierProvider<ProductListingProvider>(create: (_) => ProductListingProvider(context)),
      ChangeNotifierProvider<CartProvider>(create: (_) => CartProvider(context)),
      ChangeNotifierProvider<OrderProvider>(create: (_) => OrderProvider(context)),

    ],


    child: MaterialApp(
      title: 'KraysIn',

      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Colors.blue,
        // accentColor: const Color(0xFFFDE400),
        accentColor: Colors.blue,
        primaryColorLight: const Color(0xFFE60000),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      debugShowCheckedModeBanner: false,
      home:SplashPage(),
      // SplashPage()/*AnimationsPage()*/,
        routes:  <String,WidgetBuilder> {

          StringConfig.SPLASH_SCREEN:(BuildContext context) => SplashPage(),
          StringConfig.LOGIN_PAGE:(BuildContext context) => LoginScreen(),
          StringConfig.SIGN_UP:(BuildContext context) => SignUpScreen(),
          StringConfig.OTP_VERIFY:(BuildContext context) => OtpVerification(),
          StringConfig.FORGET_PASS:(BuildContext context) => ForgetPass(),
          StringConfig.RESET_PASS:(BuildContext context) => ResetPassword(),


        }
    ),);
  }
}


