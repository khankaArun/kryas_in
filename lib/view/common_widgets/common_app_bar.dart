import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/notification_view.dart';
import 'package:krays_in/provider/CartProvider.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/preferences_until.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:krays_in/view/market/cart_page.dart';
import 'package:provider/provider.dart';

class MyFancyAppBar extends StatefulWidget implements PreferredSizeWidget {



  static final _appBar = AppBar();
  @override
  Size get preferredSize => _appBar.preferredSize;

  @override
  _MyFancyAppBarState createState() => _MyFancyAppBarState();
}
  



  class _MyFancyAppBarState extends State<MyFancyAppBar> {
    @override
    Widget build(BuildContext context) {

      SizeConfig().init(context);
      var cartCount = Provider.of<CartProvider>(context, listen: true).cartCount;
      return AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0.01,
        //titleSpacing: 20,
        title: Row(
          children: [
            SizedBox(
              width: SizeConfig.horizontalBloc * 35,
              height: SizeConfig.horizontalBloc * 40,
              child: Image.asset(R.images.krays_logo_new_lat,),
            ),
          ],
        ),
        actions: [

          /*SizedBox(
            width:SizeConfig.horizontalBloc * 7,
            height: SizeConfig.horizontalBloc * 7,
            child: InkWell(
              onTap: (){

                if(cartCount !=null ||cartCount !=0){
                 // Navigator.push(context, MaterialPageRoute(builder: (context)=>SearchKeyWordPage()));
                }
                //
              },
              child: Image.asset(R.images.search_icon),
            ),
          ),*/

          SizedBox(width: SizeConfig.horizontalBloc * 5),


          Padding(padding: const EdgeInsets.only(top: 17),
              child: new Container(
                  width:SizeConfig.horizontalBloc * 8,
                  height: SizeConfig.horizontalBloc * 8,
                  child: new GestureDetector(
                    onTap: () {
                      if(cartCount !=null ||cartCount !=0){
                         Navigator.push(context, MaterialPageRoute(builder: (context)=>CartPage()));
                      }

                    },
                    child: new Stack(
                      children: <Widget>[
                        Icon(Icons.shopping_cart_outlined,size: 26,
                        ),
                        cartCount ==0 ? new Container() :
                        Positioned(
                            child: new Stack(
                              children: <Widget>[
                                new Icon(
                                    Icons.brightness_1,
                                    size: 18.0, color: R.color.app_color),
                                Positioned(
                                    top: 3.0,
                                    // bottom: 1.0,
                                    right: 1.0,
                                    left: 1.0,
                                    child: new Center(
                                      child: new Text(
                                        cartCount.toString(),
                                        style: new TextStyle(
                                            color: Colors.white,
                                            fontSize: 10.0,
                                            fontWeight: FontWeight.w500
                                        ),
                                      ),
                                    )),
                              ],
                            )),

                      ],
                    ),
                  )
              )),

          //SizedBox(width: SizeConfig.horizontalBloc * 5),


       /*   SizedBox(
            width:SizeConfig.horizontalBloc * 7,
            height: SizeConfig.horizontalBloc * 7,
            child: InkWell(
              onTap: (){
                // Navigator.push(context, MaterialPageRoute(builder: (context)=>NotificationPage()));
              },
              child: Image.asset(R.images.notification_icon),
            ),
          ),*/
          SizedBox(width: SizeConfig.horizontalBloc * 5),
          /* SizedBox(
              width:SizeConfig.horizontalBloc * 7,
              height: SizeConfig.horizontalBloc * 7,
              child: InkWell(
                onTap: (){
               //   Provider.of<WishlistProvider>(context,listen: false).getWishListItem(context);
               //   Navigator.push(context, MaterialPageRoute(builder: (context)=>WishListPage()));
                },
                child: Image.asset(R.images.whislist_icon),
              ),
            ),

            Padding(padding: const EdgeInsets.all(10.0),
                child: new Container(
                    width:SizeConfig.horizontalBloc * 8,
                    height: SizeConfig.horizontalBloc * 8,
                    child: new GestureDetector(
                      onTap: () {

                      },
                      child: new Stack(
                        children: <Widget>[
                          Image.asset(R.images.cart_icon),
                          cartCount ==0 ? new Container() :
                          Positioned(
                              child: new Stack(
                                children: <Widget>[
                                  new Icon(
                                      Icons.brightness_1,
                                      size: 18.0, color: R.color.app_color),
                                  Positioned(
                                      top: 3.0,
                                      // bottom: 1.0,
                                      right: 1.0,
                                      left: 1.0,
                                      child: new Center(
                                        child: new Text(
                                          cartCount.toString(),
                                          style: new TextStyle(
                                              color: Colors.white,
                                              fontSize: 10.0,
                                              fontWeight: FontWeight.w500
                                          ),
                                        ),
                                      )),
                                ],
                              )),
                        ],
                      ),
                    )
                )),*/
        ],
      );
    }

  
}
