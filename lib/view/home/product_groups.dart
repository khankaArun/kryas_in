
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/model/product_list_model.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:provider/provider.dart';


class ProductGroups extends StatefulWidget {

  List<ProductListModel> productListModel;
  String title;
  dynamic dashBoardId;

  ProductGroups({this.productListModel, this.title, this.dashBoardId});

  @override
  _ProductGroupsState createState() => _ProductGroupsState();

}

class _ProductGroupsState extends State<ProductGroups> {

  List<ProductListModel> productListModel = [];
  String title;
  dynamic dashBoardId;

  @override
  void initState() {
    // TODO: implement initState
    productListModel =  widget.productListModel;
    title =  widget.title;
    dashBoardId =  widget.dashBoardId;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    return Container(
      margin: EdgeInsets.only(top: 10),
      color: R.color.grey_background,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: SizeConfig.verticalBloc * 2.5,
              left: SizeConfig.verticalBloc * 2,
              bottom: SizeConfig.verticalBloc * 0, right: SizeConfig.verticalBloc * 2),
            child: Row(
              children: [
                Expanded(child: Text(title.toUpperCase(),
                  style: TextStyle(
                      // fontSize: SizeConfig.normalFontSize,
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.4),
                      fontWeight: FontWeight.w600,
                      color: R.color.black_color),
                ),),


                SizedBox(
                  height: SizeConfig.safeBlockVertical * 3.4,
                  child: ElevatedButton(
                      style: ButtonStyle(
                          foregroundColor: MaterialStateProperty.all<Color>(R.color.white_color),
                          backgroundColor: MaterialStateProperty.all<Color>(R.color.app_color),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                  side: BorderSide(color: R.color.app_color)
                              )
                          )
                      ),
                     onPressed: () async{

                      /*  Common.showLoadingProgress(context);

                    var res =await   Provider.of<ProductListProvider>(context, listen: false).getProductList(
                           commandTypes: "",
                           filterBrandIds: "",
                           filterColorIds: "",
                           filterSizeIds: "",
                           categoryId: "0",
                           brandId: "0",
                           priceFrom: "0",
                           priceTo: "1000000",
                           sortBy: "",
                           dashBoardId: dashBoardId,
                        catIds : ""
                       );



                        Navigator.of(context).pop();
                        if(res==true){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductListPage(title: title.toString(),)));

                        }*/



                     },
                    child: Text("View All".toUpperCase(), style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.1), fontWeight: FontWeight.w600),),
                  ),
                )
              ],
            )
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            margin: EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              color: R.color.white_color,
              borderRadius: BorderRadius.all(Radius.circular(7)),
            ),
            child: GridView.builder(
                itemCount: productListModel.length > 4 ? 4 : productListModel.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: (orientation == Orientation.portrait) ? 2 : 2,
                    childAspectRatio:  MediaQuery.of(context).size.width /
                        (MediaQuery.of(context).size.height / 1.50)),
                itemBuilder: (context, index) {
                  return _buildItem(productListModel[index]);
                }),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(ProductListModel productListModel){

    return InkWell(
      onTap: () async{
      /*  Common.showLoadingProgress(context);
        var res = await Provider.of<ProductDetailProvider>(context,listen: false)
            .getProductDetail(productListModel.productId, productListModel.matrixId,context);
        Navigator.of(context).pop();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ProductDetailPage()),
        );*/
      },
      child: Card(
        elevation: 0,
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.verticalBloc * 2,
            ),
            SizedBox(
              height: SizeConfig.verticalBloc * 22,
              width: SizeConfig.horizontalBloc * 28,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(
                          productListModel.imageUrl == null
                              || productListModel.imageUrl.toString().isEmpty
                              ? BannerBaseUrl.DefaultImage
                          : BannerBaseUrl.CatgegoryThumbImage+productListModel.imageUrl.toString().replaceAll(" ", "%20"))
                  ),
                  // borderRadius: BorderRadius.all(Radius.circular(80.0)),
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig.horizontalBloc * 2,
            ),
            Text(productListModel.productName,maxLines: 1,  textAlign: TextAlign.center, style: TextStyle(
                fontSize: ResponsiveFlutter.of(context).fontSize(1.4),
                fontWeight: FontWeight.w600,
                color: R.color.black_color),),

            SizedBox(
              height: SizeConfig.horizontalBloc * 1.6,
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(R.priceSymbol.rupee+productListModel.listPrice.toString(), style: TextStyle(
                    fontSize: SizeConfig.smallFontSize,
                    fontWeight: FontWeight.w600,
                    color: R.color.app_color),),

                SizedBox(
                  width: SizeConfig.verticalBloc * 1,
                ),

                Text(R.priceSymbol.rupee+productListModel.listPrice.toString(), style: TextStyle(
                    fontSize: SizeConfig.smallFontSize,
                    fontWeight: FontWeight.w600,
                    decoration: TextDecoration.lineThrough,
                    decorationThickness: 1.85,
                    color: R.color.grey_color),),

                SizedBox(
                  width: SizeConfig.verticalBloc * 1,
                ),

                Text(productListModel.discountPercent.toString()+R.priceSymbol.off, style: TextStyle(
                    fontSize: SizeConfig.smallFontSize,
                    fontWeight: FontWeight.w500,
                    color: R.color.green_color),),
              ],
            )

          ],
        ),
      ),
    );
  }
}
