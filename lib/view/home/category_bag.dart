
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/model/category_bag_model.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:provider/provider.dart';

class CategoryBag extends StatefulWidget {

  bool isHomePage;
  List<CategoryBagModel> categoryBagList;
  CategoryBag({this.isHomePage, this.categoryBagList});

  @override
  _CategoryBagState createState() => _CategoryBagState();
}

class _CategoryBagState extends State<CategoryBag> {
  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    final orientation = MediaQuery.of(context).orientation;
    return  Container(
            margin: EdgeInsets.only(top: 10),
            color: R.color.white_color,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(top: SizeConfig.verticalBloc * 3, left: SizeConfig.verticalBloc * 2, bottom: SizeConfig.verticalBloc * 3,),
                  child: Text( widget.isHomePage == true
                     ? R.string.top_category
                     : R.string.shop_by_category,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: ResponsiveFlutter.of(context).fontSize(1.45),
                        color: R.color.black_color),
                  ),
                ),

                Container(
                  child: GridView.builder(
                      itemCount: widget.categoryBagList.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: (orientation == Orientation.portrait) ? 3 : 3,
                          childAspectRatio:  SizeConfig.horizontalBloc * 55 /
                              (SizeConfig.verticalBloc * 50/ 1.60)),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () async{
                         /*   if(widget.isHomePage == false){

                              Common.showLoadingProgress(context);
                              var res = await Provider.of<ProductListProvider>(context, listen: false).getProductList(
                                  commandTypes: "",
                                  filterBrandIds: "",
                                  filterColorIds: "",
                                  filterSizeIds: "",
                                  categoryId: widget.categoryBagList[index].categoryId.toString(),
                                  brandId: "0",
                                  priceFrom: "0",
                                  priceTo: "1000000",
                                  sortBy: "",
                                  dashBoardId: "0",
                                  catIds : "",
                              );

                              Navigator.of(context).pop();
                              if(res==true){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductListPage(title: widget.categoryBagList[index].categoryName,)));
                              }


                            } else{
                              Provider.of<MultipleCategoryProvider>(context, listen: false)
                                  .getCategoryList(widget.categoryBagList[index].categoryId.toString(), context);
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>MultipleCategoryPage(title: widget.categoryBagList[index].categoryName,)));
                            }*/
                          },
                          child:  _buildItem(widget.categoryBagList[index]),
                        );

                      }),
                ),
              ],
            ),
          );
  }

  Widget _buildItem(CategoryBagModel categoryBagModel){
    print("Brand Url "+ BannerBaseUrl.CategoryIcon+categoryBagModel.categoryMenuImage.toString().replaceAll(" ", "%20"));
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: SizeConfig.safeBlockHorizontal * 27,
            width: SizeConfig.safeBlockHorizontal * 27,
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(BannerBaseUrl.CategoryIcon+categoryBagModel.categoryMenuImage.toString().replaceAll(" ", "%20"))
                ),
                borderRadius: BorderRadius.all(Radius.circular(80.0)),
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig.horizontalBloc * 2,
          ),
          Text(categoryBagModel.categoryName,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
              color: R.color.black_color),),
        ],
      ),
    );
  }
}
