import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/current_location.dart';
import 'package:krays_in/provider/HomeProvider.dart';
import 'package:krays_in/provider/MarketProvider.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/view/common_widgets/common_app_bar.dart';
import 'package:provider/provider.dart';

import '../../../util/preferences_until.dart';
import '../../../util/string_confi.dart';

class ShopStreetMainPage extends StatefulWidget {


  @override
  _ShopStreetMainState createState() => _ShopStreetMainState();

}



class _ShopStreetMainState extends State<ShopStreetMainPage> {

  String subLocality="";
  String address="";


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    subLocality=PreferenceUtils.getString(PreferencesKey.ADDTITLE);
    address=PreferenceUtils.getString(PreferencesKey.ADDFULL);
  //  var provider =await  Provider.of<MarketProvider>(context, listen: false);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.grey_background,
      appBar:  MyFancyAppBar(),
      body: Consumer<MarketProvider>(
          builder: (context, provider, child) {
            return Container(
              child: SingleChildScrollView(
                  child: Column(
                    children: [

                      InkWell(
                        onTap: (){
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => CurrentLocation()));
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 5,left: 10),
                              width: 18,
                              height:18,
                              child: Image.asset(R.images.location_arrow,),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5,left: 5),

                              child: Text(subLocality!=null?subLocality:"Add Address",style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.bold),)
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              width: 30,
                              height: 30,
                              child: Icon(Icons.keyboard_arrow_down),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                            margin: EdgeInsets.only(left: 20,right: 15),

                            child: Text(address!=null?address:"",style: TextStyle(color: Colors.black45,fontSize: 12,letterSpacing: 1.0),
                              overflow: TextOverflow.ellipsis,

                            )
                        ),
                      ),
                      ListView.builder(
                          itemCount: provider.pageList.length,
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return _homePageList(provider.pageList[index]);
                          }),


                      //AppFoter(),
                    ],
                  )
              ),
            );
          }),
    );
  }

  Widget _homePageList(Widget pageList){
    return pageList;
  }
}
