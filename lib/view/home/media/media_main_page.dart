import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/provider/HomeProvider.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/view/common_widgets/common_app_bar.dart';
import 'package:provider/provider.dart';

class MediaMainPage extends StatefulWidget {
  const MediaMainPage({ Key key, this.destination }) : super(key: key);

 final  dynamic destination;
  @override
  _MediaMainPageState createState() => _MediaMainPageState();
}

class _MediaMainPageState extends State<MediaMainPage> {

  @override
  void initState() {
    // TODO: implement initState
    var provider = Provider.of<HomeProvider>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
        backgroundColor: R.color.grey_background,
        appBar:  MyFancyAppBar(),
        body:Consumer<HomeProvider>(
            builder: (context, provider, child) {
              return Container(
                child: SingleChildScrollView(
                    child: Column(
                      children: [
                        ListView.builder(
                            itemCount: provider.pageList.length,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              return _homePageList(provider.pageList[index]);
                            }),
                        //AppFoter(),
                      ],
                    )
                ),
              );
            })
    );
  }

  Widget _homePageList(Widget pageList){
    return pageList;
  }


}

