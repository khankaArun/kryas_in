
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/provider/HomeProvider.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:provider/provider.dart';

class SingleBanner extends StatefulWidget {

  String bannerUrl;
  SingleBanner({this.bannerUrl});

  @override
  _BannerState createState() => _BannerState();
}

class _BannerState extends State<SingleBanner> {

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Consumer<HomeProvider>(
        builder: (context, provider, child) {
          return provider.bannerList.length !=  0 ?
          InkWell(
            onTap: () async {
            },
            child: Container(
              margin: EdgeInsets.only(top:SizeConfig.verticalBloc * 2, bottom: SizeConfig.verticalBloc * 0.50),
              height: SizeConfig.verticalBloc * 28,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(widget.bannerUrl),
                ),
                borderRadius: BorderRadius.all(Radius.circular(0.0)),
              ),
            ),
          ): SizedBox.shrink();
        });
    }
}
