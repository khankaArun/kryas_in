
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:krays_in/provider/HomeProvider.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:provider/provider.dart';

class SliderBanner extends StatefulWidget {
  @override
  _SliderBannerState createState() => _SliderBannerState();
}


class _SliderBannerState extends State<SliderBanner> {

@override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
      SizeConfig().init(context);
    return Consumer<HomeProvider>(
      builder: (context, provider, child) {
        return provider.bannerList.length !=  0 ?Container(
          height: SizeConfig.safeBlockVertical * 26,
           child: Container(
            margin: EdgeInsets.only(top: 10),
            child: Swiper(
              itemBuilder: (BuildContext context, int index){
                // print("Home Banner URl "+BannerBaseUrl.HomeBanner+provider.bannerList[index].bannerImage.replaceAll(" ", "%20"));
                return Card(
                  shape:  RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  margin: EdgeInsets.only(left: 0, right: 0),
                  child:InkWell(
                    onTap: (){
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(BannerBaseUrl.HomeBanner+provider.bannerList[index].bannerImage.replaceAll(" ", "%20")),
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                    ),
                  ),
                );
              },
              autoplay: true,
              itemCount: provider.bannerList.length,
              control: null,
              duration: 200,
              scrollDirection: Axis.horizontal,

            ),
          ),
        ) : SizedBox.shrink();
      },
    );


  }
}

// viewportFraction: 0.9,
// scale: 1,
