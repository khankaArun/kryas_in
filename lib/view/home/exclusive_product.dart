
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/provider/HomeProvider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:provider/provider.dart';


class ExclusiveProduct extends StatefulWidget {

  String title;
  ExclusiveProduct({this.title});

  @override
  _ExclusiveProductState createState() => _ExclusiveProductState();
}

class _ExclusiveProductState extends State<ExclusiveProduct> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Consumer<HomeProvider>(
       builder: (context, provider, child) {
           return provider.exclusiveProduct.length != 0 ? Container(
             margin: EdgeInsets.only(top: 10),
             color: R.color.white_color,
            // height: SizeConfig.verticalBloc * 60,
             child: Column(
               mainAxisAlignment: MainAxisAlignment.start,
               crossAxisAlignment: CrossAxisAlignment.start,
               children: [
                   Container(
                     margin: EdgeInsets.only(top: SizeConfig.verticalBloc * 3.5, left: SizeConfig.verticalBloc * 2),
                     child: Text(widget.title,
                       style: TextStyle(
                           // fontSize: SizeConfig.normalFontSize,
                           fontSize:ResponsiveFlutter.of(context).fontSize(1.4),
                           fontWeight: FontWeight.w600,
                           color: R.color.black_color),
                     ),
                   ),

                 Container(
                   margin: EdgeInsets.only(top: SizeConfig.safeBlockHorizontal * 3, left: SizeConfig.safeBlockHorizontal * 2),
                   child: Row(
                     children: [
                       InkWell(
                         child: Container(
                           height: SizeConfig.verticalBloc * 25,
                           width: SizeConfig.horizontalBloc * 48,
                           decoration: BoxDecoration(
                             image: DecorationImage(
                               fit: BoxFit.fill,
                               image: NetworkImage(BannerBaseUrl.ExclusiveProduct+provider.exclusiveProduct[0].bannerImage.replaceAll(" ", "")),
                             ),
                             borderRadius: BorderRadius.all(Radius.circular(10.0)),
                           ),
                           child: Container(
                             padding: EdgeInsets.only(left: SizeConfig.safeBlockVertical * 2,
                                 right: SizeConfig.safeBlockVertical * 2,
                                 bottom: SizeConfig.safeBlockVertical * 3,
                                 top: SizeConfig.safeBlockVertical * 2),
                             child: Align(
                               alignment: Alignment.bottomCenter,
                               child: Column(
                                 crossAxisAlignment: CrossAxisAlignment.end,
                                 mainAxisAlignment: MainAxisAlignment.end,
                                 children: [
                                   Text(""+provider.exclusiveProduct[0].dashBoardTitle,maxLines: 1,
                                       textAlign: TextAlign.center, style: TextStyle(
                                       fontSize: ResponsiveFlutter.of(context).fontSize(1.45),
                                       fontWeight: FontWeight.w600,
                                       color: R.color.white_color),),
                                   provider.exclusiveProduct[0].description != null
                                   ? SizedBox(
                                     height: SizeConfig.safeBlockVertical * 0.5,
                                   ): SizedBox.shrink(),
                                   provider.exclusiveProduct[0].description != null
                                    ? Text(provider.exclusiveProduct[0].description,maxLines: 1,
                                     textAlign: TextAlign.center, style: TextStyle(
                                         fontSize: ResponsiveFlutter.of(context).fontSize(1.45),
                                         fontWeight: FontWeight.w600,
                                         color: R.color.white_color),)
                                       : SizedBox.shrink(),

                                 ],
                               ),
                             ),
                           ),
                         ),
                         onTap: () async{

                    /*       Common.showLoadingProgress(context);
                          var res= await    Provider.of<ProductListProvider>(context, listen: false).getProductList(
                               commandTypes: "",
                               filterBrandIds: "",
                               filterColorIds: "",
                               filterSizeIds: "",
                               categoryId: "0",
                               brandId: "0",
                               priceFrom: "0",
                               priceTo: "1000000",
                               sortBy: "",
                               dashBoardId: provider.exclusiveProduct[0].dashBoardId,
                              catIds : ""
                           );
                           Navigator.of(context).pop();

                           if(res==true){
                             Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductListPage(title: provider.exclusiveProduct[0].dashBoardTitle,)));

                           }

*/



                         },
                       ),

                       SizedBox(
                         width: SizeConfig.safeBlockHorizontal * 1,
                       ),

                       provider.exclusiveProduct.length >= 2
                        ? InkWell(
                         onTap: () async{

                      /*     Common.showLoadingProgress(context);
                        var res= await   Provider.of<ProductListProvider>(context, listen: false).getProductList(
                               commandTypes: "",
                               filterBrandIds: "",
                               filterColorIds: "",
                               filterSizeIds: "",
                               categoryId: "0",
                               brandId: "0",
                               priceFrom: "0",
                               priceTo: "1000000",
                               sortBy: "",
                               dashBoardId: provider.exclusiveProduct[1].dashBoardId,
                            catIds : ""
                           );


                           Navigator.of(context).pop();

                           if(res==true){
                             Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductListPage(title: provider.exclusiveProduct[1].dashBoardTitle,)));


                           }

*/

                         },
                          child: Container(
                           height: SizeConfig.verticalBloc * 25,
                           width: SizeConfig.horizontalBloc * 47,
                           decoration: BoxDecoration(
                             image: DecorationImage(
                               fit: BoxFit.fill,
                               image: NetworkImage(BannerBaseUrl.ExclusiveProduct+provider.exclusiveProduct[1].bannerImage.replaceAll(" ", "")),
                             ),
                             borderRadius: BorderRadius.all(Radius.circular(10.0)),
                           ),
                           child: Container(
                             padding: EdgeInsets.only(left: SizeConfig.safeBlockVertical * 2,
                                 right: SizeConfig.safeBlockVertical * 2,
                                 bottom: SizeConfig.safeBlockVertical * 3,
                                 top: SizeConfig.safeBlockVertical * 2),
                             child: Align(
                               alignment: Alignment.bottomCenter,
                               child: Column(
                                 crossAxisAlignment: CrossAxisAlignment.end,
                                 mainAxisAlignment: MainAxisAlignment.end,
                                 children: [
                                   Text(provider.exclusiveProduct[1].dashBoardTitle,maxLines: 1,
                                     textAlign: TextAlign.center, style: TextStyle(
                                         fontSize: ResponsiveFlutter.of(context).fontSize(1.45),
                                         fontWeight: FontWeight.w600,
                                         color: R.color.white_color),),
                                   provider.exclusiveProduct[1].description != null
                                       ? SizedBox(
                                     height: SizeConfig.safeBlockVertical * 0.5,
                                   ): SizedBox.shrink(),
                                   provider.exclusiveProduct[1].description != null
                                       ? Text(provider.exclusiveProduct[1].description,maxLines: 1,
                                     textAlign: TextAlign.center, style: TextStyle(
                                         fontSize: ResponsiveFlutter.of(context).fontSize(1.45),
                                         fontWeight: FontWeight.w600,
                                         color: R.color.white_color),)
                                       : SizedBox.shrink(),

                                 ],
                               ),
                             ),
                           ),
                       ),
                        ):SizedBox.shrink(),
                     ],
                   ),
                 ),


                 provider.exclusiveProduct.length >= 3
                  ? InkWell(
                   onTap: () async{
/*
                     Common.showLoadingProgress(context);
                   var res= await  Provider.of<ProductListProvider>(context, listen: false).getProductList(
                         commandTypes: "",
                         filterBrandIds: "",
                         filterColorIds: "",
                         filterSizeIds: "",
                         categoryId: "0",
                         brandId: "0",
                         priceFrom: "0",
                         priceTo: "1000000",
                         sortBy: "",
                         dashBoardId: provider.exclusiveProduct[2].dashBoardId,
                       catIds : ""
                     );

                     Navigator.of(context).pop();

                     if(res==true){
                       Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductListPage(title: provider.exclusiveProduct[2].dashBoardTitle,)));


                     }*/


                   },
                    child: Container(
                     margin: EdgeInsets.only(top: SizeConfig.safeBlockHorizontal * 1, left: SizeConfig.safeBlockHorizontal * 2),
                     child: Row(
                       children: [
                         Container(
                           height: SizeConfig.verticalBloc * 25,
                           width: SizeConfig.horizontalBloc * 48,
                           decoration: BoxDecoration(
                             image: DecorationImage(
                               fit: BoxFit.fill,
                               image: NetworkImage(BannerBaseUrl.ExclusiveProduct+provider.exclusiveProduct[2].bannerImage.replaceAll(" ", "")),
                             ),
                             borderRadius: BorderRadius.all(Radius.circular(10.0)),
                           ),
                           child: Container(
                             padding: EdgeInsets.only(left: SizeConfig.safeBlockVertical * 2,
                                 right: SizeConfig.safeBlockVertical * 2,
                                 bottom: SizeConfig.safeBlockVertical * 3,
                                 top: SizeConfig.safeBlockVertical * 2),
                             child: Align(
                               alignment: Alignment.bottomCenter,
                               child: Column(
                                 crossAxisAlignment: CrossAxisAlignment.end,
                                 mainAxisAlignment: MainAxisAlignment.end,
                                 children: [
                                   Text(provider.exclusiveProduct[2].dashBoardTitle,maxLines: 1,
                                     textAlign: TextAlign.center, style: TextStyle(
                                         fontSize: ResponsiveFlutter.of(context).fontSize(1.45),
                                         fontWeight: FontWeight.w600,
                                         color: R.color.white_color),
                                   ),


                                   provider.exclusiveProduct[2].description != null
                                       ? SizedBox(
                                     height: SizeConfig.safeBlockVertical * 0.5,
                                   ): SizedBox.shrink(),


                                   provider.exclusiveProduct[2].description != null
                                       ? Text(provider.exclusiveProduct[2].description,maxLines: 1,
                                     textAlign: TextAlign.center, style: TextStyle(
                                         fontSize: ResponsiveFlutter.of(context).fontSize(1.45),
                                         fontWeight: FontWeight.w600,
                                         color: R.color.white_color),)
                                       : SizedBox.shrink(),

                                 ],
                               ),
                             ),
                           ),
                         ),

                         SizedBox(
                           width: SizeConfig.safeBlockHorizontal * 1,
                         ),

                         provider.exclusiveProduct.length >= 4
                          ? InkWell(
                           onTap: () async{
/*

                             Common.showLoadingProgress(context);
                           var res= await  Provider.of<ProductListProvider>(context, listen: false).getProductList(
                                 commandTypes: "",
                                 filterBrandIds: "",
                                 filterColorIds: "",
                                 filterSizeIds: "",
                                 categoryId: "0",
                                 brandId: "0",
                                 priceFrom: "0",
                                 priceTo: "1000000",
                                 sortBy: "",
                                 dashBoardId: provider.exclusiveProduct[3].dashBoardId
                             );


                             Navigator.of(context).pop();
                             if(res==true){
                               Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductListPage(title: provider.exclusiveProduct[3].dashBoardTitle,)));

                             }else{
                               Common.showToast("Data not found");
                             }

*/



                           },
                            child: Container(
                             height: SizeConfig.verticalBloc * 25,
                             width: SizeConfig.horizontalBloc * 47,
                             decoration: BoxDecoration(
                               image: DecorationImage(
                                 fit: BoxFit.fill,
                                 image: NetworkImage(BannerBaseUrl.ExclusiveProduct+provider.exclusiveProduct[3].bannerImage.replaceAll(" ", "")),
                               ),
                               borderRadius: BorderRadius.all(Radius.circular(10.0)),
                             ),
                             child: Container(
                               padding: EdgeInsets.only(left: SizeConfig.safeBlockVertical * 2,
                                   right: SizeConfig.safeBlockVertical * 2,
                                   bottom: SizeConfig.safeBlockVertical * 3,
                                   top: SizeConfig.safeBlockVertical * 2),
                               child: Align(
                                 alignment: Alignment.bottomCenter,
                                 child: Column(
                                   crossAxisAlignment: CrossAxisAlignment.end,
                                   mainAxisAlignment: MainAxisAlignment.end,
                                   children: [
                                     Text(provider.exclusiveProduct[3].dashBoardTitle,maxLines: 1,
                                       textAlign: TextAlign.center, style: TextStyle(
                                           fontSize: ResponsiveFlutter.of(context).fontSize(1.45),
                                           fontWeight: FontWeight.w600,
                                           color: R.color.white_color),),
                                     provider.exclusiveProduct[3].description != null
                                         ? SizedBox(
                                       height: SizeConfig.safeBlockVertical * 0.5,
                                     ): SizedBox.shrink(),
                                     provider.exclusiveProduct[3].description != null
                                         ? Text(provider.exclusiveProduct[3].description,maxLines: 1,
                                       textAlign: TextAlign.center, style: TextStyle(
                                           fontSize: ResponsiveFlutter.of(context).fontSize(1.45),
                                           fontWeight: FontWeight.w600,
                                           color: R.color.white_color),)
                                         : SizedBox.shrink(),
                                   ],
                                 ),
                               ),
                             ),
                         ),
                          ):SizedBox.shrink(),
                       ],
                     ),
                 ),
                  ) :SizedBox.shrink(),

               ],
             ),
           ) : SizedBox.shrink();
     });
  }
}
