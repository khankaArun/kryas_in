
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:krays_in/model/menu_model.dart';
import 'package:krays_in/provider/HomeProvider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:provider/provider.dart';
import 'package:readmore/readmore.dart';



class TopSliderCategory extends StatefulWidget {
  @override
  _TopSliderCategoryState createState() => _TopSliderCategoryState();
}

class _TopSliderCategoryState extends State<TopSliderCategory> {
  bool isPressed = false;

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeProvider>(
        builder: (context, provider, child) {
          return SingleChildScrollView(
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: 10,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) => Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 10,right: 10,top:0,bottom: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            new Container(
                              height: 48.0,
                              width: 48.0,
                              decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage(
                                        "https://pbs.twimg.com/profile_images/916384996092448768/PF1TSFOE_400x400.jpg")),
                              ),
                            ),
                            new SizedBox(
                              width: 10.0,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              //mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                new Text(
                                  "Julie Martin ",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                new Text(
                                  "5 hour ago",
                                  style: TextStyle(
                                    color: Color(0xff9d9b9b),
                                    fontSize: 12,
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        CircleAvatar(
                          radius: 20,
                          backgroundColor: Colors.black12,
                          child: new IconButton(
                            icon: Icon(Icons.more_horiz),
                            onPressed: null,
                          ),
                        )
                      ],
                    ),
                  ),


                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(

                      decoration: BoxDecoration(
                        color:Colors.white ,
                          border: Border.all(
                            color: Colors.white,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                      child: Stack(
                        children: [
                          Flexible(
                            fit: FlexFit.loose,

                            child: ClipRRect(
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
                                child :new Image.network(
                              "https://images.pexels.com/photos/672657/pexels-photo-672657.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
                              fit: BoxFit.cover,

                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 16.0,right: 16,top: 290 ,bottom: 10),

                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [

                                Text(
                                    "Tranding Fashion style ",textAlign: TextAlign.left ,style: TextStyle(fontWeight: FontWeight. bold),


                                ),



                                Row(
                                  children: [
                                    Text(

                                      "Good things take time... ",textAlign: TextAlign.left,


                                    ),
                                    Text(

                                      "Read more  ",textAlign: TextAlign.left,style:TextStyle(
                                      color: Colors.red,
                                    ),


                                    ),
                                  ],
                                ),
                              ],
                            ),

                          ),
                        ],
                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new Icon(Icons.favorite_rounded,color: Colors.red, size: 20,),

                            SizedBox(width: 2),
                            Text(
                              "10k",
                              style: TextStyle(
                                color: Color(0xff2c2b2b),
                                fontSize: 14,
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(width: 10),
                            new Icon(Icons.chat_bubble_outline, size: 20,),

                            SizedBox(width: 2),
                            Text(
                              "5k",
                              style: TextStyle(
                                color: Color(0xff2c2b2b),
                                fontSize: 14,
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(width: 10),

                        Transform.rotate(
                          angle: 90 * pi / 180,
                          child: new Icon(Icons.reply, size: 20,),),

                            SizedBox(width: 2),
                            Text(
                              "1k",
                              style: TextStyle(
                                color: Color(0xff2c2b2b),
                                fontSize: 14,
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(width: 6),
                            Transform.rotate(
                              angle: 90 * pi / 180,
                              child: new Icon(Icons.alarm_add, size: 20,),),

                            SizedBox(width: 2),
                            Text(
                              "1k",
                              style: TextStyle(
                                color: Color(0xff2c2b2b),
                                fontSize: 14,
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(width: 6),



                          ],
                        ),
                        new Icon(Icons.bookmark_outline_sharp, size: 20,),
                      ],
                    ),
                  ),


                ],
              ),
            ),
          );;
        });

  }

}
