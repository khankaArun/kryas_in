
import 'dart:ffi';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:freshchat_sdk/freshchat_sdk.dart';
import 'package:freshchat_sdk/freshchat_user.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/provider/CartProvider.dart';
import 'package:krays_in/provider/HomeProvider.dart';
import 'package:krays_in/provider/MarketProvider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/preferences_until.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/view/home/media/media_main_page.dart';
import 'package:krays_in/view/home/media/shop_street_main_page.dart';
import 'package:krays_in/view/user_profile/user_profile_home.dart';
import 'package:provider/provider.dart';

import '../../util/string_confi.dart';
import 'media/fresh_desk_home_page.dart';
import 'dart:io';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin<HomePage> {

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();



  int _currentIndex = 0;
  GlobalKey _bottomNavigationKey = GlobalKey();
  @override
  void initState() {
    // TODO: implement initState
    //var provider = Provider.of<HomeProvider>(context, listen: false);
   // myDataLoad();
    super.initState();
    AppUtil.sendEventToGoogleAnalytics("User At Home ","HomePage");
    saveFcmToken();
  }


  Future<Null> _refresh() async {
    //Holding pull to refresh loader widget for 2 sec.
    //You can fetch data from server.
    await new Future.delayed(const Duration(seconds: 3));

    var homeProvider     =  Provider.of<MarketProvider>(context, listen: false).getHomeData();
    var cartProvider =      Provider.of<CartProvider>(context,listen: false).getUserCart();

    return null;
  }


  Future<void> saveFcmToken() async {

    try {
       final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

      if (Platform.isAndroid) {
          AndroidDeviceInfo info = await deviceInfoPlugin.androidInfo;
         // print(info.version);
         // print(info.board);
        //  print(info.bootloader);
         // print(info.brand);
        //  print(info.device);
        //  print(info.model);




          var fcmToken=  await PreferenceUtils.getString(
              PreferencesKey.FCM_TOKEN, "");
          Provider.of<AuthProvider>(context, listen: false).userFcmToken(
            fcmToken: fcmToken,
            brand: info.brand,
            model: info.model,
          );


        }
    }catch(error) {

    }




  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        backgroundColor: R.color.grey_background,

        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          key: _bottomNavigationKey,
          currentIndex: _currentIndex,

          selectedItemColor: R.color.app_color,
          selectedFontSize: 0,
          unselectedFontSize: 0,
          iconSize: 20,
          elevation: 3,
          selectedIconTheme: IconThemeData(size: 20),
          unselectedItemColor: Theme.of(context).focusColor.withOpacity(1),
          selectedLabelStyle: Theme.of(context).textTheme.bodyText1.merge(TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.4), fontFamily: 'ralewayMedium', fontWeight: FontWeight.w700)),
          unselectedLabelStyle: Theme.of(context).textTheme.button.merge(TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.4), fontFamily: 'ralewayMedium', fontWeight: FontWeight.w700)),
          showUnselectedLabels: true,

          onTap: (int index) {
            setState(() {
              print(index);

              if(index==1){
                myChatSdk();
                //_incrementCounter();
             //  AppUtil.showUploadImageFeatures(context);
              }else{
                _currentIndex = index;
              }



            });
          },


          //currentIndex: 0, //
          items: allDestinations.map((Destination destination) {
            return BottomNavigationBarItem(
                icon: Icon(destination.icon),
                backgroundColor: destination.color,
                label: destination.title
            );
          }).toList(),
        ),

        body: RefreshIndicator(
           onRefresh: _refresh,

          key: _refreshIndicatorKey,

          child: SafeArea(

            child: bodyFunction(),
          )),
        );
  }


  Widget bodyFunction() {
    switch (_currentIndex) {
      case 0:
        return ShopStreetMainPage();
        break;
      case 1:

        return MyFreshWorkChat();/*MediaMainPage()*/;
        break;
      case 2:

        return ProfilePage();
        break;

      default:

        return ShopStreetMainPage();
        break;
    }
  }


  void myChatSdk(){


    Freshchat.init("af5107d4-1f10-4c43-beed-e08e4e0d718c",
      "d1e197f9-bc9e-40b4-bd1c-15dc5403f0ac", "msdk.in.freshchat.com",
      stringsBundle: "FCCustomLocalizable",
      themeName: "CustomTheme.plist",
      teamMemberInfoVisible:true,
      cameraCaptureEnabled:true,
      gallerySelectionEnabled:true,
      responseExpectationEnabled:true,
      userEventsTrackingEnabled: true,
      errorLogsEnabled: true

    );

    List<String> myName =[];
    myName.add("Test 123");



    Freshchat.showConversations(filteredViewTitle: "Krays.in support",tags: myName);
    /*FreshchatUser freshchatUser;
    freshchatUser.setFirstName("John");
    freshchatUser.setLastName("Doe");
    freshchatUser.setEmail("johndoe@dead.man");
    freshchatUser.setPhone("+91","1234234123");
    Freshchat.setUser(freshchatUser);*/
    //Freshchat.showConversations();






    /*  Freshchat.init("3949b67a-8673-4d65-8eef-46d49ee1a917",
      "2912ca71-867c-4fd1-b199-d6a4b161826c", "msdk.in.freshchat.com",
      stringsBundle: "FCCustomLocalizable",
      themeName: "CustomTheme.plist",
      teamMemberInfoVisible:true,
      cameraCaptureEnabled:true,
      gallerySelectionEnabled:true,
      responseExpectationEnabled:true,

    );
    */


    /**
     * This is the Firebase push notification server key for this sample app.
     * Please save this in your Freshchat account to test push notifications in Sample app.
     *
     * Server key: Please refer support documentation for the server key of this sample app.
     *
     * Note: This is the push notification server key for sample app. You need to use your own server key for testing in your application
     */
    /*   var restoreStream = Freshchat.onRestoreIdGenerated;
    var restoreStreamSubsctiption = restoreStream.listen((event) {
      print("Restore ID Generated: $event");
      notifyRestoreId(event);
    });
    if (Platform.isAndroid) {
      registerFcmToken();
      FirebaseMessaging.instance.onTokenRefresh.listen(
          Freshchat.setPushRegistrationToken);
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        var data = message.data;
        handleFreshchatNotification(data);
        print("Notification Content: $data");
      });
      FirebaseMessaging.onBackgroundMessage(myBackgroundMessageHandler);
    }*/
    // Navigator.of(context).pop();

  }



 /*Future<void>myDataLoad () async {
    print("test");
   var provider =await  Provider.of<MarketProvider>(context, listen: false);
 }*/



}





class Destination {
  const Destination(this.title, this.icon, this.color);
  final String title;
  final IconData icon;
  final MaterialColor color;


}
const List<Destination> allDestinations = <Destination>[
//  Destination('Home',Icons.home, Colors.teal),
  Destination('Market', Icons.shop_sharp, Colors.cyan),
 // Destination('Post', Icons.add_circle_outline_outlined, Colors.orange),
  Destination('DM', Icons.chat, Colors.indigo),
  Destination('Profile', Icons.person_sharp, Colors.blue)
];





