
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/model/brand_model.dart';
import 'package:krays_in/model/market_model.dart';
import 'package:krays_in/provider/HomeProvider.dart';
import 'package:krays_in/provider/MarketProvider.dart';
import 'package:krays_in/provider/ProductListingProvider.dart';
import 'package:krays_in/provider/product_detail_provider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/images.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/view/market/market_listing_page.dart';
import 'package:krays_in/view/market/product_detail_page.dart';
import 'package:provider/provider.dart';

class MarketHomePageList extends StatefulWidget {
  @override
  _HomeBrandListState createState() => _HomeBrandListState();
}

class _HomeBrandListState extends State<MarketHomePageList> {

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final orientation = MediaQuery.of(context).orientation;
    return Consumer<MarketProvider>(
        builder: (context, provider, child) {
          return Container(

            color: R.color.white_color,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    margin: EdgeInsets.only(
                        top    : SizeConfig.verticalBloc * 2,
                        left   : SizeConfig.verticalBloc * 2,
                        bottom : SizeConfig.verticalBloc * 0,
                        right  : SizeConfig.verticalBloc * 2
                    ),
                    child: Row(
                      children: [
                        Expanded(child: Text(""
                            "Top Selling",
                          style: TextStyle(
                              fontFamily: 'openSans',
                              // fontSize: SizeConfig.normalFontSize,
                              fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                              fontWeight: FontWeight.w600,
                              color: R.color.black_color),
                        ),),


                        SizedBox(
                          height: SizeConfig.safeBlockVertical * 3.4,
                          child: ElevatedButton(
                            style: ButtonStyle(
                                foregroundColor: MaterialStateProperty.all<Color>(R.color.white_color),
                                backgroundColor: MaterialStateProperty.all<Color>(R.color.app_color),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18.0),
                                        side: BorderSide(color: R.color.app_color)
                                    )
                                )
                            ),
                            onPressed: () async{
                              AppUtil.showLoadingProgress(context);
                              var myProductList =await  Provider.of<ProductListingProvider>(context, listen: false).getProductList(context: context,
                                  categoryId: provider.homeDataModel.data.topSelling[0].category,
                                  subCategoryId: provider.homeDataModel.data.topSelling[0].subcategory,
                                count: 100,
                                page: 1,
                                search: ""

                              );
                              Navigator.of(context).pop();

                              if(myProductList){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage(myTitle: "Top Selling",
                                isFruits: false,
                                isVegetables: false,
                                isOthers: true,
                                  id:  provider.homeDataModel.data.topSelling[0].category,
                                  subCatId:provider.homeDataModel.data.topSelling[0].subcategory,)));
                              }




                            },
                            child: Text("View All".toUpperCase(), style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.1), fontWeight: FontWeight.w600),),
                          ),
                        )
                      ],
                    )
                ),

                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: provider.homeDataModel.data.topSelling.length,
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 2,
                          childAspectRatio:  MediaQuery.of(context).size.width /
                              (MediaQuery.of(context).size.height / 1.6)),
                      itemBuilder: (context, index) {
                        return _buildItem(1,provider.homeDataModel, index);
                      }),
                ),

                /*fresh fruit*/



                Container(
                    margin: EdgeInsets.only(
                        top    : SizeConfig.verticalBloc * 2,
                        left   : SizeConfig.verticalBloc * 2,
                        bottom : SizeConfig.verticalBloc * 0,
                        right  : SizeConfig.verticalBloc * 2
                    ),
                    child: Row(
                      children: [
                        Expanded(child: Text("Fresh Fruits",
                          style: TextStyle(
                              fontFamily: 'openSans',
                              // fontSize: SizeConfig.normalFontSize,
                              fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                              fontWeight: FontWeight.w600,
                              color: R.color.black_color),
                        ),),


                        SizedBox(
                          height: SizeConfig.safeBlockVertical * 3.4,
                          child: ElevatedButton(
                            style: ButtonStyle(
                                foregroundColor: MaterialStateProperty.all<Color>(R.color.white_color),
                                backgroundColor: MaterialStateProperty.all<Color>(R.color.app_color),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18.0),
                                        side: BorderSide(color: R.color.app_color)
                                    )
                                )
                            ),
                            onPressed: () async{

                              AppUtil.showLoadingProgress(context);
                              var myProductList =await  Provider.of<ProductListingProvider>
                                (context, listen: false).getProductList(context: context,
                                  categoryId: provider.homeDataModel.data.freshFruits[0].category,
                                  subCategoryId: provider.homeDataModel.data.freshFruits[0].subcategory,
                                  count: 100,
                                  page: 1,
                                  search: ""

                              );
                              Navigator.of(context).pop();

                              if(myProductList){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage(myTitle: "Fresh Fruits",
                                    isFruits: true,
                                    isVegetables: false,
                                    isOthers: false,
                                    id:  provider.homeDataModel.data.freshFruits[0].category,
                                    subCatId:provider.homeDataModel.data.freshFruits[0].subcategory,)));

                              }



                            },
                            child: Text("View All".toUpperCase(), style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.1), fontWeight: FontWeight.w600),),
                          ),
                        )
                      ],
                    )
                ),

                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: provider.homeDataModel.data.freshFruits.length,
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 2,
                          childAspectRatio:  MediaQuery.of(context).size.width /
                              (MediaQuery.of(context).size.height / 1.6)),
                      itemBuilder: (context, index) {
                        return _buildItem(2,provider.homeDataModel, index);
                      }),
                ),


                /*Fresh veg*/


                Container(
                    margin: EdgeInsets.only(
                        top    : SizeConfig.verticalBloc * 2,
                        left   : SizeConfig.verticalBloc * 2,
                        bottom : SizeConfig.verticalBloc * 0,
                        right  : SizeConfig.verticalBloc * 2
                    ),
                    child: Row(
                      children: [
                        Expanded(child: Text("Fresh Vegetables",
                          style: TextStyle(
                              fontFamily: 'openSans',
                              // fontSize: SizeConfig.normalFontSize,
                              fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                              fontWeight: FontWeight.w600,
                              color: R.color.black_color),
                        ),),


                        SizedBox(
                          height: SizeConfig.safeBlockVertical * 3.4,
                          child: ElevatedButton(
                            style: ButtonStyle(
                                foregroundColor: MaterialStateProperty.all<Color>(R.color.white_color),
                                backgroundColor: MaterialStateProperty.all<Color>(R.color.app_color),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18.0),
                                        side: BorderSide(color: R.color.app_color)
                                    )
                                )
                            ),
                            onPressed: () async{

                              AppUtil.showLoadingProgress(context);
                              var myProductList =await  Provider.of<ProductListingProvider>(context, listen: false).getProductList(context: context,
                                  categoryId: provider.homeDataModel.data.freshVeggies[0].category,
                                  subCategoryId: provider.homeDataModel.data.freshVeggies[0].subcategory,
                                  count: 100,
                                  page: 1,
                                  search: ""

                              );
                              Navigator.of(context).pop();

                              if(myProductList){
                                //Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage()));

                                if(myProductList){
                                  // Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage()));


                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage(myTitle: "Fresh Veggies",
                                    isFruits: false,
                                    isVegetables: true,
                                    isOthers: false,
                                    id:  provider.homeDataModel.data.freshFruits[0].category,
                                    subCatId:provider.homeDataModel.data.freshFruits[0].subcategory,)));




                                }
                              }


                            },
                            child: Text("View All".toUpperCase(), style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.1), fontWeight: FontWeight.w600),),
                          ),
                        )
                      ],
                    )
                ),

                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: provider.homeDataModel.data.freshVeggies.length,
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 2,
                          childAspectRatio:  MediaQuery.of(context).size.width /
                              (MediaQuery.of(context).size.height / 1.6)),
                      itemBuilder: (context, index) {
                        return    _buildItem(3,provider.homeDataModel, index);
                      }),
                ),


              ],
            ),
          );
        });
  }

  Widget _buildItem( int whereToCome, MarketHomeItemModel homeItemModel,int index){

    var myList=null;
    var passedId;

    if(whereToCome==1){
      myList= homeItemModel.data.topSelling;
      passedId=myList[index].id;
    }else if(whereToCome==2){
      myList= homeItemModel.data.freshFruits;
      passedId=myList[index].id;
    }else if(whereToCome==3){
      myList= homeItemModel.data.freshVeggies;
      passedId=myList[index].id;
    }


    return InkWell(
      onTap: () async{
        AppUtil.showLoadingProgress(context);
        var res = await Provider.of<ProductDetailProvider>(context,listen: false)
            .getProductDetail(passedId,context);
        Navigator.of(context).pop();

        if(res){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ProductDetailPage(passedId)),
          );
        }



      },
      child:Card(
        elevation: 20,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child:  Stack(

          children: [


            Container(
              padding: EdgeInsets.only(left: 10,right: 10, top:30),

              decoration:  BoxDecoration(
                color: Colors.white,

              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: SizeConfig.verticalBloc * 17,
                    width: SizeConfig.horizontalBloc * 30,
                    child:


                    Container(
                      child:  Images.buildImageListingPage(myList[index].image.replaceAll(" ", "%20")),
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.horizontalBloc * 2,
                  ),

                  Container(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        Expanded(
                          flex: 1,
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                               /* Center(
                                  child: Container(
                                    // width: 120,
                                    decoration: BoxDecoration(
                                        color: R.color.app_color,
                                        borderRadius: BorderRadius.all( Radius.circular(3.0))
                                    ),
                                    child: Center(
                                      child: Padding(
                                        padding: EdgeInsets.all(3),
                                        child: Text("Item out of stock",
                                          style: TextStyle(color: R.color.white_color,
                                              fontWeight: FontWeight.w800,
                                              fontSize: ResponsiveFlutter.of(context).fontSize(1.5)),),
                                      ),
                                    ),
                                    height: SizeConfig.verticalBloc * 3,
                                    //width: 60,
                                  ),
                                ),*/


                                Text(myList[index].name,
                                  maxLines: 1,
                                  textAlign: TextAlign.left, style: TextStyle(
                                      fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
                                      fontWeight: FontWeight.w700,
                                      //letterSpacing: 1.3,
                                      color: R.color.black_color),),

                                SizedBox(width: 5,),



                                SizedBox(
                                  height: ResponsiveFlutter.of(context).hp(0.15),
                                ),

                                Container(
                                  // margin: EdgeInsets.only(left: 10.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(R.priceSymbol.rupee+myList[index].salePrice+"/"+myList[index].unit , style: TextStyle(
                                          fontSize: ResponsiveFlutter.of(context).fontSize(1.8),

                                          fontWeight: FontWeight.w700,
                                          color: R.color.black_color),),

                                      SizedBox(
                                        width: ResponsiveFlutter.of(context).wp(1),
                                      ),

                                      Text(R.priceSymbol.rupee+myList[index].price, style: TextStyle(
                                          fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                                          fontWeight: FontWeight.w500,

                                          decoration: TextDecoration.lineThrough,
                                          decorationThickness: 1.85,
                                          color: R.color.grey_color),),


                                    ],
                                  ),
                                ),

                              /*  Text(R.priceSymbol.rupee+myList[index].+"/"+myList[index].unit,
                                  maxLines: 1,
                                  textAlign: TextAlign.left, style: TextStyle(
                                      fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
                                      fontWeight: FontWeight.w600,
                                      color: R.color.grey_color),),*/
                                SizedBox(
                                  height: ResponsiveFlutter.of(context).hp(0.15),
                                ),

                           /*     Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(R.priceSymbol.rupee+"100", style: TextStyle(
                                        fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
                                        fontWeight: FontWeight.w600,
                                        color: R.color.black_color),),

                                    SizedBox(
                                      width: SizeConfig.verticalBloc * 1,
                                    ),

                                    Text(R.priceSymbol.rupee+"100", style: TextStyle(
                                        fontSize: ResponsiveFlutter.of(context).fontSize(1.4),
                                        fontWeight: FontWeight.w500,
                                        decoration: TextDecoration.lineThrough,
                                        decorationThickness: 1.85,
                                        color: R.color.grey_color),),

                                    SizedBox(
                                      width: SizeConfig.verticalBloc * 1,
                                    ),
                                    Text(*//*productListModel.discountPercent.toString()*//*"20%"+R.priceSymbol.off, style: TextStyle(
                                        fontSize: ResponsiveFlutter.of(context).fontSize(1.4  ),
                                        fontFamily: "openSans",
                                        fontWeight: FontWeight.w500,
                                        color: R.color.app_color),),
                                  ],


                                ),*/


                              ],
                            ),
                          ),
                        ),

                      /*  SizedBox(
                          height: ResponsiveFlutter.of(context).hp(3),
                          width: ResponsiveFlutter.of(context).hp(3),
                          child: InkWell(
                            onTap: () async {
                              *//*    var auth =  await Provider.of<AuthProvider>(context,listen: false);

                        if(auth.userModel.customerID.toString() == "0"){
                          // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>LoginPage()));
                          LoginBottom.show(context);
                          return;
                        }

                        ImagesItems _imageItem = new ImagesItems(
                            imageName:productListModel.imageUrl.length != 0 ?
                            BannerBaseUrl.CatgegoryThumbImage+productListModel.imageUrl.toString().replaceAll(" ", "%20")
                                : ""
                        );

                        List<ImagesItems> _imageItemList = [];
                        _imageItemList.add(_imageItem);

                        ProductDetailModel _productDetailModel =  new ProductDetailModel(
                          productID: productListModel.productId,
                          matrixID: productListModel.matrixId,
                          imagesItems:_imageItemList,
                          productName: productListModel.productName,
                          description:productListModel.description,
                        );
                        //productListModel.productName
                        var res =  Provider.of<WishlistProvider>(context, listen: false).
                        saveWishlistItem(context, 1, _productDetailModel,0);

                        provider.setWishList(index, true);*//*

                            },
                            child: Image.asset(R.images.whislist_icon, color:
                            myList[index].isWishlist=='0'?R.color.grey: R.color.app_color),
                          ),
                        ),*/

                      ],
                    ),
                  ),

                ],
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              child: Container(
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(5.0)),
                ),
                child: Text(
                  ""+myList[index].disPer  +R.priceSymbol.off,
                  style: TextStyle(color: Colors.white, fontSize: 12.0,fontFamily: 'Jost',
                    letterSpacing: 0.8,),
                ),
              ),
            ),
            Positioned(
              bottom  : 0,
              right   : 0,
              child: Container(
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  color: myList[index].isCart=='0'?Colors.grey:Colors.blue,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(5.0)),
                ),
                child: SizedBox(
                  height: 20,
                  child:   Icon(Icons.shopping_cart_outlined,
                      size: 22.0, color: Colors.white),
                ),
              ),
            ),
          ],
        )



      ),
    );
  }
}
