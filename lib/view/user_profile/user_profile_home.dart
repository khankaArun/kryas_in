
import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/provider/order_provider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/preferences_until.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:krays_in/view/common_widgets/common_app_bar.dart';
import 'package:krays_in/view/loginScreens/login_page.dart';
import 'package:krays_in/view/loginScreens/reset_password.dart';
import 'package:krays_in/view/market/cart_page.dart';
import 'package:krays_in/view/market/customer_ordered_list.dart';
import 'package:krays_in/view/market/shipping_address.dart';
import 'package:open_mail_app/open_mail_app.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';


class ProfilePage extends StatefulWidget {

  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.grey_background,
      appBar: MyFancyAppBar(),
      body: Consumer<AuthProvider>(
          builder: (context, provider, child) {
            return Column(
              children: <Widget>[
                new Container(
                  width: double.maxFinite,

                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    //  color: R.color.app_color,
                    // borderRadius: BorderRadius.only(bottomLeft: Radius.circular(25),bottomRight: Radius.circular(25)),
                  ),

                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[


                        //SizedBox(height: 20,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Padding(padding: new EdgeInsets.only(
                                left: 20, top: 10),
                              child: Text(provider.userModel.user.name,
                                textAlign: TextAlign.left, style:
                                TextStyle(fontSize: 18,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),),
                            ),
                            new Padding(padding: new EdgeInsets.only(
                                left: 20, top: 3),
                              child: Text(
                                provider.userModel.user.phone == null ? ""
                                    : /*provider.userAddressListModelItem.address[0].email+ "\n"+*/ provider
                                    .userModel.user.phone,
                                style: TextStyle(fontSize: 14,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w700),),
                            ),
                          ],
                        ),
                        SizedBox(height: 20,),
                      ],
                    ),
                  ),

                ),

                new Expanded(
                    child: new SingleChildScrollView(
                      child: Container(
                        height: double.maxFinite,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        padding: new EdgeInsets.only(
                          //  top: 20,
                            right: 0.0,
                            left: 0.0),
                        color: Colors.grey.shade900,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[


                            Container(
                              padding: const EdgeInsets.only(
                                  bottom: 15.0, top: 5),
                              color: Colors.white,
                              child: Column(
                                children: [
                                  Row(
                                    //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center,
                                    children: <Widget>[

                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 15.0, top: 5),
                                        child: Icon(
                                          Icons.account_circle_outlined,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          'My Account',
                                          style: TextStyle(
                                              fontFamily: 'openSans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.2,
                                              color: Colors.black,
                                              fontSize: SizeConfig
                                                  .normalFontSize
                                          ),
                                        ),
                                      ),


                                    ],
                                  ),


                                  InkWell(
                                    onTap: () async {
                                      // var  provider =  await Provider.of<AuthProvider>(context, listen: false);
                                      Navigator.push(context, PageTransition(
                                          type: PageTransitionType.rightToLeft,
                                          child: ShippingAddressPage(
                                              true, "User Profile")));
                                      // showBottomSheetApplyCoupon(context);
                                    },
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceBetween,
                                        children: [
                                          Text(
                                            "           See your profile details",
                                            style: TextStyle(
                                                fontSize: SizeConfig
                                                    .smallFontSize,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "openSans"),),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15.0,),
                                            child: Icon(
                                              Icons.navigate_next,
                                              color: Colors.grey,
                                              size: 15,
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  )


                                ],
                              ),
                            ),
                            Divider(height: 0.5, color: Colors.grey.shade200,),

                            Container(
                              padding: const EdgeInsets.only(
                                  bottom: 15.0, top: 5),
                              color: Colors.white,
                              child: Column(
                                children: [
                                  Row(
                                    //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center,
                                    children: <Widget>[

                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 15.0, top: 5),
                                        child: Icon(
                                          Icons.recent_actors_outlined,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          'Change Password',
                                          style: TextStyle(
                                              fontFamily: 'openSans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.2,
                                              color: Colors.black,
                                              fontSize: SizeConfig
                                                  .normalFontSize
                                          ),
                                        ),
                                      ),


                                    ],
                                  ),


                                  InkWell(
                                    onTap: () async {
                                      Navigator.push(context, MaterialPageRoute(
                                          builder: (context) =>
                                              ResetPassword()));

                                      /*   AppUtil.showLoadingProgress(context);
                                          var res =  await Provider.of<AuthProvider>(context, listen: false)
                                              .getOtp(mobileNo: provider.customerInfoModel.mobileNo.toString().trim());

                                          print(provider.customerInfoModel.mobileNo.toString().trim());
                                          Navigator.of(context).pop();

                                          bool numberFound = res.toString().contains(new RegExp(r'[0-9]'));

                                          if(numberFound){
                                            LoginBottom.showOtp(context,provider.customerInfoModel.mobileNo,R.string.from__change_password,res);

                                          }else{
                                            Fluttertoast.showToast(
                                                msg: "Error while changing your password",
                                                toastLength: Toast.LENGTH_LONG,
                                                gravity: ToastGravity.BOTTOM,
                                                backgroundColor: R.color.errorColor,
                                                textColor: Colors.white,
                                                fontSize: 16.0);
                                          }
                                        */

                                    },
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceBetween,
                                        children: [
                                          Text("           Change password ",
                                            style: TextStyle(
                                                fontSize: SizeConfig
                                                    .smallFontSize,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "openSans"),),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15.0,),
                                            child: Icon(
                                              Icons.navigate_next,
                                              color: Colors.grey,
                                              size: 15,
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  )


                                ],
                              ),
                            ),
                            Divider(height: 0.5, color: Colors.grey.shade200,),

                            Container(
                              padding: const EdgeInsets.only(
                                  bottom: 15.0, top: 5),
                              color: Colors.white,
                              child: Column(
                                children: [
                                  Row(
                                    //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center,
                                    children: <Widget>[

                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 15.0, top: 5),
                                        child: Icon(
                                          Icons.shopping_bag_outlined,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          'Orders',
                                          style: TextStyle(
                                              fontFamily: 'openSans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.2,
                                              color: Colors.black,
                                              fontSize: SizeConfig
                                                  .normalFontSize
                                          ),
                                        ),
                                      ),


                                    ],
                                  ),


                                  InkWell(
                                    onTap: () async {
                                      AppUtil.showLoadingProgress(context);
                                      var res = await Provider.of<
                                          OrderProvider>(context, listen: false)
                                          .getOrderList(context);
                                      Navigator.pop(context);

                                      if (res) {
                                        Navigator.push(context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    OrderPage()));
                                      } else {
                                        AppUtil.showToast("No record found");
                                      }


                                      // showBottomSheetApplyCoupon(context);
                                    },
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceBetween,
                                        children: [
                                          Text(
                                            "           Check your order status ",
                                            style: TextStyle(
                                                fontSize: SizeConfig
                                                    .smallFontSize,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "openSans"),),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15.0,),
                                            child: Icon(
                                              Icons.navigate_next,
                                              color: Colors.grey,
                                              size: 15,
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  )


                                ],
                              ),
                            ),
                            Divider(height: 0.5, color: Colors.grey.shade200,),


                            Container(
                              padding: const EdgeInsets.only(
                                  bottom: 15.0, top: 5),
                              color: Colors.white,
                              child: Column(
                                children: [
                                  Row(
                                    //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center,
                                    children: <Widget>[

                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 15.0, top: 5),
                                        child: Icon(
                                          Icons.shopping_cart_outlined,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          'Bag',
                                          style: TextStyle(
                                              fontFamily: 'openSans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.2,
                                              color: Colors.black,
                                              fontSize: SizeConfig
                                                  .normalFontSize
                                          ),
                                        ),
                                      ),


                                    ],
                                  ),


                                  InkWell(
                                    onTap: () {
                                      //  Provider.of<CartProvider>(context,listen: false).getCartItem(context);
                                      Navigator.push(context, MaterialPageRoute(
                                          builder: (context) => CartPage()));
                                      // showBottomSheetApplyCoupon(context);
                                    },
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceBetween,
                                        children: [
                                          Text("           Ready to shop ",
                                            style: TextStyle(
                                                fontSize: SizeConfig
                                                    .smallFontSize,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "openSans"),),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15.0,),
                                            child: Icon(
                                              Icons.navigate_next,
                                              color: Colors.grey,
                                              size: 15,
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  )


                                ],
                              ),
                            ),
                            Divider(height: 0.5, color: Colors.grey.shade200,),

                            Container(
                              padding: const EdgeInsets.only(
                                  bottom: 15.0, top: 5),
                              color: Colors.white,
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center,
                                    children: <Widget>[

                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 15.0, top: 5),
                                        child: Icon(
                                          Icons.share_outlined,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          'Share',
                                          style: TextStyle(
                                              fontFamily: 'openSans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.2,
                                              color: Colors.black,
                                              fontSize: SizeConfig
                                                  .normalFontSize
                                          ),
                                        ),
                                      ),


                                    ],
                                  ),


                                  InkWell(
                                    onTap: () {
                                      share();
                                    },
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceBetween,
                                        children: [
                                          Text(
                                            "           Share App to your circle and get rewarded",
                                            style: TextStyle(
                                                fontSize: SizeConfig
                                                    .smallFontSize,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "openSans"),),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15.0,),
                                            child: Icon(
                                              Icons.navigate_next,
                                              color: Colors.grey,
                                              size: 15,
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  )


                                ],
                              ),
                            ),
                            Divider(height: 0.5, color: Colors.grey.shade200,),

                            Container(
                              padding: const EdgeInsets.only(
                                  bottom: 15.0, top: 5),
                              color: Colors.white,
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center,
                                    children: <Widget>[

                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 15.0, top: 5),
                                        child: Icon(
                                          Icons.feed_outlined,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          'Write to Us',
                                          style: TextStyle(
                                              fontFamily: 'openSans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.2,
                                              color: Colors.black,
                                              fontSize: SizeConfig
                                                  .normalFontSize
                                          ),
                                        ),
                                      ),


                                    ],
                                  ),


                                  InkWell(
                                    onTap: () async {
                                      EmailContent email = EmailContent(
                                        to: [
                                          'contact@krays.in',
                                        ],
                                        subject: '',
                                        body: '',
                                        // cc: ['user2@domain.com', 'user3@domain.com'],
                                        // bcc: ['boss@domain.com'],
                                      );

                                      OpenMailAppResult result =
                                      await OpenMailApp
                                          .composeNewEmailInMailApp(
                                          nativePickerTitle: 'Select email app to compose',
                                          emailContent: email);

                                      if (!result.didOpen && !result.canOpen) {
                                        showNoMailAppsDialog(context);
                                      }
                                    },
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceBetween,
                                        children: [
                                          Text(
                                            "           Your valuable feedback means a lot to us",
                                            style: TextStyle(
                                                fontSize: SizeConfig
                                                    .smallFontSize,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "openSans"),),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15.0,),
                                            child: Icon(
                                              Icons.navigate_next,
                                              color: Colors.grey,
                                              size: 15,
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  )


                                ],
                              ),
                            ),
                            Divider(height: 0.5, color: Colors.grey.shade200,),


                            Container(
                              padding: const EdgeInsets.only(
                                  bottom: 15.0, top: 5),
                              color: Colors.white,
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center,
                                    children: <Widget>[

                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 15.0, top: 5),
                                        child: Icon(
                                          Icons.info_outline,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          'Terms of Use',
                                          style: TextStyle(
                                              fontFamily: 'openSans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.2,
                                              color: Colors.black,
                                              fontSize: SizeConfig
                                                  .normalFontSize
                                          ),
                                        ),
                                      ),


                                    ],
                                  ),


                                  InkWell(
                                    onTap: () async {
                                      AppUtil.launchInBrowser(API.PRIVACY_TnC_URL);
                                    },
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceBetween,
                                        children: [
                                          Text(
                                            "           Krays.in terms of use",
                                            style: TextStyle(
                                                fontSize: SizeConfig
                                                    .smallFontSize,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "openSans"),),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15.0,),
                                            child: Icon(
                                              Icons.navigate_next,
                                              color: Colors.grey,
                                              size: 15,
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  )


                                ],
                              ),
                            ),
                            Divider(height: 0.5, color: Colors.grey.shade200,),


                            Container(
                              padding: const EdgeInsets.only(
                                  bottom: 15.0, top: 5),
                              color: Colors.white,
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center,
                                    children: <Widget>[

                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 15.0, top: 5),
                                        child: Icon(
                                          Icons.policy_outlined,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          'Privacy Policy',
                                          style: TextStyle(
                                              fontFamily: 'openSans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.2,
                                              color: Colors.black,
                                              fontSize: SizeConfig
                                                  .normalFontSize
                                          ),
                                        ),
                                      ),


                                    ],
                                  ),


                                  InkWell(
                                    onTap: () async {
                                   AppUtil.launchInBrowser(API.PRIVACY_POLICY_URL);
                                    },
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceBetween,
                                        children: [
                                          Text(
                                            "           Know what krays do with your data",
                                            style: TextStyle(
                                                fontSize: SizeConfig
                                                    .smallFontSize,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "openSans"),),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15.0,),
                                            child: Icon(
                                              Icons.navigate_next,
                                              color: Colors.grey,
                                              size: 15,
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  )


                                ],
                              ),
                            ),
                            Divider(height: 0.5, color: Colors.grey.shade200,),


                            Container(
                              padding: const EdgeInsets.only(
                                  bottom: 15.0, top: 5),
                              color: Colors.white,
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center,
                                    children: <Widget>[

                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 15.0, top: 5),
                                        child: Icon(
                                          Icons.info_outline,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: Text(
                                          'About Us',
                                          style: TextStyle(
                                              fontFamily: 'openSans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.2,
                                              color: Colors.black,
                                              fontSize: SizeConfig
                                                  .normalFontSize
                                          ),
                                        ),
                                      ),


                                    ],
                                  ),


                                  InkWell(
                                    onTap: () async {
                                      AppUtil.launchInBrowser(API.ABOUT_US_URL);
                                    },
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceBetween,
                                        children: [
                                          Text(
                                            "           Know who we are (Krays.in) ",
                                            style: TextStyle(
                                                fontSize: SizeConfig
                                                    .smallFontSize,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "openSans"),),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15.0,),
                                            child: Icon(
                                              Icons.navigate_next,
                                              color: Colors.grey,
                                              size: 15,
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  )


                                ],
                              ),
                            ),
                            Divider(height: 0.5, color: Colors.grey.shade200,),

                            SizedBox(height: 30),


                            Container(
                              margin: new EdgeInsets.only(
                                  left: 15.0, right: 15),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: R.color.app_color,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(0.0),
                                      topLeft: Radius.circular(0.0),
                                      bottomRight: Radius.circular(0.0),
                                      bottomLeft: Radius.circular(0.0),
                                    ),
                                  ),
                                  minimumSize: Size(double.infinity,
                                      ResponsiveFlutter.of(context).hp(
                                          5.0)), // double.infinity is the width and 30 is the height
                                ),
                                onPressed: () async {
                                  showAlertDialogLogout(context);
                                },
                                child: Text('Logout'.toUpperCase(), style:
                                TextStyle(
                                    fontSize: ResponsiveFlutter.of(context)
                                        .fontSize(2.0,),
                                    color: R.color.white_color,
                                    fontWeight: FontWeight.bold),),
                              ),
                            ),
                            new Row(
                              children: <Widget>[
                                new Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment
                                          .center,
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment
                                          .start,
                                      children: <Widget>[
                                        new Text("",
                                          style: TextStyle(fontSize: 13,
                                              color: Colors.grey[600]),),
                                      ],
                                    )
                                ),


                              ],
                            ),


                          ],
                        ),
                      ),
                    )
                )

              ],
            );
          }
      ),
    );
  }


  showAlertDialogLogout(BuildContext context) {
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text("Logout"),
      onPressed: () async {
        //PreferenceUtils.clearPreferences();
        PreferenceUtils.setBool(PreferencesKey.IS_USER_LOGIN, false);


        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) =>
                    LoginScreen()),
                (Route<dynamic> route) => false);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Logout"),
      content: Text("Are you sure want to logout?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  //com.krays.ind.krays_in
  Future<void> share() async {
    await FlutterShare.share(
        title: 'Krays.In',
        text: 'Getting fresh fruit and veggies at doorstep download the Krays.in app',
        linkUrl: 'https://play.google.com/store/apps/details?id=com.krays.ind.krays_in',
        chooserTitle: 'Choose the app where you want to share ');
  }

  void showNoMailAppsDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Open Mail App"),
          content: Text("No mail apps installed"),
          actions: <Widget>[
            TextButton(
              child: Text("OK"),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

}