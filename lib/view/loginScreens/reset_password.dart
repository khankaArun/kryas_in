import 'package:flutter/material.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/util/Widgets/widegts_controller.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/preferences_until.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:provider/provider.dart';

class ResetPassword extends StatefulWidget {
  const ResetPassword({Key key}) : super(key: key);

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {

  final newPassController = TextEditingController();
  final confirmController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.white_color,
      appBar: AppBar(
        leading: Container(
          padding: EdgeInsets.only(left: 15,top: 0),
          child: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back_ios,color: R.color.grey_color,size: 20,),
          ),
        ),
        elevation: 0,
        backgroundColor: R.color.white_color,
        // title: Text("g"),
      ),

      body: Container(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(left: 35,right: 35),
            alignment: Alignment.center,
            child: Column(
              children: [

                Image.asset(R.images.gif_two,scale: 4,),
                SizedBox(height: 10,),

                WidgetController.text_black("Reset Password"),
                SizedBox(height: 5,),

                Text("Reset your password,choose the unique one",style: TextStyle(color: R.color.hint_grey_color,fontFamily: "ralewayMedium"),),
                SizedBox(height: 10,),

                TextField(
                  autocorrect: false,
                  decoration: InputDecoration(
                      fillColor: R.color.themeColor,
                      label: Text("New Password",style: TextStyle(color: R.color.hint_grey_color,fontSize: 17,fontFamily: "ralewayMedium"),)
                  ),
                  textInputAction: TextInputAction.send,
                  controller: newPassController,
                  autofocus: true,
                  cursorColor: R.color.themeColor,
                ),
                SizedBox(height: 10,),
                TextField(
                  autocorrect: false,
                  decoration: InputDecoration(
                      fillColor: R.color.themeColor,
                      label: Text("Confirm Password",style: TextStyle(color: R.color.hint_grey_color,fontSize: 17,fontFamily: "ralewayMedium"),)
                  ),
                  textInputAction: TextInputAction.send,
                  controller: confirmController,
                  autofocus: true,
                  cursorColor: R.color.themeColor,
                ),

                SizedBox(height: 10,),

                InkWell(

                  onTap: (){
                    validate();
                  },

                  child: WidgetController.button_theme(context, "Reset Password"),
                ),



              ],
            ),
          ),
        ),
      ),
    );
  }

  validate() async {
    if (newPassController.text.isEmpty) {
      AppUtil.showToast("Enter new password");
    } else if (confirmController.text.isEmpty) {
      AppUtil.showToast("Enter confirm password");
    } else if (confirmController.text.toString() !=
        newPassController.text.toString()) {
      AppUtil.showToast("Confirm Password Doesn't matched");
    } else {
      String password = await PreferenceUtils.getString(
          PreferencesKey.PASSWORD, "");
      AppUtil.showLoadingProgress(context);
      var res = await Provider.of<AuthProvider>(context, listen: false)
          .resetPassword(oldPassword: password,
          newPassword: newPassController.text.toString().trim());
         Navigator.of(context).pop();
         Navigator.of(context).pop();
      if (res) {
        AppUtil.showToast("Password change successfully");
      }
    }
  }
}
