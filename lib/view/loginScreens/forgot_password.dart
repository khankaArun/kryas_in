import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/util/CustomWidgets.dart';
import 'package:krays_in/util/Widgets/widegts_controller.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:provider/provider.dart';

class ForgetPass extends StatefulWidget {
  const ForgetPass({Key key}) : super(key: key);

  @override
  _ForgetPassState createState() => _ForgetPassState();
}

class _ForgetPassState extends State<ForgetPass> {

  final userNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.white_color,
      appBar: AppBar(
        leading: Container(
          padding: EdgeInsets.only(left: 15,top: 10),
          child: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back_ios,color: R.color.grey_color,size: 20,),
          ),
        ),
        elevation: 0,
        backgroundColor: R.color.white_color,
        // title: Text("g"),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(top: 0,left: 35,right: 35),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(R.images.forgetFig,scale: 2,),
                // SizedBox(height: 20,),

                WidgetController.text_black("Forgot Password"),
                SizedBox(height: 10,),
                Text("Don't worry ! it happens. Please enter the mobile number associated with your account",
                  style: TextStyle(color: R.color.hint_grey_color,fontWeight: FontWeight.bold,fontFamily: "raleway"),
                  textAlign: TextAlign.center,),
                SizedBox(height: 15,),

                TextField(
                  autocorrect: false,
                  maxLength: 10,
                    keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    fillColor: R.color.themeColor,
                    label: Text("Mobile Number",style: TextStyle(color: R.color.grey_color,fontSize: 17,fontFamily: "raleway"),)
                  ),
                  textInputAction: TextInputAction.done,
                  controller: userNameController,
                  autofocus: false,
                  cursorColor: R.color.themeColor,
                ),

                SizedBox(height: 20,),

                InkWell(

                  onTap: () async{
                    if(!AppUtil.isEmail(userNameController.text) && !AppUtil.isPhone(userNameController.text)){
                      Fluttertoast.showToast(
                          msg: "Please enter 10 digit mobile number or valid email",
                          toastLength: Toast.LENGTH_LONG,
                          gravity: ToastGravity.BOTTOM,
                          backgroundColor: R.color.themeColor,
                          textColor: Colors.white,
                          fontSize: 14.0);
                    } else{
                      AppUtil.showLoadingProgress(context);

                      var res = await Provider.of<AuthProvider>(context, listen: false).otpValidator(userNameController.text.toString());
                      Navigator.of(context).pop();
                      if(res){
                        Navigator.pushNamed(context, StringConfig.OTP_VERIFY,arguments: {'mobile':userNameController.text,'comefrom':"forgot"});
                      }else{
                        //AppUtil.showToast(provider.otpAuthItem.message);
                      }


                    }
                  },

                  child: WidgetController.button_theme(context, "submit"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
