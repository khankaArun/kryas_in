import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/util/CustomWidgets.dart';
import 'package:krays_in/util/Widgets/widegts_controller.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:provider/provider.dart';
class SignUpScreen extends StatefulWidget {



  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  bool checkBoxValue = false;
  final _confirmPassword =   TextEditingController();
  final _mobileNo =   TextEditingController();
  final _email =   TextEditingController();

  final _nameController =   TextEditingController();
  final _inviteCode =   TextEditingController();



  @override
  void initState() {
    setState(() {
      print("emaillll" + _email.text);

      // if(AppUtil.isPhone(widget.mobileNumber.toString())){
      //   _mobileNo.text =  widget.mobileNumber.toString();
      // }else{
      //   // _email.text = widget.e.toString();
      // }

    });
    super.initState();
  }


  bool _passwordObscureText =  true;

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false, // remove back button in appbar.

        leading: Container(
          padding: EdgeInsets.only(left: 15,top: 10),
          child: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back_ios,color: R.color.grey_color,size: 20,),
          ),
        ),
        elevation: 0,
        backgroundColor: R.color.white_color,
        // title: Text("g"),
      ),
      backgroundColor: R.color.white_color,
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Image.asset(R.images.redbg,
              fit: BoxFit.fitWidth,
              alignment: Alignment.bottomLeft,
            ),
          ),
          Center(
            child: Container(
              child: SingleChildScrollView(
                  child:Container(
                    margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child:  Column(
                      children: [
                        Image.asset(R.images.logo_kryas, width: 250,),
                        SizedBox(height: 10,),

                        WidgetController.textfiled(_nameController, "Enter Name"),
                        SizedBox(height: 10,),


                        WidgetController.textfiled(_email, "Enter Email "),
                        SizedBox(height: 10,),

                        WidgetController.textfiled(_mobileNo, "Enter Mobile Number"),
                        SizedBox(height: 10,),

                        Container(
                            // margin: EdgeInsets.fromLTRB(45, 20, 45, 0),
                            child:TextField(
                              autocorrect: false,
                              decoration: InputDecoration(
                                fillColor: R.color.themeColor,
                                label: Text("Enter Password",style: TextStyle(color: R.color.hint_grey_color,fontSize: 17),),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    // Based on passwordVisible state choose the icon
                                    _passwordObscureText ?  Icons.visibility_off : Icons.visibility ,
                                    color:  _passwordObscureText ? R.color.grey_color : R.color.app_color,
                                  ),
                                  onPressed: () {
                                    // Update the state i.e. toogle the state of passwordVisible variable
                                    setState(() {
                                      _passwordObscureText ? _passwordObscureText = false : _passwordObscureText = true;
                                    });
                                  },
                                ),

                              ),
                              keyboardType: TextInputType.visiblePassword,
                              textInputAction: TextInputAction.send,
                              controller: _confirmPassword,
                              obscureText: _passwordObscureText,
                              autofocus: false,
                              cursorColor: R.color.themeColor,

                            )


                        ),


                        SizedBox(height: 10,),

                        WidgetController.textfiled(_inviteCode, "Invite Code (Optional)"),
                        SizedBox(height: 6,),

                        Row(
                          //mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 5),
                              child: Checkbox(value: checkBoxValue,
                                  activeColor: R.color.app_color,
                                  onChanged:(bool newValue){
                                    setState(() {
                                      checkBoxValue = newValue;
                                    });

                                  }),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 15),
                              child:  Row(
                                children: [
                                  Text("I have read & agree to",style: TextStyle(color: R.color.hint_grey_color,
                                      fontFamily:"ralewayMedium",fontSize: 13),),


                                  InkWell(
                                    onTap: (){
                                      AppUtil.launchInBrowser(API.PRIVACY_TnC_URL);
                                    },

                                    child: Text(" Terms & Conditions",style: TextStyle(color: R.color.app_color,
                                        fontFamily:"ralewayMedium",fontSize: 13,decoration: TextDecoration.underline),),
                                  ),

                                  Text(" &",style:
                                  TextStyle(color: R.color.hint_grey_color,fontFamily:"ralewayMedium",fontSize: 13),),

                                  Text("\n ",style: TextStyle(color: R.color.hint_grey_color,fontFamily:"ralewayMedium"),),
   // Text("Privacy Policy",style: TextStyle(color: R.color.app_color,fontFamily:"ralewayMedium",decoration: TextDecoration.underline,fontSize: 13),),

                                ],
                              ),
                            ),

                          ],
                        ),

                        InkWell(

                          onTap: (){
                            AppUtil.launchInBrowser(API.PRIVACY_POLICY_URL);
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 50),
                            child: Align(
                                alignment: Alignment.topLeft,
                                child: Text("Privacy Policy",style: TextStyle(color: R.color.app_color,fontFamily:"ralewayMedium",decoration: TextDecoration.underline,fontSize: 13),)),
                          ),
                        ),
                        SizedBox(height: 10,),

                        InkWell(
                          onTap: () async {
                            validate();
                            // AppUtil.showToast(provider.otpAuthItem.message);

                          },
                          child: Container(
                            width: 328,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: R.color.app_color,
                            ),
                            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10, ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children:[
                                Text(
                                  "Sign UP",
                                  style: TextStyle(
                                    color: R.color.white_color,
                                    fontFamily: "ralewayMedium",
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 14,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              "Already have an account ? ", style: TextStyle(color: R.color.hint_grey_color,fontFamily:"ralewayMedium"
                            ),
                            ),
                            InkWell(
                              onTap: (){
                                Navigator.pushNamed(context, StringConfig.LOGIN_PAGE);
                              },
                              child:  Text(
                                  "sign in ", style: TextStyle(color: R.color.app_color,decoration: TextDecoration.underline,fontFamily:"ralewayMedium"
                              )),
                            )
                          ],
                        ),
                        SizedBox(height: 150,),
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.center,
                        //   crossAxisAlignment: CrossAxisAlignment.center,
                        //   children: [
                        //     Container(
                        //       height: 40,
                        //       width: 40 ,
                        //       decoration: new BoxDecoration(
                        //         border: Border.all(
                        //           color: R.color.hint_grey_color,
                        //           width: 2,
                        //         ), //Border.all
                        //         borderRadius: BorderRadius.circular(35),
                        //       ),
                        //     ),
                        //     SizedBox(width: 20,),
                        //
                        //     Container(
                        //       height: 40,
                        //       width: 40 ,
                        //       decoration: new BoxDecoration(
                        //         border: Border.all(
                        //           color: R.color.hint_grey_color,
                        //           width: 2,
                        //         ), //Border.all
                        //         borderRadius: BorderRadius.circular(35),
                        //       ),
                        //     )
                        //
                        //   ],
                        // ),
                        // SizedBox(height: 250,),


                      ],
                    ),
                  )
              ),
            ),
          )

        ],
      )
    );
  }

  Future<void> validate() async {

    if(_nameController.text.isEmpty){
      AppUtil.showToast("Please Enter User Name");
    }else if(_email.text.isEmpty|| !AppUtil.isEmail(_email.text.trim())){
      AppUtil.showToast("Please Enter Valid Email");
    }else if(_mobileNo.text.isEmpty|| !AppUtil.isPhone(_mobileNo.text.trim())){
      AppUtil.showToast("Please Enter 10 Digit Mobile Number");
    }else if(_confirmPassword.text.isEmpty){
      AppUtil.showToast("Please Enter Password");
    }else if(checkBoxValue == false){
      AppUtil.showToast("Please agree the Terms & Conditions");
    }else{

      AppUtil.showLoadingProgress(context);
      var res =false;
      try{
         res = await Provider.of<AuthProvider>(context, listen: false).otpValidator(_mobileNo.text.toString());
      }catch(error ){
        res=false;
      }


      Navigator.of(context).pop();
      if(res){
        Navigator.pushNamed(context, StringConfig.OTP_VERIFY,arguments: {
          'userName': _nameController.text,
          'password': _confirmPassword.text,
          'email' :_email.text,
          'mobile':_mobileNo.text,
          'comefrom':"signup",
           'referCode' :_inviteCode.text

        });

      }else{
        AppUtil.showToast("Internal server error");
      }




      //
      // AppUtil.showLoadingProgress(context);
      // var res =  await Provider.of<AuthProvider>(context, listen: false)
      //     .userRegistration(name: _nameController.text,
      //     password: _confirmPassword.text,
      //     mobile: _mobileNo.text,
      //     email: _email.text);
      // Navigator.of(context).pop();
      // if(res){
      //   Navigator.of(context).pop();
      //   print("_mobileNo :" + _mobileNo.text);
      //
      //   Navigator.pushNamed(context, StringConfig.OTP_VERIFY,arguments: _mobileNo.text);
      // }
    }
  }

}
