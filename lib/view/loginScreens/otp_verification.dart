import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pin_code_fields/flutter_pin_code_fields.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';




class OtpVerification extends StatefulWidget {
  const OtpVerification({Key key}) : super(key: key);

  @override
  _OTP_VerificationState createState() => _OTP_VerificationState();
}

class _OTP_VerificationState extends State<OtpVerification> {


  final OtpController= TextEditingController();
  String textResend = "60";
  bool enableResend = false;

  String code = "";
  int secondsRemaining = 60;
   Timer timer;

  String  mobile;
  String userName;
  String email;
  String password;
  String comefrom;

  String inviteCode;

  @override
  initState() {
    super.initState();

    timer_clock();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    timer.cancel() ;

  }



  @override
  Widget build(BuildContext context) {

    final  Map<String, Object> regData = ModalRoute.of(context).settings.arguments;
    print("rcvd fdata ${regData['mobile']}");
    comefrom = regData['comefrom'];
    if ( comefrom == "signup") {

      mobile = regData['mobile'];
      userName = regData['userName'];
      password = regData['password'];
      email = regData['email'];

      inviteCode = regData['referCode'];
    }else{
      mobile = regData['mobile'];
    }


    return  Scaffold(
      backgroundColor: R.color.white_color,
      body: Consumer<AuthProvider>(
          builder: (context, provider, child) {
            return  Container(
              child: SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.fromLTRB(20, 80, 20, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(R.images.verify_otp,scale: 2,),
                        SizedBox(height: 10,),
                        Text("Verify Your Mobile Number",textAlign: TextAlign.center,
                          style: TextStyle(color: R.color.black_color,fontFamily: "bold",fontWeight: FontWeight.bold,fontSize: 25),),
                        SizedBox(height: 20,),
                        Text("Please enter 6 digit code that we sent on\n"+mobile,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontFamily: "openSans",color: R.color.hint_grey_color),),
                        // Text(provider.otpAuthItem.otp.toString()),
                        Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8.0, horizontal: 30),
                            child: PinCodeTextField(
                              autoDisposeControllers: false,
                              appContext: context,
                              pastedTextStyle: TextStyle(
                                color: R.color.bluecolor,
                                fontWeight: FontWeight.bold,
                              ),
                              length: 6,
                              obscureText: true,
                              obscuringCharacter: '*',
                              cursorColor: R.color.themeColor,
                              animationDuration: Duration(milliseconds: 300),
                              // enableActiveFill: true,
                              controller: OtpController,
                              keyboardType: TextInputType.number,
                              onCompleted: (v) {
                              },
                              onChanged: (value) {
                                print(value);
                                setState(() {
                                });
                              },

                            )),
                        RichText(
                            text: TextSpan(children: <TextSpan>[
                              TextSpan(
                                  text: "Didn't receive the code? ",
                                  style: TextStyle(
                                      fontFamily: "openSans",
                                      fontSize: 14,
                                      color: R.color.black_color)),
                              TextSpan(

                                  text: textResend,
                                  style:  TextStyle(
                                      fontFamily: "bold",
                                      fontSize: 16,
                                      color: R.color.themeColor,decoration: TextDecoration.underline),
                                  recognizer:  TapGestureRecognizer(

                                  )
                                ..onTap = () =>myAction(provider),
                              )
                            ])),
                      // Text(provider.otpAuthItem.otp.toString()),
                      Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 20, left: 20, right: 20),
                      height: 40,
                      child: RaisedButton(
                        onPressed: () async {
                          print("controllerOTp" + OtpController.text.toString());
                          print("otpmatch"+provider.otpAuthItem.otp.toString());
                          if(OtpController.text.toString().trim() == provider.otpAuthItem.otp.toString().trim()){

                            move_to_next_page();
                          }else{

                            AppUtil.showToast("OTP Mismatch ! Please Again Enter");

                          }
                        },
                        elevation: 0.0,
                        color: R.color.app_color ,
                        child: Text("Verify OTP",
                            style: TextStyle(
                                color: R.color.white_color, fontSize: 14, fontFamily: "ralewayMedium")),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                      ),
                    )

                      ],
                    ),
                  )
              ),
            );
          })



    );

  }

  void myAction(AuthProvider provider){

   setState(() async {

     if(enableResend==false){
       return;
     }




     if(enableResend){
       AppUtil.showLoadingProgress(context);
     var isRes= await  provider.otpValidator(provider.otpAuthItem.mobile);
       Navigator.of(context).pop();
       enableResend=false;
     }else{
       enableResend=true;
     }

   });




  }


  Future<void> move_to_next_page() async {

    if (comefrom == "signup") {

    AppUtil.showLoadingProgress(context);
    var res =  await Provider.of<AuthProvider>(context, listen: false)
        .userRegistration(name: userName,
    password: password,
    mobile:mobile,
    email: email,
    referCode:  inviteCode);
    Navigator.of(context).pop();
    if(res){
    Navigator.of(context).pop();


    Navigator.pushNamed(context, StringConfig.LOGIN_PAGE,);
    }
    }else{
      AppUtil.showLoadingProgress(context);
      var res =  await Provider.of<AuthProvider>(context, listen: false).forgotPassword(
          email: mobile);

      if(res){
        Navigator.of(context).pop();

        Navigator.pushNamed(context, StringConfig.LOGIN_PAGE,);
      }


    }

  }



 void timer_clock() {
  setState(() {
    timer = Timer.periodic(Duration(seconds: 1), (_) {
      if (secondsRemaining != 0) {
        setState(() {
          secondsRemaining--;
          textResend = "$secondsRemaining sec";
        });
      } else {
        textResend = " Re-send ";
        setState(() {
          enableResend = true;
        });
      }
    });
  });
}


}
