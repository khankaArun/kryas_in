import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/provider/CartProvider.dart';
import 'package:krays_in/provider/MarketProvider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/Widgets/widegts_controller.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/preferences_until.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:krays_in/view/home/home_page.dart';
import 'package:krays_in/view/loginScreens/sing_up.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final userNameController = TextEditingController();
  final passwordController= TextEditingController();

  bool isLoading = false;
  bool darkMode = true;


  // final FirebaseAuth _auth = FirebaseAuth.instance;
  // StreamSubscription<FirebaseUser> _listener;
  //
  // FirebaseUser _currentUser;
  //
  // @override
  // void initState() {
  //   super.initState();
  //   _checkCurrentUser();
  // }

  @override
  void dispose() {
    // _listener.cancel();
    super.dispose();
  }

  bool _passwordObscureText =  true;
  bool _rememberMe =false;



  @override
  Widget build(BuildContext context) {
   // userNameController.text = "8299137305";
   // passwordController.text = "kU5MOfvs";


   return WillPopScope(
     onWillPop: () => Future.value(false),
     child: Scaffold(
     backgroundColor: R.color.white_color,
       body: Stack(
         children: <Widget>[
           Positioned.fill(
             child: Image.asset(R.images.redbg,
               fit: BoxFit.fitWidth,
               alignment: Alignment.bottomLeft,
             ),
           ),
           Center(
             child: SingleChildScrollView(
                 child: Container(
                   child: Column(

                     children: [
                       Image.asset(R.images.logo_kryas, width: 250,),
                       Container(
                         margin: EdgeInsets.fromLTRB(45, 15, 45, 0),
                         child:  WidgetController.textfiled(userNameController, "Email or Mobile Number"),
                       ),
                       SizedBox(height: 10,),
                       Container(
                         margin: EdgeInsets.fromLTRB(45, 10, 45, 0),
                         child:TextField(

                           autocorrect: false,
                           decoration: InputDecoration(
                             fillColor: R.color.themeColor,
                             label: Text("Enter Password",style: TextStyle(color: R.color.hint_grey_color,fontSize: 15),),
                             suffixIcon: IconButton(
                               icon: Icon(
                                 // Based on passwordVisible state choose the icon
                                 _passwordObscureText ?  Icons.visibility_off : Icons.visibility ,
                                 color:  _passwordObscureText ? R.color.grey_color : R.color.app_color,
                               ),
                               onPressed: () {
                                 // Update the state i.e. toogle the state of passwordVisible variable
                                 setState(() {
                                   _passwordObscureText ? _passwordObscureText = false : _passwordObscureText = true;
                                 });
                               },
                             ),

                           ),
                           keyboardType: TextInputType.visiblePassword,
                           textInputAction: TextInputAction.send,
                           controller: passwordController,
                           obscureText: _passwordObscureText,
                           autofocus: false,
                           cursorColor: R.color.themeColor,



                         )


                       ),
                       Container(
                         margin: EdgeInsets.fromLTRB(30, 10, 10, 0),
                         child: Row(
                           children: [
                             Checkbox(

                               checkColor: R.color.white_color,
                               value: _rememberMe,
                               activeColor: R.color.app_color,
                               onChanged: (bool value) {

                                 setState(() {

                                   if(_rememberMe){
                                     _rememberMe =false;
                                   }else{
                                     _rememberMe =true;
                                   }

                                 });
                               },
                             ),
                             Text("Remember Me",style: TextStyle(color:R.color.hint_grey_color),),
                             SizedBox(width: 70,),
                             InkWell(
                                 onTap: (){
                                   Navigator.pushNamed(context, StringConfig.FORGET_PASS);
                                 },
                                 child: Text("Forgot Password",style: TextStyle(color: R.color.app_color,decoration: TextDecoration.underline),)

                             )
                           ],
                         ),

                       ),

                       Container(
                         margin: EdgeInsets.fromLTRB(45, 20, 45, 0),
                         child: Row(

                           children: [
                             Text("By continuing, I agree to the",
                               style: TextStyle(color: R.color.hint_grey_color,fontFamily: "ralewayMedium"),),
                             InkWell(
                               onTap: (){

                                    AppUtil.launchInBrowser(API.PRIVACY_TnC_URL);
                               

                               },

                               child: Text(" Terms of Use ",
                                 style: TextStyle(decoration: TextDecoration.underline ,fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                                     fontWeight: FontWeight.w600,color: R.color.app_color,fontFamily: "openSans"),),
                             ),
                             Text("&",
                               style: TextStyle(color: R.color.hint_grey_color,fontFamily: "ralewayMedium"),),
                           ],
                         ),
                       ),

                       InkWell(

                         onTap: (){
                           AppUtil.launchInBrowser(API.PRIVACY_POLICY_URL);
                         },

                         child: Align(
                           alignment: Alignment.centerLeft,
                           child: Container(
                             margin: new EdgeInsets.only(left: 47.0,top: 1.5),
                             child:Text("Privacy Policy",
                               textAlign: TextAlign.start,

                               style: TextStyle(decoration: TextDecoration.underline, fontSize: ResponsiveFlutter.of(context).fontSize(1.5,),

                                   fontWeight: FontWeight.w600,color: R.color.app_color,fontFamily: "openSans"),),

                           ),
                         ),
                       ),


                       SizedBox(height: 20,),
                       signInButton,
                       SizedBox(height: 10,),




                       Row(
                         crossAxisAlignment: CrossAxisAlignment.center,
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: [
                           Text("Don't have a account ? ",style: TextStyle(color: R.color.hint_grey_color,fontFamily: "ralewayMedium"),),
                           InkWell(
                               onTap: (){

                                 //SignUpScreen

                                 //Navigator.pushNamed(context, StringConfig.SIGN_UP);

                                   Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft,
                                       child: SignUpScreen()));



                               },
                               child: Text("Sign up",style: TextStyle(color: R.color.app_color,   fontFamily: "ralewayMedium",decoration: TextDecoration.underline),)

                           )
                         ],
                       ),
                       SizedBox(height: 20,),
/*
                       Row(
                         mainAxisAlignment: MainAxisAlignment.center,
                         crossAxisAlignment: CrossAxisAlignment.center,
                         children: [
                           InkWell(
                             onTap: (){
                               // GoogleSignIn().signIn();
                             },
                             child:  Image.asset(R.images.facebook),
                           ),
                           SizedBox(width: 19,),
                           InkWell(
                               onTap:(){
                                 // FacebookAuth.instance.login();
                               },
                               child: Image.asset(R.images.google)

                           )
                         ],
                       ),*/

                     ],
                   ),
                 )
             ),
           )
         ],
       ),
   ),
   );


  }



  // void _checkCurrentUser() async {
  //   _currentUser = await _auth.currentUser();
  //   _currentUser?.getIdToken(refresh: true);
  //
  //   _listener = _auth.onAuthStateChanged.listen((FirebaseUser user) {
  //     setState(() {
  //       _currentUser = user;
  //     });
  //   });
  // }


  get signInButton {

    return Consumer<AuthProvider>(
      builder: (context, provider, child){
        return InkWell(
          onTap:() async {



            if(userNameController.text.isEmpty){
              AppUtil.showToast("Enter Email or Phone Number");
            }else if(passwordController.text.isEmpty){
              AppUtil.showToast("Enter Password");
            }
            else {

              dynamic getLat= await PreferenceUtils.getDouble(
                  PreferencesKey.userLat);

              dynamic getLong= await PreferenceUtils.getDouble(
                  PreferencesKey.userLong);

              dynamic getPin= await PreferenceUtils.getString(
                  PreferencesKey.userLocationPincode);

              dynamic getAddress= await PreferenceUtils.getString(
                  PreferencesKey.ADDFULL);



              AppUtil.showLoadingProgress(context);
              var res = await provider.login(userNameController.text.toString(),
                  passwordController.text.toString(),
                getLat,getLong,getAddress,getPin);





              if (res) {
                String token = await "Bearer " +
                    PreferenceUtils.getString(PreferencesKey.ACESS_TOKEN);

                print(token);
                var provider = await Provider.of<MarketProvider>(
                    context, listen: false).getHomeData();
                var cartProvider = await Provider.of<CartProvider>(
                    context, listen: false).getUserCart();


                if (provider) {

                  Navigator.of(context).pop();
                  Navigator.of(context).pop();

                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              HomePage()),
                          (Route<dynamic> route) => false);




                } else {
                  Navigator.of(context).pop();
                  AppUtil.showToast("Home API has some error");
                }
              }else{
                Navigator.of(context).pop();
              }
            }
          },
          child:  Container(
            width: 328,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: R.color.app_color,
            ),
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10, ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:[
                Text(
                  "Sign in",
                  style: TextStyle(
                    color: R.color.white_color,
                    fontSize: 16,
                    fontFamily: "ralewayMedium",
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  // Future<UserCredential> signInWithFacebook() async {
  //   // Trigger the sign-in flow
  //   final LoginResult loginResult = await FacebookAuth.instance.login();
  //
  //   // Create a credential from the access token
  //   final OAuthCredential facebookAuthCredential = FacebookAuthProvider.credential(loginResult.accessToken.token);
  //
  //   // Once signed in, return the UserCredential
  //   return FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
  // }
  //
  // Future<UserCredential> signInWithGoogle() async {
  //   // Trigger the authentication flow
  //   final googleUser = await GoogleSignIn().signIn();
  //
  //   // Obtain the auth details from the request
  //   final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;
  //
  //   // Create a new credential
  //   final credential = GoogleAuthProvider.credential(
  //     accessToken: googleAuth?.accessToken,
  //     idToken: googleAuth?.idToken,
  //   );
  //
  //   // Once signed in, return the UserCredential
  //   return await FirebaseAuth.instance.signInWithCredential(credential);
  // }



}
