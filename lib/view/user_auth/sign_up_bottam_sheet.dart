
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:krays_in/view/user_auth/singup_page.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';


class SignUpBottom {

  static Future <dynamic>  show({BuildContext context,
                dynamic whereItComeFrom}){

    TextEditingController _controller =  TextEditingController();

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) => Container(
        height: ResponsiveFlutter.of(context).hp(80),
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(0.0),
            topRight: const Radius.circular(0.0),
          ),
        ),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
             mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(left: 15.0,right: 15,top: 8,bottom: 8),
                height: ResponsiveFlutter.of(context).hp(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      child: Image.asset(R.images.ic_vertical_logo),
                      height: ResponsiveFlutter.of(context).hp(40.0),
                      width: ResponsiveFlutter.of(context).wp(30.0),
                    ),

                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: SizedBox(
                        child: Icon(Icons.close),
                        height: ResponsiveFlutter.of(context).hp(10.0),
                        width: ResponsiveFlutter.of(context).wp(5.0),
                      ),
                    ),

                  ],
                ),
              ),

              Container(
                height: ResponsiveFlutter.of(context).hp(15.0),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(BannerBaseUrl.SIGN_UP_BANNER),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                ),
              ),

              SizedBox(height: ResponsiveFlutter.of(context).hp(1.3),),

              Container(
                margin: EdgeInsets.all(15.0),
                child: Row(
                  children: [
                    SizedBox(width: 10,),
                    Text("Enter mobile no. or valid email ID", style:
                    TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(2.3,),
                     color: R.color.black_color,fontWeight: FontWeight.w900,fontFamily: "ralewayMedium"),),
                    SizedBox(width: 5,),
                    Text("",style:
                    TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(2.3,),
                        color: R.color.black_color,fontWeight: FontWeight.w900,fontFamily: "ralewayMedium"),),
                /*    Text("/",style:
                    TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.7,),
                        color: R.color.black_color,fontWeight: FontWeight.w500,fontFamily: "ralewayMedium"),),*/
                    SizedBox(width: 5,),
                    Text("",style:
                    TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(2.3,),
                        color: R.color.black_color,fontWeight: FontWeight.w900,fontFamily: "ralewayMedium"),)
                  ],
                ),
              ),

              SizedBox(height: ResponsiveFlutter.of(context).hp(1.5),),

        Container(
          width: double.infinity,
          margin: new EdgeInsets.only(left: 25.0, right: 25.0),
          color: Colors.white,
          child: new TextFormField(
            controller: _controller,
            validator: (value) {
              if (!AppUtil.isEmail(value) && !AppUtil.isPhone(value)) {
                return 'Please enter 10 digit mobile number or valid email';
              }
            },
            style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                fontWeight: FontWeight.w500,fontFamily: "openSans"),
            keyboardType: TextInputType.emailAddress,
            maxLength: 40,
            decoration: new InputDecoration(
                contentPadding: const EdgeInsets.all(12.0),
                border: new OutlineInputBorder(
                    borderSide:
                    new BorderSide(color: Color(0xFFE0E0E0), width: 0.1)),
                fillColor: Colors.white,
                prefixIcon: Container(
                  width: 30.0,
                  margin:  EdgeInsets.all(10.0),
                  decoration:  BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      right: BorderSide(width: 0.5, color: Colors.grey),
                    ),
                  ),
                  child: Center(child: Padding(padding:EdgeInsets.only(right: 5.0),child: Text("+91",
                    style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                        fontWeight: FontWeight.w500,fontFamily: "openSans"),))),
                ),
                hintText: 'Mobile number or email*',
                labelText: 'Mobile number or email*'),
               ),
            ),

              SizedBox(height: ResponsiveFlutter.of(context).hp(0.5),),

              Container(
                margin: new EdgeInsets.only(left: 25.0, top: 25.0),
                child: Row(
                  children: [
                  Text("By continuing, I agree to the",
                     style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                         fontWeight: FontWeight.w500,color: R.color.black_color,fontFamily: "openSans"),),
                    Text(" Terms of Use ",
                      style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                          fontWeight: FontWeight.w600,color: R.color.app_color,fontFamily: "openSans"),),
                    Text("&",
                      style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                          fontWeight: FontWeight.w500,color: R.color.black_color,fontFamily: "openSans"),),
                  ],
                ),
              ),

              Container(
                margin: new EdgeInsets.only(left: 25.0,top: 1.5),
                child:Text("Privacy Policy",
                  style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                      fontWeight: FontWeight.w600,color: R.color.app_color,fontFamily: "openSans"),),

              ),

              SizedBox(height: ResponsiveFlutter.of(context).hp(3.0),),

              Container(
                margin: new EdgeInsets.only(left: 25.0,right: 25),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: R.color.button,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    minimumSize: Size(double.infinity, ResponsiveFlutter.of(context).hp(5.0)), // double.infinity is the width and 30 is the height
                  ),
                  onPressed: () async{

                    if(!AppUtil.isEmail(_controller.text) && !AppUtil.isPhone(_controller.text)){
                      Fluttertoast.showToast(
                          msg: "Please enter 10 digit mobile number or valid email",
                          toastLength: Toast.LENGTH_LONG,
                          gravity: ToastGravity.BOTTOM,
                          backgroundColor: R.color.errorColor,
                          textColor: Colors.white,
                          fontSize: 16.0);
                    } else{

                      if(whereItComeFrom.toString()==WhereItComeFrom.FORGOT_PASSWORD.toString()){

                        AppUtil.showLoadingProgress(context);
                        var res =  await Provider.of<AuthProvider>(context, listen: false).forgotPassword(
                            email: _controller.text.trim());
                        Navigator.of(context).pop();

                        if(res){
                          Navigator.of(context).pop();
                        }
                        return;

                      }

                      Navigator.of(context).pop();

                      Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.rightToLeft, child: SingupPage(mobileNumber: _controller.text.toString())));
                    }
                  },
                  child: Text('Continue'.toUpperCase(),style:
                  TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(2.0,),
                      color: R.color.white_color,fontWeight: FontWeight.bold,fontFamily: "ralewayMedium"),),
                ),
              ),

              SizedBox(height: ResponsiveFlutter.of(context).hp(1)),

              Container(
                margin: new EdgeInsets.only(left: 25.0, top: 25.0),
                child: Row(
                  children: [
                    Text("Have trouble logging in? ",
                      style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                          fontWeight: FontWeight.w500,color: R.color.black_color,fontFamily: "openSans"),),
                    Text(" Get help ",
                      style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                          fontWeight: FontWeight.w600,color: R.color.app_color,fontFamily: "openSans"),),
                   ],
                ),
              ),

            ],
          ),
        ),
      ),
    );

  }

  static Future <dynamic> showOtp({BuildContext context,
    dynamic mobile,
    String whereToComeFrom,
    dynamic otp,
    dynamic name,
    dynamic email,
    dynamic password}){

    TextEditingController _controller =  TextEditingController();

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) => Container(
        height: ResponsiveFlutter.of(context).hp(80),
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(0.0),
            topRight: const Radius.circular(0.0),
          ),
        ),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.all(15.0),
                height: ResponsiveFlutter.of(context).hp(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      child: Image.asset(R.images.ic_vertical_logo),
                      height: ResponsiveFlutter.of(context).hp(40.0),
                      width: ResponsiveFlutter.of(context).wp(40.0),
                    ),

                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                        return false;
                      },
                      child: SizedBox(
                        child: Icon(Icons.close),
                        height: ResponsiveFlutter.of(context).hp(10.0),
                        width: ResponsiveFlutter.of(context).wp(5.0),
                      ),
                    ),

                  ],
                ),
              ),


              SizedBox(height: ResponsiveFlutter.of(context).hp(1.3),),

              Container(
                margin: EdgeInsets.all(15.0),
                child: Row(
                  children: [
                    SizedBox(width: 10,),
                    Text("Phone number verification", style:
                    TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(2.3,),
                        color: R.color.black_color,fontWeight: FontWeight.w900,fontFamily: "ralewayMedium"),),
                    SizedBox(width: 10,),
                    Text("",style:
                    TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.7,),
                        color: R.color.black_color,fontWeight: FontWeight.w500,fontFamily: "ralewayMedium"),),
                    SizedBox(width: 10,),
                    Text("",style:
                    TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(2.3,),
                        color: R.color.black_color,fontWeight: FontWeight.w900,fontFamily: "ralewayMedium"),)
                  ],
                ),
              ),

              SizedBox(height: ResponsiveFlutter.of(context).hp(1.5),),

              Container(
                width: double.infinity,
                margin: new EdgeInsets.only(left: 25.0, right: 25.0),
                color: Colors.white,
                child: new TextFormField(
                  controller: _controller,
                  autofocus: true,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter the 6 digit OTP';
                    }
                  },
                  style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                      fontWeight: FontWeight.w500,fontFamily: "openSans"),
                  keyboardType: TextInputType.number,
                  maxLength: 6,
                  decoration: new InputDecoration(
                      contentPadding: const EdgeInsets.all(12.0),
                      border: new OutlineInputBorder(
                          borderSide:
                          new BorderSide(color: Color(0xFFE0E0E0), width: 0.1)),
                      fillColor: Colors.white,
                      prefixIcon: Container(
                        width: 30.0,
                        margin:  EdgeInsets.all(10.0),
                        decoration:  BoxDecoration(
                          color: Colors.white,
                          border: Border(
                            right: BorderSide(width: 0.5, color: Colors.grey),
                          ),
                        ),
                        child: Center(child: Padding(padding:EdgeInsets.only(right: 5.0),child: Text("OTP",
                          style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                              fontWeight: FontWeight.w500,fontFamily: "openSans"),))),
                      ),
                      hintText: 'Enter 6 digit OTP*',
                      labelText: 'OTP*'),
                ),
              ),

              SizedBox(height: ResponsiveFlutter.of(context).hp(0.5),),

              Container(
                margin: new EdgeInsets.only(left: 25.0, top: 10.0),
                child: Row(
                  children: [
                    Text("Enter the code sent to",
                      style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                          fontWeight: FontWeight.w500,color: R.color.black_color,fontFamily: "openSans"),),
                    Text(" +91-${mobile} ",
                      style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                          fontWeight: FontWeight.w600,color: R.color.app_color,fontFamily: "openSans"),),
                    Text("\n",
                      style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                          fontWeight: FontWeight.w500,color: R.color.black_color,fontFamily: "openSans"),),
                  ],
                ),
              ),

              InkWell(
                onTap: () async {



                },
                child:  Container(
                  margin: new EdgeInsets.only(left: 25.0,top: 1.5),
                  child:Text("",
                    style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                      fontWeight: FontWeight.w600,color: R.color.app_color,fontFamily: "openSans",
                      decoration:
                      TextDecoration.underline,
                      height: 1.5,
                    ),
                  ),

                ),
              ),


              SizedBox(height: ResponsiveFlutter.of(context).hp(3.0),),

              Container(
                margin: new EdgeInsets.only(left: 25.0,right: 25),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: R.color.button,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    minimumSize: Size(double.infinity, ResponsiveFlutter.of(context).hp(5.0)), // double.infinity is the width and 30 is the height
                  ),
                  onPressed: () async{
                    if(_controller.text.isEmpty && _controller.text.length != 6){
                      Fluttertoast.showToast(
                          msg: "Please enter the 6 digit otp",
                          toastLength: Toast.LENGTH_LONG,
                          gravity: ToastGravity.BOTTOM,
                          backgroundColor: R.color.errorColor,
                          textColor: Colors.white,
                          fontSize: 16.0);

                    } else if(!otp.toString().trim().contains(_controller.text.trim())){
                      Fluttertoast.showToast(
                          msg: " OTP is not valid",
                          toastLength: Toast.LENGTH_LONG,
                          gravity: ToastGravity.BOTTOM,
                          backgroundColor: R.color.errorColor,
                          textColor: Colors.white,
                          fontSize: 16.0);

                    } else{
                      AppUtil.showLoadingProgress(context);
                     var res =  await Provider.of<AuthProvider>(context, listen: false)
                          .userRegistration(name: name,
                     password: password,
                     mobile: mobile,
                     email: email);

                          Navigator.of(context).pop();
                     if(res){
                       Navigator.of(context).pop();
                       Navigator.of(context).pop();
                       Navigator.pushNamed(context, StringConfig.LOGIN_PAGE);

                     }



                    }
                  },
                  child: Text('Verify'.toUpperCase(),style:
                  TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(2.0,),
                      color: R.color.white_color,fontWeight: FontWeight.bold,fontFamily: "ralewayMedium"),),
                ),
              ),

              SizedBox(height: ResponsiveFlutter.of(context).hp(3.5)),



            ],
          ),
        ),
      ),
    );

  }

}