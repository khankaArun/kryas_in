
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:krays_in/view/user_auth/sign_up_bottam_sheet.dart';
import 'package:provider/provider.dart';

class SingupPage extends StatefulWidget {
  String mobileNumber;

  SingupPage({this.mobileNumber});

  @override
  _SingupPageState createState() => _SingupPageState();
}

class _SingupPageState extends State<SingupPage> {

  final _formKey = GlobalKey<FormState>();

  TextEditingController _confirmPassword =  new TextEditingController();
  TextEditingController _firstName =  new TextEditingController();
  TextEditingController _mobileNo =  new TextEditingController();
  TextEditingController _email =  new TextEditingController();
  bool _passwordObscureText =  true;
  bool _confirmPasswordObscureText =  true;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    setState(() {

      if(AppUtil.isPhone(widget.mobileNumber.toString())){
        _mobileNo.text =  widget.mobileNumber.toString();
      }else{
        _email.text = widget.mobileNumber.toString();
      }

    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    SizeConfig().init(context);
    return Scaffold(
        backgroundColor: R.color.white_color,
      body: Consumer<AuthProvider>(
        builder: (context, provider, child) {
           return  Container(
             height: height,
             width: width,
             margin: EdgeInsets.all(20.0),
             child: SingleChildScrollView(
               child: Form(
                 key: _formKey,
                 child: Column(
                   mainAxisAlignment: MainAxisAlignment.center,
                   children: [
                     SizedBox(
                       height: SizeConfig.verticalBloc * 5,
                     ),

                     Align(
                       alignment: Alignment.topLeft,
                       child: InkWell(
                         onTap: (){
                           Navigator.of(context).pop();
                         },
                         child: Icon(Icons.arrow_back,color: R.color.black_color,size: 30.0,),
                       ),
                     ),

                     SizedBox(
                       height: SizeConfig.verticalBloc * 8,
                     ),

                     Container(
                       margin: new EdgeInsets.only(left: 15.0, right: 15.0),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: [
                           Text('Register New Account',
                             style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(2.3),
                                 fontWeight: FontWeight.w900,color: R.color.black_color),),
                         ],
                       ),
                     ),
                     SizedBox(height: ResponsiveFlutter.of(context).hp(3.5),),

                     Container(
                       width: double.infinity,
                       margin: new EdgeInsets.only(left: 15.0, right: 15.0),
                       color: Colors.white,
                       child: new TextFormField(
                         controller: _firstName,
                         validator: (text) {
                           if (text == null || text.isEmpty) {
                             return 'Enter Name';
                           }
                           return null;
                         },
                         style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                             fontWeight: FontWeight.w500,fontFamily: "openSans"),
                         decoration: new InputDecoration(
                             contentPadding: const EdgeInsets.all(12.0),
                             border: new OutlineInputBorder(
                                 borderSide:
                                 new BorderSide(color: Color(0xFFE0E0E0), width: 0.1)),
                             fillColor: Colors.white,
                             hintText: 'Name*',
                             labelText: 'Name*'),
                       ),
                     ),

                     SizedBox(height: ResponsiveFlutter.of(context).hp(3.5),),

                     Container(
                       width: double.infinity,
                       margin: new EdgeInsets.only(left: 15.0, right: 15.0),
                       color: Colors.white,
                       child: new TextFormField(
                         controller: _email,
                         keyboardType: TextInputType.emailAddress,
                         validator: (text) {
                           if (text == null || !AppUtil.isEmail(_email.text.trim())) {
                             return 'Enter Valid Email ID';
                           }
                           return null;
                         },
                         style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                             fontWeight: FontWeight.w500,fontFamily: "openSans"),
                         decoration: new InputDecoration(
                             contentPadding: const EdgeInsets.all(12.0),
                             border: new OutlineInputBorder(
                                 borderSide:
                                 new BorderSide(color: Color(0xFFE0E0E0), width: 0.1)),
                             fillColor: Colors.white,
                             hintText: 'Email ID*',
                             labelText: 'Email ID*'),
                       ),
                     ),

                     SizedBox(height: ResponsiveFlutter.of(context).hp(3.5),),

                     Container(
                       width: double.infinity,
                       margin: new EdgeInsets.only(left: 15.0, right: 15.0),
                       color: Colors.white,
                       child: new TextFormField(
                         controller: _mobileNo,
                        maxLength: 10,
                        // enabled: false,
                         validator: (text) {
                           if (text == null || text.isEmpty || !AppUtil.isPhone(_mobileNo.text.trim())) {
                             return 'Enter Mobile Number';
                           }
                           return null;
                         },
                         style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                             fontWeight: FontWeight.w500,fontFamily: "openSans"),
                         keyboardType: TextInputType.number,
                         decoration: new InputDecoration(
                             contentPadding: const EdgeInsets.all(12.0),
                             border: new OutlineInputBorder(
                                 borderSide:
                                 new BorderSide(color: Color(0xFFE0E0E0), width: 0.1)),
                             fillColor: Colors.white,
                             hintText: 'Mobile Number*',
                             labelText: 'Mobile Number*'),
                       ),
                     ),

                     SizedBox(height: ResponsiveFlutter.of(context).hp(3.5),),
                     Container(
                       width: double.infinity,
                       margin: new EdgeInsets.only(left: 15.0, right: 15.0),
                       color: Colors.white,
                       child: new TextFormField(

                         inputFormatters: [
                           new LengthLimitingTextInputFormatter(12), /// here char limit is 5
                         ],

                         controller: _confirmPassword,
                         obscureText: _passwordObscureText,
                         validator: (text) {
                           if (text == null || text.isEmpty) {
                             return 'Please Enter Valid Password';
                           }
                           return null;
                         },
                         style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                             fontWeight: FontWeight.w500,fontFamily: "openSans"),
                         decoration: new InputDecoration(
                             contentPadding: const EdgeInsets.all(12.0),
                             border: new OutlineInputBorder(
                                 borderSide:
                                 new BorderSide(color: Color(0xFFE0E0E0), width: 0.1)),
                             fillColor: Colors.white,
                             hintText: 'Password*',
                             suffixIcon: IconButton(
                               icon: Icon(
                                 // Based on passwordVisible state choose the icon
                                 _passwordObscureText ?  Icons.visibility_off : Icons.visibility ,
                                 color:  _passwordObscureText ? R.color.grey_color : R.color.app_color,
                               ),
                               onPressed: () {
                                 // Update the state i.e. toogle the state of passwordVisible variable
                                 setState(() {
                                   _passwordObscureText ? _passwordObscureText = false : _passwordObscureText = true;
                                 });
                               },
                             ),
                             labelText: 'Password*'),
                       ),
                     ),
                     SizedBox(height: ResponsiveFlutter.of(context).hp(3.5),),

                     Container(
                       width: double.infinity,
                       margin: new EdgeInsets.only(left: 15.0, right: 15.0),
                       color: Colors.white,
                       child: new TextFormField(

                         inputFormatters: [
                           new LengthLimitingTextInputFormatter(12), /// here char limit is 5
                         ],


                         obscureText: _confirmPasswordObscureText,
                         validator: (text) {
                           if (text == null || text.isEmpty || _confirmPassword.text.toString() != text.toString()) {
                             return 'Don\'t Match Password and Confirm Password';
                           }
                           return null;
                         },
                         style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                             fontWeight: FontWeight.w500,fontFamily: "openSans"),
                         decoration: new InputDecoration(
                           contentPadding: const EdgeInsets.all(12.0),
                           border: new OutlineInputBorder(
                               borderSide:
                               new BorderSide(color: Color(0xFFE0E0E0), width: 0.1)),
                           fillColor: Colors.white,
                           hintText: 'Confirm Password*',
                           suffixIcon: IconButton(
                             icon: Icon(
                               // Based on passwordVisible state choose the icon
                               _confirmPasswordObscureText ?  Icons.visibility_off : Icons.visibility ,
                               color:  _confirmPasswordObscureText ? R.color.grey_color : R.color.app_color,
                             ),
                             onPressed: () {
                               // Update the state i.e. toogle the state of passwordVisible variable
                               setState(() {
                                 _confirmPasswordObscureText ? _confirmPasswordObscureText = false : _confirmPasswordObscureText = true;
                               });
                             },
                           ),
                           labelText: 'Confirm Password*',),
                       ),
                     ),

                     SizedBox(height: ResponsiveFlutter.of(context).hp(3.5),),

                     Container(
                       margin: new EdgeInsets.only(left: 15.0,right: 15),
                       child: ElevatedButton(
                         style: ElevatedButton.styleFrom(
                           primary: R.color.button,
                           shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(8),
                           ),
                           minimumSize: Size(double.infinity, ResponsiveFlutter.of(context).hp(5.0)), // double.infinity is the width and 30 is the height
                         ),
                         onPressed: () async{
                           if (_formKey.currentState.validate()) {
                             print("Hello");

                             AppUtil.showLoadingProgress(context);
                             var res = await provider.otpValidator(_mobileNo.text.toString());
                             Navigator.of(context).pop();

                             if(res){



                                  SignUpBottom.showOtp(context :context,
                                  mobile: provider.otpAuthItem.mobile.trim(),
                                  whereToComeFrom: WhereItComeFrom.SIGNUP.toString(),
                                  otp: provider.otpAuthItem.otp,
                                  name: _firstName.text.trim(),
                                  email: _email.text.trim(),
                                  password: _confirmPassword.text.toString()
                                );



                             }else{
                               AppUtil.showToast(provider.otpAuthItem.message);
                             }

                           }
                         },
                         child: Text(R.string.singup.toUpperCase(),style:
                         TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(2.0,),
                             color: R.color.white_color,fontWeight: FontWeight.bold),),
                       ),
                     ),

                   ],
                 ),
               ),
             ),
           );
        })

    );
  }
}
