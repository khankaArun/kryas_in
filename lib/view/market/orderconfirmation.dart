import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/view/market/checkout.dart';
import 'package:number_to_words/number_to_words.dart';
import 'package:page_transition/page_transition.dart';

class OrderConfirmation extends StatefulWidget {
  var slotTime;
  OrderConfirmation(this.slotTime);

  @override
  State<OrderConfirmation> createState() => _OrderConfirmationState(slotTime);
}

class _OrderConfirmationState extends State<OrderConfirmation> {

  var slotTime;
  _OrderConfirmationState(this.slotTime);

  int ruppes = 20;
  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    final orientation = MediaQuery.of(context).orientation;
    double width = MediaQuery.of(context).size.width;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar:  AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 1,
          titleSpacing: 0,
          centerTitle: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Confirm your Order",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: ResponsiveFlutter.of(context).fontSize(2),
                fontWeight: FontWeight.w900,
                letterSpacing: 1.5,
                color: R.color.black_color),),

          actions: [
            SizedBox(
              width:SizeConfig.horizontalBloc * 7,
              height: SizeConfig.horizontalBloc * 7,
              child: InkWell(
                onTap: (){
                  //   Navigator.push(context, MaterialPageRoute(builder: (context)=>SearchKeyWordPage()));
                },
                //  child: Image.asset(R.images.search_icon),
              ),
            ),

            SizedBox(width: SizeConfig.horizontalBloc * 5),




            /*   SizedBox(
            width:SizeConfig.horizontalBloc * 7,
            height: SizeConfig.horizontalBloc * 7,
            child: InkWell(
              onTap: (){
                // Navigator.push(context, MaterialPageRoute(builder: (context)=>NotificationPage()));
              },
              child: Image.asset(R.images.notification_icon),
            ),
          ),*/

          ],
        ),
        bottomNavigationBar:   Material(
          elevation: 5.0,
          //borderRadius: BorderRadius.circular(30.0),
          child: InkWell(
            onTap: () {
              Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: CheckOut(slotTime==null?"0":slotTime)));

            },
            child: Container(
              margin: EdgeInsets.only(bottom: 10 ,left: 10,right: 10),
              width: width - 40.0,
              height: 43,
              padding: EdgeInsets.all(5.0),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: R.color.app_color,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Text(
                'Proceed',
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Jost',
                    letterSpacing: 0.7,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Center(
                child: Container(
                  margin: EdgeInsets.only(top: 50),

                  height: 220,

                  // margin: EdgeInsets.only(bottom: 370),

                  child: new Image(
                      fit: BoxFit.cover,
                      image: new AssetImage(R.images.ic_order_confirmation)),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                child: Text(
                  "₹$ruppes",
                  style: TextStyle(color: Colors.blue, fontSize: 28),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5,left: 10,right: 10),
                child: Text(
                  NumberToWord().convert('en-in', ruppes).toUpperCase() +
                      "RUPEES",
                  style: TextStyle(letterSpacing: 1,color: Colors.grey, fontSize: 12,fontWeight: FontWeight.bold,),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 2),
                child: Text(
                  "Confirm your Order",
                  style: TextStyle(letterSpacing: 1,color: Colors.black, fontSize: 16,fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5),
                child: Text(
                  "This amount will be refunded after the order is completed.",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey, fontSize: 15,letterSpacing: 0.5,),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
