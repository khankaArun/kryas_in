
/*import 'package:ecom_app/model/product_detail_model.dart';
import 'package:ecom_app/model/product_list_model.dart';
import 'package:ecom_app/provider/AuthProvider.dart';
import 'package:ecom_app/provider/cart_provider.dart';
import 'package:ecom_app/provider/product_detail_provider.dart';
import 'package:ecom_app/provider/wishlist_provider.dart';
import 'package:ecom_app/services/service.dart';
import 'package:ecom_app/until/LoginBottomSheet.dart';
import 'package:ecom_app/until/ResponsiveFlutter.dart';
import 'package:ecom_app/until/api.dart';
import 'package:ecom_app/until/api_variable.dart';
import 'package:ecom_app/until/common.dart';
import 'package:ecom_app/until/constant.dart';
import 'package:ecom_app/until/images.dart';
import 'package:ecom_app/until/sizeconfig.dart';
import 'package:ecom_app/view/mobile_view/zoom_image.dart';*/


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:krays_in/model/related_model_item.dart';
import 'package:krays_in/provider/CartProvider.dart';
import 'package:krays_in/provider/ProductListingProvider.dart';

import 'package:krays_in/provider/product_detail_provider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/images.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/view/market/app_foter.dart';
import 'package:provider/provider.dart';

import 'cart_page.dart';
import 'market_listing_page.dart';

/*
import 'cart_page.dart';
import 'check_service_available.dart';
*/

class ProductDetailPage extends StatefulWidget {

  var id;

  ProductDetailPage(this.id);


  @override
  _ProductDetailPageState createState() => _ProductDetailPageState(id);
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  final _controller = ScrollController();

  bool isDescriptionClicked=true;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  var id;  _ProductDetailPageState(this.id);


  Future<Null> _refresh() async {
    //Holding pull to refresh loader widget for 2 sec.
    //You can fetch data from server.
    await new Future.delayed(const Duration(seconds: 2));

     Provider.of<ProductDetailProvider>(context,listen: false)
        .getProductDetail(id,context);


    return null;
  }


  @override
  Widget build(BuildContext context) {
    var cartCount =Provider.of<CartProvider>(context, listen: true).cartCount;
    var isItemAdded =Provider.of<ProductDetailProvider>(context, listen: true).productDetail.data.isCart.toString();
    double width = MediaQuery.of(context).size.width;
    //print("db "+cartCount.toString());
    return  Scaffold(
      backgroundColor: R.color.grey_background,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0.5,
        titleSpacing: 0,
        centerTitle: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(R.string.product_detail,
          textAlign: TextAlign.left,
          style: TextStyle(
            fontSize: ResponsiveFlutter.of(context).fontSize(2),
            fontWeight: FontWeight.w900,
              color: R.color.black_color,
              letterSpacing: 1.5,),),
        actions: [
          Padding(padding: const EdgeInsets.all(10.0),
              child: new Container(
                  width:SizeConfig.horizontalBloc * 8,
                  height: SizeConfig.horizontalBloc * 8,
                  child: new GestureDetector(
                    onTap: () {
                     // Provider.of<CartProvider>(context,listen: false).getCartItem(context);
                     // Navigator.push(context, MaterialPageRoute(builder: (context)=>CartPage()));

                      if(cartCount !=null ||cartCount !=0){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>CartPage()));
                      }

                    },
                    child: new Stack(
                      children: <Widget>[
                       // Image.asset(R.images.cart_icon),
                        Icon(Icons.shopping_cart_outlined,size: 28, ),

                        Positioned(
                            child: new Stack(
                              children: <Widget>[
                                new Icon(
                                    Icons.brightness_1,
                                    size: 20.0, color: R.color.app_color),
                                Positioned(
                                    top: 3.0,
                                    left: 3.0,
                                    right: 4.0,
                                    child: new Center(
                                      child: new Text(
                                        /*cartCount.toString()*/cartCount.toString(),
                                        style: new TextStyle(
                                            color: Colors.white,
                                            fontSize: 11.0,
                                            fontWeight: FontWeight.w500
                                        ),
                                      ),
                                    )),
                              ],
                            )),
                      ],
                    ),
                  )
              )),

        ],
      ),
      bottomNavigationBar:   Material(
        elevation: 5.0,
        //borderRadius: BorderRadius.circular(30.0),
        child: InkWell(
          onTap: () async {

            if(isItemAdded!="0"){
              if(cartCount !=null ||cartCount !=0){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>CartPage()));
              }
              return;
            }

            /*dynamic _weight = "";

            get weight => _weight;
            dynamic _unit = "";

            get unit => _unit;
            dynamic _salePrice = "";

            get salePrice => _salePrice;
*/

             ProductDetailProvider detailsProvider  =await  Provider.of<ProductDetailProvider>(context, listen: false);
            var isItemSaved =await  Provider.of<CartProvider>(context, listen: false).doSaveCart(context: context,
              productId:detailsProvider.id,
              qty:detailsProvider.qty,
              unit:detailsProvider.unit,
                price :detailsProvider.salePrice,
                weight: detailsProvider.weight,
                variantId: detailsProvider.varientId,


            );


            if(isItemSaved){
              AppUtil.showToast("Item saved in your cart");
              detailsProvider.productDetail.data.isCart="1";
            }
            // Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: PaymentPage()));
          },
          child: Container(
            margin: EdgeInsets.only(bottom: 10 ,left: 10,right: 10),
            width: width - 40.0,
            height: 43,
            padding: EdgeInsets.all(5.0),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: R.color.app_color,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Text(
              isItemAdded!="0"? 'Go To Cart': "Add To Cart",
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Jost',
                  letterSpacing: 0.7,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        key: _refreshIndicatorKey,
        child: Consumer<ProductDetailProvider>(
            builder: (context, provider, child) {
              return Stack(
                children: [
                  Container(
                    child: SingleChildScrollView(
                      controller: _controller,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _imageList(provider),
                          _prductNamePrice(provider),

                          SizedBox(
                            height: ResponsiveFlutter.of(context).hp(0.7),
                          ),

                          _unitList(provider),

                          SizedBox(
                            height: ResponsiveFlutter.of(context).hp(0.7),
                          ),

                          _productDetails( provider),

                          SizedBox(
                            height: ResponsiveFlutter.of(context).hp(0.7),
                          ),

                          if(!isDescriptionClicked)
                            _reviewAndRating(),


                          SizedBox(
                            height: ResponsiveFlutter.of(context).hp(0.7),
                          ),

                          provider.relatedProduct!= null
                              ? Container(
                            color: R.color.white_color,
                            padding: EdgeInsets.only(left: 5.0, right: 0.0, top: 10.0),
                            width: double.infinity,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    margin: EdgeInsets.only(
                                        top    : SizeConfig.verticalBloc * 2,
                                        left   : SizeConfig.verticalBloc * 2,
                                        bottom : SizeConfig.verticalBloc * 0,
                                        right  : SizeConfig.verticalBloc * 2
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(child: Text("Top Selling",
                                          style: TextStyle(
                                              fontFamily: 'openSans',
                                              // fontSize: SizeConfig.normalFontSize,
                                              fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                              fontWeight: FontWeight.w600,
                                              color: R.color.black_color),
                                        ),),


                                        SizedBox(
                                          height: SizeConfig.safeBlockVertical * 3.4,
                                          child: ElevatedButton(
                                            style: ButtonStyle(
                                                foregroundColor: MaterialStateProperty.all<Color>(R.color.white_color),
                                                backgroundColor: MaterialStateProperty.all<Color>(R.color.app_color),
                                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                    RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.circular(18.0),
                                                        side: BorderSide(color: R.color.app_color)
                                                    )
                                                )
                                            ),
                                            onPressed: () async{

                                              AppUtil.showLoadingProgress(context);
                                              var myProductList =await  Provider.of<ProductListingProvider>(context, listen: false).getProductList(context: context,
                                                  categoryId: provider.id,
                                                  subCategoryId: provider.subId,
                                                  count: 100,
                                                  page: 1,
                                                  search: ""

                                              );
                                              Navigator.of(context).pop();

                                              if(myProductList){
                                                Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage(myTitle: "Top Selling",
                                                  isFruits: false,
                                                  isVegetables: false,
                                                  isOthers: true,
                                                  id:   provider.id,
                                                  subCatId:provider.subId,)));

                                              }


                                            },
                                            child: Text("View All".toUpperCase(), style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.1), fontWeight: FontWeight.w600),),
                                          ),
                                        )
                                      ],
                                    )
                                ),
                                Container(
                                  margin: EdgeInsets.only(/*bottom: 20.0,*/top: 10),
                                  child:Container(
                                    height: SizeConfig.verticalBloc * 28,
                                    child: provider.relatedProduct.data !=null || provider.relatedProduct.data.length != 0 ?ListView.builder(
                                        itemCount: provider.relatedProduct.data.length,
                                        scrollDirection: Axis.horizontal,
                                        itemBuilder: (context, index) {
                                          return _relatedProduct(provider.relatedProduct.data,index);
                                        }): SizedBox.shrink(),
                                  ),
                                ),
                              ],
                            ),
                          )
                              : SizedBox.shrink(),

                          SizedBox(
                            height: ResponsiveFlutter.of(context).hp(0.7),
                          ),

                          AppFoter(provider.productDetail.data.disclaimer)
                          //Common.appFooter(context),

                        ],
                      ),
                    ),
                  ),

                ],
              );
            }
        ),
      ),
    );
  }

  Widget _imageList(ProductDetailProvider provider) {
    return provider.productDetail.data.images !=  null ?Container(
      height: ResponsiveFlutter.of(context).hp(40),
      child: Container(
        color: R.color.white_color,
        child: Swiper(
          itemBuilder: (BuildContext context, int index){
          //  print("Procudt Detail Page Url "+BannerBaseUrl.DeatilPageImage+provider.productDetail.imagesItems[index].imageIds.replaceAll(" ", "%20"));
            return Container(
              padding: EdgeInsets.only(bottom: ResponsiveFlutter.of(context).hp(4)),
              child:
              Stack(
                children: [
                  Center(
                    child: InkWell(
                      onTap: (){
                        //Navigator.push(context, MaterialPageRoute(builder: (context)=>ZoomImage(imagesItems: provider.productDetail.imagesItems,clickedImage:provider.productDetail.imagesItems[index].imageIds)));
                      },

                      child: Container(
                        //  padding: EdgeInsets.only(left: 10,right: 10),
                        height: ResponsiveFlutter.of(context).hp(90),
                        width: double.maxFinite,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(provider.productDetail.data.images[index].replaceAll(" ", "%20")),
                              fit: BoxFit.fitHeight),
                        ),

                      ),
                    ),
                  ),

                ],
              ),
            );
          },
          pagination: SwiperPagination(
              builder: new DotSwiperPaginationBuilder(
                  color: Colors.grey, activeColor: Colors.blue)
          ),
          autoplay: false,
          itemCount: provider.productDetail.data.images.length,
          control: null,

          // duration: 1000,
          scrollDirection: Axis.horizontal,
        ),
      ),
    ) : SizedBox.shrink();
  }

  Widget _prductNamePrice(ProductDetailProvider provider){
    return Container(
      color: R.color.white_color,
      width: double.infinity,
      child: Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 5.0, bottom: 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [

                SizedBox(
                  width:  SizeConfig.horizontalBloc * 55,
                  child:  Text(provider.productDetail.data.name,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontFamily: 'openSans',
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
                      color: R.color.black_color,

                    ),

                  ),
                ),

                Container(
                  child: Row(
                    children: [
                      GestureDetector(
                        child: Container(
                          height:17,
                          width: 17,
                          padding: const EdgeInsets.all(1.0),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: R.color.app_color,
                          ),
                          child: Icon(
                            Icons.remove,
                            size: 13.0,
                            color: Colors.white,
                          ),
                        ),
                        onTap: () async{

                          int quntity = provider.qty;
                          quntity -= 1;
                          if(quntity == 0){

                          }else{
                            provider.updateQty(context,
                                quntity,provider.weight==null?provider.productDetail.data.weight:provider.weight,
                                provider.varientId);

                          }

                        },
                      ),

                      Container(
                        margin: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: Text("  "+provider.qty.toString()+"  ",
                          style: TextStyle(
                              fontSize: SizeConfig.headerFontSize,
                              fontWeight: FontWeight.w700,
                              color: R.color.black_color),),
                      ),

                      GestureDetector(
                        child: Container(
                          height:17,
                          width: 17,
                          padding: const EdgeInsets.all(1.0),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: R.color.app_color,
                          ),
                          child: Icon(
                            Icons.add,
                            size: 13.0,
                            color: Colors.white,
                          ),
                        ),
                        onTap: () async{

                          int quntity = provider.qty;
                          quntity += 1;
                          provider.updateQty(context,
                            quntity,provider.weight==null?provider.productDetail.data.weight:provider.weight,provider.varientId
                          );

                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),

            SizedBox(
              height: ResponsiveFlutter.of(context).hp(0.2),
            ),

            Container(
              // margin: EdgeInsets.only(left: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(R.priceSymbol.rupee+provider.productDetail.data.salePrice+"/"+provider.productDetail.data.unit , style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                      
                      fontWeight: FontWeight.w700,
                      color: R.color.black_color),),

                  SizedBox(
                    width: ResponsiveFlutter.of(context).wp(2),
                  ),

                  Text(R.priceSymbol.rupee+provider.productDetail.data.price, style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                      fontWeight: FontWeight.w500,
                      
                      decoration: TextDecoration.lineThrough,
                      decorationThickness: 1.85,
                      color: R.color.grey_color),),

                  SizedBox(
                    width: ResponsiveFlutter.of(context).wp(2),
                  ),

                  Text("("+provider.productDetail.data.disPer.toString()+R.priceSymbol.off+")", style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.6),
                      
                      color: R.color.app_color),),
                ],
              ),
            ),

            SizedBox(
              height: ResponsiveFlutter.of(context).hp(0.2),
            ),

          ],
        ),
      ),
    );
  }


  Widget _unitList(ProductDetailProvider productDetail) {

    return Container(
      color: R.color.white_color,
      padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10.0, top: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Text("Unit ",
            style: TextStyle(
                fontSize: ResponsiveFlutter.of(context).fontSize(2),
                fontWeight: FontWeight.w600,
                
                color: Colors.black),),
          SizedBox(height: ResponsiveFlutter.of(context).hp(2),),

          Container(
            height: ResponsiveFlutter.of(context).hp(8),
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemCount: productDetail.productDetail.data.unitVarients.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () async{
                      productDetail.changeUnit(context,index);
                      //provider.setColor(index);
                     // var provider =  await Provider.of<AuthProvider>(context, listen: false);
                      /*if(productDetail.colors[index].isSelected){
                        showSizeBottomSheet(context,index,productDetail,provider);
                      }*/

                    },
                    child: Container(
                      margin: EdgeInsets.all(5.0),
                      decoration: productDetail.productDetail.data.unitVarients[index].isUnitSelected == false ? BoxDecoration(
                        border: Border.all(color:Colors.grey.shade600,width: 1,),
                        borderRadius: BorderRadius.all( Radius.circular(ResponsiveFlutter.of(context).scale(5))),
                      )
                          : BoxDecoration(
                          color: R.color.app_color,
                          borderRadius: BorderRadius.all( Radius.circular(ResponsiveFlutter.of(context).scale(5)))
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                            child:  Text(" "+R.priceSymbol.rupee+productDetail.productDetail.data.unitVarients[index].salePrice+
                                "/\n"+productDetail.productDetail.data.unitVarients[index].weight.toString()+" "
                                +productDetail.productDetail.data.unitVarients[index].unit.toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                                  fontWeight: FontWeight.w700,

                                  color: productDetail.productDetail.data.unitVarients[index].isUnitSelected
                                      ? R.color.white_color
                                      : Colors.grey.shade600),
                            )
                        ),
                      ),
                    ),
                  );
                }),
          ),
          //SizedBox(height: ResponsiveFlutter.of(context).hp(3),),


          SizedBox(height: ResponsiveFlutter.of(context).hp(3),),


        ],
      ),
    );
  }

  Widget _productDetails( ProductDetailProvider provider) {

    return Container(
      color: R.color.white_color,
      width: double.infinity,
      padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 10.0, top: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Container(
            child: Container(
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        decoration: BoxDecoration(
                          color: isDescriptionClicked?R.color.app_color:R.color.grey_color,
                            borderRadius: BorderRadius.all( Radius.circular(0.0)),

                        ),
                        child: InkWell(
                          onTap: () async{
                            setState(() {

                              isDescriptionClicked=true;
                            });

                          },
                          child:  Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  /*SizedBox(
                                    width: ResponsiveFlutter.of(context).hp(3),
                                    height: ResponsiveFlutter.of(context).hp(3),
                                    child: Image.asset(Icon., color: R.color.grey_color,),
                                  ),*/

                                  SizedBox(width: ResponsiveFlutter.of(context).wp(2),),

                                  Text("Description",
                                    style: TextStyle(color: R.color.white_color,
                                        fontWeight: FontWeight.w800,
                                        letterSpacing: 1,
                                        fontSize: ResponsiveFlutter.of(context).fontSize(1.8)),),
                                ],
                              )),
                        ),
                        height: SizeConfig.verticalBloc * 6,
                      ),
                    ),



                    Expanded(
                      flex: 1,
                      child: Container(
                        decoration: BoxDecoration(
                            color: isDescriptionClicked?R.color.grey_color:R.color.app_color,
                            borderRadius: BorderRadius.all( Radius.circular(0.0))
                        ),
                        child: InkWell(
                          onTap: ()  async{

                            setState(() {

                              isDescriptionClicked=false;
                            });



                          },
                          child:  Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                /*  SizedBox(
                                    width: ResponsiveFlutter.of(context).hp(3),
                                    height: ResponsiveFlutter.of(context).hp(3),
                                    child: Image.asset(R.images.cart_icon, color: R.color.white_color,),
                                  ),
*/
                                  SizedBox(width: ResponsiveFlutter.of(context).wp(2),),
                                  Text("Reviews",
                                    style: TextStyle(color: R.color.white_color,
                                        fontWeight: FontWeight.w800,
                                        letterSpacing: 1.0,
                                        fontSize: ResponsiveFlutter.of(context).fontSize(1.8)),),
                                ],
                              )),
                        ),
                        height: SizeConfig.verticalBloc * 6,
                      ),
                    ),
                  ],
                )
            ),
          ),
          SizedBox(height: ResponsiveFlutter.of(context).hp(0.5),),


          SizedBox(height: ResponsiveFlutter.of(context).hp(1.2),),


          if(isDescriptionClicked)
            Html(data: "${provider.productDetail.data.desc}",
                defaultTextStyle: TextStyle(
                    fontSize: ResponsiveFlutter.of(context).fontSize(1.9),
                    //fontWeight: FontWeight.w500,
                    letterSpacing: 0.8,
                    color: Colors.grey))
         /* Text(provider.productDetail.data.desc,

            style: TextStyle(
              fontSize: ResponsiveFlutter.of(context).fontSize(1.9),
              //fontWeight: FontWeight.w500,
              letterSpacing: 0.8,
              color: Colors.grey),),*/

        ],
      ),
    );
  }


  Widget _reviewAndRating() {
    return Container(
      color: R.color.white_color,
      width: double.infinity,
      padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10.0, top: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(R.string.review_rating,
            style: TextStyle(
                fontSize: ResponsiveFlutter.of(context).fontSize(1.6),
                
                fontWeight: FontWeight.w700,

                color: R.color.black_color),),

          Container(
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: EdgeInsets.only(left: 5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text("4", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(6),
                                fontWeight: FontWeight.w600,
                                color: R.color.black_color),),

                            Container(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: Icon(Icons.star, color: R.color.green_color,
                                size: ResponsiveFlutter.of(context).scale(17.5),),
                            ),

                          ],
                        ),

                        Text("7 Verified Buyers", style: TextStyle(
                            fontSize: ResponsiveFlutter.of(context).fontSize(1.6),
                            fontWeight: FontWeight.w500,
                            color: R.color.black_color),),
                      ],
                    ),

                  ),
                ),

                Expanded(
                  flex: 3,
                  child: Container(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text("5", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),

                            Icon(Icons.star, color:  Colors.grey[400], size: ResponsiveFlutter.of(context).scale(16),),
                            Container(
                              margin: EdgeInsets.only(left: 5.0, right: 20.0),
                              color: R.color.green_color,
                              width: ResponsiveFlutter.of(context).wp(35),
                              height: 3.3,
                              child: Container(
                                margin: EdgeInsets.only(left: ResponsiveFlutter.of(context).wp(7*4)),
                                color: Colors.grey[400],
                              ),
                            ),
                            Text("4", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),
                          ],
                        ),

                        Row(
                          children: [
                            Text("4", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),

                            Icon(Icons.star, color:  Colors.grey[400], size: ResponsiveFlutter.of(context).scale(16),),
                            Container(
                              margin: EdgeInsets.only(left: 5.0, right: 20.0),
                              color: R.color.green_color,
                              width: ResponsiveFlutter.of(context).wp(35),
                              height: 3.3,
                              child: Container(
                                margin: EdgeInsets.only(left: ResponsiveFlutter.of(context).wp(7*1)),
                                color: Colors.grey[400],
                              ),
                            ),
                            Text("1", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),
                          ],
                        ),

                        Row(
                          children: [
                            Text("3", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),

                            Icon(Icons.star, color:  Colors.grey[400], size: ResponsiveFlutter.of(context).scale(16),),
                            Container(
                              margin: EdgeInsets.only(left: 5.0, right: 20.0),
                              color: R.color.green_color,
                              width: ResponsiveFlutter.of(context).wp(35),
                              height: 3.3,
                              child: Container(
                                margin: EdgeInsets.only(left: ResponsiveFlutter.of(context).wp(7*0)),
                                color: Colors.grey[400],
                              ),
                            ),
                            Text("0", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),
                          ],
                        ),

                        Row(
                          children: [
                            Text("5", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),

                            Icon(Icons.star, color:  Colors.grey[400], size: ResponsiveFlutter.of(context).scale(16),),
                            Container(
                              margin: EdgeInsets.only(left: 5.0, right: 20.0),
                              color: R.color.green_color,
                              width: ResponsiveFlutter.of(context).wp(35),
                              height: 3.3,
                              child: Container(
                                margin: EdgeInsets.only(left: ResponsiveFlutter.of(context).wp(7*4)),
                                color: Colors.grey[400],
                              ),
                            ),
                            Text("4", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),
                          ],
                        ),

                        Row(
                          children: [
                            Text("2", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),

                            Icon(Icons.star, color:  Colors.grey[400], size: ResponsiveFlutter.of(context).scale(16),),
                            Container(
                              margin: EdgeInsets.only(left: 5.0, right: 20.0),
                              color: Colors.orangeAccent,
                              width: ResponsiveFlutter.of(context).wp(35),
                              height: 3.3,
                              child: Container(
                                margin: EdgeInsets.only(left: ResponsiveFlutter.of(context).wp(7*1)),
                                color: Colors.grey[400],
                              ),
                            ),
                            Text("1", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),
                          ],
                        ),

                        Row(
                          children: [
                            Text("1", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),

                            Icon(Icons.star, color:  Colors.grey[400], size: ResponsiveFlutter.of(context).scale(16),),
                            Container(
                              margin: EdgeInsets.only(left: 5.0, right: 20.0),
                              color: Colors.redAccent,
                              width: ResponsiveFlutter.of(context).wp(35),
                              height: 3.3,
                              child: Container(
                                margin: EdgeInsets.only(left: ResponsiveFlutter.of(context).wp(7*0)),
                                color: Colors.grey[400],
                              ),
                            ),
                            Text("0", style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                fontWeight: FontWeight.w700,
                                color: R.color.black_color),),
                          ],
                        ),

                      ],
                    ),

                  ),
                ),
              ],
            ),
          ),

        ],
      ),
    );
  }



  Widget _relatedProduct(List<Data> data,int index){
    return InkWell(
      onTap: () async{
        AppUtil.showLoadingProgress(context);
        var res = await Provider.of<ProductDetailProvider>(context,listen: false)
            .getProductDetail(data[index].id,context);
        Navigator.of(context).pop();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ProductDetailPage(data[index].id)),
        );
      },
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(right: 1, top: 30),
              padding: EdgeInsets.only(left: 10),
              child: Column(
                children: [
                  SizedBox(
                   // height: SizeConfig.verticalBloc * 15,
                    width: SizeConfig.horizontalBloc * 30,
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.white,
                          width: 0.2,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      ),
                      child: Images.buildImage(data[index].image.toString().replaceAll(" ", "%20")),

                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.horizontalBloc * 2,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width/2.9,
                    child: Text(""+data[index].name,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,  textAlign: TextAlign.left, style: TextStyle(
                          fontSize: SizeConfig.smallFontSize,
                          fontWeight: FontWeight.bold,
                          color: R.color.black_color)
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.horizontalBloc * 1,
                  ),
              SizedBox(
                width: MediaQuery.of(context).size.width/2.9,
                child:  Text(R.priceSymbol.rupee+data[index].discountprice+"/"+data[index].unit,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,  textAlign: TextAlign.left,style: TextStyle(
                      fontSize: SizeConfig.smallFontSize,
                      fontWeight: FontWeight.w700,
                     // decoration: TextDecoration.lineThrough,
                      decorationThickness: 1.85,
                      color: R.color.grey_color),),
              )


                ],
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              child: Container(
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(5.0)),
                ),
                child: Text( ""+data[index].disPer  +R.priceSymbol.off,
                  style: TextStyle(color: Colors.white, fontSize: 12.0,fontFamily: 'Jost',
                    letterSpacing: 0.8,),
                ),
              ),
            ),
            Positioned(
              bottom  : 0,
              right   : 0,
              child: Container(
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  color: data[index].isCart=='0'?Colors.grey:Colors.blue,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(5.0)),
                ),
                child: SizedBox(
                  height: 20,
                  child:   Icon(Icons.shopping_cart_outlined,
                      size: 22.0, color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }


}
