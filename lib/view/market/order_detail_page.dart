
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:krays_in/model/order_lis_model.dart';
import 'package:krays_in/model/user_address.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/provider/order_provider.dart';
import 'package:krays_in/tovendor_rating.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/images.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import 'cart_page.dart';

class OrderDetailPage extends StatefulWidget {

  List<Data> _orderList;
  int index;
  UserAddressListModelItem userAddress;
  OrderDetailPage( this._orderList,this.index,this.userAddress);

  @override
  _OrderDetailPageState createState() => _OrderDetailPageState(_orderList,index,userAddress);
}

class _OrderDetailPageState extends State<OrderDetailPage> {

  TextEditingController _textFieldController =  new TextEditingController();


  final _buttonOptions = [
    TimeValue(30,  "Change my mind"),
    TimeValue(60,  "Wrong shippinng address"),
    TimeValue(120, "Duplicate order"),
    TimeValue(240, "Reason not listed"),
    TimeValue(480, "Other"),];


  List<Data> _orderList;
  int index;

  String imageTrackName="";
  UserAddressListModelItem userAddress;
  _OrderDetailPageState( this._orderList,this.index,this.userAddress);




    Widget _myImage (String orderStatus){

      print(orderStatus);

   /*   {
        "id": "1",
      "status": "Order Confirmed"
      },
      {
      "id": "2",
      "status": "In Processing"
      },
      {
      "id": "3",
      "status": "Shipped"
      },
      {
      "id": "8",
      "status": "Delivered"
      },

      {
      "orderStatus":"Cancelled",
      "status":"6"
      }


      */

      print("orderStatus"+orderStatus);

      if(orderStatus=="1"){
         return Image(
             image: new AssetImage(R.images.confirm));

      }else if(orderStatus=="2"){
        return Image(
            image: new AssetImage(R.images.placed));
      }else if(orderStatus=="3"){
        return   Image(
            image: new AssetImage(R.images.ready));
      }else if(orderStatus=="8"){
        return  Image(
            image: new AssetImage(R.images.completed));
      }else if(orderStatus=="6"){
        return  Image(
            image: new AssetImage(R.images.cancelled));
      }else if(orderStatus=="13"){
        return Image(
            image: new AssetImage(R.images.confirm));
      }else if(orderStatus=="12"){
        return Image(
            image: new AssetImage(R.images.ready));
      } else if(orderStatus=="9"){
        return Image(
            image: new AssetImage(R.images.ready));
      } else{
        return Image(
            image: new AssetImage(R.images.placed));
      }


    }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
        title: Text(R.string.order_detail,
          style: TextStyle(


              fontWeight: FontWeight.w700,

              fontSize: SizeConfig.headerFontSize,
              color: R.color.black_color),),
      ),
      body: Consumer<OrderProvider> (
        builder: (context, provider, child){
          return Container(
            margin: EdgeInsets.only(left: 10.0, right: 10.00),
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              "Order ID : 000"+_orderList[index].id,
                              style: TextStyle(color: Colors.black,
                                  fontSize: 14,
                                  letterSpacing: 0.3,
                              fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.only(left: 55),
                            child:  Text(
                              AppUtil.getDateTime(_orderList[index].createdAt),
                              style: TextStyle(color: Colors.grey, fontSize: 12),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    // InkWell(
                    //   onTap: (){
                    //     Navigator.push(context, MaterialPageRoute(builder: (context)=>ToVendorRating()));
                    //   },
                    //   child: Align(
                    //     alignment: Alignment.topRight,
                    //     child: Container(
                    //
                    //       margin: EdgeInsets.only(left: 20),
                    //       child: Text(
                    //         "Vendor Rating",
                    //         style: TextStyle(color: Colors.blue,
                    //           decoration: TextDecoration.underline,
                    //           decorationColor: Colors.blue,
                    //
                    //           fontSize: 14,
                    //           letterSpacing: 0.3,
                    //           fontWeight: FontWeight.bold,),
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    Container(
                      margin: EdgeInsets.only(left: 5, right: 5),
                      width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15),
                                    child: Text(
                                      "Name",
                                      style: TextStyle(
                                          letterSpacing: 0.5,
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15),
                                    child: Text(
                                      ""+userAddress.address[0].firstName+" " +userAddress.address[0].lastName,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 13,
                                          letterSpacing: 0.3,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15),
                                    child: Text(
                                      "Payment mode",
                                      style: TextStyle(
                                          letterSpacing: 0.5,
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15),
                                    child: Text(
                                      ""+_orderList[index].paymentMode,
                                      style: TextStyle(
                                          fontSize: 13,
                                          letterSpacing: 0.3,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15),
                                    child: Text(
                                      "Paid Amount",
                                      style: TextStyle(
                                          letterSpacing: 0.5,
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15),
                                    child: Text(
                                      R.priceSymbol.rupee+_orderList[index].payableAmt,
                                      style: TextStyle(
                                        fontSize: 13,
                                        letterSpacing: 0.3,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15),
                                    child: Text(
                                      "Discount",
                                      style: TextStyle(
                                          letterSpacing: 0.5,
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15),
                                    child: Text(
                                      R.priceSymbol.rupee+_orderList[index].totalDiscount.toString(),
                                      style: TextStyle(
                                        fontSize: 13,
                                        letterSpacing: 0.3,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),


                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15,),
                                    child: Text(
                                      "Total Amount",
                                      style: TextStyle(
                                          letterSpacing: 0.5,
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15),
                                    child: Text(
                                      R.priceSymbol.rupee+_orderList[index].totalAmt.toString(),
                                      style: TextStyle(
                                        fontSize: 13,
                                        letterSpacing: 0.3,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15,bottom: 10),
                                    child: Text(
                                      "Net Payable",
                                      style: TextStyle(
                                          letterSpacing: 0.5,
                                          color: Colors.blue, fontSize: 14,fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 15),
                                    child: Text(
                                      R.priceSymbol.rupee+netPayable(_orderList[index].netAmt,_orderList[index].payableAmt),
                                      style: TextStyle(
                                        fontSize: 14,
                                        letterSpacing: 0.3,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.blue,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),

                          ],
                        ),
                        elevation: 2,
                        shadowColor: Colors.grey,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.0),
                          ),
                          side: BorderSide(
                            color: Colors.grey.withOpacity(0.2),
                            width: 1,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),



                    if(_orderList[index].status=="8")
                      Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child:Container(
                                          margin: EdgeInsets.only(left: 10),

                                          child: Text(
                                            "Vendor Rating",
                                            style: TextStyle(color: Colors.black,
                                                fontSize: 14,
                                                letterSpacing: 0.3,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                              ),
                              Expanded(
                                flex: 1,
                                child:Container(

                                  margin: EdgeInsets.only(left: 40,right: 20),
                                  child: InkWell(
                                    onTap: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>ToVendorRating()));
                                    },
                                    child: Text(
                                      "Rate To Vendor",
                                      style: TextStyle(color: Colors.blue,
                                        decoration: TextDecoration.underline,
                                        decorationColor: Colors.blue,

                                        fontSize: 14,
                                        letterSpacing: 0.3,
                                        fontWeight: FontWeight.bold,),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),


                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 5, right: 5),
                            width: MediaQuery.of(context).size.width,
                           // height: 55,

                            child: Card(
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          margin: EdgeInsets.only(left: 10, top: 15),
                                          child: Text(
                                            "Vendor Name:",
                                            style: TextStyle(
                                                letterSpacing: 0.5,
                                                color: Colors.black54, fontSize: 14),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          margin: EdgeInsets.only(left: 40, top: 15),
                                          child: Text(
                                            ""+ provider.orderDetailList.data[0].vendorName,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 13,
                                                letterSpacing: 0.3,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),

                                    ],
                                  ),

                                 /* Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          margin: EdgeInsets.only(left: 10, top: 15),
                                          child: Text(
                                            ":",
                                            style: TextStyle(
                                                letterSpacing: 0.5,
                                                color: Colors.black54, fontSize: 14),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          margin: EdgeInsets.only(left: 40, top: 15),
                                          child: Text(
                                            ""+ provider.orderDetailList.data[0].vendorName,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 13,
                                                letterSpacing: 0.3,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),

                                    ],
                                  ),*/

                                  SizedBox(height: 13,)

                                ],
                              ),
                              elevation: 2,
                              shadowColor: Colors.grey,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(8.0),
                                ),
                                side: BorderSide(
                                  color: Colors.grey.withOpacity(0.2),
                                  width: 1,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                      // Row(
                      //   children: [
                      //     Expanded(
                      //       flex: 1,
                      //       child: Container(
                      //         margin: EdgeInsets.only(left: 10),
                      //
                      //         child: Text(
                      //           "vendor Rating",
                      //           style: TextStyle(color: Colors.black,
                      //               fontSize: 14,
                      //               letterSpacing: 0.3,
                      //               fontWeight: FontWeight.bold),
                      //         ),
                      //       ),
                      //     ),
                      //     InkWell(
                      //       onTap: (){
                      //
                      //       },
                      //       // child: Expanded(
                      //       //   flex: 1,
                      //       //   child: Container(
                      //       //
                      //       //     margin: EdgeInsets.only(left: 20,right: 20),
                      //       //     child: Text(
                      //       //       "Rate To Vendor",
                      //       //       style: TextStyle(color: Colors.blue,
                      //       //         decoration: TextDecoration.underline,
                      //       //         decorationColor: Colors.blue,
                      //       //
                      //       //         fontSize: 14,
                      //       //         letterSpacing: 0.3,
                      //       //         fontWeight: FontWeight.bold,),
                      //       //     ),
                      //       //   ),
                      //       // ),
                      //     ),
                      //   ],
                      // ),



                   new Divider(
                      color: Colors.grey.shade300,
                      thickness: 1,
                    ),


                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 60,
                          child:
                          Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Text(
                              "Order Tracking",
                              style: TextStyle(color: Colors.black,
                                  fontSize: 14,
                                  letterSpacing: 0.3,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 40,
                          child: Container(
                            margin: EdgeInsets.only(left: 45),
                            child: const Text(
                              "",
                              style: TextStyle(color: Colors.blue, fontSize: 14),
                            ),
                          ),
                        )
                      ],
                    ),


                    Container(
                      margin: EdgeInsets.only(left: 15, right: 30,top: 10),
                      child: _myImage(""+_orderList[index].status),
                    ),


                    SizedBox(
                      height: 15,
                    ),
                    new Divider(
                      color: Colors.grey.shade300,
                      thickness: 1,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 70,
                          child: Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Text(
                              "Delivery Time",
                              style: TextStyle(color: Colors.black,
                                  fontSize: 14,
                                  letterSpacing: 0.3,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      /*  Expanded(
                          flex: 30,
                          child: Container(
                            margin: EdgeInsets.only(left: 33),
                            child: const Text(
                              "Change",
                              style: TextStyle(color: Colors.blue,
                                 letterSpacing: 0.3,
                                  decoration: TextDecoration.underline,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),*/
                      ],
                    ),
                    SizedBox(height: 20),



                    Container(
                      child: Container(
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.black12,
                                      borderRadius: BorderRadius.all( Radius.circular(5.0))
                                  ),
                                  child: InkWell(
                                    onTap: () async{



                                    },
                                    child:  Center(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                         /*   SizedBox(
                                              width: ResponsiveFlutter.of(context).hp(3),
                                              height: ResponsiveFlutter.of(context).hp(3),
                                              child: Image.asset(R.images.whislist_icon, color: R.color.grey_color,),
                                            ),
*/
                                            SizedBox(width: ResponsiveFlutter.of(context).wp(2),),

                                            Text(""+myDate(_orderList[index].slotTimeFrom),
                                              style: TextStyle(color: R.color.black_color,
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: ResponsiveFlutter.of(context).fontSize(1.8)),),
                                          ],
                                        )),
                                  ),
                                  height: SizeConfig.verticalBloc * 6,
                                ),
                              ),

                              SizedBox(
                                width: ResponsiveFlutter.of(context).wp(3),
                              ),

                              Expanded(
                                flex: 1,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.black12,
                                      borderRadius: BorderRadius.all( Radius.circular(5.0))
                                  ),
                                  child: InkWell(
                                    onTap: ()  async{


                                    },
                                    child:  Center(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [

                                           /*
                                            SizedBox(
                                              width: ResponsiveFlutter.of(context).hp(3),
                                              height: ResponsiveFlutter.of(context).hp(3),
                                              child: Image.asset(R.images.cart_icon, color: R.color.white_color,),
                                            ),
*/
                                            SizedBox(width: ResponsiveFlutter.of(context).wp(2),),
                                            Text(""+myTime(_orderList[index].slotTimeFrom),
                                              style: TextStyle(color: R.color.black_color,
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: ResponsiveFlutter.of(context).fontSize(1.8)),),
                                          ],
                                        )),
                                  ),
                                  height: SizeConfig.verticalBloc * 6,
                                ),
                              ),
                            ],
                          )
                      ),
                    ),



                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Text(
                              "Item Details",
                              style: TextStyle(color: Colors.black,
                                  fontSize: 14,
                                  letterSpacing: 0.3,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.only(left: 45),
                            child: InkWell(
                                onTap: () async {

/*
                                  if(orderStatus.toLowerCase().contains("cancelled")||orderStatus.toLowerCase().contains("declined")
                                      ||orderStatus.toLowerCase().contains("delivered")){

                                    return "Re-Order";

                                  }else if(orderStatus.toLowerCase().contains("order confirmed") ||
                                  orderStatus.toLowerCase().contains("accepted")){
                                    return "Cancel Order";
                                  }
                                  */


                                  if( provider.orderList.data[index].orderStatus.toLowerCase().contains("cancelled")||
                                      provider.orderList.data[index].orderStatus.toLowerCase().contains("delivered")){

                                    AppUtil.showLoadingProgress(context);

                                    var res =  await provider.reOrder(context,
                                        provider.orderList.data[index].id);

                                    Navigator.of(context).pop();

                                    if(res){
                                      Navigator.of(context).pop();
                                      Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: CartPage()));
                                    }





                                  }else{
                                    bottomSheet( context);
                                  }

                                },
                                child:  Text(
                                 myString( provider.orderList.data[index].orderStatus),
                                  style:
                                  TextStyle(color: provider.orderList.data[index].orderStatus.toLowerCase().contains("cancelled")||provider.orderList.data[index].orderStatus.toLowerCase().contains("delivered")?Colors.blue:Colors.red,   fontSize: 14,
                                      letterSpacing: 0.3,
                                      decoration: TextDecoration.underline,
                                      fontWeight: FontWeight.bold),
                                )
                            ),
                          ),
                        ),
                      ],
                    ),



                    Padding(
                      padding:  EdgeInsets.all(8.0),
                      child:ListView.builder(
                        itemCount: provider.orderDetailList.data.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index){
                          return Container(
                            child: Column(
                              children: [

                                Card(
                                  elevation: 3,
                                  shape: RoundedRectangleBorder(
                                    //  side: BorderSide(color: Colors.white70, width: 1),
                                    borderRadius: BorderRadius.circular(10),
                                  ),

                                  child:  Column(
                                    children: [


                                      Container(
                                        margin: EdgeInsets.all(5.0),
                                        child: Row(
                                          children: [

                                            SizedBox(
                                              //height: SizeConfig.verticalBloc * 23,
                                              width: SizeConfig.horizontalBloc * 25,
                                              child: Container(
                                                margin: EdgeInsets.all(5.0),
                                                child:Container(
                                                  padding: EdgeInsets.all(10),
                                                  child: Images.buildImage(provider.orderDetailList.data[index].image),
                                                ),
                                              ),
                                            ),

                                            SizedBox(
                                              width: SizeConfig.verticalBloc * 1,
                                            ),



                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [

                                                SizedBox(
                                                  width:  SizeConfig.horizontalBloc * 50,
                                                  child:  Text(provider.orderDetailList.data[index].productName,
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      fontFamily: 'openSans',
                                                      letterSpacing: 0.3,
                                                      fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
                                                      color: R.color.black_color,

                                                    ),

                                                  ),
                                                ),

                                                SizedBox(
                                                  height: SizeConfig.verticalBloc * 0.1,
                                                ),


                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [

                                                    Text("Quantity :"+provider.orderDetailList.data[index].qty+" "/*+provider.orderDetailList.data[index].unit*/,
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          // decoration: TextDecoration.lineThrough,
                                                          letterSpacing: 1,
                                                          color: R.color.grey_color),
                                                    ),

                                                    SizedBox(
                                                      width: SizeConfig.verticalBloc * 1,
                                                    ),



                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [

                                                    Text("Price :"+R.priceSymbol.rupee+provider.orderDetailList.data[index].salePrice+"/"+provider.orderDetailList.data[index].weight.toString()+provider.orderDetailList.data[index].unit,
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          // decoration: TextDecoration.lineThrough,
                                                          letterSpacing: 1,
                                                          color: R.color.grey_color),
                                                    ),

                                                    SizedBox(
                                                      width: SizeConfig.verticalBloc * 1,
                                                    ),



                                                  ],
                                                ),



                                              ],
                                            ),


                                          ],
                                        ),
                                      ),




                                    ],
                                  ),
                                ),

                              ],
                            ),
                          );


                        },
                      ),
                    ),



                    Container(
                      margin: EdgeInsets.only(left: 15, top: 10),
                      child: Text(
                        "Shipping Address",
                        style: TextStyle( fontSize: 14,
                            letterSpacing: 0.3,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10,  bottom: 20),

                      width: MediaQuery.of(context).size.width,
                      child: Card(
                        elevation: 4,
                        shadowColor: Colors.grey,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 20, top: 20,right: 8),
                              child: Text(
                                ""+userAddress.address[0].firstName+" " +userAddress.address[0].lastName,
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 15,
                                    letterSpacing: 0.3,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),


                            Container(
                              margin: EdgeInsets.only(left: 20, top: 5,right: 8),
                              child: Text(
                                          ""+provider.orderList.data[index].address,
                                maxLines: 20,
                                style: TextStyle(color: Colors.black87),
                              ),
                            ),



                            Container(
                              margin: EdgeInsets.only(left: 20, top: 9,bottom: 40),
                              child: Text(
                                provider.orderList.data[index].phone,
                                style: TextStyle(color: Colors.black87),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }




  bottomSheet (BuildContext context) {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Consumer<OrderProvider>(
            builder: (context, provider, child){
              return  SizedBox(
                height: 500,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,

                    children: <Widget>[
                      SizedBox(height: 20,),
                      Container(
                          alignment: Alignment.center,
                          child: Text('Cancel Your Order',style: TextStyle(color: Colors.black,
                              fontSize: 16,fontWeight: FontWeight.bold),)),
                      Container(
                          margin: EdgeInsets.only(top: 20,left: 20),
                          alignment: Alignment.topLeft,
                          child: Text('The following information is only for our records '
                              'and will not prevent you from cancelling order',
                            style: TextStyle(  fontSize: 15,
                                letterSpacing: 0.3,
                                fontWeight: FontWeight.w300),)),

                      Expanded(
                        child: Container(

                          child: ListView(
                            padding: EdgeInsets.all(8.0),
                            children: _buttonOptions.map((timeValue) => RadioListTile<dynamic>(
                              groupValue: provider.selectedReason,
                              title: Text(timeValue._value),
                              value: timeValue._value,
                              onChanged: (val ) {
                                setState(() {
                                  debugPrint('VAL = $val');
                                  provider.checkSelectedRadio(val);
                                });
                              },
                            )).toList(),
                          ),
                        ),
                      ),

                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Container(
                              margin: EdgeInsets.only(left: 20,bottom: 20),
                              width: 140,
                              height: 40,
                              child:   ElevatedButton(

                                  child: Text(
                                      "No",
                                      style: TextStyle(fontSize: 14)
                                  ),
                                  style: ButtonStyle(
                                      foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                      backgroundColor: MaterialStateProperty.all<Color>(Colors.grey),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(12),

                                          )
                                      )
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  }
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(
                              margin: EdgeInsets.only(left: 20,bottom: 20,right: 20),
                              width: 140,
                              height: 40,
                              child:   ElevatedButton(

                                  child: Text(
                                      "Cancel Order",
                                      style: TextStyle(  fontSize: 14,
                                          letterSpacing: 0.3,
                                          fontWeight: FontWeight.bold)
                                  ),
                                  style: ButtonStyle(
                                      foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                      backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(12),

                                          )
                                      )
                                  ),
                                  onPressed: () async {

                                    if(provider.selectedReason==null){
                                      AppUtil.showToast("Kindle select remark ");
                                      return;
                                    }
                                    AppUtil.showLoadingProgress(context);

                                    var res =  await provider.getCancelOrder(context,
                                        provider.orderList.data[index].id,
                                        provider.orderList.data[index].status,
                                        "",
                                    provider.selectedReason );

                                    _textFieldController.text = "";
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                  }
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),




              );
            },
          );

        });
  }



  String netPayable(dynamic neyPayable, dynamic paidAmt){

      print(neyPayable);
      print(paidAmt);

     var myInt = neyPayable;
     var paidTwo = double.parse(paidAmt);

     dynamic c= myInt-paidTwo;
     return c.toString();

  }


  String myDate(String _selectedDate){

    DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(_selectedDate);
    final df = new DateFormat('dd-MM-yyyy');
    df.format(tempDate);

    return df.format(tempDate).toString();
  }

  String myTime(String _selectedDate){

    DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(_selectedDate);


    return  DateFormat('hh:mm a').format(tempDate).toString();
  }

  String myString(String orderStatus){

      if(orderStatus.toLowerCase().contains("cancelled")
      ||orderStatus.toLowerCase().contains("delivered")){

        return "Re-Order";

      }else if(orderStatus.toLowerCase().contains("order confirmed") ||orderStatus.toLowerCase().contains("accepted")
      ||orderStatus.toLowerCase().contains("declined")||orderStatus.toLowerCase().contains("assign")){
        return "Cancel Order";
      }else {
        return "Re-Order";
      }



  }
}
class TimeValue {
  final int _key;
  final String _value;
  TimeValue(this._key, this._value);


}