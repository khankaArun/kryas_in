

import 'package:date_picker_timeline/date_picker_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/view/market/orderconfirm.dart';
import 'package:krays_in/view/market/orderconfirmation.dart';
import 'package:krays_in/view/market/shipping_address.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:time_picker_widget/time_picker_widget.dart';


import 'checkout.dart';

class ProceedAddress extends StatefulWidget {

  @override
  State<ProceedAddress> createState() => _ProceedAddressState();
}

class _ProceedAddressState extends State<ProceedAddress> {
   String datetime3;

   String _timeSlot="Select Time Slot";
   String _selectedDate='';

   String selectedTime="Select Time Slot";



  @override
  void initState() {
    DateTime datetime = DateTime.now();
    print(datetime.toString());
    datetime3 = DateFormat.MMMM().format(datetime);
    print(datetime3.toString());


  }
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    final orientation = MediaQuery.of(context).orientation;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      bottomNavigationBar:  Material(
        elevation: 5.0,
        //borderRadius: BorderRadius.circular(30.0),
        child: InkWell(
          onTap: () async {

            if(_selectedDate ==null ||_selectedDate.isEmpty){
              AppUtil.showToast("Please choose the date");
              return;
            }

            if(selectedTime ==null ||selectedTime=='Select Time Slot'){
              AppUtil.showToast("Please select the time");
              return;
            }



            if(selectedTime.toLowerCase().contains("pm") ||selectedTime.toLowerCase().contains("am")){
              print("mydate"+selectedTime);

              selectedTime=selectedTime.replaceAll(" ", ":");
              List <String> splitedString = selectedTime.split(":");
              int  hour=0;
              if(selectedTime.toLowerCase().contains("pm")){
                hour= int.parse(splitedString[0]) + 12;
              }else{
                hour= int.parse(splitedString[0]);
              }

              int  minute= int.parse(splitedString[1]) ;
              print("hour"+hour.toString());
              print("minute"+minute.toString());
              _timeSlot= hour.toString()+":"+minute.toString()+":00";


            //  String newFormat = splitedString[2].contains("PM") ?  hour.toString(): splitedString[0] + ":" + splitedString[1] + ":" + splitedString[2].substring(0,2);

             // print("newFormat"+newFormat);

            //  DateTime date= DateFormat.jms().parse(selectedTime.trim(),true);


            //  _timeSlot=DateFormat("HH:mm:ss").format(date);
            //  _timeSlot=   DateFormat.jm().format(DateFormat("hh:mm:ss").parse(selectedTime));

            }




            print(_selectedDate);
            print(_timeSlot);

            Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: OrderConfirmation(_selectedDate.trim()+" "+_timeSlot.trim())));






          },
          child: Container(
            margin: EdgeInsets.only(bottom: 10 ,left: 10,right: 10),
            width: width - 40.0,
            height: 43,
            padding: EdgeInsets.all(5.0),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: R.color.app_color,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Text(
              'Proceed',
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Jost',
                  letterSpacing: 0.7,
                  fontSize: 17.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 1,
        titleSpacing: 0,
        centerTitle: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Checkout",
          textAlign: TextAlign.left,
          style: TextStyle(
              fontSize: ResponsiveFlutter.of(context).fontSize(2),
              fontWeight: FontWeight.w900,
              letterSpacing: 1.5,
              color: R.color.black_color),),

        actions: [
          SizedBox(
            width:SizeConfig.horizontalBloc * 7,
            height: SizeConfig.horizontalBloc * 7,
            child: InkWell(
              onTap: (){
                //   Navigator.push(context, MaterialPageRoute(builder: (context)=>SearchKeyWordPage()));
              },
              //  child: Image.asset(R.images.search_icon),
            ),
          ),

          SizedBox(width: SizeConfig.horizontalBloc * 5),




          /*   SizedBox(
            width:SizeConfig.horizontalBloc * 7,
            height: SizeConfig.horizontalBloc * 7,
            child: InkWell(
              onTap: (){
                // Navigator.push(context, MaterialPageRoute(builder: (context)=>NotificationPage()));
              },
              child: Image.asset(R.images.notification_icon),
            ),
          ),*/

        ],
      ),
      body:  Consumer<AuthProvider>(
      builder: (context, provider, child){
        return  SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
//
// print(datetime3);
            children: [
              Container(
                margin: EdgeInsets.only(top: 10,left:10,right: 20),
                width: MediaQuery.of(context).size.width,
                height: 170,
                child: Card(
                  elevation: 10,
                  shadowColor:Colors.black54,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                            left:20,top: 20
                        ),
                        child: Text(
                          ""+provider.userAddressListModelItem.address[0].firstName.toUpperCase()+" "+
                              provider.userAddressListModelItem.address[0].lastName.toUpperCase(),
                          style: TextStyle(color: Colors.black87,fontWeight: FontWeight.bold,fontSize: 15,letterSpacing: 0.5),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 75,
                            child: Container(

                              margin: EdgeInsets.only(
                                  left:20,top: 5
                              ),
                              child: Text(
                               ""+provider.userAddressListModelItem.address[0].text+"\n"
                                +"Contact :"+provider.userAddressListModelItem.address[0].contactNo,
                                style: TextStyle(color: Colors.black87,fontSize :13,letterSpacing: 0.5),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 20,
                            child: Container(
                              margin: EdgeInsets.only(
                                  right: 15,top: 20
                              ),
                              child: InkWell(
                                onTap: (){
                                  Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ShippingAddressPage(true,R.string.shipping_address)));
                                },
                                child: Text(
                                  "Change",
                                  style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold,letterSpacing: 0.7,decoration: TextDecoration.underline),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),

                    ],
                  ),
                ),

              ),
              Row(
                children: [
                  Expanded(
                    flex: 80,
                    child: Container(
                      margin: EdgeInsets.only(
                          left:20,top: 20
                      ),
                      child: Text(
                        "Delivery Date",
                        style: TextStyle(color: Colors.black87,fontSize: 16,letterSpacing: 0.7,fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 25,
                    child: Container(
                      margin: EdgeInsets.only(
                          left:0,top: 20
                      ),
                      child: Text(
                        datetime3,
                        style: TextStyle(color: Colors.black87,fontSize: 16,letterSpacing: 0.7),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20,),
              Container(
                margin: EdgeInsets.only(left: 10,right: 10),
                child: DatePicker(
                  DateTime.now(),
                  width: 60,
                  height: 100,
                  //initialSelectedDate: DateTime.now(),

                  daysCount: 6,
                  selectionColor: Colors.blue,
                  selectedTextColor: Colors.white,
                   inactiveDates: [
                     DateTime.now(),
                    // DateTime.now().add(Duration(days: 4)),
                    // DateTime.now().add(Duration(days: 7))
                   ],
                  onDateChange: (date) {
                    // New date selected
                    setState(() {
                      _selectDate(context,date);
                    });
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    left:20,top: 20
                ),
                child: Text(
                  "Time Slot",
                  style: TextStyle(color: Colors.black87,fontSize: 16,letterSpacing: 0.7,fontWeight: FontWeight.bold),
                ),
              ),


              InkWell(
                onTap: (){


                  List<int> _availableHours = [13,14,15,16,17,18,19,20,7,8,9,10,11,12];
                  List<int> _availableMinutes = [0,05, 10,15,20,25, 30,35,40, 45, 50,55];


                //  _selectTime(context);

                  showCustomTimePicker(
                      context: context,

                      onFailValidation: (context){

                      },

                      initialTime: TimeOfDay(
                          hour: _availableHours.first,
                          minute: _availableMinutes.first),
                      selectableTimePredicate: (time) =>
                      _availableHours.indexOf(time.hour) != -1 &&
                          _availableMinutes.indexOf(time.minute) != -1).then(
                          (time) {

                            setState(() {

                              selectedTime = time?.format(context);

                           //   1:00:00:PM
                           //   13:00:

                              //13:00
                              _timeSlot = selectedTime;

                              if(selectedTime.toLowerCase().contains("pm")||selectedTime.toLowerCase().contains("am")){
                                selectedTime=selectedTime;
                              }else{
                                selectedTime=   DateFormat.jm().format(DateFormat("hh:mm:ss").parse(selectedTime+":00"));

                              }

                              print(selectedTime);

                            });



                  }
                  );



                 //  print(selectedTime);
                 // selectedTime=   DateFormat.jm().format(DateFormat("hh:mm:ss").parse(selectedTime+""));

                 // print(selectedTime);




                },
                child:  Container(
                  margin: EdgeInsets.only(top: 0,left:10,right: 20),
                  width: MediaQuery.of(context).size.width,
                  height: 67,
                  child: Card(
                    elevation: 10,
                    shadowColor:Colors.black54,

                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    ),
                    child: Center(
                      child:  Row(
                        children: [
                          Expanded(
                            flex:15,
                            child: Container(margin: EdgeInsets.only(top:0,left: 30),
                                child:   Icon(Icons.calendar_today_sharp,size: 18,color: Colors.blue,)/*Image.asset("assets/img/radio-checked.png"*/
                            ),
                          ),
                          Expanded(
                            flex:75,
                            child: Container(margin: EdgeInsets.only(top:0,left: 30, ),
                              child: Text(selectedTime==null?"Select Time Slot":selectedTime,  style: TextStyle(color: Colors.black87,
                                  fontSize: 14,letterSpacing: 0.7,fontWeight: FontWeight.bold)
                                ,),
                            ),
                          )
                        ],
                      ),

                    ),
                  ),

                ),
              ),


              Container(
                margin: EdgeInsets.only(
                  left:20,top: 20,
                ),
                child:Text("Delivery Method",  style: TextStyle(color: Colors.black87,fontSize: 15,letterSpacing: 0.5,
                    fontWeight: FontWeight.bold),),
              ),
              Container(
                margin: EdgeInsets.only(top: 0,left:10,right: 20),
                width: MediaQuery.of(context).size.width,
                height: 67,
                child: Card(
                  elevation: 10,
                  shadowColor:Colors.black54,

                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                  ),
                  child: Center(
                    child:  Row(
                      children: [
                        Expanded(
                          flex:15,
                          child: Container(margin: EdgeInsets.only(top:0,left: 30),
                            child:    SizedBox(
                              width: SizeConfig.horizontalBloc * 18,
                              height: SizeConfig.horizontalBloc * 18,
                              child: Image.asset(R.images.ic_checked_out,),
                            ),/*Image.asset("assets/img/radio-checked.png"*/
                          ),
                        ),
                        Expanded(
                          flex:75,
                          child: Container(margin: EdgeInsets.only(top:0,left: 30, ),
                            child: Text("Buy at Home",  style: TextStyle(color: Colors.black87,
                                fontSize: 14,letterSpacing: 0.7,fontWeight: FontWeight.bold)
                              ,),
                          ),
                        )
                      ],
                    ),

                  ),
                ),

              ),
            ],
          ),
        );
      }

      ),
    );



  }


   _selectDate(BuildContext context, var date) async {
     String   formattedDate = await DateFormat('yyyy-MM-dd').format(date);

     _selectedDate =formattedDate;
     print(_selectedDate);

   }



/*
  _selectTime(BuildContext context) async {
    final TimeOfDay timeOfDay = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour: 7, minute: 15,),
      initialEntryMode: TimePickerEntryMode.dial,

    );
    final localizations = MaterialLocalizations.of(context);
    print(timeOfDay);
    final formattedTimeOfDay = localizations.formatTimeOfDay(timeOfDay);

      setState(() {
        _timeSlot=formattedTimeOfDay.toString();
         print(formattedTimeOfDay.toString());

        DateTime date= DateFormat.jm().parse(formattedTimeOfDay.toString());

        _selectedTime=DateFormat("HH:mm:ss").format(date);
        print(_selectedTime);



        checkRestaurentStatus("07:00 AM","09:00 PM");
      });

  }
*/


}
