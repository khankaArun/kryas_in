import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/view/home/home_page.dart';
import 'package:number_to_words/number_to_words.dart';

class OrderConfirmPage extends StatefulWidget {

   double myPayableAmt;
   OrderConfirmPage(this.myPayableAmt);


  @override
  State<OrderConfirmPage> createState() => _OrderConfirmationState(myPayableAmt);
}

class _OrderConfirmationState extends State<OrderConfirmPage> {

  double myPayableAmt=0.00;

 // double myPayableAmt;
  _OrderConfirmationState(this.myPayableAmt);

  int ruppes=0;

  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    super.initState();

     ruppes = myPayableAmt.toInt();
    print(ruppes); // 8

  }

  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    final orientation = MediaQuery.of(context).orientation;
    double width = MediaQuery.of(context).size.width;

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar:  AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 1,
          titleSpacing: 0,
          centerTitle: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Order Confirmation",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: ResponsiveFlutter.of(context).fontSize(2),
                fontWeight: FontWeight.w900,
                letterSpacing: 1.5,
                color: R.color.black_color),),

          actions: [
            SizedBox(
              width:SizeConfig.horizontalBloc * 7,
              height: SizeConfig.horizontalBloc * 7,
              child: InkWell(
                onTap: (){


                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              HomePage()),
                          (Route<dynamic> route) => true);
                  //   Navigator.push(context, MaterialPageRoute(builder: (context)=>SearchKeyWordPage()));
                },
                //  child: Image.asset(R.images.search_icon),
              ),
            ),

            SizedBox(width: SizeConfig.horizontalBloc * 5),




            /*   SizedBox(
            width:SizeConfig.horizontalBloc * 7,
            height: SizeConfig.horizontalBloc * 7,
            child: InkWell(
              onTap: (){
                // Navigator.push(context, MaterialPageRoute(builder: (context)=>NotificationPage()));
              },
              child: Image.asset(R.images.notification_icon),
            ),
          ),*/

          ],
        ),
        bottomNavigationBar:   Material(
          elevation: 5.0,
          //borderRadius: BorderRadius.circular(30.0),
          child: InkWell(
            onTap: () {

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          HomePage()),
                      (Route<dynamic> route) => false);

            },
            child: Container(
              margin: EdgeInsets.only(bottom: 10 ,left: 10,right: 10),
              width: width - 40.0,
              height: 43,
              padding: EdgeInsets.all(5.0),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: R.color.app_color,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Text(
                'Continue Shopping',
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Jost',
                    letterSpacing: 0.7,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [

              Container(
                margin: EdgeInsets.only(left: 15, right: 30),
                child: new Image(
                    image: new AssetImage(R.images.ic_status_final_stpes)),
              ),
              Container(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  child: Divider(
                    height: 2,
                    color: Colors.grey.shade300,
                    thickness: 2,
                  )),

              Center(
                child: Container(
                  margin: EdgeInsets.only(top: 50),

                  height: 220,

                  // margin: EdgeInsets.only(bottom: 370),

                  child: new Image(
                      fit: BoxFit.cover,
                      image: new AssetImage(R.images.ic_confirmation_page)),
                ),
              ),

              Container(
                margin: EdgeInsets.only(top: 25,left: 15,right: 15),
                child: Center(

                  child: Text(
                    "Congratulations! Your order has been placed Successfully.",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black, fontSize: 16,letterSpacing: 1,fontWeight: FontWeight.bold),
                  ),
                ),
              ),
          /*    Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "Congratulations! your order has been placed Successfully.",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey, fontSize: 15, letterSpacing: 0.5),
                ),
              )*/
            ],
          ),
        ),
      ),
    );
  }
}
