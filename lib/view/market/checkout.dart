import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:krays_in/model/user_address.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/provider/CartProvider.dart';
import 'package:krays_in/provider/product_detail_provider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

import 'orderconfirm.dart';


class CheckOut extends StatefulWidget {

  var slotTime;
  CheckOut(this.slotTime);



  @override
  State<CheckOut> createState() => _CheckOutState(slotTime);
}

class _CheckOutState extends State<CheckOut> {



  var slotTime;
  _CheckOutState(this.slotTime);


  static const platform = const MethodChannel("razorpay_flutter");
   Razorpay _razorpay;
   dynamic myPayableAmt;


  int paymentValue = 1;
  bool isShowPaymentBreakups= true;

  @override
  void initState() {
    super.initState();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }


  @override
  Widget build(BuildContext context) {
    var myProfile =Provider.of<AuthProvider>(context, listen: true).userAddressListModelItem;
    var myCart =Provider.of<CartProvider>(context, listen: true).userCartList;
    myPayableAmt=myCart==null?"0":myCart.payAmt;
    SizeConfig().init(context);
    final orientation = MediaQuery.of(context).orientation;
    double width = MediaQuery.of(context).size.width;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
       /* bottomNavigationBar: Container(
            // margin: EdgeInsets.only(left: 20,right: 20),
            margin: EdgeInsets.only(bottom: 5, left: 10, right: 20),
            width: double.maxFinite,
            child: MaterialButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
              color: Colors.blue,
              height: 60,
              child: Text("Confirm Order",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 16)),
              onPressed: () async {},
            )),*/
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 1,
          titleSpacing: 0,
          centerTitle: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Checkout",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: ResponsiveFlutter.of(context).fontSize(2),
                fontWeight: FontWeight.w900,
                letterSpacing: 1.5,
                color: R.color.black_color),),

          actions: [
            SizedBox(
              width:SizeConfig.horizontalBloc * 7,
              height: SizeConfig.horizontalBloc * 7,
              child: InkWell(
                onTap: (){
                  //   Navigator.push(context, MaterialPageRoute(builder: (context)=>SearchKeyWordPage()));
                },
                //  child: Image.asset(R.images.search_icon),
              ),
            ),

            SizedBox(width: SizeConfig.horizontalBloc * 5),




            /*   SizedBox(
            width:SizeConfig.horizontalBloc * 7,
            height: SizeConfig.horizontalBloc * 7,
            child: InkWell(
              onTap: (){
                // Navigator.push(context, MaterialPageRoute(builder: (context)=>NotificationPage()));
              },
              child: Image.asset(R.images.notification_icon),
            ),
          ),*/

          ],
        ),
        body: Column(
          // mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(left: 15, right: 30),
              child: new Image(
                  image: new AssetImage(R.images.ic_payment)),
            ),
            Container(
                margin: EdgeInsets.only(left: 10, right: 10),
                child: Divider(
                  height: 2,
                  color: Colors.grey.shade300,
                  thickness: 2,
                )),



            SizedBox(height: 20,),

            Center(
              child: SizedBox(
                width: 300,
                child: Column(
                  children: [

                    Row(
                      children: [
                        Radio(
                          value: 1,
                          groupValue: paymentValue,
                          onChanged: (value) {
                            setState(() {
                              paymentValue = value as int;
                              isShowPaymentBreakups=true;
                            });
                          },
                          activeColor: Colors.blue,
                        ),
                        Text("Full Payment",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,letterSpacing: 1),),
                      ],
                    ),
                    Row(
                      children: [
                        Radio(
                          value: 2,
                          groupValue: paymentValue,
                          onChanged: (value) {
                            setState(() {
                              paymentValue = value as int;
                              isShowPaymentBreakups=false;
                            });
                          },
                          activeColor: Colors.blue,
                        ),
                        Text("Token Money (Refundable ₹20)",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,letterSpacing: 1),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Radio(
                          value: 3,
                          groupValue: paymentValue,
                          onChanged: (value) {
                            setState(() {
                              paymentValue = value as int;
                              isShowPaymentBreakups=true;
                            });
                          },
                          activeColor: Colors.blue,
                        ),
                        Text("Pay at Home",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,letterSpacing: 1),
                        ),
                      ],
                    ),
                  ],
                ),),
            ),


            paymentValue==3?Center(

              child: Container(
                child: SizedBox(
                  width: 150,
                  child: ElevatedButton(
                    style: ButtonStyle(
                        foregroundColor: MaterialStateProperty.all<Color>(R.color.white_color),
                        backgroundColor: MaterialStateProperty.all<Color>(R.color.app_color),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: BorderSide(color: R.color.app_color)
                            )
                        )
                    ),
                    onPressed: () async{
                      placeOrder(0.00,"Pay at Home","");
                    },
                    child: Text("Place Order".toUpperCase(), style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.5), fontWeight: FontWeight.w600),),
                  ),
                ),
              ),

            ): Center(
              child:   Column(children: [
                Container(
                  margin: EdgeInsets.only(left: 15, top: 20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                        bottomLeft: Radius.circular(10),
                      ),
                      border: Border.all(color: Colors.blue)),
                  height: 80,
                  width: 80,
                  child: Container(
                    alignment: Alignment.center, // use aligment
                    child: InkWell(
                      onTap: () async {

                        double myAmt=await double.parse(myCart.payAmt.toString());

                        if(paymentValue==1){
                          openCheckout(myAmt,myProfile);
                          myPayableAmt=myAmt;
                        }else{
                          openCheckout(20.00,myProfile);
                          myPayableAmt=20.00;
                        }


                      },
                      child: Image.asset(
                        R.images.ic_razor_pay,
                        height: 45,
                        width: 45,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 22, top: 5),
                  child: Text(
                    "Pay Via (Razorpay)",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
              ),
            ),








            Visibility(
                visible: isShowPaymentBreakups,
                child: Column(

              children: [


                Container(
                  margin: EdgeInsets.only(top: 30, left: 10, right: 20),
                  width: MediaQuery.of(context).size.width,
                  height: 220,
                  child: Card(
                    shadowColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    ),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 60,
                              child: Container(
                                margin: EdgeInsets.only(
                                  left: 10,
                                ),
                                child: Text(
                                  "Subtotal",
                                  style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,letterSpacing: 0.5,fontSize: 15),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 30,
                              child: Container(
                                child: Text(
                                    myCart==null?"₹0":

                                  "₹"+myCart.grossAmt.toString(),
                                  style: TextStyle(color: Colors.black, letterSpacing: 0.5,fontSize: 14),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 60,
                              child: Container(
                                margin: EdgeInsets.only(
                                  left: 10,
                                ),
                                child: Text(
                                  "Discount",
                                  style: TextStyle(color: Colors.blueGrey,fontSize: 14,letterSpacing: 0.5),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 30,
                              child: Container(
                                child: Text(
                                  myCart==null?"₹ 0":
                                  "₹"+myCart.disAmt.toString(),
                                  style: TextStyle(color: Colors.blueGrey,fontSize: 13,letterSpacing: 0.5),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 60,
                              child: Container(
                                margin: EdgeInsets.only(
                                  left: 10,
                                ),
                                child: Text(
                                  "Delivery Charges",
                                  style: TextStyle(color: Colors.blueGrey,fontSize: 14,letterSpacing: 0.5),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 30,
                              child: Container(
                                child: Text(
                                  "Free",
                                  style: TextStyle(color: Colors.red,fontSize: 13,letterSpacing: 0.5),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 10, right: 20),
                            child: Divider(
                              height: 2,
                              color: Colors.grey.shade300,
                              thickness: 2,
                            )),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 60,
                              child: Container(
                                margin: EdgeInsets.only(
                                  left: 10,
                                ),
                                child: Text(
                                  "Total Payable",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,fontSize: 15,letterSpacing: 0.5),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 30,
                              child: Container(
                                child: Text(
                                  myCart==null?"₹ 0":
                                  "₹"+myCart.payAmt.toString(),
                                  style: TextStyle(color: Colors.blue,fontSize: 14,letterSpacing: 0.5),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ))

          ],
        ),
      ),
    );
  }
  void openCheckout(dynamic amount, UserAddressListModelItem userAddressListModelItem) async {



    double myAmount =amount*100;

    var options = {
     // rzp_live_ILgsfZCZoFIKMb
      //
      'key':'rzp_test_XRLG8pl20cZ60d',
      'amount': myAmount,
      'name': 'Krays.in',
      'description': 'Krays.in',
      'retry': {'enabled': true, 'max_count': 1},
    //  'send_sms_hash': true,
      'prefill': {'contact': userAddressListModelItem.address[0].contactNo,
        'email': userAddressListModelItem.address[0].email},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint('Error: $e');
    }
  }

  Future<void> _handlePaymentSuccess(PaymentSuccessResponse response) async {
    print('Success Response: $response');

    placeOrder(myPayableAmt,"Online",response.paymentId);



  }

  void _handlePaymentError(PaymentFailureResponse response) {
    print('Error Response: $response');
   // Fluttertoast.showToast( msg: "Error: " +  response.code.toString(),
      //  toastLength: Toast.LENGTH_SHORT);
    AppUtil.showToast("Payment failed");

  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    print('External SDK Response: $response');
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName,
        toastLength: Toast.LENGTH_SHORT);
  }
  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

   void placeOrder(dynamic myPayableAmt,dynamic paymentMode,dynamic txnId) async {


    AppUtil.showLoadingProgress(context);
    var old =  Provider.of<AuthProvider>(context, listen: false).userAddressListModelItem;
    var saveSlot = await Provider.of<AuthProvider>(context, listen: false).saveSlot(
      address:""+ old.address[0].text+" "+ old.address[0].addressLine2,
      lat: "0.00",
      lng:"0.00",
      phone: old.address[0].contactNo,
      frSlotTm: slotTime,
      toSlotTm: slotTime,
      payableAmt:myPayableAmt,
      paymentMode: paymentMode,
      transactionId: txnId,

    );

    if(saveSlot) {

       Provider.of<CartProvider>(context, listen: false).deleteCart(loginId: 0);


      Navigator.pop(context);

      Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: OrderConfirmPage(myPayableAmt)));




    }else{
      print("Order not placed");


    }




  }



}

