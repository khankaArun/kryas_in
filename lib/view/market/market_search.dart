import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/view/market/search_keyword_page.dart';
import 'package:page_transition/page_transition.dart';

class MarketSearch extends StatefulWidget {


  @override
  _MarketSearchState createState() => _MarketSearchState();
}

class _MarketSearchState extends State<MarketSearch> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        Row(
          children: [
            InkWell(

              onTap: (){


                Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: SearchKeyWordPage()));


              },
              child:  Container(
                width: SizeConfig.horizontalBloc * 90,
                margin: EdgeInsets.only(top: 15,left: 25, right: 10,),
                height: 44,
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7.0),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 1.0,
                      spreadRadius: 1.0,
                      color: Colors.white10,
                    ),
                  ],
                  color: Colors.white,
                  border: Border.all(
                      color: Colors.grey,
                      style: BorderStyle.solid,
                      width: 0.40),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.search,
                      size: 20.0,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text("${"Search ..."}",
                        style: TextStyle(fontFamily: 'Jost',
                          color: Colors.grey,
                          fontSize: 15,
                          letterSpacing: 1.3,)
                    ),


                  ],
                ),

              ),
            ),
           /* InkWell(

              onTap: (){

              },
              child:  Container(
                width: SizeConfig.horizontalBloc * 17,
                margin: EdgeInsets.only(top: 15,left: 5,right: 10,bottom: 10),
                height: 41,
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7.0),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 1.0,
                      spreadRadius: 1.0,
                      color: Colors.white10,
                    ),
                  ],
                  color: Colors.white,
                  border: Border.all(
                      color: Colors.grey,
                      style: BorderStyle.solid,
                      width: 0.40),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      child: Image.asset(R.images.ic_filter,height: 25,width: 20,),
                    )



                  ],
                ),

              ),
            ),*/
          ],
        ),







        // GetProducts()
      ],
    );
  }
}
