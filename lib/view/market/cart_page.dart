import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/model/related_model_item.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/provider/CartProvider.dart';
import 'package:krays_in/provider/ProductListingProvider.dart';
import 'package:krays_in/provider/product_detail_provider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/images.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/view/home/home_page.dart';
import 'package:krays_in/view/market/product_detail_page.dart';
import 'package:krays_in/view/market/shipping_address.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import 'checkoutaddress.dart';
import 'market_listing_page.dart';

class CartPage extends StatefulWidget {

  var myTitle;
  var isFruits=false;
  var isVegetables=false;
  var isOthers=false;

  CartPage({this.myTitle,this.isFruits,this.isVegetables,this.isOthers});

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {

  var isFruits=false;
  var isVegetables=false;
  var isOthers=false;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();


  }
  @override
  Widget build(BuildContext context) {
    var relatedProductProvider1 =Provider.of<ProductDetailProvider>(context, listen: true).relatedProduct;
   var relatedProductProvider;
    if(relatedProductProvider1!=null){
       relatedProductProvider =Provider.of<ProductDetailProvider>(context, listen: true).relatedProduct.data;
    }


    SizeConfig().init(context);
    final orientation = MediaQuery.of(context).orientation;
    double width = MediaQuery.of(context).size.width;




    return Scaffold(

        appBar:  AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 1,
          titleSpacing: 0,
          centerTitle: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Cart",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: ResponsiveFlutter.of(context).fontSize(2),
                fontWeight: FontWeight.w900,
                letterSpacing: 1.5,
                color: R.color.black_color),),

          actions: [
            SizedBox(
              width:SizeConfig.horizontalBloc * 7,
              height: SizeConfig.horizontalBloc * 7,
              child: InkWell(
                onTap: (){
                  //   Navigator.push(context, MaterialPageRoute(builder: (context)=>SearchKeyWordPage()));
                },
              //  child: Image.asset(R.images.search_icon),
              ),
            ),

            SizedBox(width: SizeConfig.horizontalBloc * 5),




            /*   SizedBox(
            width:SizeConfig.horizontalBloc * 7,
            height: SizeConfig.horizontalBloc * 7,
            child: InkWell(
              onTap: (){
                // Navigator.push(context, MaterialPageRoute(builder: (context)=>NotificationPage()));
              },
              child: Image.asset(R.images.notification_icon),
            ),
          ),*/

          ],
        ),


        bottomNavigationBar:  Provider.of<CartProvider>(context, listen: true)==null ? SizedBox.shrink() :

        Card(
            shape:  RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    child:  Center(
                        child: Text(Provider.of<CartProvider>(context, listen: true).userCartList==null?"":"Total "+R.priceSymbol.rupee+" "+Provider.of<CartProvider>(context, listen: true).userCartList.netAmt.toString(),
                          style: TextStyle(color: R.color.app_color,
                              fontWeight: FontWeight.w600,
                              fontSize: SizeConfig.headerFontSize),)),
                    height: SizeConfig.verticalBloc * 6,
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Provider.of<CartProvider>(context, listen: true).userCartList==null?R.color.grey:R.color.app_color,
                      borderRadius: BorderRadius.only(
                        topRight :Radius.circular(5.0),
                        bottomRight: Radius.circular(5.0),),
                    ),
                    child: InkWell(
                      onTap: ()  async{

                        var userAddressList =Provider.of<AuthProvider>(context, listen: false).userAddressListModelItem;
                        // Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: PaymentPage()));

                        var userCartCount =Provider.of<CartProvider>(context, listen: false).cartCount;
                        if(userCartCount ==null || userCartCount==0){
                          return;
                        }


                        if(userAddressList!=null){
                          print("user has address");
                          Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ProceedAddress()));
                        }else{
                          print("user don't have address");
                          Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ShippingAddressPage(false,R.string.shipping_address)));
                        }

                      },
                      child:  Center(
                          child: Text("CHECKOUT",
                            style: TextStyle(color: R.color.white_color,
                                fontWeight: FontWeight.w600,
                                fontSize: SizeConfig.normalFontSize),)),
                    ),
                    height: SizeConfig.verticalBloc * 6,
                  ),
                ),
              ],
            )
        ),

        body: Consumer<CartProvider>(builder: (context,provider,child){
          return (provider.userCartList==null)
              ? Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
            /*    Image.asset(
                  'assets/empty_bag.png',
                  height: 170.0,
                ),*/
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  'Hey, it feels so light!',
                  style: TextStyle(
                    fontFamily: 'Jost',
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.7,
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  'There is nothing in your cart. Let\'s add some items.',
                  style: TextStyle(
                    fontSize: 13.5,
                    color: Colors.grey,
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => HomePage()),
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(color: Colors.lightBlue),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 10.0, bottom: 10.0, right: 15.0, left: 15.0),
                      child: Text(
                        'ADD ITEMS TO CART',
                        style: TextStyle(
                          color: Colors.lightBlue,
                          fontFamily: 'Jost',
                          fontSize: 16.0,
                          letterSpacing: 0.8,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ):Container(
           // color: Colors.black12,
            child:  SingleChildScrollView(
              child: Column(
                children: [

                  Visibility(
                    visible: false,
                    child: Container(
                      margin: EdgeInsets.only(left: 20,right: 20),
                      child:  Container(
                        //color: Colors.white,
                        width: width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            ButtonTheme(
                              minWidth: ((width) / 2.5),
                              height: 40.0,
                              child: RaisedButton(
                                color: isVegetables?R.color.app_color:R.color.grey,
                                child: Text(
                                  'Shopping Cart',
                                  style: TextStyle(
                                    color: isVegetables?Colors.white:R.color.black_color,
                                    fontSize: 15.0,
                                    fontFamily: 'Jost',
                                    letterSpacing: 0.7,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                onPressed: () {

                                },

                              ),
                            ),
                            SizedBox(width: 7,),
                            ButtonTheme(
                              // minWidth: ((width - 60.0) / 2),
                              minWidth: ((width) / 2.5),
                              height: 40.0,
                              child: RaisedButton(
                                color: isFruits==false?R.color.app_color:R.color.grey,
                                child: Text(
                                  'Buy at Home',
                                  style: TextStyle(
                                    color: isFruits==false?R.color.white_color:Colors.black,
                                    fontSize: 15.0,
                                    fontFamily: 'Jost',
                                    letterSpacing: 0.7,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                onPressed: () {
                                  /*      Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: Delivery()));*/
                                },

                              ),
                            ),
                          ],
                        ),
                      ),
                    ),),


                  Container(
                    margin: EdgeInsets.all(10.0),
                    child: ListView.builder(
                      itemCount: provider.userCartList.data.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index){
                        return Card(
                          elevation: 3,
                          shape: RoundedRectangleBorder(
                          //  side: BorderSide(color: Colors.white70, width: 1),
                            borderRadius: BorderRadius.circular(10),
                          ),

                          child: Dismissible(
                            key: UniqueKey(),
                            background: slideRightBackground(),
                            secondaryBackground: slideLeftBackground(),
                            onDismissed: (direction) async {

                              AppUtil.showLoadingProgress(context);
                              var isItemDeleted =await Provider.of<CartProvider>(context, listen: false)
                                  .deleteItemInCart(productId: provider.userCartList.data[index].id,variantId: provider.userCartList.data[index].variantId);

                              //sItemAdded!="0"
                             // var isItemAdded =Provider.of<ProductDetailProvider>(context, listen: true).productDetail.data.isCart.toString();



                              // Then show a snackbar.
                              Navigator.pop(context);
                              if(isItemDeleted != null){
                                Scaffold.of(context)
                                    .showSnackBar(SnackBar(content: Text("Item Removed")));
                               /// 867868677960507220924779605072209247960507220924
                                ///
                                ///
                              }

                            },

                            child:  Column(
                              children: [

                               
                                  Icon(
                                    Icons.restore_from_trash,
                                    color: Colors.white,
                                  ),



                                Container(
                                  margin: EdgeInsets.all(5.0),
                                  child: Row(
                                    children: [

                                      InkWell(
                                        onTap: () async {


                                          AppUtil.showLoadingProgress(context);
                                          var res = await Provider.of<ProductDetailProvider>(context,listen: false)
                                              .getProductDetail(provider.userCartList.data[index].id,context);
                                          Navigator.of(context).pop();

                                          if(res){
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(builder: (context) => ProductDetailPage(provider.userCartList.data[index].id)),
                                            );
                                          }
                                        },
                                        child: SizedBox(
                                          //height: SizeConfig.verticalBloc * 23,
                                          width: SizeConfig.horizontalBloc * 30,
                                          child: Container(
                                            margin: EdgeInsets.all(5.0),
                                            child:Container(
                                              padding: EdgeInsets.all(15),
                                              child: Images.buildImage(provider.userCartList.data[index].image),
                                            ),
                                          ),
                                        ),
                                      ),



                                      SizedBox(
                                        width: SizeConfig.verticalBloc * 1,
                                      ),



                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [

                                          InkWell(

                                            onTap: () async {
                                              /*      Common.showLoadingProgress(context);
                                    var res = await Provider.of<ProductDetailProvider>(context,listen: false)
                                        .getProductDetail(provider.cartDetails.cartItems[index].productID,
                                        provider.cartDetails.cartItems[index].matrixID,context);
                                    Navigator.of(context).pop();

                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => ProductDetailPage()),
                                    );*/
                                            },
                                            child: SizedBox(
                                              width:  SizeConfig.horizontalBloc * 25,
                                              child:  Text(""+provider.userCartList.data[index].name,
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'openSans',
                                                  letterSpacing: 0.3,
                                                  fontSize: ResponsiveFlutter.of(context).fontSize(2),
                                                  color: R.color.black_color,

                                                ),

                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: SizeConfig.verticalBloc * 0.5,
                                          ),


                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [

                                              Text(R.priceSymbol.rupee+provider.userCartList.data[index].salePrice+"/"+provider.userCartList.data[index].weight+" "+provider.userCartList.data[index].unit.toUpperCase()/*provider.cartDetails.cartItems[index].quantity.toString()=='1'?'':R.priceSymbol.rupee+provider.cartDetails.cartItems[index].finalPrice.toString()*/,
                                                style: TextStyle(
                                                    fontSize: SizeConfig.smallFontSize,
                                                    fontWeight: FontWeight.w700,
                                                    // decoration: TextDecoration.lineThrough,
                                                    decorationThickness: 1.85,
                                                    letterSpacing: 0.3,
                                                    color: R.color.grey_color),
                                              ),

                                              SizedBox(
                                                width: SizeConfig.verticalBloc * 1,
                                              ),



                                            ],
                                          ),

                                          SizedBox(
                                            height: SizeConfig.verticalBloc * 0.5,
                                          ),

                                          Text(provider.userCartList.data[index].disPer+R.priceSymbol.off, style: TextStyle(
                                              fontSize: SizeConfig.smallFontSize,
                                              fontWeight: FontWeight.w500,
                                              color: R.color.app_color),),


                                          SizedBox(
                                            height: SizeConfig.verticalBloc * 0.5,
                                          ),




                                        ],
                                      ),

                                      Container(
                                        width:  SizeConfig.horizontalBloc * 30,
                                        child: Column(
                                          children: [
                                            GestureDetector(
                                              child: Container(
                                                height:25,
                                                width: 25,
                                                padding: const EdgeInsets.all(1.0),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.rectangle,
                                                  color: R.color.app_color,
                                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(5),bottomLeft: Radius.circular(5),bottomRight: Radius.circular(5),topRight: Radius.circular(5)),
                                                ),
                                                child: Icon(
                                                  Icons.remove,
                                                  size: 13.0,
                                                  color: Colors.white,
                                                ),
                                              ),
                                              onTap: () async{


                                                     int quntity = int.parse(provider.userCartList.data[index].quantity);
                                                quntity -= 1;
                                                if(quntity == 0){

                                                }else{

                                                  provider.updateQty(quntity,index,provider.userCartList.data[index].variantId);
                                                }


                                              },

                                            ),

                              Text("    \n"+provider.userCartList.data[index].quantity.toString()+"  \n  ",
                                style: TextStyle(
                                    fontSize: SizeConfig.smallFontSize,
                                    fontWeight: FontWeight.w700,
                                    color: R.color.black_color),),

                                            GestureDetector(
                                              child: Container(
                                                height:25,
                                                width: 25,
                                                padding: const EdgeInsets.all(1.0),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.rectangle,
                                                  color: R.color.app_color,
                                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(5),bottomLeft: Radius.circular(5),bottomRight: Radius.circular(5),topRight: Radius.circular(5)),
                                                ),
                                                child: Icon(
                                                  Icons.add,
                                                  size: 13.0,
                                                  color: Colors.white,
                                                ),
                                              ),
                                              onTap: () async{

                                                 int quntity = int.parse(provider.userCartList.data[index].quantity);
                                                quntity += 1;
                                                 provider.updateQty(quntity,index,provider.userCartList.data[index].variantId);


                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),




                              ],
                            ),
                          ),
                        );


                      },
                    ),
                  ),

                   if(relatedProductProvider!=null)
                     Container(
                       color: R.color.white_color,
                       padding: EdgeInsets.only(left: 5.0, right: 0.0, top: 10.0),
                       width: double.infinity,
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           Container(
                               margin: EdgeInsets.only(
                                   top    : SizeConfig.verticalBloc * 2,
                                   left   : SizeConfig.verticalBloc * 2,
                                   bottom : SizeConfig.verticalBloc * 0,
                                   right  : SizeConfig.verticalBloc * 2
                               ),
                               child: Row(
                                 children: [
                                   Expanded(child: Text("You might also like",
                                     style: TextStyle(
                                         fontFamily: 'openSans',
                                         // fontSize: SizeConfig.normalFontSize,
                                         fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                         fontWeight: FontWeight.w600,
                                         color: R.color.black_color),
                                   ),),


                                   SizedBox(
                                     height: SizeConfig.safeBlockVertical * 3.4,
                                     child: ElevatedButton(
                                       style: ButtonStyle(
                                           foregroundColor: MaterialStateProperty.all<Color>(R.color.white_color),
                                           backgroundColor: MaterialStateProperty.all<Color>(R.color.app_color),
                                           shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                               RoundedRectangleBorder(
                                                   borderRadius: BorderRadius.circular(18.0),
                                                   side: BorderSide(color: R.color.app_color)
                                               )
                                           )
                                       ),
                                       onPressed: () async{

                                         AppUtil.showLoadingProgress(context);
                                         // var myRelatedProductProvider =await  Provider.of<ProductDetailProvider>(context, listen: false)k
                                         var myProductList =await  Provider.of<ProductListingProvider>(context, listen: false).getProductList(context: context,
                                             categoryId: provider.userCartList.data[0].id,
                                             subCategoryId: provider.userCartList.data[0].subcategory,
                                             count: 100,
                                             page: 1,
                                             search: ""

                                         );
                                         Navigator.of(context).pop();

                                         if(myProductList){
                                           Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage(myTitle: "Top Selling",
                                             isFruits: false,
                                             isVegetables: false,
                                             isOthers: true,
                                             id:  provider.userCartList.data[0].id,
                                             subCatId: provider.userCartList.data[0].subcategory,)));

                                         }


                                       },
                                       child: Text("View All".toUpperCase(), style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.1), fontWeight: FontWeight.w600),),
                                     ),
                                   )
                                 ],
                               )
                           ),
                           Container(
                             margin: EdgeInsets.only(/*bottom: 20.0,*/top: 10),
                             child:Container(
                               height: SizeConfig.verticalBloc * 28,
                               child: relatedProductProvider !=null || relatedProductProvider.length != 0 ?ListView.builder(
                                   itemCount: relatedProductProvider.length,
                                   scrollDirection: Axis.horizontal,
                                   itemBuilder: (context, index) {
                                     return _relatedProduct(relatedProductProvider,index);
                                   }): SizedBox.shrink(),
                             ),
                           ),
                         ],
                       ),
                     ),



                ],
              ),
            ),
          );
        })
    );
  }

  Widget slideRightBackground() {
    return Container(
      color: Colors.red,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: 20,
            ),
            Icon(
              Icons.delete_outlined,
              color: Colors.white,
            ),
            Text(
              " ",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ],
        ),
        alignment: Alignment.centerLeft,
      ),
    );
  }


  Widget slideLeftBackground() {
    return Container(
      color: Colors.red,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.delete_outlined,
              color: Colors.white,
            ),
            Text(
              "",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              width: 20,
            ),
          ],
        ),
        alignment: Alignment.centerRight,
      ),
    );
  }

  Widget _relatedProduct(List<Data> data,int index){
    return InkWell(
      onTap: () async{
        AppUtil.showLoadingProgress(context);
        var res = await Provider.of<ProductDetailProvider>(context,listen: false)
            .getProductDetail(data[index].id,context);
        Navigator.of(context).pop();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ProductDetailPage(data[index].id)),
        );
      },
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(right: 1, top: 30),
              padding: EdgeInsets.only(left: 10),
              child: Column(
                children: [
                  SizedBox(
                    // height: SizeConfig.verticalBloc * 15,
                    width: SizeConfig.horizontalBloc * 30,
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.white,
                          width: 0.2,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      ),
                      child: Images.buildImage(data[index].image.toString().replaceAll(" ", "%20")),

                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.horizontalBloc * 2,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width/2.9,
                    child: Text(""+data[index].name,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,  textAlign: TextAlign.left, style: TextStyle(
                            fontSize: SizeConfig.smallFontSize,
                            fontWeight: FontWeight.bold,
                            color: R.color.black_color)
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.horizontalBloc * 1,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width/2.9,
                    child:  Text(R.priceSymbol.rupee+data[index].discountprice+"/"+data[index].unit,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,  textAlign: TextAlign.left,style: TextStyle(
                          fontSize: SizeConfig.smallFontSize,
                          fontWeight: FontWeight.w700,
                          // decoration: TextDecoration.lineThrough,
                          decorationThickness: 1.85,
                          color: R.color.grey_color),),
                  )


                ],
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              child: Container(
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(5.0)),
                ),
                child: Text( ""+data[index].disPer  +R.priceSymbol.off,
                  style: TextStyle(color: Colors.white, fontSize: 12.0,fontFamily: 'Jost',
                    letterSpacing: 0.8,),
                ),
              ),
            ),
            Positioned(
              bottom  : 0,
              right   : 0,
              child: Container(
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  color: data[index].isCart=='0'?Colors.grey:Colors.blue,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(5.0)),
                ),
                child: SizedBox(
                  height: 20,
                  child:   Icon(Icons.shopping_cart_outlined,
                      size: 22.0, color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}