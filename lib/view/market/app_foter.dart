
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';


class AppFoter extends StatefulWidget {

  String disclaimer;
  AppFoter(this.disclaimer);

  @override
  _AppFoterState createState() => _AppFoterState(disclaimer);
}

class _AppFoterState extends State<AppFoter> {

  String disclaimer;
  _AppFoterState(this.disclaimer);


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10,right: 10),
      padding: EdgeInsets.all(5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            decoration: BoxDecoration(
              color: R.color.white_color,
              borderRadius: BorderRadius.all(Radius.circular(7)),
            ),
            padding: EdgeInsets.all(5.0),
            child: Row(
              children: [
                Expanded(
                  child: SizedBox(
                   height: 45,
                   width: 80,
                   child: Icon(CupertinoIcons.info,),
                  ),
                  flex: 0,
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Disclaimer", style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.4), color: R.color.black_color, fontWeight: FontWeight.bold),),
                      SizedBox(
                        height: 0.70,
                      ),
                      Text(""+disclaimer,
                        //maxLines: 4,
                        style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.1), color: R.color.black_color, fontWeight: FontWeight.normal),)
                    ],
                  ),
                )
              ],
            ),
          ),

          SizedBox(
            height: 25,
          ),

          Center(
            child: Container(
              width: SizeConfig.horizontalBloc * 20,
              height: 1,
              color: R.color.grey_color,
            ),
          ),

          SizedBox(
            height: 15,
          ),

          Center(
            child: Text('Buy at Home\n“Because it’s a matter of your health and freshness of products”',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.1),
                  color: R.color.black_color, fontWeight: FontWeight.w500),),
          ),

          SizedBox(
            height: 7,
          ),

          Center(
            child: Text('Krays.in',
              style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.1),
                  color: R.color.grey_color, fontWeight: FontWeight.w500),),
          ),

          SizedBox(
            height: 10,
          ),

        ],
      ),
    );
  }
}