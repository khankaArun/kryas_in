import 'dart:async';
import 'dart:convert';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/model/search_item_model.dart';
import 'package:krays_in/provider/ProductListingProvider.dart';
import 'package:krays_in/provider/product_detail_provider.dart';
import 'package:krays_in/services/service.dart';
import 'package:krays_in/util/api.dart';
import 'package:krays_in/util/api_variable.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/view/market/product_detail_page.dart';
import 'package:provider/provider.dart';

import 'market_listing_page.dart';



class SearchKeyWordPage extends StatefulWidget {


  @override
  _SearchKeyWordPageState createState() => _SearchKeyWordPageState();
}

class _SearchKeyWordPageState extends State<SearchKeyWordPage> {


  final TextEditingController _filter = new TextEditingController();
 // List filteredNames = []; // names filtered by search text

  var _controller = TextEditingController();
  Timer _debounce;
  SearchModelItem _searchModelItem;
  bool showProgressIndicator=false;


  Widget _buildList() {
    return showProgressIndicator? Align(

      alignment: Alignment.topCenter,
      child: Container(
        margin: EdgeInsets.all(10),
        child: SizedBox(
          width: 15,
          height: 15,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(Colors.blue),
            strokeWidth: 3.0,

          ),
        ),
      ),
    )

        : ListView.separated(
      itemCount:_searchModelItem==null?0:_searchModelItem.data.length,
      itemBuilder: (BuildContext context, int index) {
        return new ListTile(
          title: Text(_searchModelItem==null?"":"   "+_searchModelItem.data[index].name,style: TextStyle(
            fontWeight: FontWeight.bold, letterSpacing: 1,fontSize: 14
          ),),
          onTap: () async{
            print("Arun");

            AppUtil.showLoadingProgress(context);

          /*  var myProductList =await  Provider.of<ProductListingProvider>(context, listen: false).getProductList(context: context,
                categoryId: _searchModelItem.data[index].id,
                subCategoryId: _searchModelItem.data[index].subCatId,
                count: 100,
                page: 1,
                search: ""

            );*/

            var res = await Provider.of<ProductDetailProvider>(context,listen: false)
                .getProductDetail(_searchModelItem.data[index].id,context);

              Navigator.of(context).pop();

              if(res){
              //  _searchModelItem=null;

                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProductDetailPage(_searchModelItem.data[index].id)),
                );


              /*  Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage(myTitle: _searchModelItem.data[index].name,
                  isFruits: false,
                  isVegetables: false,
                  isOthers: true,)));
*/
              }else{
                AppUtil.showToast("Product not available with this search");
              }






          },
        );
      },
      separatorBuilder: (context, index) {
        return Divider();
      },
    );
  }

  //STep6
  Widget _buildBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(color: Colors.black),
      elevation: 1,
      title: TextField(
        autofocus: true,
        textInputAction: TextInputAction.search,

        onSubmitted: (value) async {

          if(value.isNotEmpty&& value.toString().length>2) {

           // myNo.floor().isEven ? getSearckWordList(value) : "It's odd";

            getSearckWordList(value);
          }

                 /*  if(res){

            Navigator.push(context, MaterialPageRoute(builder: (context)=>MarketListingPage
              (myTitle: "Fresh Fruits",
              isFruits: false,
              isVegetables: true,
              isOthers: false,)
            )
            );




          }else{
            AppUtil.showToast("Products not available with this search");
          }
*/




        },
        controller: _controller,
        onChanged: (String value) async {
          setState(() {
            _searchModelItem=null;
          });
           Timer debounce = Timer(const Duration(seconds: 1), () {

             if(value.isNotEmpty&& value.toString().length>2) {

               int myNo = value.length;
              // myNo.floor().isEven ? getSearckWordList(value) : "It's odd";

               getSearckWordList(value);
             }

          /*
            if(value.isNotEmpty) {
              getSearckWordList(value);
            }
         */


          });

        },
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: 'Search keyword...',
          suffixIcon: IconButton(
            onPressed: (){

              setState(() {
                _controller.text = "";
                _searchModelItem.data = [];
              });

            },
            icon: Icon(Icons.clear),
          ),
        ),
      ),
    );
  }
  @override
  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _controller.text = "";
  }

  Future<void> getSearckWordList(String keyWord) async {

    setState(() {
      showProgressIndicator=true;
    });
    var res =  await Service().post(API.KEYWORD_Search,
        body: APIVariable().GetKeyWordSearch(keyWord));
    if(res != "error"){

      setState(() {
        showProgressIndicator=false;
      });

      if(res.toString().contains("No record found")){
      AppUtil.showToast("We are working on it");

      }else{
        showProgressIndicator=false;
        _searchModelItem =  SearchModelItem.fromJson(jsonDecode(res));
        setState(() {
        });
        return true;
      }

    } else{

      setState(() {
        showProgressIndicator=false;
      });
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.white_color,
      appBar: _buildBar(context),
      body: Container(
        child: _buildList(),
      ),
    );
  }
}
