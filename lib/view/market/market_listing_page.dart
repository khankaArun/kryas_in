import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/provider/CartProvider.dart';
import 'package:krays_in/provider/ProductListingProvider.dart';
import 'package:krays_in/provider/product_detail_provider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/images.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/view/market/product_detail_page.dart';
import 'package:krays_in/view/market/search_keyword_page.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import 'cart_page.dart';

class MarketListingPage extends StatefulWidget {

  var myTitle;
  var isFruits=false;
  var isVegetables=false;
  var isOthers=false;
  var id,subCatId;


  MarketListingPage({this.myTitle,this.isFruits,this.isVegetables,this.isOthers,this.id,this.subCatId});

  @override
  _MarketListingPageState createState() => _MarketListingPageState(myTitle: myTitle,isFruits:isFruits,isVegetables: isVegetables,isOthers: isOthers,id: id,subCatId: subCatId);
}

class _MarketListingPageState extends State<MarketListingPage> {

  var myTitle;
  var isFruits=false;
  var isVegetables=false;
  var isOthers=false;

  var id,subCatId;

  _MarketListingPageState({this.myTitle,this.isFruits,this.isVegetables,this.isOthers,this.id,this.subCatId});
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(isOthers){
      isOthers=false;
    }else{
    isOthers=true;
    }
  }

  Future<Null> _refresh() async {
    //Holding pull to refresh loader widget for 2 sec.
    //You can fetch data from server.
    await new Future.delayed(const Duration(seconds: 2));

        Provider.of<ProductListingProvider>(context, listen: false).getProductList(context: context,
        categoryId: id,
        subCategoryId: subCatId,
        count: 100,
        page: 1,
        search: ""


    );

    return null;
  }
  @override
  Widget build(BuildContext context) {

   var cartCount = Provider.of<CartProvider>(context, listen: true).cartCount;
    SizeConfig().init(context);
    final orientation = MediaQuery.of(context).orientation;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(

      appBar:  AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
        titleSpacing: 0,
        centerTitle: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(myTitle,
          textAlign: TextAlign.left,
          style: TextStyle(
              fontSize: ResponsiveFlutter.of(context).fontSize(2),
              fontWeight: FontWeight.w900,
              letterSpacing: 1.5,
              color: R.color.black_color),),

        actions: [

          SizedBox(width: SizeConfig.horizontalBloc * 5),


          Padding(padding: const EdgeInsets.only(top: 17),
              child: new Container(
                  width:SizeConfig.horizontalBloc * 8,
                  height: SizeConfig.horizontalBloc * 8,
                  child: new GestureDetector(
                    onTap: () {

                      if(cartCount !=null ||cartCount !=0){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>CartPage()));
                      }

                    },
                    child: new Stack(
                      children: <Widget>[
                        Icon(Icons.shopping_cart_outlined,size: 26,
                        ),
                        cartCount ==0 ? new Container() :
                        Positioned(
                            child: new Stack(
                              children: <Widget>[
                                new Icon(
                                    Icons.brightness_1,
                                    size: 18.0, color: R.color.app_color),
                                Positioned(
                                    top: 3.0,
                                    // bottom: 1.0,
                                    right: 1.0,
                                    left: 1.0,
                                    child: new Center(
                                      child: new Text(
                                        cartCount.toString(),
                                        style: new TextStyle(
                                            color: Colors.white,
                                            fontSize: 10.0,
                                            fontWeight: FontWeight.w500
                                        ),
                                      ),
                                    )),
                              ],
                            )),
                      ],
                    ),
                  )
              )),


          SizedBox(width: SizeConfig.horizontalBloc * 5),

          /*SizedBox(
            width:SizeConfig.horizontalBloc * 7,
            height: SizeConfig.horizontalBloc * 7,
            child: InkWell(
              onTap: (){
                // Navigator.push(context, MaterialPageRoute(builder: (context)=>NotificationPage()));
              },
              child: Image.asset(R.images.notification_icon),
            ),
          ),
          SizedBox(width: SizeConfig.horizontalBloc * 5),*/
        ],
      ),
      bottomNavigationBar:   Material(
        elevation: 5.0,
        //borderRadius: BorderRadius.circular(30.0),
        child: InkWell(
          onTap: () {
            Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: CartPage()));
          },

          child: Container(
            margin: EdgeInsets.only(bottom: 10 ,left: 10,right: 10),
            width: width - 40.0,
            height: 43,
            padding: EdgeInsets.all(5.0),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: R.color.app_color,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Text(
              'Go To Cart',
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Jost',
                  letterSpacing: 0.7,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
      body: RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _refresh,
        child: Consumer<ProductListingProvider>(builder: (context,provider,child){
          return Container(
            child:  SingleChildScrollView(
              child: Column(
                children: [

                  Row(
                    children: [
                      InkWell(

                        onTap: (){

                          Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: SearchKeyWordPage()));

                        },
                        child:  Container(
                          width: SizeConfig.horizontalBloc * 90,
                          margin: EdgeInsets.only(top: 15,left: 25, right: 10,bottom: 10),
                          height: 41,
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(7.0),
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 1.0,
                                spreadRadius: 1.0,
                                color: Colors.white10,
                              ),
                            ],
                            color: Colors.white,
                            border: Border.all(
                                color: Colors.grey,
                                style: BorderStyle.solid,
                                width: 0.40),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.search,
                                size: 20.0,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                              Text("${"Search ..."}",
                                  style: TextStyle(fontFamily: 'Jost',
                                    color: Colors.grey,
                                    fontSize: 15,
                                    letterSpacing: 1.3,)
                              ),


                            ],
                          ),

                        ),
                      ),


                      /*   InkWell(

                      onTap: (){

                      },
                      child:  Container(
                        width: SizeConfig.horizontalBloc * 17,
                        margin: EdgeInsets.only(top: 15,left: 5,right: 10,bottom: 10),
                        height: 41,
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7.0),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 1.0,
                              spreadRadius: 1.0,
                              color: Colors.white10,
                            ),
                          ],
                          color: Colors.white,
                          border: Border.all(
                              color: Colors.grey,
                              style: BorderStyle.solid,
                              width: 0.40),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              child: Image.asset(R.images.ic_filter,height: 25,width: 20,),
                            )

                          ],
                        ),

                      ),
                    ),*/
                    ],
                  ),

                  Visibility(
                    visible: isOthers,
                    child: Container(
                      margin: EdgeInsets.only(left: 20,right: 20),
                      child:  Container(
                        //color: Colors.white,
                        width: width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            ButtonTheme(
                              minWidth: ((width) / 2.5),
                              height: 40.0,
                              child: RaisedButton(
                                color: isVegetables?R.color.app_color:R.color.grey,
                                child: Text(
                                  'Vegetables',
                                  style: TextStyle(
                                    color: isVegetables?Colors.white:R.color.black_color,
                                    fontSize: 15.0,
                                    fontFamily: 'Jost',
                                    letterSpacing: 0.7,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                onPressed: () {

                                },

                              ),
                            ),
                            SizedBox(width: 7,),
                            ButtonTheme(
                              // minWidth: ((width - 60.0) / 2),
                              minWidth: ((width) / 2.5),
                              height: 40.0,
                              child: RaisedButton(
                                color: isFruits?R.color.app_color:R.color.grey,
                                child: Text(
                                  'Fruits',
                                  style: TextStyle(
                                    color: isFruits?R.color.white_color:Colors.black,
                                    fontSize: 15.0,
                                    fontFamily: 'Jost',
                                    letterSpacing: 0.7,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                onPressed: () {
                                  /*      Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: Delivery()));*/
                                },

                              ),
                            ),
                          ],
                        ),
                      ),
                    ),),


                  Container(
                    margin: EdgeInsets.all(10.0),
                    child: ListView.builder(
                      itemCount: provider.productListing.data.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index){
                        return Column(
                          children: [

                            Container(
                              margin: EdgeInsets.all(5.0),
                              child: Row(
                                children: [

                                  InkWell(
                                    onTap: () async {


                                      AppUtil.showLoadingProgress(context);
                                      var res = await Provider.of<ProductDetailProvider>(context,listen: false)
                                          .getProductDetail(provider.productListing.data[index].id,context);
                                      Navigator.of(context).pop();

                                      if(res){
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(builder: (context) => ProductDetailPage(provider.productListing.data[index].id)),
                                        );
                                      }
                                    },
                                    child: SizedBox(
                                      //height: SizeConfig.verticalBloc * 23,
                                      width: SizeConfig.horizontalBloc * 40,
                                      child: Container(
                                        margin: EdgeInsets.all(5.0),
                                        child: Card(
                                          elevation: 10,
                                          shape: RoundedRectangleBorder(
                                            side: BorderSide(color: Colors.white70, width: 1),
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          child: Container(
                                            padding: EdgeInsets.all(15),
                                            child: Images.buildImage(provider.productListing.data[index].image),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),



                                  SizedBox(
                                    width: SizeConfig.verticalBloc * 1,
                                  ),



                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [

                                      InkWell(

                                        onTap: () async {
                                          /*      Common.showLoadingProgress(context);
                                    var res = await Provider.of<ProductDetailProvider>(context,listen: false)
                                        .getProductDetail(provider.cartDetails.cartItems[index].productID,
                                        provider.cartDetails.cartItems[index].matrixID,context);
                                    Navigator.of(context).pop();

                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => ProductDetailPage()),
                                    );*/
                                        },
                                        child: SizedBox(
                                          width:  SizeConfig.horizontalBloc * 40,
                                          child:  Text(""+provider.productListing.data[index].name,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'openSans',
                                              letterSpacing: 0.3,
                                              fontSize: ResponsiveFlutter.of(context).fontSize(2),
                                              color: R.color.black_color,

                                            ),

                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: SizeConfig.verticalBloc * 0.5,
                                      ),


                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [

                                          Text(R.priceSymbol.rupee+provider.productListing.data[index].salePrice+" Per "+provider.productListing.data[index].unit/*provider.cartDetails.cartItems[index].quantity.toString()=='1'?'':R.priceSymbol.rupee+provider.cartDetails.cartItems[index].finalPrice.toString()*/,
                                            style: TextStyle(
                                                fontSize: SizeConfig.smallFontSize,
                                                fontWeight: FontWeight.w700,
                                                // decoration: TextDecoration.lineThrough,
                                                decorationThickness: 1.85,
                                                letterSpacing: 0.3,
                                                color: R.color.grey_color),
                                          ),

                                          SizedBox(
                                            width: SizeConfig.verticalBloc * 1,
                                          ),



                                        ],
                                      ),

                                      SizedBox(
                                        height: SizeConfig.verticalBloc * 0.5,
                                      ),

                                      Text(provider.productListing.data[index].disPer+R.priceSymbol.off, style: TextStyle(
                                          fontSize: SizeConfig.smallFontSize,
                                          fontWeight: FontWeight.w500,
                                          color: R.color.app_color),),


                                      SizedBox(
                                        height: SizeConfig.verticalBloc * 0.5,
                                      ),

                                      provider.productListing.data[index].isCart=="0"?SizedBox.shrink():
                                      Container(
                                        child: Row(
                                          children: [
                                            GestureDetector(
                                              child: Container(
                                                height:28,
                                                width: 40,
                                                padding: const EdgeInsets.all(1.0),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.rectangle,
                                                  color: R.color.app_color,
                                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(5),bottomLeft: Radius.circular(5)),
                                                ),
                                                child: Icon(
                                                  Icons.remove,
                                                  size: 13.0,
                                                  color: Colors.white,
                                                ),
                                              ),
                                              onTap: () async{


                                                int quntity = provider.productListing.data[index].quantity;
                                                quntity -= 1;
                                                if(quntity == 0){

                                                }else{
                                                  provider.setQuantity(context,index, quntity);
                                                }


                                              },
                                            ),

                                            Container(
                                              height:28,
                                              decoration: BoxDecoration(color: Colors.lightBlueAccent),
                                              child: Center(
                                                child: Text("    "+provider.productListing.data[index].quantity.toString()+"    ",
                                                  style: TextStyle(
                                                      fontSize: SizeConfig.smallFontSize,
                                                      fontWeight: FontWeight.w700,
                                                      color: R.color.white_color),),
                                              ),
                                            ),

                                            GestureDetector(
                                              child: Container(
                                                height:28,
                                                width: 40,
                                                padding: const EdgeInsets.all(1.0),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.rectangle,
                                                  color: R.color.app_color,
                                                  borderRadius: BorderRadius.only(topRight: Radius.circular(5),bottomRight: Radius.circular(5)),
                                                ),
                                                child: Icon(
                                                  Icons.add,
                                                  size: 13.0,
                                                  color: Colors.white,
                                                ),
                                              ),
                                              onTap: () async{

                                                int quntity = provider.productListing.data[index].quantity;
                                                quntity += 1;
                                                provider.setQuantity(context,index, quntity);


                                              },
                                            ),
                                          ],
                                        ),
                                      ),

                                      SizedBox(
                                        height: SizeConfig.verticalBloc * 1,
                                      ),




                                      provider.productListing.data[index].isCart=="0"? Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(color: R.color.app_color,width: 1,),
                                            borderRadius: BorderRadius.all( Radius.circular(5.0))
                                        ),
                                        child: InkWell(
                                          onTap: () async{

                                            var isItemSaved =await  Provider.of<CartProvider>(context, listen: false).doSaveCart(context: context,
                                              productId:provider.productListing.data[index].id,
                                              qty:provider.productListing.data[index].quantity,
                                              unit: provider.productListing.data[index].unit,
                                              price: provider.productListing.data[index].salePrice,
                                              weight: provider.productListing.data[index].weight,
                                              variantId: 0,
                                            );
                                            if(isItemSaved){
                                              provider.setIsItemAddedInCart(index, "1");
                                              AppUtil.showToast("Item has been moved on your cart");
                                            }


                                          },
                                          child:  Center(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [

                                                  Text("     "+"Add to Cart"+"     ",
                                                    style: TextStyle(color: R.color.app_color,
                                                        fontWeight: FontWeight.w800,
                                                        letterSpacing: 0.7,
                                                        fontSize: ResponsiveFlutter.of(context).fontSize(1.8)),),
                                                ],
                                              )),
                                        ),
                                        height: SizeConfig.verticalBloc * 4,
                                      ):SizedBox.shrink(),


                                    ],
                                  )
                                ],
                              ),
                            ),




                          ],
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        }),
      )
    );
  }


}