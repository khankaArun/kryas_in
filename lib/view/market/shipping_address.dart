
import 'package:flutter/cupertino.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:provider/provider.dart';

import '../../current_location.dart';
import '../../map.dart';
import '../../util/preferences_until.dart';


class ShippingAddressPage extends StatefulWidget {
  @override
  _ShippingAddressPageState createState() => _ShippingAddressPageState(this.isComeFromCart,this.title);

  bool isComeFromCart=false;
  String  title;
  ShippingAddressPage(this.isComeFromCart,this.title);
}

class _ShippingAddressPageState extends State<ShippingAddressPage> {


  bool isComeFromCart=false;
 // bool isPincodeAvailable=false;


  String  title;
  _ShippingAddressPageState(this.isComeFromCart,this.title);


  final _formKey = GlobalKey<FormState>();

  //TextEditingController _confirmPassword =  new TextEditingController();


  TextEditingController _firstName =  new TextEditingController();
  TextEditingController _lastName =  new TextEditingController();
  TextEditingController _mobileNo =  new TextEditingController();
  TextEditingController _emailId =  new TextEditingController();

  String _address =  "";
  String _zipCode =  "";
  double _lat=0.00;
  double _long=0.00;





  bool _passwordObscureText =  true;
  bool _confirmPasswordObscureText =  true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    try{

      var res =Provider.of<AuthProvider>(context, listen: false).userAddressListModelItem;
      if(isComeFromCart){
        _firstName =  new TextEditingController(text: res.address[0].firstName);
        _lastName =  new TextEditingController(text: res.address[0].lastName);
        _mobileNo =  new TextEditingController(text: res.address[0].contactNo);
        _emailId =  new TextEditingController(text: res.address[0].email);
       // _address =   res.address[0].text;

      //  _zipCode = res.address[0].pincode;
        //getAddress();


      }


    }catch(error){

    }
    getAddress();

  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    SizeConfig().init(context);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 1,
          title: Text(title,
            style: TextStyle(
                fontSize: ResponsiveFlutter.of(context).fontSize(2),
                fontWeight: FontWeight.w900,

                color: R.color.black_color),),
        ),
        body: Consumer<AuthProvider>(
            builder: (context, provider, child) {
              return  Container(
                height: height,
                width: width,
                //margin: EdgeInsets.all(20.0),
                child: SingleChildScrollView(
                  child: Column(
                    children: [

                 /*     SizedBox(
                        width: SizeConfig.horizontalBloc * 90,
                        child: Image.asset(R.images.journey_image2,),
                      ),
*/
                   //   SizedBox(height: 10,),
                      Container(
                        child:  Form(
                          key: _formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [


                              Container(
                                width: double.maxFinite,
                                padding: EdgeInsets.only(top: 10,bottom: 10),
                                color: Colors.grey.shade400,
                                child:  Padding(padding: EdgeInsets.only(left: 20,bottom: 5,top: 5),
                                  child:  Text(
                                    'CONTACT DETAILS',
                                    style: TextStyle(
                                        fontFamily: 'openSans',
                                        fontWeight: FontWeight.bold,
                                        letterSpacing: 0.8,
                                        color: Colors.black,
                                        fontSize: SizeConfig.normalFontSize
                                    ),
                                  ),
                                ),
                              ),

                              Container(
                                padding: EdgeInsets.only(top: 10,bottom: 10,left: 10,right: 10),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 70,
                                      child: TextFormField(

                                        controller: _firstName,
                                        decoration: InputDecoration(
                                          hintText: 'First Name',
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5.0),
                                          ),
                                        ),
                                        validator: (text) {
                                          if (text == null || text.isEmpty) {
                                            return 'Enter First Name';
                                          }
                                          return null;
                                        },
                                      ),
                                    ),

                                    SizedBox(height: 20.0,),

                                    SizedBox(
                                      height: 70,
                                      child: TextFormField(
                                        controller: _lastName,
                                        decoration: InputDecoration(
                                          hintText: 'Last Name',
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5.0),
                                          ),
                                        ),
                                        validator: (text) {
                                           if (text == null || text.isEmpty) {
                                            return 'Enter last Name';
                                          }
                                          return null;
                                        },
                                      ),),

                                    SizedBox(height: 20.0,),


                                    SizedBox(
                                      height: 70,
                                      child:  TextFormField(
                                        maxLength: 10,
                                        keyboardType: TextInputType.number,
                                        controller: _mobileNo,
                                        decoration: InputDecoration(
                                          hintText: 'Mobile No.',
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5.0),
                                          ),
                                        ),
                                        validator: (text) {
                                          if (text == null || text.isEmpty || text.length != 10) {
                                            return 'Enter 10 Digit Mobile No.';
                                          }
                                          return null;
                                        },
                                      ),),

                                    SizedBox(height: 20.0,),

                                    SizedBox(
                                      height: 70,
                                      child: TextFormField(
                                        controller: _emailId,
                                        decoration: InputDecoration(
                                          hintText: 'Email Id',
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5.0),
                                          ),
                                        ),
                                        validator: (text) {
                                          if (text == null || text.isEmpty || AppUtil.isEmailInvalid(text.trim())) {
                                            return 'Enter Valid Email Id';
                                          }
                                          return null;
                                        },
                                      ),),


                                    SizedBox(height: 10.0,),
                                  ],
                                ),
                              ),


                              Container(
                                width: double.maxFinite,
                                padding: EdgeInsets.only(top: 10,bottom: 10),
                                color: Colors.grey.shade400,
                                child:  Padding(padding: EdgeInsets.only(left: 20,bottom: 5,top: 5),
                                  child:  Text(
                                    'ADDRESS',
                                    style: TextStyle(
                                        fontFamily: 'openSans',
                                        fontWeight: FontWeight.bold,
                                        letterSpacing: 0.8,
                                        color: Colors.black,
                                        fontSize: SizeConfig.normalFontSize
                                    ),
                                  ),
                                ),
                              ),


                              Container(
                                  padding: EdgeInsets.only(top: 10,bottom: 10,left: 10,right: 10),
                                  child: Column(
                                    children: [

                                      /*SizedBox(
                                        height: 70,
                                        child: TextFormField(
                                          maxLength: 40,
                                          controller: _address,
                                          decoration: InputDecoration(
                                            hintText: 'Address 1',
                                            border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(5.0),
                                            ),
                                          ),
                                          validator: (text) {
                                            if (text == null || text.isEmpty) {
                                              return 'Enter address ';
                                            }
                                            return null;
                                          },
                                        ),

                                      ),

                                      SizedBox(height: 20.0,),



                                      SizedBox(
                                        height: 70,
                                        child:  TextFormField(
                                          maxLength: 40,
                                          controller: _address2,
                                          decoration: InputDecoration(
                                            hintText: 'Address 2',
                                            border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(5.0),
                                            ),
                                          ),
                                          validator: (text) {
                                            if (text == null || text.isEmpty) {
                                              return 'Enter Address 2';
                                            }
                                            return null;
                                          },
                                        ),
                                      ),

                                      SizedBox(height: 20.0,),

                                      SizedBox(
                                        height: 70,
                                        child:TextFormField(
                                          maxLength: 6,
                                          keyboardType: TextInputType.number,
                                          controller: _zipCode,
                                          decoration: InputDecoration(
                                            hintText: 'Pin Code ',
                                            border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(5.0),
                                            ),
                                          ),
                                          validator: (text) {
                                            if (text == null || text.isEmpty) {
                                              return 'Enter Pin Code';
                                            }
                                            return null;
                                          },
                                          onChanged: (String pinCode) async{
                                           if(pinCode.length == 6){
                                              AppUtil.showLoadingProgress(context);
                                              var res =  await provider.getStateCity( pincode: pinCode);
                                              Navigator.of(context).pop();
                                              if(res){
                                                setState(() {
                                                  isPincodeAvailable=true;
                                                });


                                             *//*   setState(() {

                                               *//**//*
                                                  _state.text = provider.state.toString().trim()=="null" || provider.state.toString().isEmpty?"":provider.state.toString();
                                                  _city.text = provider.city.toString().trim()=="null" || provider.city.toString().isEmpty ?"":provider.city.toString().trim();
*//**//*
                                                  if(provider.state.toString().trim()=="null" || provider.state.toString().isEmpty){
                                                    Common.myShowAlertDialogDetails(context: context,msg:"Sorry, Pincode not serviceable",title: "Service Alert",howManyPop: 1);
                                                  }
                                                });*//*
                                              }else{
                                                isPincodeAvailable=false;
                                                AppUtil.showAlertDialog(context,"Sorry, Pincode not serviceable","Service Alert");

                                              }

                                            }
                                          },
                                        ),
                                      ),

                                 */



                                      Container(

                                        width: MediaQuery.of(context).size.width,
                                        child: Card(
                                          child: Column(
                                            children: [
                                              Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Expanded(
                                                    flex: 1,
                                                    child: Container(
                                                      margin: EdgeInsets.only(left: 10, top: 15),
                                                      child: Text(
                                                        "Address :",
                                                        style: TextStyle(
                                                            letterSpacing: 0.5,
                                                            color: Colors.black54, fontSize: 14),
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 3,
                                                    child: Container(
                                                      margin: EdgeInsets.only(left: 10, top: 15),
                                                      child: Text(
                                                        ""+_address,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 13,
                                                            letterSpacing: 0.3,
                                                            fontWeight: FontWeight.bold),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Expanded(
                                                    flex: 1,
                                                    child: Container(
                                                      margin: EdgeInsets.only(left: 10, top: 15),
                                                      child: Text(
                                                        "Pincode :",
                                                        style: TextStyle(
                                                            letterSpacing: 0.5,
                                                            color: Colors.black54, fontSize: 14),
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 3,
                                                    child: Container(
                                                      margin: EdgeInsets.only(left: 10, top: 15),
                                                      child: Text(
                                                        ""+_zipCode,
                                                        style: TextStyle(
                                                          fontSize: 13,
                                                          letterSpacing: 0.3,
                                                          fontWeight: FontWeight.bold,
                                                          color: Colors.black,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),


                                              InkWell(

                                                onTap: (){

                                                 /* Navigator.pushReplacement(context,
                                                      MaterialPageRoute(builder: (context) =>
                                                          CurrentLocation()));*/

                                                  Navigator.push(context,
                                                      MaterialPageRoute(builder: (context) =>
                                                          MapScreen(false, _lat,
                                                              _long,WHERE_IT_COME_FROM.userUpdateAddress.toString()
                                                          )
                                                      )
                                                  );



                                             /*     Navigator.pushNamed(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (BuildContext context) =>
                                                              CurrentLocation()),
                                                          (Route<dynamic> route) => false);,*/
                                                },

                                                child: Row(
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(left: 10, top: 15,bottom: 10),
                                                      child: Text(
                                                        "Change Delivery Address",
                                                        style: TextStyle(
                                                            letterSpacing: 0.5,
                                                            decoration: TextDecoration.underline,
                                                            color: Colors.blue, fontSize: 14,fontWeight: FontWeight.bold),
                                                      ),
                                                    ),

                                                  ],
                                                ),
                                              ),

                                              SizedBox(height: 10,)

                                            ],
                                          ),
                                          elevation: 2,
                                          shadowColor: Colors.grey,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(8.0),
                                            ),
                                            side: BorderSide(
                                              color: Colors.grey.withOpacity(0.2),
                                              width: 1,
                                            ),
                                          ),
                                        ),
                                      ),


                                      SizedBox(height: 10,),
                                      Container(
                                        margin: new EdgeInsets.only(left: 25.0,right: 25),
                                        child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            primary: R.color.app_color,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(0.0),
                                                topLeft: Radius.circular(0.0),
                                                bottomRight: Radius.circular(0.0),
                                                bottomLeft: Radius.circular(0.0),
                                              ),
                                            ),
                                            minimumSize: Size(double.infinity, ResponsiveFlutter.of(context).hp(5.0)), // double.infinity is the width and 30 is the height
                                          ),
                                          onPressed: () async{
                                            if (_formKey.currentState.validate()) {
                                             /* if(_city.text.isEmpty){
                                              *//*  Common.myShowAlertDialogDetails(context: context,msg:"Sorry, Pincode not serviceable",
                                                    title: "Service Alert",howManyPop: 1);*//*
                                                return;

                                              }*/


                                             /* if(!isPincodeAvailable){
                                                AppUtil.showAlertDialog(context,"Sorry, Pincode not serviceable","Service Alert");
                                                return;
                                              }*/


                                              var phone =provider.userModel.user.phone;

                                        /*      if(phone.toString()!=_mobileNo.text.toString().trim()){
                                                Navigator.pushNamed(context, StringConfig.OTP_VERIFY,arguments: {'name': "", 'password':"",'email' :"",'mobile':_mobileNo.text,'comefrom':"shippingAddress"});

                                              }else{*/

       if(_zipCode.length == 6) {
         AppUtil.showLoadingProgress(context);
         var res = await provider.getStateCity(pincode: _zipCode);
         Navigator.of(context).pop();
         if (true) {
           AppUtil.showLoadingProgress(context);

           String addressdId = "0";
           if(provider.userAddressListModelItem !=null){
             addressdId = provider.userAddressListModelItem.address[0].id.toString();
           }
           var res = await provider.saveShippingAddress(
               addressId: addressdId,
               fName:  _firstName.text.toString().trim(),
               lName:   _lastName.text.toString().trim(),
               contactNo: _mobileNo.text.toString().trim(),
               email: _emailId.text.toString().trim(),
               address:  _address.trim(),
               addressLine2: _address.trim(),
               pincode: _zipCode.trim(),
               isDefaultAddress: true,
               type: "Home",
               latitude: _lat,
               longitude: _long);

           Navigator.of(context).pop();
           if(res == true){
             Navigator.of(context).pop();
           }

         }else{
           AppUtil.showAlertDialog(context,"Sorry, Pincode not serviceable","Service Alert");

         }


         }
       }






                                           /* }*/
                                          },
                                          child: Text('SAVE'.toUpperCase(),style:
                                          TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(2.0,),
                                              color: R.color.white_color,fontWeight: FontWeight.bold,fontFamily: "ralewayMedium"),),
                                        ),
                                      ),

                                      SizedBox(height:20.0),
                                    ],

                                  )
                              ),






                            ],
                          ),
                        ),
                      )

                    ],
                  ),
                ),
              );
            })

    );
  }

  Future<void> getAddress()  async {




      _lat= await PreferenceUtils.getDouble(
          PreferencesKey.userLat);

      _long= await PreferenceUtils.getDouble(
          PreferencesKey.userLong);


      _zipCode= await PreferenceUtils.getString(
          PreferencesKey.userLocationPincode);


      _address= await PreferenceUtils.getString(
          PreferencesKey.ADDFULL);


      setState(() {

      });




  }
}
