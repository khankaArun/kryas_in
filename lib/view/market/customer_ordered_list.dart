
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:krays_in/model/order_lis_model.dart';
import 'package:krays_in/model/user_address.dart';
import 'package:krays_in/provider/AuthProvider.dart';
import 'package:krays_in/provider/order_provider.dart';
import 'package:krays_in/util/ResponsiveFlutter.dart';
import 'package:krays_in/util/app_util.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:provider/provider.dart';

import 'order_detail_page.dart';


class OrderPage extends StatefulWidget {
  OrderPage({Key key}) : super(key: key);

  @override
  _OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
        title: Text(R.string.order_page,
          style: TextStyle(
              fontSize: SizeConfig.headerFontSize,
              fontWeight: FontWeight.w700,
              color: R.color.black_color),),
      ),
      body: Consumer<OrderProvider>(
        builder: (context, provider, child) {
          return  provider.orderList !=  null ?
          SingleChildScrollView(
            child: Container(
                margin: EdgeInsets.only(left: 10.0, right: 10.00),
                child:  Container(
                  margin: EdgeInsets.all(10.0),
                  child: ListView.builder(
                    itemCount: provider.orderList.data.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index){
                      return Container(
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 15.0,left: 15,bottom: 0),
                              child: Row(
                                children: [
                                  Text("Order Date - ",
                                    style: TextStyle(
                                      letterSpacing: 0.5,
                                        fontSize: SizeConfig.medimFontSize,
                                        color: Colors.grey),
                                  ),

                                  Text(AppUtil.getDateTime(provider.orderList.data[index].createdAt.toString()),
                                    style: TextStyle(

                                        letterSpacing: 0.3,
                                        fontSize: SizeConfig.medimFontSize,
                                        color: Colors.grey),),
                                ],
                              ),
                            ),

                         InkWell(
                           onTap: () async {

                             AppUtil.showLoadingProgress(context);

                             UserAddressListModelItem  userAddress=Provider.of<AuthProvider>(context,listen: false).userAddressListModelItem;
                             var res= await  Provider.of<OrderProvider>(context,listen: false).getDetails(context,provider.orderList.data[index].id);
                             Navigator.pop(context);
                             if(res){
                               Navigator.push(context, MaterialPageRoute(builder: (context)=>OrderDetailPage(provider.orderList.data,index,userAddress)));
                             }

                           },
                           child:  Card(
                             elevation: 3,
                             shape: RoundedRectangleBorder(
                               //  side: BorderSide(color: Colors.white70, width: 1),
                               borderRadius: BorderRadius.circular(10),
                             ),

                             child:  Column(
                               children: [


                                 Container(
                                   margin: EdgeInsets.all(5.0),
                                   child: Row(
                                     children: [

                                       SizedBox(
                                         //height: SizeConfig.verticalBloc * 23,
                                         width: SizeConfig.horizontalBloc * 30,
                                         child: Container(
                                           margin: EdgeInsets.all(5.0),
                                           child:Container(
                                             padding: EdgeInsets.all(15),
                                             child: Image.asset(R.images.ic_veg_bucket,/* color: R.color.app_color,*/),
                                           ),
                                         ),
                                       ),

                                       SizedBox(
                                         width: SizeConfig.verticalBloc * 1,
                                       ),



                                       Column(
                                         mainAxisAlignment: MainAxisAlignment.start,
                                         crossAxisAlignment: CrossAxisAlignment.start,
                                         children: [

                                           SizedBox(
                                             width:  SizeConfig.horizontalBloc * 40,
                                             child:  Text("Order ID 000"+provider.orderList.data[index].id,
                                               textAlign: TextAlign.start,
                                               style: TextStyle(
                                                 fontWeight: FontWeight.bold,
                                                 fontFamily: 'openSans',
                                                 letterSpacing: 0.3,
                                                 fontSize: ResponsiveFlutter.of(context).fontSize(2),
                                                 color: R.color.black_color,

                                               ),

                                             ),
                                           ),

                                           SizedBox(
                                             height: SizeConfig.verticalBloc * 0.1,
                                           ),


                                           Row(
                                             mainAxisAlignment: MainAxisAlignment.center,
                                             crossAxisAlignment: CrossAxisAlignment.center,
                                             children: [

                                               Text(provider.orderList.data[index].totalItems.toString()+" Items",
                                                 style: TextStyle(
                                                     fontSize: SizeConfig.medimFontSize,
                                                     // decoration: TextDecoration.lineThrough,
                                                     decorationThickness: 1.85,
                                                     letterSpacing: 1,
                                                     color: R.color.grey_color),
                                               ),

                                               SizedBox(
                                                 width: SizeConfig.verticalBloc * 1,
                                               ),



                                             ],
                                           ),

                                           SizedBox(
                                             height: SizeConfig.verticalBloc * 0.5,
                                           ),

                                           Text(provider.orderList.data[index].orderStatus.toLowerCase().contains("declined")?"Assign to Vendor":provider.orderList.data[index].orderStatus, style: TextStyle(
                                               fontSize: SizeConfig.smallFontSize,
                                               fontWeight: FontWeight.w500,
                                               letterSpacing: 1,
                                               color: provider.orderList.data[index].orderStatus.toLowerCase().contains("cancelled")?Colors.red:R.color.app_color),
                                           ),


                                           SizedBox(
                                             height: SizeConfig.verticalBloc * 0.5,
                                           ),




                                         ],
                                       ),


                                     ],
                                   ),
                                 ),




                               ],
                             ),
                           ),
                         )

                          ],
                        ),
                      );


                    },
                  ),
                ),
            ),
          )

              : Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    child: Image.asset(R.images.empty_cart,scale: 2,),
                  ),
                  Text("No order found",
                    style: TextStyle(
                        fontSize: SizeConfig.headerFontSize,
                        fontWeight: FontWeight.w500,
                        color: R.color.black_color),),
                ],
              )
          );
        },
      ),
    );
  }



}