class OrderListItem {
  bool status;
  String message;
  List<Data> data;

  OrderListItem({this.status, this.message, this.data});

  OrderListItem.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String orderStatus;
  String status;
  String orderType;
  String orderTypeNm;
  String id;
  String createdAt;
  String updatedAt;
  String user;
  String pstatus;
  String tax;
  String hint;
  String active;
  String method;
  String address;
  String phone;
  String lat;
  String lng;
  String percent;
  String couponName;
  String vendor;
  String slotTimeFrom;
  String slotTimeTo;
  String paymentMode;
  String payableAmt;
  String transactionId;
  dynamic totalItems;
  int totalQty;
  int netAmt;
  int totalTax;
  dynamic totalDiscount;
  int totalAmt;

  Data(
      {this.orderStatus,
        this.status,
        this.orderType,
        this.orderTypeNm,
        this.id,
        this.createdAt,
        this.updatedAt,
        this.user,
        this.pstatus,
        this.tax,
        this.hint,
        this.active,
        this.method,
        this.address,
        this.phone,
        this.lat,
        this.lng,
        this.percent,
        this.couponName,
        this.vendor,
        this.slotTimeFrom,
        this.slotTimeTo,
        this.paymentMode,
        this.payableAmt,
        this.transactionId,
        this.totalItems,
        this.totalQty,
        this.netAmt,
        this.totalTax,
        this.totalDiscount,
        this.totalAmt});

  Data.fromJson(Map<String, dynamic> json) {
    orderStatus = json['orderStatus'];
    status = json['status'];
    orderType = json['orderType'];
    orderTypeNm = json['orderTypeNm'];
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    user = json['user'];
    pstatus = json['pstatus'];
    tax = json['tax'];
    hint = json['hint'];
    active = json['active'];
    method = json['method'];
    address = json['address'];
    phone = json['phone'];
    lat = json['lat'];
    lng = json['lng'];
    percent = json['percent'];
    couponName = json['couponName'];
    vendor = json['vendor'];
    slotTimeFrom = json['slot_time_from'];
    slotTimeTo = json['slot_time_to'];
    paymentMode = json['payment_mode'];
    payableAmt = json['payable_amt'];
    transactionId = json['transaction_id'];
    totalItems = json['totalItems'];
    totalQty = json['totalQty'];
    netAmt = json['netAmt'];
    totalTax = json['totalTax'];
    totalDiscount = json['totalDiscount'];
    totalAmt = json['totalAmt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderStatus'] = this.orderStatus;
    data['status'] = this.status;
    data['orderType'] = this.orderType;
    data['orderTypeNm'] = this.orderTypeNm;
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['user'] = this.user;
    data['pstatus'] = this.pstatus;
    data['tax'] = this.tax;
    data['hint'] = this.hint;
    data['active'] = this.active;
    data['method'] = this.method;
    data['address'] = this.address;
    data['phone'] = this.phone;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['percent'] = this.percent;
    data['couponName'] = this.couponName;
    data['vendor'] = this.vendor;
    data['slot_time_from'] = this.slotTimeFrom;
    data['slot_time_to'] = this.slotTimeTo;
    data['payment_mode'] = this.paymentMode;
    data['payable_amt'] = this.payableAmt;
    data['transaction_id'] = this.transactionId;
    data['totalItems'] = this.totalItems;
    data['totalQty'] = this.totalQty;
    data['netAmt'] = this.netAmt;
    data['totalTax'] = this.totalTax;
    data['totalDiscount'] = this.totalDiscount;
    data['totalAmt'] = this.totalAmt;
    return data;
  }
}
