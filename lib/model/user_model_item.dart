class UserModelItem {
  dynamic error;
  User user;
  dynamic accessToken;
  dynamic notify;
  dynamic appType;

  UserModelItem(
      {this.error, this.user, this.accessToken, this.notify, this.appType});

  UserModelItem.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    accessToken = json['access_token'];
    notify = json['notify'];
    appType = json['appType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['access_token'] = this.accessToken;
    data['notify'] = this.notify;
    data['appType'] = this.appType;
    return data;
  }
}

class User {
  int id;
  String name;
  String email;
  Null emailVerifiedAt;
  String password;
  String createdAt;
  String updatedAt;
  String role;
  String imageid;
  String address;
  String phone;
  Null nToken;
  String fcbToken;
  Null active;
  String vendor;
  String typeReg;
  String lat;
  String lng;
  String speed;
  String sex;
  String age;
  String occupation;
  String interest;
  String avatar;
  dynamic refer_code;

  User(
      {this.id,
        this.name,
        this.email,
        this.emailVerifiedAt,
        this.password,
        this.createdAt,
        this.updatedAt,
        this.role,
        this.imageid,
        this.address,
        this.phone,
        this.nToken,
        this.fcbToken,
        this.active,
        this.vendor,
        this.typeReg,
        this.lat,
        this.lng,
        this.speed,
        this.sex,
        this.age,
        this.occupation,
        this.interest,
        this.avatar,
        this.refer_code});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    password = json['password'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    role = json['role'];
    imageid = json['imageid'];
    address = json['address'];
    phone = json['phone'];
    nToken = json['_token'];
    fcbToken = json['fcbToken'];
   // active = json['active'];
    vendor = json['vendor'];
    typeReg = json['typeReg'];
    lat = json['lat'];
    lng = json['lng'];
    speed = json['speed'];
    sex = json['sex'];
    age = json['age'];
    occupation = json['occupation'];
    interest = json['interest'];
    avatar = json['avatar'];
    refer_code = json['refer_code'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['password'] = this.password;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['role'] = this.role;
    data['imageid'] = this.imageid;
    data['address'] = this.address;
    data['phone'] = this.phone;
    data['_token'] = this.nToken;
    data['fcbToken'] = this.fcbToken;
    data['active'] = this.active;
    data['vendor'] = this.vendor;
    data['typeReg'] = this.typeReg;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['speed'] = this.speed;
    data['sex'] = this.sex;
    data['age'] = this.age;
    data['occupation'] = this.occupation;
    data['interest'] = this.interest;
    data['avatar'] = this.avatar;

    data['refer_code'] = this.refer_code;
    return data;
  }
}


class OtpAuthItem {
  dynamic error;
  dynamic mobile;
  dynamic otp;
  List<String> message;

  OtpAuthItem({this.error, this.mobile, this.otp, this.message});

  OtpAuthItem.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    mobile = json['mobile'];
    otp = json['otp'];
    message = json['message'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['mobile'] = this.mobile;
    data['otp'] = this.otp;
    data['message'] = this.message;
    return data;
  }
}


class SignupItem {
  dynamic error;
  UserSignUp user;
  String accessToken;

  SignupItem({this.error, this.user, this.accessToken});

  SignupItem.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    user = json['user'] != null ? new UserSignUp.fromJson(json['user']) : null;
    accessToken = json['access_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['access_token'] = this.accessToken;
    return data;
  }
}

class UserSignUp {
  String email;
  String typeReg;
  String name;
  String password;
  String role;
  String updatedAt;
  String createdAt;
  dynamic id;
  String avatar;

  UserSignUp(
      {this.email,
        this.typeReg,
        this.name,
        this.password,
        this.role,
        this.updatedAt,
        this.createdAt,
        this.id,
        this.avatar});

  UserSignUp.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    typeReg = json['typeReg'];
    name = json['name'];
    password = json['password'];
    role = json['role'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['typeReg'] = this.typeReg;
    data['name'] = this.name;
    data['password'] = this.password;
    data['role'] = this.role;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['id'] = this.id;
    data['avatar'] = this.avatar;
    return data;
  }
}

