class OrderDetailsItem {
  bool status;
  String message;
  List<Data> data;

  OrderDetailsItem({this.status, this.message, this.data});

  OrderDetailsItem.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String orderId;
  String productId;
  String productName;
  String qty;
  String salePrice;
  String image;
  String status;
  String statusName;
  String unit;
  String desc;


  Data(
      {this.orderId,
        this.productId,
        this.productName,
        this.qty,
        this.salePrice,
        this.image,
        this.status,
        this.statusName,
        this.unit,
        this.desc,
      });

  Data.fromJson(Map<String, dynamic> json) {
    orderId = json['orderId'];
    productId = json['productId'];
    productName = json['productName'];
    qty = json['qty'];
    salePrice = json['salePrice'];
    image = json['image'];
    status = json['status'];
    statusName = json['statusName'];
    unit = json['unit'];
    desc = json['desc'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['productId'] = this.productId;
    data['productName'] = this.productName;
    data['qty'] = this.qty;
    data['salePrice'] = this.salePrice;
    data['image'] = this.image;
    data['status'] = this.status;
    data['statusName'] = this.statusName;
    data['unit'] = this.unit;
    data['desc'] = this.desc;

    return data;
  }
}


class OrderCompleteDetailsItem {
  bool status;
  String message;
  List<Data1> data;

  OrderCompleteDetailsItem({this.status, this.message, this.data});

  OrderCompleteDetailsItem.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = <Data1>[];
      json['data'].forEach((v) {
        data.add(new Data1.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data1 {
  String orderId;
  String productId;
  String productName;
  String qty;
  String salePrice;
  String image;
  String status;
  String statusName;
  String unit;
  String desc;
  dynamic weight;
  String vendorName;

  Data1(
      {this.orderId,
        this.productId,
        this.productName,
        this.qty,
        this.salePrice,
        this.image,
        this.status,
        this.statusName,
        this.unit,
        this.desc,
        this.weight,
        this.vendorName});

  Data1.fromJson(Map<String, dynamic> json) {
    orderId = json['orderId'];
    productId = json['productId'];
    productName = json['productName'];
    qty = json['qty'];
    salePrice = json['salePrice'];
    image = json['image'];
    status = json['status'];
    statusName = json['statusName'];
    unit = json['unit'];
    desc = json['desc'];
    weight = json['weight'];
    vendorName=json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['productId'] = this.productId;
    data['productName'] = this.productName;
    data['qty'] = this.qty;
    data['salePrice'] = this.salePrice;
    data['image'] = this.image;
    data['status'] = this.status;
    data['statusName'] = this.statusName;
    data['unit'] = this.unit;
    data['desc'] = this.desc;
    data['weight'] = this.weight;
    data['name'] = this.vendorName;
    return data;
  }
}

