class SearchModelItem {
  bool status;
  List<Data> data;
  String message;

  SearchModelItem({this.status, this.data, this.message});

  SearchModelItem.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    return data;
  }
}

class Data {
  String id;
  String name;
  String subCatId;
  String subCat;
  String companyName;
  String brandName;

  Data(
      {this.id,
        this.name,
        this.subCatId,
        this.subCat,
        this.companyName,
        this.brandName});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    subCatId = json['subCatId'];
    subCat = json['subCat'];
    companyName = json['companyName'];
    brandName = json['brandName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['subCatId'] = this.subCatId;
    data['subCat'] = this.subCat;
    data['companyName'] = this.companyName;
    data['brandName'] = this.brandName;
    return data;
  }
}
