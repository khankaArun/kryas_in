class UserAddressListModelItem {
bool status;
List<Address> address;

UserAddressListModelItem({this.status, this.address});

UserAddressListModelItem.fromJson(Map<String, dynamic> json) {
status = json['status'];
if (json['address'] != null) {
address = <Address>[];
json['address'].forEach((v) { address.add(new Address.fromJson(v)); });
}
}

Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['status'] = this.status;
  if (this.address != null) {
    data['address'] = this.address.map((v) => v.toJson()).toList();
  }
  return data;
}
}

class Address {
  String id;
  String createdAt;
  String updatedAt;
  String user;
  String firstName;
  String lastName;
  String contactNo;
  String email;
  String text;
  String addressLine2;
  String pincode;
  String lat;
  String lng;
  String type;
  String default1;
  String isDefaultAddress;

  Address({this.id, this.createdAt, this.updatedAt, this.user, this.firstName, this.lastName,
  this.contactNo, this.email, this.text, this.addressLine2, this.pincode,
  this.lat, this.lng, this.type, this.default1, this.isDefaultAddress});

Address.fromJson(Map<String, dynamic> json) {
id = json['id'];
createdAt = json['created_at'];
updatedAt = json['updated_at'];
user = json['user'];
firstName = json['first_name'];
lastName = json['last_name'];
contactNo = json['contact_no'];
email = json['email'];
text = json['text'];
addressLine2 = json['address_line2'];
pincode = json['pincode'];
lat = json['lat'];
lng = json['lng'];
type = json['type'];
default1 = json['default1'];
isDefaultAddress = json['default'];
}

Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['id'] = this.id;
  data['created_at'] = this.createdAt;
  data['updated_at'] = this.updatedAt;
  data['user'] = this.user;
  data['first_name'] = this.firstName;
  data['last_name'] = this.lastName;
  data['contact_no'] = this.contactNo;
  data['email'] = this.email;
  data['text'] = this.text;
  data['address_line2'] = this.addressLine2;
  data['pincode'] = this.pincode;
  data['lat'] = this.lat;
  data['lng'] = this.lng;
  data['type'] = this.type;
  data['default1'] = this.default1;
  data['default'] = this.isDefaultAddress;
  return data;
}
}