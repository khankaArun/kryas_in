
import 'dart:convert';

List<BrandModel> fromJsonToBrand(String json) {
  List jsonData = jsonDecode(json);
  return jsonData.map((map) => BrandModel.fromJson(map)).toList();
}

class BrandModel {
  int brandID;
  dynamic cRMBrandID;
  String brandName;
  dynamic description;
  dynamic displayOrder;
  dynamic imageURL;
  dynamic metaDescription;
  dynamic metaTitle;
  dynamic metaKeys;
  dynamic createdOn;
  dynamic modifiedOn;
  dynamic createdBy;
  dynamic modifiedBy;
  bool isActive;
  bool isSelected =  false;

  BrandModel({
        this.brandID,
        this.cRMBrandID,
        this.brandName,
        this.description,
        this.displayOrder,
        this.imageURL,
        this.metaDescription,
        this.metaTitle,
        this.metaKeys,
        this.createdOn,
        this.modifiedOn,
        this.createdBy,
        this.modifiedBy,
        this.isActive,
        this.isSelected});


  factory BrandModel.fromJson(Map<String, dynamic> json) {
    print(json);
    return BrandModel(
        brandID : json['BrandID'],
        cRMBrandID : json['CRMBrandID'],
        brandName : json['BrandName'],
        description : json['Description'],
        displayOrder : json['DisplayOrder'],
        imageURL : json['ImageURL'],
        metaDescription : json['MetaDescription'],
        metaTitle : json['MetaTitle'],
        metaKeys : json['MetaKeys'],
        createdOn : json['CreatedOn'],
        modifiedOn : json['ModifiedOn'],
        createdBy : json['CreatedBy'],
        modifiedBy : json['ModifiedBy'],
        isActive : json['IsActive'],
    );
  }
}