class UserCartModel {
  bool status;
  String message;
  List<Data> data;
  dynamic totalQty;
  dynamic netAmt;
  dynamic disAmt;
  dynamic payAmt;
  dynamic grossAmt;

  UserCartModel(
      {this.status,
        this.message,
        this.data,
        this.totalQty,
        this.netAmt,
        this.disAmt,
        this.payAmt,
        this.grossAmt});

  UserCartModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    totalQty = json['totalQty'];
    netAmt = json['netAmt'];
    disAmt = json['disAmt'];
    payAmt = json['payAmt'];
    grossAmt = json['grossAmt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['totalQty'] = this.totalQty;
    data['netAmt'] = this.netAmt;
    data['disAmt'] = this.disAmt;
    data['payAmt'] = this.payAmt;
    data['grossAmt'] = this.grossAmt;
    return data;
  }
}

class Data {
  String id;
  String createdAt;
  String updatedAt;
  String name;
  String code;
  String imageid;
  String price;
  String discountprice;
  String desc;
  String restaurant;
  String category;
  String subcategory;
  String ingredients;
  String unit;
  String packageCount;
  String weight;
  String canDelivery;
  String stars;
  String published;
  String extras;
  String nutritions;
  String vendor;
  String images;
  String dimension;
  String excerpt;
  String sku;
  String stock;
  String stockStatus;
  String productTag;
  String disclaimer;
  String companyId;
  String brandId;
  String expire;
  String metaId;
  String attributeId;
  String duration;
  String image;
  String companyName;
  dynamic brandName;
  String isCart;
  String isWishlist;
  String disPer;
  String quantity;
  String salePrice;
  dynamic variantId;

  Data(
      {this.id,
        this.createdAt,
        this.updatedAt,
        this.name,
        this.code,
        this.imageid,
        this.price,
        this.discountprice,
        this.desc,
        this.restaurant,
        this.category,
        this.subcategory,
        this.ingredients,
        this.unit,
        this.packageCount,
        this.weight,
        this.canDelivery,
        this.stars,
        this.published,
        this.extras,
        this.nutritions,
        this.vendor,
        this.images,
        this.dimension,
        this.excerpt,
        this.sku,
        this.stock,
        this.stockStatus,
        this.productTag,
        this.disclaimer,
        this.companyId,
        this.brandId,
        this.expire,
        this.metaId,
        this.attributeId,
        this.duration,
        this.image,
        this.companyName,
        this.brandName,
        this.isCart,
        this.isWishlist,
        this.disPer,
        this.quantity,
        this.salePrice,
        this.variantId
      });

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    name = json['name'];
    code = json['code'];
    imageid = json['imageid'];
    price = json['price'];
    discountprice = json['discountprice'];
    desc = json['desc'];
    restaurant = json['restaurant'];
    category = json['category'];
    subcategory = json['subcategory'];
    ingredients = json['ingredients'];
    unit = json['unit'];
    packageCount = json['packageCount'];
    weight = json['weight'];
    canDelivery = json['canDelivery'];
    stars = json['stars'];
    published = json['published'];
    extras = json['extras'];
    nutritions = json['nutritions'];
    vendor = json['vendor'];
    images = json['images'];
    dimension = json['dimension'];
    excerpt = json['excerpt'];
    sku = json['sku'];
    stock = json['stock'];
    stockStatus = json['stock_status'];
    productTag = json['product_tag'];
    disclaimer = json['disclaimer'];
    companyId = json['company_id'];
    brandId = json['brand_id'];
    expire = json['expire'];
    metaId = json['meta_id'];
    attributeId = json['attribute_id'];
    duration = json['duration'];
    image = json['image'];
    companyName = json['companyName'];
    brandName = json['brandName'];
    isCart = json['isCart'];
    isWishlist = json['isWishlist'];
    disPer = json['disPer'];
    quantity = json['quantity'];
    salePrice = json['salePrice'];
    variantId= json['variantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['name'] = this.name;
    data['code'] = this.code;
    data['imageid'] = this.imageid;
    data['price'] = this.price;
    data['discountprice'] = this.discountprice;
    data['desc'] = this.desc;
    data['restaurant'] = this.restaurant;
    data['category'] = this.category;
    data['subcategory'] = this.subcategory;
    data['ingredients'] = this.ingredients;
    data['unit'] = this.unit;
    data['packageCount'] = this.packageCount;
    data['weight'] = this.weight;
    data['canDelivery'] = this.canDelivery;
    data['stars'] = this.stars;
    data['published'] = this.published;
    data['extras'] = this.extras;
    data['nutritions'] = this.nutritions;
    data['vendor'] = this.vendor;
    data['images'] = this.images;
    data['dimension'] = this.dimension;
    data['excerpt'] = this.excerpt;
    data['sku'] = this.sku;
    data['stock'] = this.stock;
    data['stock_status'] = this.stockStatus;
    data['product_tag'] = this.productTag;
    data['disclaimer'] = this.disclaimer;
    data['company_id'] = this.companyId;
    data['brand_id'] = this.brandId;
    data['expire'] = this.expire;
    data['meta_id'] = this.metaId;
    data['attribute_id'] = this.attributeId;
    data['duration'] = this.duration;
    data['image'] = this.image;
    data['companyName'] = this.companyName;
    data['brandName'] = this.brandName;
    data['isCart'] = this.isCart;
    data['isWishlist'] = this.isWishlist;
    data['disPer'] = this.disPer;
    data['quantity'] = this.quantity;
    data['salePrice'] = this.salePrice;
    data['variantId'] = this.variantId;

    return data;
  }
}
