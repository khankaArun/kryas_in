
import 'dart:convert';

List<CategoryBagModel> fromJsonCategoryToBag(String json) {
  List jsonData = jsonDecode(json);
  return jsonData.map((map) => CategoryBagModel.fromJson(map)).toList();
}

class CategoryBagModel {

  String categoryName;
  dynamic stock;
  dynamic categoryId;
  String categoryThumbImage;
  String categoryMenuImage;

  CategoryBagModel({
    this.categoryName, this.stock, this.categoryId, this.categoryThumbImage, this.categoryMenuImage});

  factory CategoryBagModel.fromJson(Map<String, dynamic> json) {
    return CategoryBagModel(
      categoryName: json['CategoryName'],
      stock: json['Stock'],
      categoryId: json['CategoryID'],
      categoryThumbImage :json['CategoryThumbImage'],
      categoryMenuImage: json['CateogoryMenuImage']
    );
  }

}