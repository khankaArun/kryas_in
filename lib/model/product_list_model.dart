
import 'dart:convert';

import 'package:krays_in/model/color_model.dart';


List<ProductListModel> fromJsonToProductList(String json) {
  List jsonData = jsonDecode(json);
  return jsonData.map((map) => ProductListModel.fromJson(map)).toList();
}

class ProductListModel {

  ProductListModel({
    this.dashBoardId,
    this.productName,
    this.discountPrice,
    this.color,
    this.ram,
    this.rom,
    this.productFullName,
    this.onSalePrice,
    this.description,
    this.shortDescription,
    this.categoryId,
    this.categoryName,
    this.parentCategoryName,
    this.warrantyStatus,
    this.brandName,
    this.colors,
    this.discountPercent,
    this.productSize,
    this.modelId,
    this.isImageTag,
    this.imageTagUrl,
    this.categoryDescription,
    this.brandId,
    this.videoUrl,
    this.matrixItems,
    this.imagesItems,
    this.relatedProducts,
    this.productCategory,
    this.matrixId,
    this.sku,
    this.productId,
    this.productSku,
    this.stock,
    this.soldOut,
    this.costPrice,
    this.mrp,
    this.listPrice,
    this.imageUrl,
    this.size,
    this.serialNo,
    this.longDescription,
    this.specialPrice,
    this.b2BPrice,
    this.wholeSalePrice,
    this.displayOrder,
    this.isOnSale,
    this.salePrice,
    this.saleStartDate,
    this.saleEndDate,
    this.metaDesc,
    this.metaKeys,
    this.pageTitle,
    this.imageAlText,
    this.stockLevelAlert,
    this.minimumQty,
    this.orderType,
    this.isBackOrder,
    this.createdOn,
    this.modifiedOn,
    this.createdBy,
    this.modifiedBy,
    this.isActive,
    this.isDeleted,
    this.imageAvailable,
    this.capacity,
    this.waterResistant,
    this.material,
    this.isWishList,
  });

  int dashBoardId;
  String productName;
  double discountPrice;
  String color;
  dynamic ram;
  dynamic rom;
  String productFullName;
  double onSalePrice;
  dynamic description;
  dynamic shortDescription;
  int categoryId;
  String categoryName;
  dynamic parentCategoryName;
  int warrantyStatus;
  String brandName;
  List<Color> colors;
  String discountPercent;
  String productSize;
  int modelId;
  bool isImageTag;
  dynamic imageTagUrl;
  dynamic categoryDescription;
  int brandId;
  dynamic videoUrl;
  dynamic matrixItems;
  dynamic imagesItems;
  dynamic relatedProducts;
  dynamic productCategory;
  int matrixId;
  String sku;
  int productId;
  dynamic productSku;
  int stock;
  dynamic soldOut;
  dynamic costPrice;
  double mrp;
  double listPrice;
  dynamic imageUrl;
  dynamic size;
  dynamic serialNo;
  dynamic longDescription;
  dynamic specialPrice;
  dynamic b2BPrice;
  double wholeSalePrice;
  dynamic displayOrder;
  bool isOnSale;
  dynamic salePrice;
  dynamic saleStartDate;
  dynamic saleEndDate;
  dynamic metaDesc;
  dynamic metaKeys;
  dynamic pageTitle;
  String imageAlText;
  dynamic stockLevelAlert;
  dynamic minimumQty;
  dynamic orderType;
  dynamic isBackOrder;
  dynamic createdOn;
  dynamic modifiedOn;
  dynamic createdBy;
  dynamic modifiedBy;
  dynamic isActive;
  dynamic isDeleted;
  dynamic imageAvailable;
  dynamic capacity;
  dynamic waterResistant;
  dynamic material;
  bool isWishList = false;

  factory ProductListModel.fromJson(Map<String, dynamic> json) {
    return ProductListModel(
        dashBoardId : json['DashBoardID'],
        productName : json['ProductName'],
        discountPrice : json['DiscountPrice'],
        color : json['Color'],
        ram : json['Ram'],
        rom : json['Rom'],
        productFullName : json['ProductFullName'],
        onSalePrice : json['OnSalePrice'],
        description : json['Description'],
        shortDescription : json['ShortDescription'],
        categoryId : json['CategoryID'],
        categoryName : json['CategoryName'],
        parentCategoryName : json['ParentCategoryName'],
        warrantyStatus : json['WarrantyStatus'],
        brandName : json['BrandName'],
    discountPercent : json['DiscountPercent'],
    productSize : json['ProductSize'],
    modelId : json['ModelID'],
    isImageTag : json['IsImageTag'],
    imageTagUrl : json['ImageTagURL'],
    categoryDescription : json['CategoryDescription'],
    brandId : json['BrandID'],
    videoUrl : json['VideoURL'],
    matrixItems : json['MatrixItems'],
    imagesItems : json['ImagesItems'],
    relatedProducts : json['RelatedProducts'],
    productCategory : json['ProductCategory'],
    matrixId : json['MatrixID'],
    sku : json['SKU'],
    productId : json['ProductID'],
    productSku : json['ProductSKU'],
    stock : json['Stock'],
    soldOut : json['SoldOut'],
    costPrice : json['CostPrice'],
    mrp : json['MRP'],
    listPrice : json['ListPrice'],
    imageUrl : json['ImageURL'],
    size : json['Size'],
    serialNo : json['SerialNo'],
    longDescription : json['LongDescription'],
    specialPrice : json['SpecialPrice'],
    b2BPrice : json['B2BPrice'],
    wholeSalePrice : json['WholeSalePrice'],
    displayOrder : json['DisplayOrder'],
    isOnSale : json['IsOnSale'],
    salePrice : json['SalePrice'],
    saleStartDate : json['SaleStartDate'],
    saleEndDate : json['SaleEndDate'],
    metaDesc : json['MetaDesc'],
    metaKeys : json['MetaKeys'],
    pageTitle : json['PageTitle'],
    imageAlText : json['ImageAlText'],
    stockLevelAlert : json['StockLevelAlert'],
    minimumQty : json['MinimumQty'],
    orderType : json['OrderType'],
    isBackOrder : json['IsBackOrder'],
    createdOn : json['CreatedOn'],
    modifiedOn : json['ModifiedOn'],
    createdBy : json['CreatedBy'],
    modifiedBy : json['ModifiedBy'],
    isActive : json['IsActive'],
    isDeleted : json['IsDeleted'],
    imageAvailable : json['ImageAvailable'],
    capacity : json['Capacity'],
    waterResistant : json['WaterResistant'],
    material : json['Material'],
    colors: json['Colors'] !=  null
        ? json['Colors'].map<Color>((value) => Color.fromJson(value)).toList()
        : [],
    );
  }

}

