
import 'dart:convert';

List<ProductGroupsModel> fromJsonToProductGroups(String json) {
  List jsonData = jsonDecode(json);
  return jsonData.map((map) => ProductGroupsModel.fromJson(map)).toList();
}

class ProductGroupsModel {

  dynamic dashboardID;
  String title;

  ProductGroupsModel({
    this.dashboardID,
    this.title});

  factory ProductGroupsModel.fromJson(Map<String, dynamic> json) {
    return ProductGroupsModel(
      dashboardID: json['DashboardID'],
      title: json['Title']
    );}

}