class SaveCartModelItem {
  bool  status;
  String message;
  List<Data> data;

  SaveCartModelItem({this.status, this.message, this.data});

  SaveCartModelItem.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String id;
  String user;
  String productId;
  String quantity;
  String createdDate;
  String updatedDate;
  String createdBy;
  String modifiedBy;

  Data(
      {this.id,
        this.user,
        this.productId,
        this.quantity,
        this.createdDate,
        this.updatedDate,
        this.createdBy,
        this.modifiedBy});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    user = json['user'];
    productId = json['productId'];
    quantity = json['quantity'];
    createdDate = json['createdDate'];
    updatedDate = json['updatedDate'];
    createdBy = json['createdBy'];
    modifiedBy = json['modifiedBy'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user'] = this.user;
    data['productId'] = this.productId;
    data['quantity'] = this.quantity;
    data['createdDate'] = this.createdDate;
    data['updatedDate'] = this.updatedDate;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    return data;
  }
}
