class ProductDetailModel {
  bool status;
  Data data;
  String message;

  ProductDetailModel({this.status, this.data, this.message});

  ProductDetailModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Data {
  String id;
  String createdAt;
  String updatedAt;
  String name;
  String code;
  String imageid;
  String price;
  String discountprice;
  String desc;
  String restaurant;
  String category;
  String subcategory;
  String ingredients;
  String unit;
  String packageCount;
  dynamic weight;
  String canDelivery;
  String stars;
  String published;
  String extras;
  String nutritions;
  String vendor;
  List<String> images;
  String dimension;
  String excerpt;
  String sku;
  String stock;
  String stockStatus;
  String productTag;
  String disclaimer;
  String companyId;
  String brandId;
  String expire;
  String metaId;
  String attributeId;
  String duration;
  String seasonal;
  String image;
  String timeago;
  String companyName;
  String brandName;
  String categoryName;
  String restaurantName;
  int drating;
  String rating;
  String isCart;
  String isWishlist;
  String salePrice;
  String disPer;
  List<Reviews> reviews;
  List<UnitVarients> unitVarients;
  dynamic initalQty;
  bool isSelected;
  dynamic cartQty;


  Data(
      {this.id,
        this.createdAt,
        this.updatedAt,
        this.name,
        this.code,
        this.imageid,
        this.price,
        this.discountprice,
        this.desc,
        this.restaurant,
        this.category,
        this.subcategory,
        this.ingredients,
        this.unit,
        this.packageCount,
        this.weight,
        this.canDelivery,
        this.stars,
        this.published,
        this.extras,
        this.nutritions,
        this.vendor,
        this.images,
        this.dimension,
        this.excerpt,
        this.sku,
        this.stock,
        this.stockStatus,
        this.productTag,
        this.disclaimer,
        this.companyId,
        this.brandId,
        this.expire,
        this.metaId,
        this.attributeId,
        this.duration,
        this.seasonal,
        this.image,
        this.timeago,
        this.companyName,
        this.brandName,
        this.categoryName,
        this.restaurantName,
        this.drating,
        this.rating,
        this.isCart,
        this.isWishlist,
        this.salePrice,
        this.disPer,
        this.reviews,
        this.unitVarients,
        this.initalQty,
        this.isSelected,
      this.cartQty});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    name = json['name'];
    code = json['code'];
    imageid = json['imageid'];
    price = json['price'];
    discountprice = json['discountprice'];
    desc = json['desc'];
    restaurant = json['restaurant'];
    category = json['category'];
    subcategory = json['subcategory'];
    ingredients = json['ingredients'];
    unit = json['unit'];
    packageCount = json['packageCount'];
    weight = json['weight'];
    canDelivery = json['canDelivery'];
    stars = json['stars'];
    published = json['published'];
    extras = json['extras'];
    nutritions = json['nutritions'];
    vendor = json['vendor'];
    images = json['images'].cast<String>();
    dimension = json['dimension'];
    excerpt = json['excerpt'];
    sku = json['sku'];
    stock = json['stock'];
    stockStatus = json['stock_status'];
    productTag = json['product_tag'];
    disclaimer = json['disclaimer'];
    companyId = json['company_id'];
    brandId = json['brand_id'];
    expire = json['expire'];
    metaId = json['meta_id'];
    attributeId = json['attribute_id'];
    duration = json['duration'];
    seasonal = json['seasonal'];
    image = json['image'];
    timeago = json['timeago'];
    companyName = json['companyName'];
    brandName = json['brandName'];
    categoryName = json['categoryName'];
    restaurantName = json['restaurantName'];
    drating = json['drating'];
    rating = json['rating'];
    isCart = json['isCart'];
    isWishlist = json['isWishlist'];
    salePrice = json['salePrice'];
    disPer = json['disPer'];
    cartQty = json['cartQty'];
    initalQty=1;
    isSelected=false;
    if (json['reviews'] != null) {
      reviews = <Reviews>[];
      json['reviews'].forEach((v) {
        reviews.add(new Reviews.fromJson(v));
      });
    }
    if (json['unitVarients'] != null) {
      unitVarients = <UnitVarients>[];
      json['unitVarients'].forEach((v) {
        unitVarients.add(new UnitVarients.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['name'] = this.name;
    data['code'] = this.code;
    data['imageid'] = this.imageid;
    data['price'] = this.price;
    data['discountprice'] = this.discountprice;
    data['desc'] = this.desc;
    data['restaurant'] = this.restaurant;
    data['category'] = this.category;
    data['subcategory'] = this.subcategory;
    data['ingredients'] = this.ingredients;
    data['unit'] = this.unit;
    data['packageCount'] = this.packageCount;
    data['weight'] = this.weight;
    data['canDelivery'] = this.canDelivery;
    data['stars'] = this.stars;
    data['published'] = this.published;
    data['extras'] = this.extras;
    data['nutritions'] = this.nutritions;
    data['vendor'] = this.vendor;
    data['images'] = this.images;
    data['dimension'] = this.dimension;
    data['excerpt'] = this.excerpt;
    data['sku'] = this.sku;
    data['stock'] = this.stock;
    data['stock_status'] = this.stockStatus;
    data['product_tag'] = this.productTag;
    data['disclaimer'] = this.disclaimer;
    data['company_id'] = this.companyId;
    data['brand_id'] = this.brandId;
    data['expire'] = this.expire;
    data['meta_id'] = this.metaId;
    data['attribute_id'] = this.attributeId;
    data['duration'] = this.duration;
    data['seasonal'] = this.seasonal;
    data['image'] = this.image;
    data['timeago'] = this.timeago;
    data['companyName'] = this.companyName;
    data['brandName'] = this.brandName;
    data['categoryName'] = this.categoryName;
    data['restaurantName'] = this.restaurantName;
    data['drating'] = this.drating;
    data['rating'] = this.rating;
    data['isCart'] = this.isCart;
    data['isWishlist'] = this.isWishlist;
    data['salePrice'] = this.salePrice;
    data['disPer'] = this.disPer;
    data['cartQty'] = this.cartQty;
    if (this.reviews != null) {
      data['reviews'] = this.reviews.map((v) => v.toJson()).toList();
    }
    if (this.unitVarients != null) {
      data['unitVarients'] = this.unitVarients.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Reviews {
  String id;
  String createdAt;
  String updatedAt;
  String desc;
  String user;
  String rate;
  String food;

  Reviews(
      {this.id,
        this.createdAt,
        this.updatedAt,
        this.desc,
        this.user,
        this.rate,
        this.food});

  Reviews.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    desc = json['desc'];
    user = json['user'];
    rate = json['rate'];
    food = json['food'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['desc'] = this.desc;
    data['user'] = this.user;
    data['rate'] = this.rate;
    data['food'] = this.food;
    return data;
  }
}

class UnitVarients {
  dynamic weight;
  String unit;
  String salePrice;
  bool isUnitSelected;
  dynamic variantId;

  UnitVarients({this.weight, this.unit, this.salePrice,this.isUnitSelected,this.variantId});

  UnitVarients.fromJson(Map<String, dynamic> json) {
    weight = json['weight'];
    unit = json['unit'];
    salePrice = json['salePrice'];
    variantId = json['variantId'];
    isUnitSelected=false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['weight'] = this.weight;
    data['unit'] = this.unit;
    data['salePrice'] = this.salePrice;
    data['variantId'] = this.variantId;
    return data;
  }
}
