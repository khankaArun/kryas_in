
import 'dart:convert';

List<ExclusiveModel> fromExclusiveProduct(String json) {
  List jsonData = jsonDecode(json);
  return jsonData.map((map) => ExclusiveModel.fromJson(map)).toList();
}

class ExclusiveModel {

  String dashBoardTitle;
  String pageHeading;
  String description;
  String bannerType;
  String bannerImage;
  dynamic dashBoardId;

  ExclusiveModel({
    this.dashBoardTitle,
    this.pageHeading,
    this.description,
    this.bannerType,
    this.bannerImage,
    this.dashBoardId});


  factory ExclusiveModel.fromJson(Map<String, dynamic> json) {
    return ExclusiveModel(
      dashBoardTitle: json['DashboardTitle'],
      pageHeading: json['PageHeading'],
      description: json['Description'],
      bannerType: json['BannerType'],
      bannerImage: json['BannerImage'],
      dashBoardId: json['DashboardTitleID']
    );
  }
}