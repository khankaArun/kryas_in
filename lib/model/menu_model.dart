import 'dart:convert';

List<MenuModel> fromJsonToMenu(String json) {
  List jsonData = jsonDecode(json);
  return jsonData.map((map) => MenuModel.fromJson(map)).toList();
}

class MenuModel{
  dynamic catID;
  String categoryName;
  dynamic description;
  String imageURL;
  dynamic displayOrder;
  dynamic idParentCategory;
  bool isSubCategory;
  dynamic parentCategoryName;
  dynamic isShowTop;
  dynamic tagImageURL;
  dynamic categoryButtonImg;
  bool displayTagImage;
  dynamic pageTitle;
  dynamic metaDescription;
  dynamic metaKeys;
  dynamic imageAltText;
  dynamic categoryLevel;
  dynamic createdOn;
  dynamic modifiedOn;
  dynamic createdBy;
  dynamic modifiedBy;
  bool isActive;

  MenuModel({
        this.catID,
        this.categoryName,
        this.description,
        this.imageURL,
        this.displayOrder,
        this.idParentCategory,
        this.isSubCategory,
        this.parentCategoryName,
        this.isShowTop,
        this.tagImageURL,
        this.categoryButtonImg,
        this.displayTagImage,
        this.pageTitle,
        this.metaDescription,
        this.metaKeys,
        this.imageAltText,
        this.categoryLevel,
        this.createdOn,
        this.modifiedOn,
        this.createdBy,
        this.modifiedBy,
        this.isActive});


  factory MenuModel.fromJson(Map<String, dynamic> json) {
     return MenuModel(
    catID : json['CatID'],
    categoryName : json['CategoryName'],
    description : json['Description'],
    imageURL : json['ImageURL'],
    displayOrder : json['DisplayOrder'],
    idParentCategory : json['IdParentCategory'],
    isSubCategory : json['IsSubCategory'],
    parentCategoryName : json['ParentCategoryName'],
    isShowTop : json['IsShowTop'],
    tagImageURL : json['TagImageURL'],
    categoryButtonImg : json['CategoryButtonImg'],
    displayTagImage : json['DisplayTagImage'],
    pageTitle : json['PageTitle'],
    metaDescription : json['MetaDescription'],
    metaKeys : json['MetaKeys'],
    imageAltText : json['ImageAltText'],
    categoryLevel : json['CategoryLevel'],
    createdOn : json['CreatedOn'],
    modifiedOn : json['ModifiedOn'],
    createdBy : json['CreatedBy'],
    modifiedBy : json['ModifiedBy'],
    isActive : json['IsActive'],);
 }



}