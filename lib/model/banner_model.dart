import 'dart:convert';

List<BannerModel> fromJsonToBanner(String json) {
  List jsonData = jsonDecode(json);
  return jsonData.map((map) => BannerModel.fromJson(map)).toList();
}

class BannerModel {

  dynamic id;
  dynamic pageId;
  String bannerImage;
  String link;
  dynamic bannerType;
  String bannerText;
  dynamic displayOrder;
  dynamic validFrom;
  dynamic validTill;
  String stateName;
  dynamic stateCode;
  String description;
  dynamic addedOn;
  dynamic modifiedOn;
  dynamic addedBy;
  dynamic modifiedBy;
  bool isActive;
  bool isDeleted;

  BannerModel({
    this.id,
    this.pageId,
    this.bannerImage,
    this.link,
    this.bannerType,
    this.bannerText,
    this.displayOrder,
    this.validFrom,
    this.validTill,
    this.stateName,
    this.stateCode,
    this.description,
    this.addedOn,
    this.modifiedOn,
    this.addedBy,
    this.modifiedBy,
    this.isActive,
    this.isDeleted
   });

  factory BannerModel.fromJson(Map<String, dynamic> json) {
           return BannerModel(
              id: json['ID'],
              pageId: json['PageID'],
              bannerImage: json['BannerImage'],
              link: json['link'],
              bannerType: json['BannerType'],
              bannerText: json['BannerText'],
              displayOrder: json['DisplayOrder'],
              validFrom: json['ValidFrom'],
              validTill : json['ValidTill'],
              stateName: json['StateName'],
              stateCode: json['StateCode'],
              description: json['Description'],
              addedOn: json['AddedOn'],
              addedBy: json['AddedBy'],
              modifiedOn: json['ModifiedOn'],
              modifiedBy: json['ModifiedBy'],
             isActive: json['IsActive'],
             isDeleted: json['IsDeleted']
           );
  }

}