class Color {
  String colorID;
  String color;
  var size;
  int matrixID;
  int productID;
  int dashBoardID;
  bool isSelected;
  bool isSelectedSize;
  String setSelectedColorCss;
  String colorImage;
  List<ProductSizes> productSizes;

  Color(
      {this.colorID,
        this.color,
        this.size,
        this.matrixID,
        this.productID,
        this.dashBoardID,
        this.isSelected,
        this.isSelectedSize,
        this.setSelectedColorCss,
        this.colorImage,
        this.productSizes});

  Color.fromJson(Map<String, dynamic> json) {
    colorID = json['ColorID'];
    color = json['Color'];
    size = json['Size'];
    matrixID = json['MatrixID'];
    productID = json['ProductID'];
    dashBoardID = json['DashBoardID'];
    isSelected = false;
    isSelectedSize = json['IsSelectedSize'];
    setSelectedColorCss = json['SetSelectedColorCss'];
    colorImage = json['ColorImage'];
    if (json['ProductSizes'] != null) {
      productSizes = new List<ProductSizes>();
      json['ProductSizes'].forEach((v) {
        productSizes.add(new ProductSizes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ColorID'] = this.colorID;
    data['Color'] = this.color;
    data['Size'] = this.size;
    data['MatrixID'] = this.matrixID;
    data['ProductID'] = this.productID;
    data['DashBoardID'] = this.dashBoardID;
    data['IsSelected'] = this.isSelected;
    data['IsSelectedSize'] = this.isSelectedSize;
    data['SetSelectedColorCss'] = this.setSelectedColorCss;
    data['ColorImage'] = this.colorImage;
    if (this.productSizes != null) {
      data['ProductSizes'] = this.productSizes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProductSizes {
  String sizeID;
  String size;
  int matrixID;
  int productID;
  bool isSelected;
  String setSelectedSizeCss;
  int sizeStock;

  ProductSizes(
      {this.sizeID,
        this.size,
        this.matrixID,
        this.productID,
        this.isSelected,
        this.setSelectedSizeCss,
        this.sizeStock});

  ProductSizes.fromJson(Map<String, dynamic> json) {
    sizeID = json['SizeID'];
    size = json['Size'];
    matrixID = json['MatrixID'];
    productID = json['ProductID'];
    isSelected = false/*json['IsSelected']*/;
    setSelectedSizeCss = json['SetSelectedSizeCss'];
    sizeStock = json['SizeStock'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['SizeID'] = this.sizeID;
    data['Size'] = this.size;
    data['MatrixID'] = this.matrixID;
    data['ProductID'] = this.productID;
    data['IsSelected'] = this.isSelected;
    data['SetSelectedSizeCss'] = this.setSelectedSizeCss;
    data['SizeStock'] = this.sizeStock;
    return data;
  }
}