import 'dart:async' show Future;

import 'package:shared_preferences/shared_preferences.dart';


class PreferenceUtils {

  static Future<SharedPreferences> get _instance async =>
      _prefsInstance ??= await SharedPreferences.getInstance();
  static SharedPreferences _prefsInstance;

  static Future<SharedPreferences> init() async {
    _prefsInstance = await _instance;
    return _prefsInstance;
  }

  static String getString(String key, [String defValue]) {
    return _prefsInstance.getString(key) ?? defValue ?? null;

  }

    static Future saveSharedPref(String key, String value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(key, value);
    }

    static Future<String> getSharedPref(String key) async {
      final prefs = await SharedPreferences.getInstance();

      if(prefs.getString(key) != null) {
        return prefs.getString(key);
      } else {
        return 'Not found';
      }
    }

  static getAllKeys() {
    return _prefsInstance.getKeys();
  }

  static Future<bool> setString(String key, String value) async {
    var prefs = await _instance;
    return prefs?.setString(key, value) ?? Future.value(false);
  }

  static List<String> getStringList(String key, [String defValue]) {
    return _prefsInstance.getStringList(key) ?? defValue ?? [];
  }

  static double getDouble(String key, [double defValue]) {
    return _prefsInstance.getDouble(key) ?? defValue ?? null;
  }

  static Future<bool> setDouble(String key, double value) async {
    var prefs = await _instance;
    return prefs?.setDouble(key, value) ?? Future.value(false);
  }

  static Future<bool> setStringList(String key, List<String> value) async {
    var prefs = await _instance;
    return prefs?.setStringList(key, value) ?? Future.value(false);
  }

  static Future<bool> setBool(String key, bool value) async {
    var prefs = await _instance;
    return prefs?.setBool(key, value) ?? Future.value(false);
  }

  static bool getBool(String key, [bool defValue]) {
    return _prefsInstance.getBool(key) ?? defValue ?? false;
  }

  static Future<bool> removeKey(String key) async {
    bool result = await _prefsInstance.remove(key);
    return result;
  }

  static Future<bool> clearPreferences() async {
    bool result = await _prefsInstance.clear();
    return result;
  }
}
