
class APIVariable {


  Map<String, dynamic> getCategory(){
    var value = {
      "categoryId": 2
    };
    return value;
  }

  Map<String, dynamic> otpRequest(dynamic recipientMobileEmail ){
    var value = {
      "mobile" : recipientMobileEmail.toString().trim(),
    };
    return value;
  }


  Map<String, dynamic> userRegistration({dynamic name,dynamic email,
    dynamic mobile, dynamic password,
    dynamic referCode, dynamic latitude,dynamic longitude,dynamic address,dynamic pincode }){

    var value = {
      "email": email.toString().trim(),
      "typeReg": "email",
      "name": name.toString().trim(),
      "mobile": mobile,
      "password": password.toString().trim(),
      "refercode": referCode,
      "role": "4",
    "latitude" : latitude,
    "longitude" : longitude,
    "address" : address,
    "pincode" : pincode,
    };
    print(value.toString());
    return value;
  }


  Map<String, dynamic> forgotPassword({dynamic email}){
    var value = {
      "email": email.toString().trim(),
    };
    return value;
  }

  Map<String, dynamic> resetPassword({dynamic oldPassword,dynamic newPassword}){
    var value = {
      "oldPassword": oldPassword,
      "newPassword": newPassword
    };
    return value;
  }


  Map<String, dynamic> addressList({dynamic loginId}){
    var value = {
      "loginId": loginId,
    };
    return value;
  }

  Map<String, dynamic> pincodeAvailable({dynamic pincode}){
    var value = {
      "pincode": pincode,
    };
    return value;
  }

   Map<String, dynamic> marketHome({
     String latitude,
     String longitude,
     String categoryId,
     String subCategoryId,

     String search,
     String page,
     String count,

   }){
      var value = {
        "latitude":      0.000000000,
        "longitude":     0.000000000,
        "categoryId":    2,
        "subCategoryId": 0,
        "search":        "",
        "page":          1,
        "count":         20
      };
      return value;
  }

   Map<String, dynamic> ProductsGroupWise(String DashBoardId){
     var value = {
       "BannerType" : DashBoardId,
     };
     return value;
   }

   Map<String, dynamic> ProductDetail(dynamic ProductID){
     var value = {
       "productId": ProductID


     };
     return value;
   }


  Map<String, dynamic> RelatedProduct({dynamic categoryId,dynamic subCategoryId,dynamic productName}){
    var value = {

       /* "categoryId": 2,
        "subCategoryId": 0,
        "productName": "papaya"*/

    "categoryId": categoryId,
    "subCategoryId":subCategoryId,
    "productName": productName

    };
    return value;
  }


  Map<String, dynamic> ProductList({dynamic categoryId, dynamic subCategoryId, dynamic search, dynamic page, dynamic count}){
    var value = {

      "categoryId": categoryId,
      "subCategoryId": subCategoryId,
      "search": search,
      "page": page,
      "count": count

    };
    return value;
  }

  Map<String, dynamic> saveCart({dynamic productId, dynamic qty,dynamic price,dynamic unit,dynamic weight,dynamic variantId}){
    var value =  {
      "productId": productId,
      "qty": qty,
      "price": price,
      "unit": unit,
      "weight" :weight,
      "variantId" :variantId,

    };
    return value;
  }

  Map<String, dynamic> updateItemQtyCart({dynamic productId, dynamic qty,dynamic price,dynamic unit,dynamic weight,dynamic variantId}){
    var value =  {
      "productId": productId,
       "qty": qty,
       "weight" :weight,
      "price": price,
      "unit": unit,
      "variantId": variantId,
    };
    return value;
  }


  Map<String, dynamic> getCart({dynamic loginId, dynamic qty}){
    var value =  {
      "loginId": loginId,

    };
    return value;
  }

  Map<String, dynamic> getDeleteCartItem({dynamic productId,dynamic variantId}){
    var value =  {
      "productId": productId,
      "variantId": variantId,
    };
    return value;
  }


  Map<String, dynamic> deleteCart({dynamic loginId}){
    var value =  {
      "loginId": loginId,
    };
    return value;
  }




   Map<String, dynamic> GetProductGroupsList(dynamic DashBoardId, String type){
     var value = {
       "DashBoardId" : DashBoardId,
       "Type":type
     };
     return value; //
   }

   Map<String, dynamic> GetBrandList(dynamic topItemSelection){
     var value = {
       "topItemSelection" : topItemSelection,
     };
     return value;
   }

   Map<String, dynamic> GetMobileNo(String mobileNo){
     var value = {
       "MobileNo" : mobileNo,
     };
     return value;
   }

   Map<String, dynamic> GetLogin(dynamic userName, dynamic password,
       dynamic latitude,dynamic longitude,dynamic address,dynamic pincode
       ){
     var value = {
       "email" : userName,
       "phone" : userName,
       "password" : password,

       "latitude" : latitude,
       "longitude" : longitude,
       "address" : address,
       "pincode" : pincode,



   /*  dynamic getLong= await PreferenceUtils.getDouble(
     PreferencesKey.userLong);

     dynamic getLat= await PreferenceUtils.getDouble(
         PreferencesKey.userLat);

     dynamic getPin= await PreferenceUtils.getString(
         PreferencesKey.userLocationPincode);

     dynamic getAddress= await PreferenceUtils.getDouble(
         PreferencesKey.ADDFULL);

*/


   };
     return value;
   }

   Map<String, dynamic> GetSingup(dynamic firstName, dynamic mobileNo, dynamic password){
     var value = {
       "FirstName" : firstName,
       "MobileNo" : mobileNo,
       "Password" : password,
     };
     return value;
   }

   Map<String, dynamic> GetDBSessionId(String customerId){
     var value = {
       "CustomerID" : customerId,
     };
     return value;
   }

   Map<String, dynamic> GetCategory(dynamic categoryId){
     return {
       "categoryID" : categoryId
     };
   }

   Map<String, dynamic> GetSaveCart(
       dynamic DBCartSessionID, dynamic Quantity, dynamic CustomerID,
       dynamic ImageURL, dynamic ProductID, dynamic MatrixID, dynamic ItemDescription,dynamic colorId){
     var value = {
       "DBCartSessionID": DBCartSessionID,
       "Quantity": Quantity,
       "CustomerID": CustomerID,
       "ImageURL": ImageURL,
       "ProductID": ProductID,
       "MatrixID": MatrixID,
       "ItemDescription": ItemDescription,
       "DeviceType":"Mobile",
       "IsAddonProduct":"false",
       "IsFallPico":"false",
       "SystemIP":"",
       "UserTypeID":"",
       "IsFallPico":"false",
       "IsPolish":"false",
       "IsPetiCoat":"false",
       "Status":"0",
       "colorid":colorId,
      /* {

     }
*/
     };
     return value;
   }

   Map<String, dynamic> GetCartDetails(dynamic DBCartSessionID, dynamic customerId){
     var value = {
       "DBCartSessionID": DBCartSessionID,
       "CustomerID": customerId
     };
     return value;
   }

   Map<String, dynamic> GetUpdateQuntity(dynamic CartID,dynamic DBCartSessionID, dynamic customerId, dynamic Quantity){
     var value = {
       "CartID": CartID,
       "DBCartSessionID": DBCartSessionID,
       "CustomerID":  customerId,
       "Quantity":Quantity
     };
     return value;
   }

   Map<String, dynamic> GetDeleteCartItem(dynamic CartID,dynamic DBCartSessionID, dynamic customerId){
     var value = {
       "CartID": CartID,
       "CustomerID": customerId,
       "DBCartSessionID": DBCartSessionID
     };
     return value;
   }

   Map<String, dynamic> GetAddressList(dynamic addressID, dynamic GUID){
     var value = {
       "userID": addressID,
       "GUID": GUID,
     };
     return value;
   }
   Map<String, dynamic> GetCustomerInfo(dynamic CustomerID, dynamic GUID){
     var value = {
       "customerID": CustomerID,
       "guid": GUID,
     };
     return value;
   }
   Map<String, dynamic> GetAdreees(dynamic addressID, dynamic GUID){
     var value = {
       "addressID": addressID,
       "GUID": GUID,
     };
     return value;
   }

   Map<String, dynamic> GetForgetPassword(dynamic mobileNo, dynamic password,dynamic otp){
     var value = {
       "UserName": mobileNo,
       "Password": password,
       "OTP": otp
     };
     return value;
   }

   Map<String, dynamic> GetOrderList(dynamic customerid){
     var value = {
       "loginId": customerid,

     };
     return value;
   }

  Map<String, dynamic> OrderId(dynamic orderId){
    var value = {
      "orderId": orderId,

    };
    return value;
  }

   Map<String, dynamic> GetRemoveWishList(dynamic wishlistID, dynamic customerId){
     var value = {
       "WishlistID": wishlistID,
       "CustomerID": customerId
     };
     return value;
   }
  Map<String, dynamic> ratingvendorPara({dynamic Vendorid, dynamic ratE, dynamic desC, dynamic orderid}){
    var value = {
      "vendorId": Vendorid,
      "rate": ratE,
      "desc": desC,
      "orderId": orderid
    };
    return value;
  }

   Map<String, dynamic> getShippingAddress( {dynamic addressId, dynamic fName,
       dynamic lName, dynamic contactNo, dynamic email,
       dynamic address, dynamic addressLine2,
       dynamic pincode, dynamic isDefaultAddress,
       dynamic type,dynamic latitude,dynamic longitude}){

     var value = {
     "addressId":  addressId,
     "fName" : fName,
     "lName": lName,
     "contactNo": contactNo,
     "email":email,
     "address": address,
     "addressLine2":addressLine2,
     "pincode":pincode,
     "default":isDefaultAddress,
     "type":type,
       "lat":latitude,
       "lng":longitude,
     };
     return value;
   }


  Map<String, dynamic> saveTimeSlot( {dynamic address, dynamic phone,
    dynamic lat, dynamic lng, dynamic frSlotTm,dynamic toSlotTm,dynamic paymentMode,dynamic payableAmt,dynamic transactionId
    }){

    var value = {
      "addressId" :1,
      "address": address,
      "phone": phone,
      "lat" : lat,
      "lng" : lng,
      "frSlotTm" : frSlotTm,
      "toSlotTm" :toSlotTm,

      "paymentMode" : paymentMode,
      "payableAmt" :payableAmt,
      "transactionId" : transactionId,



    };
    return value;
  }

   Map<String, dynamic> GetUpdateInfo( dynamic customerId, dynamic addressId, dynamic mobileNo, dynamic firstName, dynamic lastName,
       dynamic emailId, dynamic address, dynamic address2, dynamic state, dynamic city, dynamic zipCode, dynamic country,
       dynamic countryCode, dynamic makeThisAddressDefault, dynamic isactive, dynamic addressType, dynamic guid, dynamic customerTypeId){
     var value = {
       "customerID":  customerId,
       // "AddressID" :  addressId,
       "MobileNo": mobileNo,
       "FirstName": firstName,
       "LastName":lastName,
       "EmailID": emailId,
       "Address":address,
       "Address2":address2,
       "State":state,
       "City":city,
       "ZipCode":zipCode,
       // "Country":country,
       // "CountryCode":countryCode,
       // "MakeThisAddressDefault":makeThisAddressDefault,
       "IsActive": true,
       // "AddressType":addressType,
       // "GUID":guid,
       "guid" : guid,
       "CustomerTypeID":customerTypeId
     };
     return value;
   }

   Map<String, dynamic> GetKeyWordSearch(dynamic keyword) {
     var value = {

         "latitude": 0.000000000,
         "longitude": 0.000000000,
         "search": keyword

     };
     return value;
   }

   Map<String, dynamic> checkAvailability(dynamic keyword) {
     var value = {
       "pinCode" : keyword
     };
     return value;
   }

   Map<String, dynamic> GetSearch(dynamic searchKeyWord, dynamic pageSize, dynamic pageIndex) {
     var value = {
       "CommandTypes": "",
       "SearchKeyword": "$searchKeyWord",
       "PageSize": pageSize,
       "PageIndex": pageIndex,
       "filterBrandIds": "-1",
       "filterSizeIds": "",
       "filterColorIds": "",
       "priceFrom": 0,
       "brandId": 0,
       "priceTo": 1000000,
       "Catids": "0",
       "sortBy": ""
     };
     return value;
   }

   Map<String ,dynamic> GetProductList(dynamic CommandTypes, String filterBrandIds, String filterSizeIds,
       String filterColorIds, dynamic categoryId, dynamic brandId, dynamic PageIndex, dynamic PageSize, dynamic priceFrom,
                    dynamic priceTo, dynamic sortBy, dynamic dashBoardId,dynamic catIds) {
     var value = {
       "DashBoardId" : dashBoardId,
       "CommandTypes":CommandTypes,
       "filterBrandIds":filterBrandIds,
       "filterSizeIds":filterSizeIds,
       "filterColorIds":filterColorIds,
       "categoryId":categoryId,
       "brandId":brandId,
       "PageIndex":PageIndex,
       //"PageSize":PageSize,
       "PageSize":50,
       "priceFrom": priceFrom,
       "priceTo":priceTo,
       "sortBy":sortBy,
        "Catids" :catIds

     };
     return value;
   }

   Map<String,dynamic> GetSaveOrder(
       String customerId, String dbSessionId, String addressId, String orderAmount, String orderQty,
       String paymentMode,  String paymentMethod, String quantity,String netamount, String customerName, String mobileNumber,
       dynamic guid) {
     var value = {
       "CustomerID": customerId,
       "DBCartSessionID": dbSessionId,
       "AddressID": addressId,
       "OrderAmount": orderAmount,
       "OrderedQty": orderQty,
       "PaymentMode": "cod",
       "BrowserIP": "192.168.0.201",
       "OrderType": "android",
       "Quantity": quantity,
       "PaymentMethod": "cod",
       "StoreAddress": "0",
       "CartCouponCode": "0",
       "BaseCurrency": "inr",
       "ShippingCharge": "0",
       "Discount": "0",
       "NetAmount": netamount,
       "CustomerTypeID": "0",
       "CustomerName": customerName,
       "MobileNo": mobileNumber,
       "CartAutoDiscountCode": "0",
       "AdditionalDiscount": "0",
       "UTMSource": "andorid app",
       "UTMMedium": "android app",
       "PGSignature": "0",
       "GUID":guid,
     };
     return value;
   }

   Map<String, dynamic> GetOrderCancel(dynamic orderId,dynamic status,dynamic productIds,dynamic remark){

      //6 whole order cancel no need ro product id
     // 7 partially cancelled with ids


     var value = {

         "orderId": orderId,
         "status": 6,
         "productIds":productIds,
       "remark":remark

     };
     return value;
   }

  Map<String, dynamic> reOrder(dynamic orderId){

    var value = {
      "orderId": orderId,
    };
    return value;
  }


  Map<String, dynamic> saveFcmToken({dynamic fcmToken,dynamic brand,dynamic model}){
    var value = {
      "fcbToken": fcmToken,
      "brand": brand,
      "model": model
    };
    return value;
  }
}




// {
// "CustomerID": "1",
// "DBCartSessionID": "30",
// "AddressID": "3",
// "OrderAmount": "760",
// "OrderedQty": "1",
// "PaymentMode": "cod",
// "BrowserIP": "192.168.0.201",
// "OrderType": "android",
// "Quantity": "1",
// "PaymentMethod": "cod",
// "StoreAddress": "",
// "CartCouponCode": "",
// "BaseCurrency": "inr",
// "ShippingCharge": "0",
// "Discount": "0",
// "NetAmount": "760",
// "CustomerTypeID": "0",
// "CustomerName": "surendra",
// "MobileNo": "8750360927",
// "CartAutoDiscountCode": "",
// "AdditionalDiscount": "0",
// "UTMSource": "andorid app",
// "UTMMedium": "android app",
// "PGSignature": ""
// }