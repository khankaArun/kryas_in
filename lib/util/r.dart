

import 'package:flutter/material.dart';
import 'package:krays_in/util/color_code.dart';
import 'package:krays_in/util/images.dart';
import 'package:krays_in/util/string_confi.dart';

class R {
  static final string =  new StringName();
  static final color =  new ColorCodes();
  static final images =  new Images();
  static final priceSymbol =  new PriceSymbol();
  // static final fonts = Fonts();
}

