import 'package:flutter/material.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double horizontalBloc;
  static double verticalBloc;
  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;
  static double headerFontSize;
  static double normalFontSize;
  static double medimFontSize;
  static double smallFontSize;
  static double verySmallFontSize;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    horizontalBloc = screenWidth / 100;
    verticalBloc = screenHeight / 100;

    _safeAreaHorizontal  = _mediaQueryData.padding.left + _mediaQueryData.padding.right;

    _safeAreaVertical    = _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    safeBlockHorizontal  = (screenWidth - _safeAreaHorizontal) / 100;

    safeBlockVertical    = (screenHeight - _safeAreaVertical) / 100;

    normalFontSize = 13;
    medimFontSize = 14.5;
    headerFontSize = 16.0;
    smallFontSize =  12.0;
    verySmallFontSize =  11.0;


  }
}
