import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'api.dart';

class   Images {


  final logo = 'assets/images/ic_logo.png';
  final cart_icon = 'assets/images/ic_cart.png';
  final notification_icon = 'assets/images/ic_notification.png';
  final whislist_icon = 'assets/images/ic_wishlist.png';
  final search_icon = 'assets/images/ic_search.png';
  final profile_icon = 'assets/images/ic_profile.png';
  final category_icon = 'assets/images/ic_category.png';
  final shop_icon =  'assets/images/ic_shop.png';
  final explore_icon =  'assets/images/ic_explore.png';
  final free_icon = 'assets/images/ic_free.png';
  final empty_cart = 'assets/images/ic_empty_cart.png';
  final order_icon = 'assets/images/ic_order.png';
  final sort_filer_icon = 'assets/images/ic_sort_filer.png';
  final journey_image1 = 'assets/images/1_bag.png';
  final journey_image2 = 'assets/images/2_address.png';
  final journey_image3 = 'assets/images/3_payment.png';
  final original_icon = 'assets/images/original_icon.png';
  final quality_icon = 'assets/images/7_steps.png';
  final fresh_fashion_icon = 'assets/images/fresh_fassion.png';
  final final_page_icon = 'assets/images/order-confirm.jpg';
  final ic_no_brand = 'assets/images/ic_no_brand.png';

  final ic_onboard1 = 'assets/images/onboarding_1.png';
  final ic_onboard2 = 'assets/images/onboarding_2.png';
  final ic_onboard3 = 'assets/images/onboarding_3.png';
  final ic_vertical_logo='assets/images/KraysLogo-vertical.png';

  final re_blue_bg ='assets/images/bd_deign.png';
  final animate_logo  = 'assets/images/anima_logo.png';
  final logo_kryas = 'assets/images/logo.png';
  final facebook = 'assets/images/facebook.png';
  final google = 'assets/images/Fixed.png';
  final forgetFig = 'assets/images/fogot_pass.gif';
  final verify_otp = 'assets/images/verify_otp.gif';
  final gif_two = 'assets/images/gif_two.gif';
  final redbg = 'assets/images/bg.png';

  final ic_filter='assets/images/filter_icon.png';

  final ic_checked_out='assets/images/radio-checked.png';
  final ic_payment='assets/images/payment-steps.png';
  final ic_razor_pay='assets/images/razeray.png';
  final ic_confirmation_page='assets/images/freepik.png';
  final ic_status_final_stpes='assets/images/payment-options.png';
  final ic_order_confirmation='assets/images/Refund-pana.png';
  final ic_veg_bucket='assets/images/veg_bucket.png';
  final ic_order_step='assets/images/order_steps.png';

  final krays_logo_new_lat='assets/images/krays_logo.png';
  final location_arrow='assets/images/locationarrow.png';

  final cancelled='assets/images/cancelled.png';
  final completed='assets/images/completed.png';
  final confirm='assets/images/confirm.png';
  final placed='assets/images/placed.png';
  final ready='assets/images/ready.png';

  final curvefirst='assets/images/curvefirst.png';
  final curvesecond='assets/images/curvesecond.png';

  final chat_icon='assets/images/chat_icon.png';
  final home_icon='assets/images/home_icon.png';
  final google_icon='assets/images/google.png';










 // final ic_filter='assets/images/radio-checked.png';



  static Widget buildImage(String imagePath) {
    return imagePath==null||imagePath.isEmpty?Image.network(imagePath,fit: BoxFit.fitHeight) :
    Image.network(imagePath,fit: BoxFit.fitHeight,
        errorBuilder: (context, error, stackTrace){
          return Image.network(imagePath,fit: BoxFit.fitHeight);
        },

        loadingBuilder:(BuildContext context, Widget child, ImageChunkEvent loadingProgress){
          if (loadingProgress == null)
            return child;
          return Center(
            child: SizedBox(
                height: 15.0,
                width: 15.0,
                child:
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.pink),
                  strokeWidth: 3.0,
                  value: loadingProgress.expectedTotalBytes != null
                      ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                      : null,
                )
            ),

          );
        }

    );


  }

  static Widget buildImageListingPage(String imagePath) {
    return imagePath==null||imagePath.isEmpty?Image.network(imagePath,fit: BoxFit.fitHeight) :
    Image.network(imagePath,fit: BoxFit.fitWidth,
        errorBuilder: (context, error, stackTrace){
          return Image.network(imagePath,fit: BoxFit.fitHeight);
        },

        loadingBuilder:(BuildContext context, Widget child, ImageChunkEvent loadingProgress){
          if (loadingProgress == null)
            return child;
          return Center(
            child: SizedBox(
                height: 15.0,
                width: 15.0,
                child:
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.pink),
                  strokeWidth: 3.0,
                  value: loadingProgress.expectedTotalBytes != null
                      ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                      : null,
                )
            ),

          );
        }

    );


  }
}