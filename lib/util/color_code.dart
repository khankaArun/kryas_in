import 'package:flutter/material.dart';

class ColorCodes {

   get app_color => Colors.lightBlueAccent.shade700;
   get errorColor => Colors.red;
   get white_color => Colors.white;
   get black_color => Colors.black;
   get grey_color => Colors.grey;
   get green_color => Colors.green;
   get grey_background =>  HexColor.fromHex('#f5f5f5');
   get themeColor => Color(0xFF0062A8);
   get grey =>  Color(0xFFD6D6D6);
   get bluecolor => Color(0xFF00007f);
   get hint_grey_color => Color(0xFF606060);
   get seletes_color_type => Color(0xff1e2d40);
   get buttonColor =>  Color(0xFF0062A8);
   get redColor =>  Color(0xFFB71C1C);
   get light_bc => Color(0xFF3a3b3c);
   get light_grey => Color(0xFFF0F0F0);
   get greenColor => Color(0xFF4dbd97);
   get blue_theme => Color(0xFF115093);
   get button => Color(0xFF2d2669);
   get blue_color => Color(0xFF8aacd3);

}

extension HexColor on Color {
   /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
   static Color fromHex(String hexString) {
      final buffer = StringBuffer();
      if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
      buffer.write(hexString.replaceFirst('#', ''));
      return Color(int.parse(buffer.toString(), radix: 16));
   }

   /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
   String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
       '${alpha.toRadixString(16).padLeft(2, '0')}'
       '${red.toRadixString(16).padLeft(2, '0')}'
       '${green.toRadixString(16).padLeft(2, '0')}'
       '${blue.toRadixString(16).padLeft(2, '0')}';
}