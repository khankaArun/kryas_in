
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../r.dart';

class WidgetController {

  static Widget text_black (String msg){
    return Text(msg, style: TextStyle(fontSize: 23,  color: R.color.black_color,fontWeight: FontWeight.bold),
    );
  }

  static Widget button_theme(BuildContext context,String message){
    return Container(
      // margin: EdgeInsets.fromLTRB(35, 0, 35, 0),
      alignment: Alignment.center,
      padding: const EdgeInsets.only(left: 0, right: 0, top: 15, bottom: 15),
      height: 80,
      child:  Container(
        alignment: Alignment.center,
        height: 40,
        width: MediaQuery.of(context).size.width,
        decoration:  BoxDecoration(
            borderRadius:  BorderRadius.all(const Radius.circular(8)),
            color: R.color.app_color),
        child:  Text(message,
          style:  TextStyle(
            fontFamily: "bold",
            fontSize: 18,
            color: R.color.white_color,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  static Widget textfiled(TextEditingController controller,hint){
    return  TextField(

      autocorrect: false,
      decoration: InputDecoration(
          fillColor: R.color.themeColor,
          label: Text(hint,style: TextStyle(color: R.color.hint_grey_color,fontSize: 15,fontFamily: "ralewayMedium"),)
      ),
      keyboardType: TextInputType.name,
      textInputAction: TextInputAction.send,
      controller: controller,
      autofocus: false,
      cursorColor: R.color.themeColor,
    );
  }

  static Widget textpassord(TextEditingController controller,hint){
    return  TextField(
      autocorrect: false,
      decoration: InputDecoration(
          fillColor: R.color.themeColor,
          label: Text(hint,style: TextStyle(color: R.color.hint_grey_color,fontSize: 15),),

      ),
      keyboardType: TextInputType.visiblePassword,
      textInputAction: TextInputAction.send,
      controller: controller,
      autofocus: false,
      cursorColor: R.color.themeColor,

    );
  }

}