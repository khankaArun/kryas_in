class StringName{

  get  routeMediaMain => "/MediaMain";
  get routeShopStreetMain => "/ShopStreetMain";
  get routeBusinessCartMain => "/BusinessCartMain";
  get routeDmMain => "/DmMain";
  get routeProfileMain => "/ProfileMain";


  get app_name => "E-Com APP";
  get exit_msg =>  "Press once more to exit";
  get top_category => "TOP CATEGORIES";
  get shop_by_category => "SHOP BY CATEGORIES";
  get top_picks => "TOP PICKS";
  get top_brand => "TOP BRAND";
  get category_to_bag => "CATEGORY TO BAG";
  get product_detail => "Product Detail";
  get cart_detail => "Cart Detail";
  get payment_info => "Payment Info";
  get day_return_policy => "Day Return Policy";
  get cash_on_delivery_available => "Cash on Delivery available";
  get related_product => "View Similar";
  get buy_now =>  "BUY NOW";
  get add_cart => "ADD TO BAG";
  get dont_account => "Don\'t have an account? ";
  get login => "Login";
  get singup => "Singup";
  get forget_password => "Forgot Password?";
  get successfully_login => "Successfully Login";
  get this_mobile_no_alreadt_register => "This Mobile No Already Register";
  get internal_server_error => "Internal Server Error";
  get save  => "Save";
  get shipping_address => "Shipping Address";
  get wishlist_page => "WishList";
  get order_page => "My Order";
  get order_detail => "Order Detail";
  get pincode_error =>  "Enter Pincode Not Valid.";
  get update_customer_info => "Update Customer Info";
  get clear_filter => "Clear";
  get apply_filter => "Apply";
  get notification => "Notification";
  get inclusive_all_teaxes =>  "inclusive of all teaxes";
  get easy_return_title => "Easy 30 days returns and exchanges";
  get easy_return_description => "Choose to return or exchange for a different size\n(if available) withing 30 days";
  get please_select_size_color => "Please Select Size and Color";
  get review_rating => "Rating & Reviews";
  get genuine_products => "Genuine Products";
  get step_quality_check => "7 Step Quality Check";
  get fresh_fashion => "Fresh Fashion";
  get not_available => "Service not available with this Pincode";
  get service_available => "Service available with this ";
  get from__change_password => "From Change Password";



}

class PriceSymbol {
  get rupee => "₹";
  get off => "% OFF";
}

class PreferencesKey {

  static get USER_LOGIN_NAME => "userLoginName";
  static get PASSWORD => "password";
  static get ACESS_TOKEN => "accessToken";
  static get APP_TYPE => "appType";
  static get IS_USER_LOGIN => "isuserlogin";
  static get FCM_TOKEN => "fcmToken";
  static get TITLE_NOTIFICATION=> "title";
  static get BODY_NOTIFICATION=> "body";
  static get ADDTITLE=> "title";
  static get ADDFULL=> "address";

  static get REFFER_CODE=> "userRefferrCode";

  static get userLat=> "userLat";
  static get userLong=> "userLong";
  static get userLocationPincode=> "userLocationPincode";
  static get userLocationAddress=> "userLocationAddress";

}

enum WhereItComeFrom {
  SIGNUP,
  LOGIN,
  EMAIL,
  FORGOT_PASSWORD
}


class StringConfig{
  static String SPLASH_SCREEN           = '/splash';
  static String LOGIN_PAGE           = '/login';
  static String ON_BORAD_SLIDER      = '/on_borad_slider';
  static String SIGN_UP              = '/sig_up';
  static String HOME_PAGE            = '/home_page';
  static String OTP_VERIFY            = '/otp_verify';
  static String FORGET_PASS            = '/forget_pass';
  static String RESET_PASS            = '/reset_pass';




}
