

import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:krays_in/util/r.dart';
import 'package:krays_in/util/sizeconfig.dart';
import 'package:krays_in/util/string_confi.dart';
import 'package:url_launcher/url_launcher.dart';


import 'ResponsiveFlutter.dart';
import 'api.dart';

class AppUtil {

 // static bool isEmail(String input) => EmailValidator.validate(input);
  static FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  static bool isEmail(String input){

    return true;
  }

  static bool isPhone(String input) => RegExp(
      r'^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$'
  ).hasMatch(input);

  static showToast(dynamic msg){
    Fluttertoast.showToast(
      msg: "$msg",
      backgroundColor: Colors.black,
      textColor: Colors.white,
    );
  }

  static showLoadingProgress(BuildContext context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => SpinKitFoldingCube(
          color: R.color.app_color,
          // size: SizeConfig.horizontalBloc * 17

      ),
    );
  }

  static void showAlertDialog(BuildContext ctx ,message,title){
    showCupertinoDialog(
        context: ctx,
        builder: (BuildContext ctx) {
          return CupertinoAlertDialog(
            title: Text(title,style: new TextStyle(fontFamily: "medium",fontSize: 15),),
            content: Text(message,style: new TextStyle(fontFamily: "regular",fontSize: 15),),
            actions: [
              // The "Yes" button
              // The "No" button
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
                child: Text('Cancel',style: new TextStyle(fontFamily: "medium",fontSize: 15),),
                isDefaultAction: false,
                isDestructiveAction: false,
              ),
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
                child: Text('Ok',style: new TextStyle(fontFamily: "medium",fontSize: 15),),
                isDefaultAction: false,
                isDestructiveAction: false,
              )
            ],
          );
        });
  }


  static showUploadImageFeatures(BuildContext context){

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) => Container(
        height: ResponsiveFlutter.of(context).hp(30),
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(0.0),
            topRight: const Radius.circular(0.0),
          ),
        ),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [


              Container(
                margin: new EdgeInsets.only(left: 25.0, top: 17.0, bottom: 13),
                child: Row(
                  children: [
                    SizedBox(
                      height: SizeConfig.safeBlockHorizontal * 6,
                      width: SizeConfig.safeBlockHorizontal * 6,
                      child: Image.asset(R.images.home_icon, color: R.color.app_color,),
                    ),

                    InkWell(
                      onTap: () async {
                       // final albums = await PhotoManager.getAssetPathList();
                       // Common.launchInBrowser(WebUrl.FAQ);
                      },
                      child: Text(" Post ",
                        style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                            fontWeight: FontWeight.w600,color: R.color.app_color,fontFamily: "openSans"),),
                    ),


                  ],
                ),
              ),

              Divider(),

              Container(
                margin: new EdgeInsets.only(left: 25.0, top: 13.0, bottom: 13),
                child: Row(
                  children: [
                    SizedBox(
                      height: SizeConfig.safeBlockHorizontal * 6,
                      width: SizeConfig.safeBlockHorizontal * 6,
                      child: Image.asset(R.images.home_icon, color: R.color.app_color,),
                    ),

                    InkWell(
                      onTap: (){
                        // Common.launchInBrowser(WebUrl.FAQ);
                      },
                      child: Text(" Clip ",
                        style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                            fontWeight: FontWeight.w600,color: R.color.app_color,fontFamily: "openSans"),),
                    ),


                  ],
                ),
              ),
              Divider(),
              Container(
                margin: new EdgeInsets.only(left: 25.0, top: 13.0, bottom: 13),
                child: Row(
                  children: [
                    SizedBox(
                      height: SizeConfig.safeBlockHorizontal * 6,
                      width: SizeConfig.safeBlockHorizontal * 6,
                      child: Image.asset(R.images.home_icon, color: R.color.app_color,),
                    ),

                    InkWell(
                      onTap: (){
                        // Common.launchInBrowser(WebUrl.FAQ);
                      },
                      child: Text(" Channel Video ",
                        style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                            fontWeight: FontWeight.w600,color: R.color.app_color,fontFamily: "openSans"),),
                    ),


                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );

  }

  static isEmailInvalid(var email){
    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);

    if(emailValid){
      return false;
    }else{
      return true;
    }


  }

  static String getDateTime(String _selectedDate){

    try{
      DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(_selectedDate);
      final df = new DateFormat('dd-MM-yyyy');
      df.format(tempDate);

      return df.format(tempDate).toString() +" "+DateFormat('hh:mm a').format(tempDate).toString();
    }catch(error ){
      return "Invalid date format";
    }

  }

  static Future<void> launchInBrowser(String url) async {

    try{

      await launch(
        url,
        forceSafariVC: true,
        forceWebView: false,

        headers: <String, String>{'my_header_key': 'my_header_value'},
      );

    }catch(error ){
      print(error.toString());
    }
   /* if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: true,
        forceWebView: false,

        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }*/
  }

  static Future<void> sendEventToGoogleAnalytics(String screenName,String className)  async {


    analytics.setCurrentScreen(
      screenName: screenName,
      screenClassOverride: className,
    );
    print(screenName +" : "+className);
  }


  static Map<String, dynamic> readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  Map<String, dynamic> _readLinuxDeviceInfo(LinuxDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'version': data.version,
      'id': data.id,
      'idLike': data.idLike,
      'versionCodename': data.versionCodename,
      'versionId': data.versionId,
      'prettyName': data.prettyName,
      'buildId': data.buildId,
      'variant': data.variant,
      'variantId': data.variantId,
      'machineId': data.machineId,
    };
  }

  Map<String, dynamic> _readWebBrowserInfo(WebBrowserInfo data) {
    return <String, dynamic>{
      'browserName': describeEnum(data.browserName),
      'appCodeName': data.appCodeName,
      'appName': data.appName,
      'appVersion': data.appVersion,
      'deviceMemory': data.deviceMemory,
      'language': data.language,
      'languages': data.languages,
      'platform': data.platform,
      'product': data.product,
      'productSub': data.productSub,
      'userAgent': data.userAgent,
      'vendor': data.vendor,
      'vendorSub': data.vendorSub,
      'hardwareConcurrency': data.hardwareConcurrency,
      'maxTouchPoints': data.maxTouchPoints,
    };
  }

  Map<String, dynamic> _readMacOsDeviceInfo(MacOsDeviceInfo data) {
    return <String, dynamic>{
      'computerName': data.computerName,
      'hostName': data.hostName,
      'arch': data.arch,
      'model': data.model,
      'kernelVersion': data.kernelVersion,
      'osRelease': data.osRelease,
      'activeCPUs': data.activeCPUs,
      'memorySize': data.memorySize,
      'cpuFrequency': data.cpuFrequency,
      'systemGUID': data.systemGUID,
    };
  }

  Map<String, dynamic> _readWindowsDeviceInfo(WindowsDeviceInfo data) {
    return <String, dynamic>{
      'numberOfCores': data.numberOfCores,
      'computerName': data.computerName,
      'systemMemoryInMegabytes': data.systemMemoryInMegabytes,
    };
  }

}

class AssetPathEntity {


  // The albums' ID
  String id;
  // The albums name
  String name;
  // How many assets the album holds
  int assetCount;
  // The album's type
  int albumType;

}

enum WHERE_IT_COME_FROM {
  search,
  userUpdateAddress,
  placeOrder,
}