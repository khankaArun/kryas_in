class API {

/*

  static String BASE_URL = "http://ecomapi.gadgetwood.in/";
  static String IMAGE_BASE_URL= "http://swadmin.gadgetwood.in/";


*/



  static String BASE_URL = "https://admin.krays.in/public/api/";
  static String IMAGE_BASE_URL= "https://admin.krays.in/public/images/";






  static String Login                   = "https://admin.krays.in/public/api/login";
  static String OTP                     = "https://admin.krays.in/public/api/otpSend";
  static String USER_REGISTRATION       = "https://admin.krays.in/public/api/regUser";
  static String FORGOT_PASSWORD         = "https://admin.krays.in/public/api/forgot";
  static String MARKET_HOME             = "https://admin.krays.in/public/api/productsBuyAtHome";
  static String MARKET_CATEGORY         = "https://admin.krays.in/public/api/subCategories";
  static String PRODUCT_DETAILS         = "https://admin.krays.in/public/api/productDetails";
  static String PRODUCT_LISTING         = "https://admin.krays.in/public/api/productList";
  static String SAVE_CART               = "https://admin.krays.in/public/api/saveCart";
  static String GET_USER_CART           = "https://admin.krays.in/public/api/cartList";
  static String GET_RELATED_PRODUCT        = "https://admin.krays.in/public/api/similarProductList";
  static String DELETE_CART_ITEM           = "https://admin.krays.in/public/api/deleteCart";
  static String UPDATE_CART_ITEM           = "https://admin.krays.in/public/api/updateCart";
  static String GET_USER_ADDRESS           = "https://admin.krays.in/public/api/getAddress";
  static String SAVE_USER_ADDRESS           = "https://admin.krays.in/public/api/saveAddress";
  static String SAVE_ORDER_SLOT           = "https://admin.krays.in/public/api/orderPlace";
  static String DELETE_CART               = "https://admin.krays.in/public/api/deleteAllCart";
  static String GetOrderList =               "https://admin.krays.in/public/api/orders";
  static String GetDetails =               "https://admin.krays.in/public/api/orderDetails";
  static String OrderCancel               =  "https://admin.krays.in/public/api/orderUpdate";
  static String changePassword               =  "https://admin.krays.in/public/api/changePassword";
  static String pincode               =  "https://admin.krays.in/public/api/pincodeAvailable";
  static String KEYWORD_Search    =  "https://admin.krays.in/public/api/productSuggestions";
  static String SAVE_FCM_TOKEN    =  "https://admin.krays.in/public/api/fcbToken";

  static String VENDOR_RATING          = BASE_URL+"addVendorReview";

  static String RE_ORDER    =  "https://admin.krays.in/public/api/reOrder";

  static String PRIVACY_POLICY_URL="https://krays.in/privacy.html";
  static String PRIVACY_TnC_URL="https://krays.in/tc.html";
  static String ABOUT_US_URL="https://krays.in/about.html";



  static String HomeBanner =  BASE_URL+"api/Dashboard/GetHomeSliderBanner";
  static String ThumbBanner = BASE_URL+"api/Dashboard/GetThumbBanner";
  static String ProductGroups = BASE_URL+"api/Dashboard/GetProductGroups";
  static String ProductGroupWise = BASE_URL+"api/Dashboard/GetProductGroupsList";
  static String ProductDetail = BASE_URL+"api/Products/GetProductDetails";
  static String MENU          = BASE_URL+"api/Products/GetCategoryList";
  static String BrandList     = BASE_URL+"api/DashBoard/GetBrandList";
  static String CheckMobileNo  = BASE_URL+"api/User/CheckMobileNo";
  static String Singup        =  BASE_URL+"api/User/UserSignUp";

  static String SaveCart      =  BASE_URL +"api/Cart/SaveCartDetails";
  static String DBSessionID  =  BASE_URL+"api/Cart/GetDBCartSessionID";
  static String CartDetail   =  BASE_URL+"api/Cart/GetCartDetails";
  static String UpdateQunityt =  BASE_URL+"api/Cart/AddCartQuantity";
  static String DeleteCartItem  =  BASE_URL+"api/Cart/DeleteCartItems";
  static String GetAddress        =  BASE_URL+"api/Cart/GetBillingAddresses";
  static String ForgetPassword = BASE_URL+"api/User/ForgetPassword";
  static String ShippingAdress = BASE_URL+"api/Cart/SaveCustomerShippingAddress";
  static String GetShippingAddress  =  BASE_URL+"api/User/GetShippingAddress";
  static String CustomerInfomation =  BASE_URL+"api/User/GetCustomerInformation";
  static String SaveWishList  =  BASE_URL+"api/Cart/SaveWishList";
  static String GetWishList  =  BASE_URL+"api/Cart/GetWishListItems";

  static String GetStateCity =  BASE_URL+"api/Cart/GetStateCity";
  static String GetProductList = BASE_URL+"api/Products/GetProductList";
  static String UpdateCustomerAddress =  BASE_URL+"api/User/UpdateCutomerInformation";
  static String DeleteWishListItem  =  BASE_URL+"api/User/DeleteCustomerWishList";
  static String SaveOrder           =  BASE_URL+"api/Cart/SaveOrder";

  static String Category_Bag       =  BASE_URL+"api/Products/GetParentCategory";
  static String Category_List       =  BASE_URL+"api/Products/GetStockCatgoryies";

  static String SearchKeyWord     =   BASE_URL +"api/Products/ProductFullSearch";
  static String checkAvailability     =   BASE_URL +"api/Cart/GetStateCity";
  static String GET_OTP               =   BASE_URL +"api/User/CheckUserID";


}


class BannerBaseUrl {


  static String SIGN_UP_BANNER   ="https://www.qub.ac.uk/home/media/Media,827379,en.jpg";


  static String HomeBanner          = API.IMAGE_BASE_URL+"ItemImages/Banner/Large/Mobile/";
  static String ThumBanner          = API.IMAGE_BASE_URL+"ItemImages/Banner/Thumb/";
  static String CatgegoryBanner     = API.IMAGE_BASE_URL+ "ItemImages/Catalog/Products/Normal/";
  static String DeatilPageImage     = API.IMAGE_BASE_URL+ "ItemImages/Catalog/Products/mobilelarge/";
  static String CatgegoryThumbImage =  API.IMAGE_BASE_URL+"/ItemImages/Catalog/Products/mobileThumb/";
  static String CartThumbImage      =  API.IMAGE_BASE_URL+"ItemImages/Catalog/Products/ThumbNail/";
  static String CategoryIcon        =  API.IMAGE_BASE_URL+"ItemImages/Category/Mobile/Icon/";
  static String BrandIcon           =  API.IMAGE_BASE_URL;
  static String ExclusiveProduct    = API.IMAGE_BASE_URL+"ItemImages/HomeProductGroup/Mobile/thumb/";
  static String DefaultImage        =  API.IMAGE_BASE_URL+'ItemImages/Catalog/no-image-available-detail-page.jpg';

}